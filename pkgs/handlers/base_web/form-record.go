package base_web

import (
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/cesmanager/base-server/core/base_domain"
	"gitlab.com/cesmanager/base-server/pkgs/base_blocs"
	"gitlab.com/cesmanager/base-server/pkgs/base_repository"
)

type formRecord struct {
	c base_domain.AppConfig
}

func NewFormRecord(c base_domain.AppConfig) *formRecord {
	return &formRecord{c: c}
}

func (h *formRecord) List(w http.ResponseWriter, r *http.Request) {
	resp := NewResponse(w, r)
	//response callback
	defer func() {
		resp.Execute(nil)
	}()
	db, close := base_repository.MustMySQL(h.c.DataBase[h.c.ActiveDB], r.Header.Get("coy-db"))
	defer close()

	req := ParseRequest(h.c, r)
	params := req.PayLoad()
	delete(params, "count")
	delete(params, "pages")
	page := "1"
	if _, ok := params["page"]; ok {
		page = fmt.Sprint(params["page"])
		delete(params, "page")
	}
	pageSize := "1"
	if _, ok := params["page_size"]; ok {
		pageSize = fmt.Sprint(params["page_size"])
		delete(params, "page_size")
	}

	serv := base_blocs.NewFormRecord(base_repository.NewFormRecord(db))
	data, err := serv.List(params, req.OrderBy, page, pageSize)
	if err != nil {
		resp.SetError("error", err.Error())
		return
	}
	resp.SetBody(data)
}

func (h *formRecord) Create(w http.ResponseWriter, r *http.Request) {
	resp := NewResponse(w, r)
	defer func() {
		resp.Execute(nil)
	}()
	db, close := base_repository.MustMySQL(h.c.DataBase[h.c.ActiveDB], r.Header.Get("coy-db"))
	defer close()

	params := ParseRequest(h.c, r).PayLoad()

	//begin transaction
	tx := db.Begin()
	user, ok := UserContext(r.Context())
	if ok {
		//set the profile
		params["profile_id"] = user.ProfileID
	}
	formRecordBloc := base_blocs.NewFormRecord(base_repository.NewFormRecord(tx))
	formRecord, err := formRecordBloc.Create(params, base_blocs.NewFormRecordEntry(base_repository.NewFormRecordEntry(tx)))
	if err != nil {
		resp.SetError("error", err.Error())
		tx.Rollback()
		return
	}
	formRecord, err = formRecordBloc.Get(fmt.Sprint(formRecord.ID))
	if err != nil {
		resp.SetError("error", err.Error())
		tx.Rollback()
		return
	}
	tx.Commit()
	//get system setting
	settingsbloc := base_blocs.NewSettings(base_repository.NewSettings(db))
	settings, err := settingsbloc.Get()
	if err != nil {
		resp.SetError("error", err.Error())
		return
	}
	//send the mail in a non blocking way
	go func() {
		if formRecord.Profile == nil {
			return
		}
		utils := Utilities(h.c, r)
		subject := fmt.Sprint(formRecord.Form.Title, " ", "submitted")
		msg := fmt.Sprint("Hello ", formRecord.Profile.Fname, ",\n",
			`This is an acknowledgment that we have recieved your `,
			formRecord.Form.Title, " ",
			"application.")
		smtp := base_blocs.NewSMTP(&settings)
		err = smtp.Reciepents([]string{*formRecord.Profile.Email, *formRecord.Profile.AltEmail}).SetMsg(subject, msg).Send()
		if err != nil {
			utils.Log(err.Error(), base_blocs.SMTPError)
		}
	}()
	//sent an email to the user
	resp.SetBody(formRecord)
}

func (h *formRecord) Update(w http.ResponseWriter, r *http.Request) {
	resp := NewResponse(w, r)
	defer func() {
		resp.Execute(nil)
	}()
	db, close := base_repository.MustMySQL(h.c.DataBase[h.c.ActiveDB], r.Header.Get("coy-db"))
	defer close()

	params := ParseRequest(h.c, r).PayLoad()
	if _, ok := params["id"]; !ok {
		resp.SetError("error", "id is required")
		return
	}
	//begin transaction
	tx := db.Begin()
	user, ok := UserContext(r.Context())
	if !ok {
		resp.SetError("error", "unable to identify you")
		return
	}
	formRecordBloc := base_blocs.NewFormRecord(base_repository.NewFormRecord(tx))
	formRecord, err := formRecordBloc.Update(fmt.Sprint(params["id"]), params, user.ProfileID, base_blocs.NewFormRecordEntry(base_repository.NewFormRecordEntry(tx)))
	if err != nil {
		resp.SetError("error", err.Error())
		return
	}
	tx.Commit()
	//get system setting
	settingsbloc := base_blocs.NewSettings(base_repository.NewSettings(db))
	settings, err := settingsbloc.Get()
	if err != nil {
		resp.SetError("error", err.Error())
		return
	}
	//send the mail in a non blocking way
	go func() {
		utils := Utilities(h.c, r)
		subject := fmt.Sprint(formRecord.Form.Title, " application update")
		msg := fmt.Sprint("Hello ", user.Profile.Fname, ",\n",
			`Your `, formRecord.Form.Title, " submited by ", formRecord.CreatedAt, " has been updated. \n",
			"Status: ", formRecord.Status, "\n",
			"Comment: ", formRecord.Comment,
		)
		smtp := base_blocs.NewSMTP(&settings)
		err = smtp.Reciepents([]string{*formRecord.Profile.Email, *formRecord.Profile.AltEmail}).SetMsg(subject, msg).Send()
		if err != nil {
			utils.Log(err.Error(), base_blocs.SMTPError)
		}
	}()
	resp.SetBody(formRecord)
}

func (h *formRecord) Get(w http.ResponseWriter, r *http.Request) {
	resp := NewResponse(w, r)
	defer func() {
		resp.Execute(nil)
	}()
	db, close := base_repository.MustMySQL(h.c.DataBase[h.c.ActiveDB], r.Header.Get("coy-db"))
	defer close()

	param := mux.Vars(r)
	if _, ok := param["id"]; !ok {
		resp.SetError("error", "id is required")
		return
	}
	formRecordBloc := base_blocs.NewFormRecord(base_repository.NewFormRecord(db))
	formRecord, err := formRecordBloc.Get(fmt.Sprint(param["id"]))
	if err != nil {
		resp.SetError("error", err.Error())
		return
	}
	resp.SetBody(formRecord)
}

func (h *formRecord) Find(w http.ResponseWriter, r *http.Request) {
	resp := NewResponse(w, r)
	defer func() {
		resp.Execute(nil)
	}()
	db, close := base_repository.MustMySQL(h.c.DataBase[h.c.ActiveDB], r.Header.Get("coy-db"))
	defer close()

	params := ParseRequest(h.c, r).PayLoad()
	//if the request was open then we restrict it to the currect user
	if _, ok := params["open"]; ok {
		delete(params, "open")
		user_id := r.Header.Get("user_id")
		if user_id == "" {
			resp.SetError("error", "unable to identify you")
		}
		userServ := base_blocs.NewUser(base_repository.NewUser(db))
		user, err := userServ.Get(user_id)
		if err != nil {
			resp.SetError("error", err.Error())
			return
		}
		params["profile_id"] = user.ProfileID
	}
	formRecordBloc := base_blocs.NewFormRecord(base_repository.NewFormRecord(db))
	formRecords, err := formRecordBloc.Find(params)
	if err != nil {
		resp.SetError("error", err.Error())
		return
	}
	resp.SetBody(formRecords)
}

func (h *formRecord) Status(w http.ResponseWriter, r *http.Request) {
	resp := NewResponse(w, r)
	resp.SetBody([]string{"Pending Review", "In Review", "Incomplete", "Completed"})
	resp.Execute(nil)
}
