package base_web

import (
	"fmt"
	"net/http"
	"runtime/debug"

	"github.com/gorilla/mux"
	"gitlab.com/cesmanager/base-server/core/base_domain"
	"gitlab.com/cesmanager/base-server/pkgs/base_blocs"
	"gitlab.com/cesmanager/base-server/pkgs/base_repository"
)

type Profile struct {
	c base_domain.AppConfig
}

func NewProfile(c base_domain.AppConfig) *Profile {
	return &Profile{c: c}
}

// get profile by id
func (h *Profile) Get(w http.ResponseWriter, r *http.Request) {
	resp := NewResponse(w, r)
	defer func() {
		if err := recover(); err != nil {
			panic(fmt.Sprintf("%v\n\t%v", err, string(debug.Stack())))
		}
		resp.Execute(nil)
	}()
	db, close := base_repository.MustMySQL(h.c.DataBase[h.c.ActiveDB], r.Header.Get("coy-db"))
	defer close()
	//check if id is provided
	vars := mux.Vars(r)
	id, ok := vars["id"]
	if !ok {
		_id := r.Header.Get("User_id")
		//get the user
		userServ := base_blocs.NewUser(base_repository.NewUser(db))
		u, err := userServ.Get(_id)
		if err != nil {
			resp.SetError("error", "unable to identify user")
			return
		}
		id = fmt.Sprint(u.ProfileID)
	}
	profServ := base_blocs.NewProfile(base_repository.NewProfile(db))
	profile, err := profServ.Get(id)

	if err != nil {
		resp.SetError("error", "Sorry an error occured")
		return
	}
	// utils := Utilities(h.c, r)
	// p := utils.GetPhoto(*profServ.Mod, profile.ID)
	// if len(p) > 0 {
	// 	profile.Photo = p[0]
	// }
	resp.SetBody(profile)
}

func (h *Profile) List(w http.ResponseWriter, r *http.Request) {
	resp := NewResponse(w, r)
	//response callback
	defer func() {
		resp.Execute(nil)
	}()
	db, close := base_repository.MustMySQL(h.c.DataBase[h.c.ActiveDB], r.Header.Get("coy-db"))
	defer close()

	req := ParseRequest(h.c, r)
	params := req.PayLoad()
	delete(params, "count")
	delete(params, "pages")
	page := "1"
	if _, ok := params["page"]; ok {
		page = fmt.Sprint(params["page"])
		delete(params, "page")
	}
	pageSize := "1"
	if _, ok := params["page_size"]; ok {
		pageSize = fmt.Sprint(params["page_size"])
		delete(params, "page_size")
	}

	bloc := base_blocs.NewProfile(base_repository.NewProfile(db))
	//get current user
	_user, ok := UserContext(r.Context())
	if !ok {
		resp.SetError("error", "unable to get user context")
		return
	}
	if resp.CheckSelfPermit(*bloc.Mod) {
		params["id"] = _user.ProfileID
	}
	list, err := bloc.List(params, req.OrderBy, page, pageSize)
	if err != nil {
		resp.SetError("error", err.Error())
		return
	}
	resp.SetBody(list)
}

// get profile by id
func (h *Profile) Find(w http.ResponseWriter, r *http.Request) {
	resp := NewResponse(w, r)

	defer func() {
		if err := recover(); err != nil {
			panic(fmt.Sprintf("%v\n\t%v", err, string(debug.Stack())))
		}
		resp.Execute(nil)
	}()
	db, close := base_repository.MustMySQL(h.c.DataBase[h.c.ActiveDB], r.Header.Get("coy-db"))
	defer close()

	profServ := base_blocs.NewProfile(base_repository.NewProfile(db))

	params := ParseRequest(h.c, r).PayLoad()
	profiles, err := profServ.Find(params)
	if err != nil {
		resp.SetError("error", err.Error())
		return
	}
	utils := Utilities(h.c, r)
	for i := 0; i < len(profiles); i++ {
		p := utils.GetPhoto(*profServ.Mod, profiles[i].ID)
		if len(p) > 0 {
			profiles[i].Photo = p[0]
		}
	}
	resp.SetBody(profiles)
}

// Create a Profile
func (h *Profile) Create(w http.ResponseWriter, r *http.Request) {
	resp := NewResponse(w, r)

	defer func() {
		if err := recover(); err != nil {
			panic(fmt.Sprintf("%v\n\t%v", err, string(debug.Stack())))
		}
		resp.Execute(nil)
	}()
	db, close := base_repository.MustMySQL(h.c.DataBase[h.c.ActiveDB], r.Header.Get("coy-db"))
	defer close()

	req := ParseRequest(h.c, r)
	params := req.PayLoad()

	//do basic validation
	fails := h.VetParams(params, []string{"bname", "fname", "lname", "email", "type"})
	if len(fails) > 0 {
		for k, v := range fails {
			resp.SetError(k, v)
		}
		return
	}
	//begin a transaction
	tx := db.Begin()
	profServ := base_blocs.NewProfile(base_repository.NewProfile(tx))

	//check if email is already registered
	profiles, err := profServ.Find(map[string]interface{}{"email": params["email"]})
	if err != nil {
		resp.SetCode(http.StatusBadRequest)
		resp.SetError("email_error", "unable to verify email")
		return
	}
	if len(profiles) > 0 {
		resp.SetCode(http.StatusNotAcceptable)
		resp.SetError("email_error", "email is registered to another user")
		return
	}

	profile, err := profServ.Create(params)
	if err != nil {
		resp.SetCode(http.StatusBadRequest)
		resp.SetError("error", "profile creation failed")
		tx.Rollback()
		return
	}
	//upload photo
	url, err := req.UploadPix(*profServ.Mod, profile.ID, "photo", "photo")
	if err != nil {
		resp.SetError("error", "photo upload failed")
		tx.Rollback()
		return
	}
	if url != "" {
		profile.Photo = url
		_, err = profServ.UpdateByInstance(profile)
		if err != nil {
			resp.SetError("error", "unable to update uploaded photo")
			tx.Rollback()
			return
		}
	}
	//commit transaction
	tx.Commit()
	resp.SetBody(profile)
}

// Update a Profile
func (h *Profile) Update(w http.ResponseWriter, r *http.Request) {
	resp := NewResponse(w, r)

	defer func() {
		if err := recover(); err != nil {
			panic(fmt.Sprintf("%v\n\t%v", err, string(debug.Stack())))
		}
		resp.Execute(nil)
	}()
	db, close := base_repository.MustMySQL(h.c.DataBase[h.c.ActiveDB], r.Header.Get("coy-db"))
	defer close()

	req := ParseRequest(h.c, r)
	params := req.PayLoad()
	if _, ok := params["id"]; !ok {
		resp.SetError("error", "id is required")
		return
	}

	//do basic validation
	fails := h.VetParams(params, []string{"bname", "fname", "lname", "email", "type"})
	if len(fails) > 0 {
		for k, v := range fails {
			resp.SetError(k, v)
		}
		return
	}
	//begin a transaction
	tx := db.Begin()
	profServ := base_blocs.NewProfile(base_repository.NewProfile(tx))

	profile, err := profServ.Update(fmt.Sprint(params["id"]), params)
	if err != nil {
		resp.SetCode(http.StatusBadRequest)
		resp.SetError("error", err.Error())
		tx.Rollback()
		return
	}
	//upload photo
	url, err := req.UploadPix(*profServ.Mod, profile.ID, "photo", "photo")
	if err != nil {
		resp.SetError("error", "photo upload failed")
		tx.Rollback()
		return
	}
	if url != "" {
		profile.Photo = url
		_, err = profServ.UpdateByInstance(profile)
		if err != nil {
			resp.SetError("error", "unable to update uploaded photo")
			tx.Rollback()
			return
		}
	}
	//commit transaction
	tx.Commit()
	resp.SetBody(profile)
}

func (h *Profile) VetParams(params map[string]interface{}, checks []string) map[string]string {
	var respErr = map[string]string{}
	for _, check := range checks {
		switch check {
		case "bname", "fname", "lname":
			_, bok := params["bname"]
			_, fok := params["fname"]
			_, lok := params["lname"]
			if (!fok && lok) && !bok {
				respErr["fname_error"] = "first name is required"
			}
			if (fok && !lok) && !bok {
				respErr["lname_error"] = "last name is required"
			}
			if !lok && !fok && !bok {
				respErr["bname_error"] = "business name is required"
			}
		case "email":
			if _, ok := params["email"]; !ok {
				respErr["email_error"] = "email is required"
			}
		case "type":
			if _, ok := params["type"]; !ok {
				respErr["type_error"] = "profile type is required"
			}
		}
	}
	return respErr
}
