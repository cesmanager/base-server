package base_web

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"runtime/debug"
	"strconv"
	"strings"

	"github.com/gorilla/mux"
	"gitlab.com/cesmanager/base-server/cmb/migration/gormigrator"
	"gitlab.com/cesmanager/base-server/cmb/migration/migrants"
	"gitlab.com/cesmanager/base-server/core/base_domain"
	"gitlab.com/cesmanager/base-server/pkgs/base_blocs"
	"gitlab.com/cesmanager/base-server/pkgs/base_repository"
)

type Coy struct {
	c base_domain.AppConfig
}

func NewCoy(c base_domain.AppConfig) *Coy {
	return &Coy{c: c}
}

func (h *Coy) Info(w http.ResponseWriter, r *http.Request) {
	resp := NewResponse(w, r)
	//response callback
	defer func() {
		if err := recover(); err != nil {
			panic(fmt.Sprintf("%v\n\t%v", err, string(debug.Stack())))
		}
		resp.Execute(nil)
	}()
	//create a connection to base database
	db, close := base_repository.MustMySQL(h.c.DataBase[h.c.ActiveDB], "")
	defer close()

	//get domain from host
	sub, domain, _ := SlitHost(r.Host)
	coyService := base_blocs.NewCoyInfo(base_repository.NewCCoyInfo(db))
	if len(sub) > 0 {
		domain = fmt.Sprintf("%s.%s", sub, domain)
	}
	coys, err := coyService.Find(map[string]interface{}{"domain": domain})
	if err != nil {
		resp.SetError("error", err.Error())
		return
	}
	if len(coys) == 0 {
		resp.SetError("error", "no company found")
		return
	}
	coy := coys[0]
	//connect to the specific company database
	dbname := h.c.DataBase[h.c.ActiveDB].DBPrefix + coy.Tag
	sdb, sdbclose, err := base_repository.MySQL(h.c.DataBase[h.c.ActiveDB], dbname)
	if err != nil {
		resp.SetError("error", "unable to connect db")
		return
	}
	defer sdbclose()

	settingsServ := base_blocs.NewSettings(base_repository.NewSettings(sdb))
	st, err := settingsServ.Get()
	if err != nil {
		resp.SetError("error", err.Error())
		return
	}

	settings := map[string]interface{}{
		"name":   st.Name,
		"slog":   st.Slog,
		"domain": coy.Domain,
		"photo":  st.Photo,
	}
	resp.SetBody(settings)
}

func (h *Coy) Get(w http.ResponseWriter, r *http.Request) {
	resp := NewResponse(w, r)
	//response callback
	defer func() {
		if err := recover(); err != nil {
			panic(fmt.Sprintf("%v\n\t%v", err, string(debug.Stack())))
		}
		resp.Execute(nil)
	}()
	//create a connection to base database
	db, close := base_repository.MustMySQL(h.c.DataBase[h.c.ActiveDB], "")
	defer close()

	//get parameters
	vars := mux.Vars(r)
	//check if id is provided
	id, ok := vars["id"]
	if !ok {
		resp.SetError("error", "id is required")
		return
	}
	//get company info
	coyService := base_blocs.NewCoyInfo(base_repository.NewCCoyInfo(db))
	coy, err := coyService.Get(id)
	if err != nil {
		resp.SetError("error", err.Error())
		return
	}
	//get root company info
	rcoy, err := coyService.Find(map[string]interface{}{"root": true})
	if err != nil {
		resp.SetError("error", err.Error())
		return
	}

	//connect to the specific company database
	dbname := h.c.DataBase[h.c.ActiveDB].DBPrefix + coy.Tag
	sdb, sdbclose, err := base_repository.MySQL(h.c.DataBase[h.c.ActiveDB], dbname)
	if err != nil {
		resp.SetError("error", "unable to connect db")
		return
	}
	//defer close
	defer sdbclose()
	//connect to root database
	dbname = h.c.DataBase[h.c.ActiveDB].DBPrefix + rcoy[0].Tag
	rdb, rdbclose, err := base_repository.MySQL(h.c.DataBase[h.c.ActiveDB], dbname)
	if err != nil {
		resp.SetError("error", "unable to connect db")
		return
	}
	//defer close
	defer rdbclose()

	//get settings
	settingsServ := base_blocs.NewSettings(base_repository.NewSettings(sdb))
	setting, err := settingsServ.Get()
	if err != nil {
		resp.SetError("error", err.Error())
		return
	}
	//get contact person
	profServ := base_blocs.NewProfile(base_repository.NewProfile(rdb))
	profile, err := profServ.Get(fmt.Sprint(*coy.Contact))
	if err != nil {
		resp.SetError("error", err.Error())
		return
	}
	resp.SetBody(base_domain.Company{
		ID:        coy.ID,
		Name:      coy.Name,
		Status:    coy.Status,
		TypeID:    coy.TypeID,
		Type:      coy.Type,
		CreatedAt: coy.CreatedAt,
		UpdatedAt: coy.UpdatedAt,
		CoyInfo:   coy,
		Photo:     setting.Photo,
		Profile:   &profile,
		Settings:  &setting,
	})
}

func (h *Coy) List(w http.ResponseWriter, r *http.Request) {
	resp := NewResponse(w, r)
	//response callback
	defer func() {
		if err := recover(); err != nil {
			panic(fmt.Sprintf("%v\n\t%v", err, string(debug.Stack())))
		}
		resp.Execute(nil)
	}()
	//create a connection to base database
	db, close := base_repository.MustMySQL(h.c.DataBase[h.c.ActiveDB], "")
	defer close()

	params := ParseRequest(h.c, r).PayLoad()
	delete(params, "count")
	delete(params, "pages")
	page := "1"
	if _, ok := params["page"]; ok {
		page = fmt.Sprint(params["page"])
		delete(params, "page")
	}
	pageSize := "1"
	if _, ok := params["page_size"]; ok {
		pageSize = fmt.Sprint(params["page_size"])
		delete(params, "page_size")
	}
	order := map[string]interface{}{}

	listServ := base_blocs.NewCoyInfo(base_repository.NewCCoyInfo(db))
	list, err := listServ.List(params, order, page, pageSize)
	if err != nil {
		resp.SetError("error", err.Error())
		return
	}
	resp.SetBody(list)
}

func (h *Coy) Create(w http.ResponseWriter, r *http.Request) {
	resp := NewResponse(w, r)
	utils := Utilities(h.c, r)
	//response callback
	defer func() {
		if err := recover(); err != nil {
			panic(fmt.Sprintf("%v\n\t%v", err, string(debug.Stack())))
		}
		resp.Execute(nil)
	}()
	//create a connection to base database
	db, close := base_repository.MustMySQL(h.c.DataBase[h.c.ActiveDB], "")
	defer close()

	req := ParseRequest(h.c, r)
	params := req.PayLoad()
	params["tag"] = strings.ToLower(fmt.Sprint(params["tag"]))
	if _, ok := params["domain"]; !ok || strings.TrimSpace(fmt.Sprint(params["domain"])) == "" {
		params["domain"] = fmt.Sprintf("%s.%s", params["tag"], h.c.Domain)
	}
	//strip root from params
	delete(params, "root")
	//begin transaction
	tx := db.Begin()

	coyService := base_blocs.NewCoyInfo(base_repository.NewCCoyInfo(tx))
	coyinfo, err := coyService.Create(params)
	if err != nil {
		utils.Log(err, base_blocs.SQLError)
		resp.SetError("error", err.Error())
		tx.Rollback()
		return
	}
	//create the company's database
	sdbname := h.c.DataBase[h.c.ActiveDB].DBPrefix + coyinfo.Tag
	err = base_repository.MakeSchema(h.c.DataBase[h.c.ActiveDB], sdbname)
	if err != nil {
		utils.Log(err, base_blocs.SQLError)
		resp.SetError("error", err.Error())
		tx.Rollback()
		base_repository.RemoveSchema(h.c.DataBase[h.c.ActiveDB], sdbname)
		return
	}
	//get root company info
	rcoy, err := coyService.Find(map[string]interface{}{"root": true})
	if err != nil {
		resp.SetError("error", err.Error())
		return
	}

	//connect to new database
	sdb, sdbclose, err := base_repository.MySQL(h.c.DataBase[h.c.ActiveDB], sdbname)
	if err != nil {
		tx.Rollback()
		resp.SetError("error", "unable to connect to new db")
		utils.Log(err, base_blocs.SQLError)
		err = base_repository.RemoveSchema(h.c.DataBase[h.c.ActiveDB], sdbname)
		if err != nil {
			panic(err.Error())
		}
		return
	}
	defer sdbclose()
	//connect to root database
	rdbname := h.c.DataBase[h.c.ActiveDB].DBPrefix + rcoy[0].Tag
	rdb, rdbclose, err := base_repository.MySQL(h.c.DataBase[h.c.ActiveDB], rdbname)
	if err != nil {
		tx.Rollback()
		resp.SetError("error", "unable to connect db")
		utils.Log(err, base_blocs.SQLError)
		err = base_repository.RemoveSchema(h.c.DataBase[h.c.ActiveDB], sdbname)
		if err != nil {
			panic(err.Error())
		}
		return
	}
	//defer close
	defer rdbclose()

	//do a full migration for the current company
	migrator := gormigrator.Gormigrator{
		Action: "up",
		System: "base_server",
		Items:  migrants.Migrants,
	}
	//set migrants globals
	migrants.Company = coyinfo
	migrants.Config = h.c

	err = migrator.Run(sdb)
	if err != nil {
		tx.Rollback()
		resp.SetError("error", err.Error())
		err = base_repository.RemoveSchema(h.c.DataBase[h.c.ActiveDB], sdbname)
		if err != nil {
			panic(err.Error())
		}
		return
	}

	//create the profile data for the company.
	fails := (&Profile{}).VetParams(params, []string{"fname", "lname", "email"})
	if len(fails) > 0 {
		for k, v := range fails {
			resp.SetError(k, v)
		}
		err = base_repository.RemoveSchema(h.c.DataBase[h.c.ActiveDB], sdbname)
		if err != nil {
			panic(err.Error())
		}
		return
	}
	profServ := base_blocs.NewProfile(base_repository.NewProfile(rdb))
	//check if email is already registered
	profiles, err := profServ.Find(map[string]interface{}{"email": params["email"]})
	if err != nil {
		tx.Rollback()
		resp.SetCode(http.StatusBadRequest)
		resp.SetError("email_error", "unable to verify email")
		err = base_repository.RemoveSchema(h.c.DataBase[h.c.ActiveDB], sdbname)
		if err != nil {
			panic(err.Error())
		}
		return
	}
	if len(profiles) > 0 {
		tx.Rollback()
		resp.SetCode(http.StatusNotAcceptable)
		resp.SetError("email_error", "email is registered to another user")
		err = base_repository.RemoveSchema(h.c.DataBase[h.c.ActiveDB], sdbname)
		if err != nil {
			panic(err.Error())
		}
		return
	}
	profile, err := profServ.Create(params)
	if err != nil {
		tx.Rollback()
		resp.SetCode(http.StatusBadRequest)
		resp.SetError("error", "profile creation failed")
		err = base_repository.RemoveSchema(h.c.DataBase[h.c.ActiveDB], sdbname)
		if err != nil {
			panic(err.Error())
		}
		return
	}

	//update settings
	settingsServ := base_blocs.NewSettings(base_repository.NewSettings(sdb))
	settings, err := settingsServ.Update(params)
	if err != nil {
		tx.Rollback()
		resp.SetError("error", err.Error())
		err = base_repository.RemoveSchema(h.c.DataBase[h.c.ActiveDB], sdbname)
		if err != nil {
			panic(err.Error())
		}
		return
	}
	req.utils.SetTag(coyinfo.Tag)
	settings.Photo, err = req.UploadPix(*settingsServ.Mod, 1, "photo", "photo")
	if err != nil {
		tx.Rollback()
		resp.SetError("error", "photo upload failed")
		err = base_repository.RemoveSchema(h.c.DataBase[h.c.ActiveDB], sdbname)
		if err != nil {
			panic(err.Error())
		}
		return
	}
	err = settingsServ.UpdateByInstance(&settings)
	if err != nil {
		tx.Rollback()
		resp.SetError("error", "setting update failed")
		err = base_repository.RemoveSchema(h.c.DataBase[h.c.ActiveDB], sdbname)
		if err != nil {
			panic(err.Error())
		}
		return
	}

	//update coyinfo with the contact id
	_, err = coyService.Update(fmt.Sprint(coyinfo.ID), map[string]interface{}{"contact": profile.ID})
	if err != nil {
		tx.Rollback()
		resp.SetError("error", "company update failed")
		err = base_repository.RemoveSchema(h.c.DataBase[h.c.ActiveDB], sdbname)
		if err != nil {
			panic(err.Error())
		}
		return
	}
	//commit transaction
	tx.Commit()
	resp.SetBody(coyinfo)
}

func (h *Coy) Update(w http.ResponseWriter, r *http.Request) {
	resp := NewResponse(w, r)
	utils := Utilities(h.c, r)
	//response callback
	defer func() {
		if err := recover(); err != nil {
			panic(fmt.Sprintf("%v\n\t%v", err, string(debug.Stack())))
		}
		resp.Execute(nil)
	}()
	//create a connection to base database
	db, close := base_repository.MustMySQL(h.c.DataBase[h.c.ActiveDB], "")
	defer close()

	req := ParseRequest(h.c, r)
	params := req.PayLoad()
	if _, ok := params["id"]; !ok {
		resp.SetError("error", "id is required")
		return
	}
	params["tag"] = strings.ToLower(fmt.Sprint(params["tag"]))
	if _, ok := params["domain"]; !ok || strings.TrimSpace(fmt.Sprint(params["domain"])) == "" {
		params["domain"] = fmt.Sprintf("%s.%s", params["tag"], h.c.Domain)
	}
	//strip root from params
	delete(params, "root")

	//begin transaction
	tx := db.Begin()
	coyService := base_blocs.NewCoyInfo(base_repository.NewCCoyInfo(tx))
	coyinfo, err := coyService.Update(fmt.Sprint(params["id"]), params)
	if err != nil {
		utils.Log(err, base_blocs.SQLError)
		resp.SetError("error", err.Error())
		tx.Rollback()
		return
	}
	//ensure that a root company will always have the company management feature
	if coyinfo.Root {
		feats := map[string]bool{}
		for _, v := range strings.Split(strings.TrimSpace(fmt.Sprint(params["features"])), ",") {
			feats[v] = true
		}
		if _, ok := feats[*coyService.Feature.Mod]; !ok { //add it
			if strings.TrimSpace(fmt.Sprint(params["features"])) == "" {
				params["features"] = *coyService.Feature.Mod
			} else {
				params["features"] = fmt.Sprint(params["features"], ",", *coyService.Feature.Mod)
			}
			_, err := coyService.Update(fmt.Sprint(params["id"]), map[string]interface{}{
				"features": params["features"],
			})
			if err != nil {
				utils.Log(err, base_blocs.SQLError)
				resp.SetError("error", err.Error())
				tx.Rollback()
				return
			}
		}
	}
	//get root company info
	rcoy, err := coyService.Find(map[string]interface{}{"root": true})
	if err != nil {
		resp.SetError("error", err.Error())
		return
	}

	//connect to the company database
	sdbname := h.c.DataBase[h.c.ActiveDB].DBPrefix + coyinfo.Tag
	sdb, sdbclose, err := base_repository.MySQL(h.c.DataBase[h.c.ActiveDB], sdbname)
	if err != nil {
		tx.Rollback()
		resp.SetError("error", "unable to connect to company db")
		utils.Log(err, base_blocs.SQLError)
		return
	}
	defer sdbclose()
	//connect to root database
	rdbname := h.c.DataBase[h.c.ActiveDB].DBPrefix + rcoy[0].Tag
	rdb, rdbclose, err := base_repository.MySQL(h.c.DataBase[h.c.ActiveDB], rdbname)
	if err != nil {
		tx.Rollback()
		resp.SetError("error", "unable to connect db")
		utils.Log(err, base_blocs.SQLError)
		return
	}
	//defer close
	defer rdbclose()

	//create the profile data for the company.
	fails := (&Profile{}).VetParams(params, []string{"fname", "lname", "email"})
	if len(fails) > 0 {
		for k, v := range fails {
			resp.SetError(k, v)
		}
		return
	}
	rdb_tx := rdb.Begin()
	profServ := base_blocs.NewProfile(base_repository.NewProfile(rdb_tx))
	//check if email is already registered
	profiles, err := profServ.Find(map[string]interface{}{
		"email": map[string]interface{}{"val": params["email"]},
		"id":    map[string]interface{}{"con": "!=", "val": params["contact"]},
	})
	if err != nil {
		rdb_tx.Rollback()
		tx.Rollback()
		resp.SetError("email_error", "unable to verify email change")
		return
	}
	if len(profiles) > 0 {
		rdb_tx.Rollback()
		tx.Rollback()
		resp.SetError("email_error", "email is registered to another user")
		return
	}
	//strip conflicting fields
	delete(params, "type_id")
	delete(params, "status")
	_, err = profServ.Update(fmt.Sprint(params["contact"]), params)
	if err != nil {
		rdb_tx.Rollback()
		tx.Rollback()
		resp.SetError("error", "contact person update failed")
		return
	}
	//update settings
	sdb_tx := sdb.Begin()
	settingsServ := base_blocs.NewSettings(base_repository.NewSettings(sdb_tx))
	settings, err := settingsServ.Update(params)
	if err != nil {
		rdb_tx.Rollback()
		sdb_tx.Rollback()
		tx.Rollback()
		resp.SetError("error", err.Error())
		return
	}
	req.utils.SetTag(coyinfo.Tag)
	settings.Photo, err = req.UploadPix(*settingsServ.Mod, 1, "photo", "photo")
	if err != nil {
		rdb_tx.Rollback()
		sdb_tx.Rollback()
		tx.Rollback()
		resp.SetError("error", "photo upload failed")
		return
	}
	err = settingsServ.UpdateByInstance(&settings)
	if err != nil {
		rdb_tx.Rollback()
		sdb_tx.Rollback()
		tx.Rollback()
		resp.SetError("error", "setting update failed")
		return
	}

	//commit transaction
	tx.Commit()
	sdb_tx.Commit()
	rdb_tx.Commit()
	resp.SetBody(coyinfo)
}

func (h *Coy) Setup(w http.ResponseWriter, r *http.Request) {
	resp := NewResponse(w, r)
	utils := Utilities(h.c, r)
	//response callback
	defer func() {
		if err := recover(); err != nil {
			panic(fmt.Sprintf("%v\n\t%v", err, string(debug.Stack())))
		}
		resp.Execute(nil)
	}()
	//create a connection to base database
	db, close := base_repository.MustMySQL(h.c.DataBase[h.c.ActiveDB], "")
	defer close()

	req := ParseRequest(h.c, r)
	params := req.PayLoad()
	//confirm setup key
	if strings.TrimSpace(h.c.Setup) == "" || fmt.Sprint(params["setup_key"]) != h.c.Setup {
		resp.SetError("error", "setup_key declined")
		return
	}
	if _, ok := params["domain"]; !ok || strings.TrimSpace(fmt.Sprint(params["domain"])) == "" {
		params["domain"] = fmt.Sprintf("%s.%s", params["tag"], h.c.Domain)
	}
	params["tag"] = strings.ToLower(fmt.Sprint(params["tag"]))
	params["root"] = 1    //ensure that this company is the root company
	params["contact"] = 1 //contact would be admin user

	//begin transaction
	tx := db.Begin()
	coyService := base_blocs.NewCoyInfo(base_repository.NewCoyInfo(tx))
	//confirm that their is no root company
	coys, err := coyService.Find(map[string]interface{}{"root": 1})
	if err != nil {
		utils.Log(err, base_blocs.SQLError)
		resp.SetError("error", err.Error())
		return
	}
	if len(coys) > 0 {
		resp.SetError("error", "system setup has been completed previously")
		return
	}
	params["features"] = *coyService.Mod

	coyinfo, err := coyService.Create(params)
	if err != nil {
		utils.Log(err, base_blocs.SQLError)
		resp.SetError("error", err.Error())
		tx.Rollback()
		return
	}
	//sync features
	featureServ := base_blocs.NewFeature(base_repository.NewFeature(tx))
	err = featureServ.SyncFeature()
	if err != nil {
		utils.Log(err, base_blocs.SQLError)
		resp.SetError("error", err.Error())
		tx.Rollback()
		return
	}
	//add company to the root company feature

	//create the companies database
	dbname := h.c.DataBase[h.c.ActiveDB].DBPrefix + coyinfo.Tag
	err = base_repository.MakeSchema(h.c.DataBase[h.c.ActiveDB], dbname)
	if err != nil {
		utils.Log(err, base_blocs.SQLError)
		resp.SetError("error", err.Error())
		tx.Rollback()
		base_repository.RemoveSchema(h.c.DataBase[h.c.ActiveDB], dbname)
		return
	}
	//populate the created database
	sdb, sdbclose, err := base_repository.MySQL(h.c.DataBase[h.c.ActiveDB], dbname)
	if err != nil {
		tx.Rollback()
		resp.SetError("error", "unable to connect to new db")
		utils.Log(err, base_blocs.SQLError)
		err = base_repository.RemoveSchema(h.c.DataBase[h.c.ActiveDB], dbname)
		if err != nil {
			panic(err.Error())
		}
		return
	}
	defer sdbclose()

	//do a full migration for the current company
	migrator := gormigrator.Gormigrator{
		Action: "up",
		System: "base_server",
		Items:  migrants.Migrants,
	}
	//set migrants globals
	migrants.Company = coyinfo
	migrants.Config = h.c

	err = migrator.Run(sdb)
	if err != nil {
		tx.Rollback()
		resp.SetError("error", "database migration failed")
		err = base_repository.RemoveSchema(h.c.DataBase[h.c.ActiveDB], dbname)
		if err != nil {
			panic(err.Error())
		}
		return
	}

	//upload photo to settings
	settingsServ := base_blocs.NewSettings(base_repository.NewSettings(sdb))
	settings, err := settingsServ.Update(params)
	if err != nil {
		tx.Rollback()
		resp.SetError("error", err.Error())
		err = base_repository.RemoveSchema(h.c.DataBase[h.c.ActiveDB], dbname)
		if err != nil {
			panic(err.Error())
		}
		return
	}
	//update coy_tag in utils
	req.utils.SetTag(coyinfo.Tag)
	settings.Photo, err = req.UploadPix(*settingsServ.Mod, 1, "photo", "photo")
	if err != nil {
		tx.Rollback()
		resp.SetError("error", "photo upload failed")
		err = base_repository.RemoveSchema(h.c.DataBase[h.c.ActiveDB], dbname)
		if err != nil {
			panic(err.Error())
		}
		return
	}
	err = settingsServ.UpdateByInstance(&settings)
	if err != nil {
		tx.Rollback()
		resp.SetError("error", "setting update failed")
		err = base_repository.RemoveSchema(h.c.DataBase[h.c.ActiveDB], dbname)
		if err != nil {
			panic(err.Error())
		}
		return
	}

	//commit transaction
	tx.Commit()
	resp.SetBody(map[string]interface{}{"msg": "setup completed"})
}

func (h *Coy) SyncFeature(w http.ResponseWriter, r *http.Request) {
	resp := NewResponse(w, r)

	//response callback
	defer func() {
		if err := recover(); err != nil {
			panic(fmt.Sprintf("%v\n\t%v", err, string(debug.Stack())))
		}
		resp.Execute(nil)
	}()
	//create a connection to base database
	db, close := base_repository.MustMySQL(h.c.DataBase[h.c.ActiveDB], "")
	defer close()

	featureServ := base_blocs.NewFeature(base_repository.NewFeature(db))
	err := featureServ.SyncFeature()
	if err != nil {
		resp.SetError("error", err.Error())
		return
	}

	resp.SetBody(map[string]string{"msg": "syncronisation completed"})
}

func (h *Coy) Features(w http.ResponseWriter, r *http.Request) {
	resp := NewResponse(w, r)

	//response callback
	defer func() {
		if err := recover(); err != nil {
			panic(fmt.Sprintf("%v\n\t%v", err, string(debug.Stack())))
		}
		resp.Execute(nil)
	}()
	//create a connection to base database
	db, close := base_repository.MustMySQL(h.c.DataBase[h.c.ActiveDB], "")
	defer close()

	req := ParseRequest(h.c, r)
	params := req.PayLoad()
	featureServ := base_blocs.NewFeature(base_repository.NewFeature(db))
	features, err := featureServ.Find(map[string]interface{}{"parent_id": nil}, nil)
	if err != nil {
		resp.SetError("error", err.Error())
		return
	}
	if tag, ok := params["tag"]; ok {
		utils := Utilities(h.c, r)
		utils.SetTag(tag)
		//get all addons
		addons, err := utils.AllAddons()
		if err == nil && len(addons) > 0 {
			//loop through active addons
			for _, addon := range addons {
				if !addon.Active {
					continue
				}
				port := ""
				if len(addon.Port) > 0 {
					port = ":" + addon.Port
				}
				url := fmt.Sprintf("%s://%s%s/module/features", addon.Scheme, addon.Domain, port)
				proxyReq, err := http.NewRequest("GET", url, r.Body)
				if err != nil {
					continue
				}
				proxyReq.Header = r.Header

				client := &http.Client{}

				proxyRes, err := client.Do(proxyReq)
				if err != nil {
					continue
				}
				defer proxyRes.Body.Close()
				//read the body
				body, err := io.ReadAll(proxyRes.Body)
				if err != nil {
					continue
				}
				resp := base_domain.Response{}
				err = json.Unmarshal(body, &resp)
				if err != nil || !resp.Status {
					continue
				}
				feats := []base_domain.Feature{}
				data, err := json.Marshal(resp.Body)
				if err != nil {
					continue
				}
				err = json.Unmarshal(data, &feats)
				if err != nil {
					continue
				}
				// if addon.UILink != "" {
				// 	for i, pmt := range pmts {
				// 		pmts[i].Url = addon.UILink + "/" + resp.Token + "/goto/" + strings.ReplaceAll(strings.ToLower(pmt.Title), " ", "-")
				// 	}
				// }
				features = append(features, feats...)
			}
		}
	}

	resp.SetBody(features)
}

func (h *Coy) ActiveFeatures(w http.ResponseWriter, r *http.Request) {
	resp := NewResponse(w, r)
	utils := Utilities(h.c, r)
	//response callback
	defer func() {
		if err := recover(); err != nil {
			panic(fmt.Sprintf("%v\n\t%v", err, string(debug.Stack())))
		}
		resp.Execute(nil)
	}()
	//create a connection to base database
	db, close := base_repository.MustMySQL(h.c.DataBase[h.c.ActiveDB], "")
	defer close()

	root, _ := strconv.ParseBool(r.Header.Get("coy-root"))
	featureServ := base_blocs.NewFeature(base_repository.NewFeature(db))
	features, err := featureServ.ActiveFeatures(r.Header.Get("coy-features"), root)
	if err != nil {
		resp.SetError("error", err.Error())
		return
	}

	//get all addons
	addons, err := utils.AllAddons()
	if err == nil && len(addons) > 0 {
		//loop through active addons
		for _, addon := range addons {
			if !addon.Active {
				continue
			}
			port := ""
			if len(addon.Port) > 0 {
				port = ":" + addon.Port
			}
			url := fmt.Sprintf("%s://%s%s/module/features", addon.Scheme, addon.Domain, port)
			proxyReq, err := http.NewRequest("GET", url, r.Body)
			if err != nil {
				continue
			}
			proxyReq.Header = r.Header

			client := &http.Client{}

			proxyRes, err := client.Do(proxyReq)
			if err != nil {
				continue
			}
			defer proxyRes.Body.Close()
			//read the body
			body, err := io.ReadAll(proxyRes.Body)
			if err != nil {
				continue
			}
			resp := base_domain.Response{}
			err = json.Unmarshal(body, &resp)
			if err != nil || !resp.Status {
				continue
			}
			feats := []base_domain.Feature{}
			data, err := json.Marshal(resp.Body)
			if err != nil {
				continue
			}
			err = json.Unmarshal(data, &feats)
			if err != nil {
				continue
			}
			var setUrl func([]base_domain.Feature) []base_domain.Feature
			setUrl = func(fs []base_domain.Feature) []base_domain.Feature {
				for i, f := range fs {
					fs[i].Url = addon.UILink + "/" + resp.Token + "/goto/" + strings.ReplaceAll(strings.ToLower(f.Name), " ", "-")
					if len(f.Features) > 0 {
						fs[i].Features = setUrl(f.Features)
					}
				}
				return fs
			}
			if addon.UILink != "" {
				feats = setUrl(feats)
			}
			features = append(features, feats...)
		}
	}

	resp.SetBody(features)
}
