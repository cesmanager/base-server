package base_web

import (
	"fmt"
	"net/http"
	"os"
	"path/filepath"
	"runtime/debug"

	"github.com/gorilla/mux"
	"gitlab.com/cesmanager/base-server/core/base_domain"
	"gitlab.com/cesmanager/base-server/pkgs/base_blocs"
	"gitlab.com/cesmanager/base-server/pkgs/base_repository"
)

type Currency struct {
	c base_domain.AppConfig
}

func NewCurrency(c base_domain.AppConfig) *Currency {
	return &Currency{c: c}
}

// get currency by id
func (h *Currency) Get(w http.ResponseWriter, r *http.Request) {
	resp := NewResponse(w, r)

	defer func() {
		if err := recover(); err != nil {
			panic(fmt.Sprintf("%v\n\t%v", err, string(debug.Stack())))
		}
		resp.Execute(nil)
	}()
	db, close := base_repository.MustMySQL(h.c.DataBase[h.c.ActiveDB], r.Header.Get("coy-db"))
	defer close()

	vars := mux.Vars(r)
	//check if id is provided
	id, ok := vars["id"]
	if !ok {
		resp.SetError("error", "id is required")
		return
	}
	profServ := base_blocs.NewCurrency(base_repository.NewCurrency(db))

	currency, err := profServ.Get(id)

	if err != nil {
		resp.SetError("error", err.Error())
		return
	}
	utils := Utilities(h.c, r)
	p := utils.GetPhoto(*profServ.Mod, currency.ID)
	if len(p) > 0 {
		currency.Photo = p[0]
	}
	resp.SetBody(currency)
}

func (h *Currency) List(w http.ResponseWriter, r *http.Request) {
	resp := NewResponse(w, r)
	//response callback
	defer func() {
		resp.Execute(nil)
	}()
	db, close := base_repository.MustMySQL(h.c.DataBase[h.c.ActiveDB], r.Header.Get("coy-db"))
	defer close()

	params := ParseRequest(h.c, r).PayLoad()
	delete(params, "count")
	delete(params, "pages")
	page := "1"
	if _, ok := params["page"]; ok {
		page = fmt.Sprint(params["page"])
		delete(params, "page")
	}
	pageSize := "1"
	if _, ok := params["page_size"]; ok {
		pageSize = fmt.Sprint(params["page_size"])
		delete(params, "page_size")
	}
	search := ""
	if _, ok := params["search"]; ok {
		search = fmt.Sprint(params["search"])
		delete(params, "search")
	}
	order := map[string]interface{}{}

	listServ := base_blocs.NewCurrency(base_repository.NewCurrency(db))
	list, err := listServ.List(search, order, page, pageSize)
	if err != nil {
		resp.SetError("error", err.Error())
		return
	}
	resp.SetBody(list)
}

// get currency by id
func (h *Currency) Find(w http.ResponseWriter, r *http.Request) {
	resp := NewResponse(w, r)

	defer func() {
		if err := recover(); err != nil {
			panic(fmt.Sprintf("%v\n\t%v", err, string(debug.Stack())))
		}
		resp.Execute(nil)
	}()
	db, close := base_repository.MustMySQL(h.c.DataBase[h.c.ActiveDB], r.Header.Get("coy-db"))
	defer close()

	profServ := base_blocs.NewCurrency(base_repository.NewCurrency(db))

	params := ParseRequest(h.c, r).PayLoad()
	currencys, err := profServ.Find(params)
	if err != nil {
		resp.SetError("error", err.Error())
		return
	}
	utils := Utilities(h.c, r)
	for i := 0; i < len(currencys); i++ {
		p := utils.GetPhoto(*profServ.Mod, currencys[i].ID)
		if len(p) > 0 {
			currencys[i].Photo = p[0]
		}
	}
	resp.SetBody(currencys)
}

// Create a Currency
func (h *Currency) Create(w http.ResponseWriter, r *http.Request) {
	resp := NewResponse(w, r)

	defer func() {
		if err := recover(); err != nil {
			panic(fmt.Sprintf("%v\n\t%v", err, string(debug.Stack())))
		}
		resp.Execute(nil)
	}()
	db, close := base_repository.MustMySQL(h.c.DataBase[h.c.ActiveDB], r.Header.Get("coy-db"))
	defer close()

	req := ParseRequest(h.c, r)
	params := req.PayLoad()

	//do basic validation
	fails := h.VetParams(params)
	if len(fails) > 0 {
		for k, v := range fails {
			resp.SetError(k, v)
		}
		return
	}
	//begin a transaction
	tx := db.Begin()
	profServ := base_blocs.NewCurrency(base_repository.NewCurrency(tx))
	//check if email is already registered
	currencys, err := profServ.Find(map[string]interface{}{"email": params["email"]})
	if err != nil {
		resp.SetCode(http.StatusBadRequest)
		resp.SetError("email_error", "unable to verify email")
		return
	}
	if len(currencys) > 0 {
		resp.SetCode(http.StatusNotAcceptable)
		resp.SetError("email_error", "email is registered to another user")
		return
	}

	currency, err := profServ.Create(params)
	if err != nil {
		resp.SetCode(http.StatusBadRequest)
		resp.SetError("error", "currency creation failed")
		tx.Rollback()
		return
	}
	//upload the photo if any
	utils := Utilities(h.c, r)
	//upload photo
	if r.Form.Has("photo") {
		pt := utils.PixPath(*profServ.Mod, currency.ID)
		//remove the previous photo if any
		if files, ok := utils.ReadDir(pt); ok == nil && len(files) > 0 {
			for _, file := range files {
				os.Remove(pt + string(filepath.Separator) + file.Name())
			}
		}
		url, err := req.UploadFile("pix", *profServ.Mod, currency.ID, "photo", "photo")
		if err != nil {
			resp.SetError("error", "photo upload failed")
			tx.Rollback()
			return
		}
		if len(url) != 0 {
			//set the photo to the currency record
			currency, err = profServ.Update(fmt.Sprint(currency.ID), map[string]interface{}{"photo": url})
			if err != nil {
				resp.SetError("error", "unable to update uploaded photo")
				tx.Rollback()
				return
			}
		}
	}
	//commit transaction
	tx.Commit()
	resp.SetBody(currency)
}

// Create a Currency
func (h *Currency) Update(w http.ResponseWriter, r *http.Request) {
	resp := NewResponse(w, r)

	defer func() {
		if err := recover(); err != nil {
			panic(fmt.Sprintf("%v\n\t%v", err, string(debug.Stack())))
		}
		resp.Execute(nil)
	}()
	db, close := base_repository.MustMySQL(h.c.DataBase[h.c.ActiveDB], r.Header.Get("coy-db"))
	defer close()
	req := ParseRequest(h.c, r)
	params := req.PayLoad()
	if _, ok := params["id"]; !ok {
		resp.SetError("error", "id is required")
		return
	}
	//do basic validation
	fails := h.VetParams(params)
	if len(fails) > 0 {
		for k, v := range fails {
			resp.SetError(k, v)
		}
		return
	}
	//begin a transaction
	tx := db.Begin()
	profServ := base_blocs.NewCurrency(base_repository.NewCurrency(tx))

	currency, err := profServ.Update(fmt.Sprint(params["id"]), params)
	if err != nil {
		resp.SetCode(http.StatusBadRequest)
		resp.SetError("error", "currency update failed")
		tx.Rollback()
		return
	}
	utils := Utilities(h.c, r)
	//upload photo
	if r.Form.Has("photo") {
		pt := utils.PixPath(*profServ.Mod, currency.ID)
		//remove the previous photo if any
		if files, ok := utils.ReadDir(pt); ok == nil && len(files) > 0 {
			for _, file := range files {
				os.Remove(pt + string(filepath.Separator) + file.Name())
			}
		}
		url, err := req.UploadFile("pix", *profServ.Mod, currency.ID, "photo", "photo")
		if err != nil {
			resp.SetError("error", "photo upload failed")
			tx.Rollback()
			return
		}
		if len(url) != 0 {
			//set the photo to the currency record
			currency, err = profServ.Update(fmt.Sprint(currency.ID), map[string]interface{}{"photo": url})
			if err != nil {
				resp.SetError("error", "unable to update uploaded photo")
				tx.Rollback()
				return
			}
		}
	}
	tx.Commit()
	resp.SetBody(currency)
}

func (h *Currency) VetParams(params map[string]interface{}) map[string]string {
	var respErr = map[string]string{}
	_, bok := params["bname"]
	_, fok := params["fname"]
	_, lok := params["lname"]
	if (!fok || lok) && !bok {
		respErr["fname_error"] = "first name is required"
	}
	if (fok || !lok) && !bok {
		respErr["lname_error"] = "last name is required"
	}
	if !lok && !fok && !bok {
		respErr["bname_error"] = "business name is required"
	}
	if _, ok := params["email"]; !ok {
		respErr["email_error"] = "email is required"
	}
	if _, ok := params["type"]; !ok {
		respErr["type_error"] = "currency type is required"
	}
	return respErr
}
