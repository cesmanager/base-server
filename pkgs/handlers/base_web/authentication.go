package base_web

import (
	"fmt"
	"math/rand"
	"net/http"
	"runtime/debug"
	"strconv"
	"time"

	"gitlab.com/cesmanager/base-server/core/base_domain"
	"gitlab.com/cesmanager/base-server/pkgs/base_blocs"
	"gitlab.com/cesmanager/base-server/pkgs/base_repository"
	samgotemplate "gitlab.com/sammybigboy440/samgo-template"
	"golang.org/x/crypto/bcrypt"
)

type authentication struct {
	c base_domain.AppConfig
}

func NewAuthentication(c base_domain.AppConfig) *authentication {
	return &authentication{c: c}
}

func (h *authentication) SignUp(w http.ResponseWriter, r *http.Request) {
	resp := NewResponse(w, r)
	//response callback

	defer func() {
		if err := recover(); err != nil {
			panic(fmt.Sprintf("%v\n\t%v", err, string(debug.Stack())))
		}
		resp.Execute(nil)
	}()
	db, close := base_repository.MustMySQL(h.c.DataBase[h.c.ActiveDB], r.Header.Get("coy-db"))
	defer close()

	req := ParseRequest(h.c, r)
	params := req.PayLoad()
	//Profile instance
	profileHandler := NewProfile(h.c)
	//vet user
	for k, v := range h.VetParams(params) {
		resp.SetError(k, v)
	}
	//profile vet params
	for k, v := range profileHandler.VetParams(params, []string{"bname", "fname", "lname", "email", "type"}) {
		resp.SetError(k, v)
	}
	if resp.IsError() {
		return
	}
	//begin a transaction
	tx := db.Begin()
	//check if the username already exists
	user_bloc := base_blocs.NewUser(base_repository.NewUser(tx))
	u, err := user_bloc.Find(map[string]interface{}{"uname": params["uname"]})
	if err != nil {
		resp.SetError("uname_error", "username error")
		return
	}
	if len(u) > 0 {
		resp.SetCode(http.StatusBadRequest)
		resp.SetError("uname_error", "username is registered to another user")
		return
	}
	//if alt email is sent,
	profile_bloc := base_blocs.NewProfile(base_repository.NewProfile(tx))
	if _, ok := params["alt_email"]; !ok {
		p, err := profile_bloc.Find(map[string]interface{}{"email": params["email"]})
		if err != nil {
			resp.SetError("uname_error", "email error")
			return
		}
		if len(p) > 0 {
			resp.SetCode(http.StatusBadRequest)
			resp.SetError("uname_error", "email is registered to another profile")
			return
		}
	} else {
		delete(params, "email")
	}

	//create profile
	profile, err := profile_bloc.Create(params)
	if err != nil {
		resp.SetError("error", err.Error())
		tx.Rollback()
		return
	}
	params["profile_id"] = profile.ID
	//generate verification code
	min := 100000
	max := 999999
	rand := rand.New(rand.NewSource(time.Now().UnixNano()))
	params["vcode"] = strconv.Itoa(rand.Intn(max-min) + min)
	//create the user
	user, err := user_bloc.Create(params)
	if err != nil {
		resp.SetError("error", err.Error())
		tx.Rollback()
		return
	}
	outletID := "1"
	//get the profile outlet
	if profile.OutletID != nil {
		outletID = fmt.Sprint((*profile.OutletID))
	}
	//send out email
	outletServ := base_blocs.NewOutlet(base_repository.NewOutlet(tx))
	outlet, err := outletServ.Get(outletID)
	if err != nil {
		resp.SetError("error", err.Error())
		tx.Rollback()
		return
	}
	settingService := base_blocs.NewSettings(base_repository.NewSettings(tx))
	settings, err := settingService.Get()
	if err != nil {
		resp.SetError("error", err.Error())
		tx.Rollback()
		return
	}
	//send the verification email
	go h.sendMail(outlet.VerificationTemplate, settings, params, req.utils, profile.Email, profile.AltEmail)
	//commit transaction
	tx.Commit()
	resp.SetBody(user)
}

func (h *authentication) SignIn(w http.ResponseWriter, r *http.Request) {
	resp := NewResponse(w, r)
	var token string
	//response callback
	defer func() {
		if err := recover(); err != nil {
			panic(fmt.Sprintf("%v\n\t%v", err, string(debug.Stack())))
		}
		err := resp.Execute(&token)
		if err != nil {
			panic(fmt.Sprintf("%v\n\t%v", err, string(debug.Stack())))
		}
	}()
	db, close := base_repository.MustMySQL(h.c.DataBase[h.c.ActiveDB], r.Header.Get("coy-db"))
	defer close()

	//session
	sesHandle := NewSession(h.c)

	params := ParseRequest(h.c, r).PayLoad()
	if _, ok := params["uname"]; !ok {
		resp.SetError("uname_error", "username/email is required")
	}
	if _, ok := params["pword"]; !ok {
		resp.SetError("pword_error", "password is required")
	}
	if resp.IsError() {
		resp.SetCode(http.StatusForbidden)
		return
	}
	//check for active session
	user, token, err := sesHandle.ActiveUser(w, r)
	if err == nil {
		//return the logged in user if it matches the username on the login request
		if user.Uname == params["uname"] {
			resp.SetBody(user)
			return
		}
		//log the current userout
		sesHandle.SetAll(w, r, map[string]interface{}{
			"auth":       false,
			h.c.TokenKey: nil,
			"user":       nil,
			"permits":    nil,
		})
		if token != "" {
			accessServ := base_blocs.NewAccess(base_repository.NewAccess(db))
			err = accessServ.Delete(token)
			if err != nil {
				resp.SetError("error", "unable to logout prevous user")
				return
			}
		}
	}
	//check if the user exists
	userServ := base_blocs.NewUser(base_repository.NewUser(db))
	musers, err := userServ.Find(map[string]interface{}{"uname": params["uname"]})
	if err != nil || len(musers) == 0 {
		resp.SetError("uname_error", "username error")
		return
	}
	user = musers[0]
	err = bcrypt.CompareHashAndPassword([]byte(user.Pword), []byte(fmt.Sprint(params["pword"])))
	//remove the password
	user.Pword = ""
	if err != nil {
		resp.SetCode(http.StatusUnauthorized)
		resp.SetError("error", "invalid username / password combination")
		return
	}
	//get settings
	settings, err := base_blocs.NewSettings(base_repository.NewSettings(db)).Get()
	if err != nil {
		resp.SetError("error", err.Error())
		return
	}
	if settings.UserActive != user.Status {
		switch user.Status {
		case 0:
			resp.SetCode(http.StatusUnauthorized)
			resp.SetError("error", "activation required")
		case settings.UserDisabled:
			resp.SetCode(http.StatusNotAcceptable)
			resp.SetError("error", "access has been suspended")
		default:
			resp.SetCode(http.StatusNotAcceptable)
			resp.SetError("error", "user status is not recorgnized (contact system administrator)")
		}
		return
	}
	//create a login token
	accessServ := base_blocs.NewAccess(base_repository.NewAccess(db))
	x := accessServ.Log(fmt.Sprint(user.ID), ClientInfo(r))
	token = x.Token
	//get the users permission
	ugServ := base_blocs.NewUserGroup(base_repository.NewUserGroup(db))
	ug, err := ugServ.Get(fmt.Sprint(user.UserGroupID))
	if err != nil {
		resp.SetCode(http.StatusForbidden)
		resp.SetError("error", err.Error())
		return
	}

	err = sesHandle.SetAll(w, r, map[string]interface{}{
		"auth":       true,
		h.c.TokenKey: token,
		"user":       user,
		"permits":    &ug.MapPermits().MappedPermits,
	})
	if err != nil {
		resp.SetCode(http.StatusForbidden)
		resp.SetError("error", err.Error())
		return
	}
	resp.SetBody(user)
}

// Verification
func (h *authentication) Verification(w http.ResponseWriter, r *http.Request) {
	resp := NewResponse(w, r)
	//response callback

	defer func() {
		if err := recover(); err != nil {
			panic(fmt.Sprintf("%v\n\t%v", err, string(debug.Stack())))
		}
		resp.Execute(nil)
	}()
	db, close := base_repository.MustMySQL(h.c.DataBase[h.c.ActiveDB], r.Header.Get("coy-db"))
	defer close()

	params := ParseRequest(h.c, r).PayLoad()

	user_bloc := base_blocs.NewUser(base_repository.NewUser(db))

	users, err := user_bloc.Find(map[string]interface{}{"uname": params["uname"]})
	if err != nil {
		resp.SetError("error", err.Error())
		return
	}
	if len(users) == 0 {
		resp.SetError("username_error", "invalide username")
		return
	}
	user := users[0]
	if user.Vcode != fmt.Sprint(params["code"]) {
		resp.SetError("username_error", "invalide code")
		return
	}
	//get settings
	settings, err := base_blocs.NewSettings(base_repository.NewSettings(db)).Get()
	if err != nil {
		resp.SetError("error", err.Error())
		return
	}
	_, err = user_bloc.Update(fmt.Sprint(user.ID), map[string]interface{}{"vcode": " ", "status": settings.UserActive})
	if err != nil {
		resp.SetError("error", err.Error())
		return
	}
	resp.SetBody(map[string]interface{}{
		"message": "account activated",
	})
}

func (h *authentication) sendMail(template string, s base_domain.Settings, params map[string]interface{}, utils *base_blocs.Utilities, emails ...*string) {
	//make the template
	st := samgotemplate.NewEngine(template, &params)
	msg, _ := st.Execute()
	//log the error
	m := base_blocs.NewSMTP(&s)
	_emails := []string{}
	for _, v := range emails {
		if v != nil {
			_emails = append(_emails, *v)
		}
	}
	err := m.Reciepents(_emails).SetMsg("Account Verification", msg).Send()
	if err != nil {
		utils.Log(err.Error(), base_blocs.StdLog)
	}
}
func (h *authentication) VetParams(params map[string]interface{}) map[string]string {
	var respErr = map[string]string{}
	if _, ok := params["uname"]; !ok {
		respErr["uname_error"] = "username is required"
	}
	if _, ok := params["pword"]; !ok {
		respErr["pword_error"] = "password is required"
	}
	return respErr
}

// Logout
func (h *authentication) Logout(w http.ResponseWriter, r *http.Request) {
	resp := NewResponse(w, r)
	var token string
	//response callback
	defer func() {
		if err := recover(); err != nil {
			panic(fmt.Sprintf("%v\n\t%v", err, string(debug.Stack())))
		}
		resp.Execute(nil)
	}()
	db, close := base_repository.MustMySQL(h.c.DataBase[h.c.ActiveDB], r.Header.Get("coy-db"))
	defer close()
	//drop the session
	sesHandle := NewSession(h.c)
	err := sesHandle.Drop(w, r)
	if err != nil {
		resp.SetError("error", err.Error())
		return
	}
	token = r.Header.Get(h.c.TokenKey)
	if len(token) == 0 {
		resp.SetBody(map[string]interface{}{"message": "logout successful"})
		return
	}
	accessServ := base_blocs.NewAccess(base_repository.NewAccess(db))
	err = accessServ.Delete(token)
	if err != nil {
		resp.SetError("error", "logout failed")
		return
	}
	resp.SetBody(map[string]interface{}{"message": "logout successful"})
}

// VetLogin ...
func (h *authentication) VetLogin(w http.ResponseWriter, r *http.Request) {
	resp := NewResponse(w, r)
	//response callback
	defer func() {
		if err := recover(); err != nil {
			panic(fmt.Sprintf("%v\n\t%v", err, string(debug.Stack())))
		}
		resp.Execute(nil)
	}()
	//get current user
	user, ok := UserContext(r.Context())
	if !ok {
		resp.SetError("error", "unable to get user context")
		return
	}
	resp.SetBody(user)
}
