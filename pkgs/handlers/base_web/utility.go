package base_web

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"regexp"
	"strings"

	"github.com/gorilla/mux"
	"gitlab.com/cesmanager/base-server/core/base_domain"
	"gitlab.com/cesmanager/base-server/pkgs/base_blocs"
	"gitlab.com/cesmanager/base-server/pkgs/base_repository"
)

type request struct {
	*http.Request
	contentType    string
	OrderBy        map[string]interface{}
	ColumnOperator map[string]interface{}
	utils          *base_blocs.Utilities
}

func ParseRequest(config base_domain.AppConfig, r *http.Request) *request {
	contentType := strings.Split(r.Header.Get("content-type"), ";")[0]
	if contentType != "application/json" {
		//run user permission
		if contentType == "multipart/form-data" {
			_ = r.ParseMultipartForm(4096)
		} else {
			_ = r.ParseForm()
		}
	}
	return &request{
		Request:     r,
		contentType: contentType,
		utils:       Utilities(config, r),
	}
}

// handle pix single file upload
func (req *request) UploadPix(mod string, recID interface{}, field string, name string) (url string, err error) {
	return req.UploadFile("pix", mod, recID, field, name)
}

// handle any single file upload
func (req *request) UploadFile(group string, mod string, recID interface{}, field string, name string) (url string, err error) {
	if file, fileHeader, err := req.FormFile(field); err == nil {
		//get the file extension
		ext := ""
		fragName := strings.Split(fileHeader.Filename, ".")
		if len(fragName) > 1 {
			ext = "." + fragName[len(fragName)-1]
		}
		//use original name if no name is provided
		if strings.TrimSpace(name) == "" {
			name = fileHeader.Filename
			if len(fragName[:len(fragName)-1]) > 0 {
				name = strings.Join(fragName[:len(fragName)-1], ".")
			}
		}
		path := ""
		switch group {
		case "pix":
			path = req.utils.PixPath(mod, recID)
		case "vid":
			path = req.utils.VidPath(mod, recID)
		case "doc":
			path = req.utils.DocPath(mod, recID)
		}
		path, err = req.utils.UploadFile(file, path, name+ext)
		url = strings.Replace(path, req.utils.GetPath(), "/static", 1)
		return url, err
	}
	return
}

func (req *request) PayLoad() map[string]interface{} {
	params := map[string]interface{}{}
	if req.contentType == "application/json" {
		bodyBytes, _ := io.ReadAll(req.Body)
		json.Unmarshal([]byte(bodyBytes), &params)
	} else {
		//get url params
		for k, v := range mux.Vars(req.Request) {
			params[k] = v
		}
		//get the query values
		for k, v := range req.URL.Query() {
			params[k] = v[0]
		}
		//let form data override query values if it is the case
		for k, v := range req.PostForm {
			params[k] = v[0]
		}
	}
	req.orderBy(&params)
	req.columnOperator(&params)
	return params
}
func (req *request) PayLoadList() (params map[string]interface{}, page string, size string) {
	params = req.PayLoad()
	delete(params, "count")
	delete(params, "pages")
	page = "1"
	if _, ok := params["page"]; ok {
		page = fmt.Sprint(params["page"])
		delete(params, "page")
	}
	size = "1"
	if _, ok := params["page_size"]; ok {
		size = fmt.Sprint(params["page_size"])
		delete(params, "page_size")
	}
	return
}

// orderBy - functin used to pick all order_by parameter in a request
func (req *request) orderBy(params *map[string]interface{}) {
	/**
	* expected valus sample -  column:true;column:false
	* where true means descending and false ascending
	**/
	if _, ok := (*params)["order_by"]; ok {
		req.OrderBy = map[string]interface{}{}
		for _, rpair := range strings.Split(fmt.Sprint((*params)["order_by"]), ";") {
			pair := strings.Split(rpair, ":")
			req.OrderBy[pair[0]] = "false"
			if len(pair) > 1 {
				req.OrderBy[pair[0]] = pair[1]
			}
		}
		delete((*params), "order_by")
	}
}

// ColumnOperator - function used for getting the right oprators to be used for quesry comparison
func (req *request) columnOperator(params *map[string]interface{}) {
	/**
	* expected valus sample - column:Like;column:=;column:>;column:<;column:<=
	**/
	allowed := map[string]interface{}{"RLIKE": true, "LIKE": true, "=": true, ">": true, ">=": true, "<": true, "<=": true}
	if _, ok := (*params)["column_operator"]; ok {
		req.ColumnOperator = map[string]interface{}{}
		for _, rpair := range strings.Split(fmt.Sprint((*params)["column_operator"]), ";") {
			pair := strings.Split(rpair, ":")
			req.ColumnOperator[pair[0]] = "="
			if len(pair) > 1 {
				if _, ok := allowed[pair[1]]; ok {
					req.ColumnOperator[pair[0]] = pair[1]
				}
			}
		}
		delete((*params), "column_operator")
	}
}

func Utilities(c base_domain.AppConfig, r *http.Request) *base_blocs.Utilities {
	utils := base_blocs.NewUtils(c, nil)
	if r != nil {
		utils.SetTag(r.Header.Get("coy-tag"))
		sftp, ok := SftpContext(r.Context())
		if ok {
			utils.SetSftp(sftp)
		}
	}
	return utils
}

// ResponseData ...
func ResponseData(w http.ResponseWriter, r *http.Request, rep base_domain.Response) {
	w.WriteHeader(rep.Code)
	// rep.Token = r.Header.Get(config.TokenKey)
	// w.Header().Add(config.TokenKey)
	json.NewEncoder(w).Encode(rep)
}

// Response this is a standard Response type to be used throughout the software
type response struct {
	Status bool        `json:"status"`
	Token  string      `json:"token"`
	Body   interface{} `json:"body"`
	code   int
	errors map[string]interface{}
	format string
	c      base_domain.AppConfig
	w      http.ResponseWriter
	r      *http.Request
}

func NewResponse(w http.ResponseWriter, r *http.Request) response {
	c, ok := ConfigContext(r.Context())
	if !ok {
		panic("no config in context")
	}
	return response{
		Token:  r.Header.Get(c.TokenKey),
		errors: map[string]interface{}{},
		format: "application/json",
		c:      *c,
		w:      w,
		r:      r,
	}
}

func (resp *response) SetError(key string, value interface{}) *response {
	resp.errors[key] = value
	return resp
}
func (resp *response) SetCleanError(key string, value string) *response {
	utils := Utilities(resp.c, resp.r)
	//clean coy dir
	value = strings.ReplaceAll(value, utils.CoyDir(), "")
	resp.errors[key] = value
	return resp
}

func (resp *response) IsError() bool {
	return len(resp.errors) > 0
}

func (resp *response) SetCode(code int) *response {
	resp.code = code
	return resp
}
func (resp *response) GetCode() int {
	return resp.code
}

func (resp *response) SetBody(body interface{}) *response {
	resp.Body = body
	return resp
}

func (resp *response) Execute(token *string) error {
	resp.Status = true
	if resp.IsError() {
		if resp.code == 0 {
			resp.code = http.StatusBadRequest
		}
		resp.Status = false
		resp.Body = resp.errors
	}
	if resp.code == 0 {
		resp.code = 200
	}
	if token == nil {
		resp.w.Header().Add(resp.c.TokenKey, resp.Token)
	} else {
		resp.Token = *token
		resp.w.Header().Add(resp.c.TokenKey, resp.Token)
	}
	resp.w.Header().Set("content-type", resp.format)
	//write thr response code
	resp.w.WriteHeader(resp.code)
	return json.NewEncoder(resp.w).Encode(resp)
}

func (resp *response) CheckPermit(mod string, action string) error {
	user, ok := UserContext(resp.r.Context())
	if !ok {
		return fmt.Errorf("user not in context")
	}
	if user.UserGroup.SuperGroup {
		return nil
	}
	permits, ok := PermitsContext(resp.r.Context())
	if !ok {
		return fmt.Errorf("permits not in context")
	}
	if permit, ok := (*permits)[mod]; ok {
		if val, ok := (*permit.MappedAction)[action]; ok && val {
			return nil
		}
	}
	return fmt.Errorf("action not allowed")
}
func (resp *response) CheckSelfPermit(mod string) bool {
	user, ok := UserContext(resp.r.Context())
	if !ok || user.UserGroup.SuperGroup {
		return false
	}
	permits, ok := PermitsContext(resp.r.Context())
	if ok {
		if permit, ok := (*permits)[mod]; ok {
			return permit.Self
		}
	}
	return false
}

// dmain manager for handling domain validation
type domainManager struct {
	c base_domain.AppConfig
	r *http.Request
}

func NewDomainManager(c base_domain.AppConfig, r *http.Request) *domainManager {
	return &domainManager{c: c, r: r}
}

// CheckDomain ...
func (h *domainManager) CheckDomain() (coy base_domain.CoyInfo, err error) {
	//create a connection to base database
	db, close, err := base_repository.MySQL(h.c.DataBase[h.c.ActiveDB], "")
	if err != nil {
		return
	}
	//close the connection once done
	defer close()

	sub, domain, _ := SlitHost(h.r.Host)
	coyService := base_blocs.NewCoyInfo(base_repository.NewCCoyInfo(db))
	dm := domain
	if len(sub) > 0 {
		dm = fmt.Sprintf("%s.%s", sub, domain)
	}
	coys, err := coyService.Find(map[string]interface{}{"domain": dm})
	if err != nil {
		return coy, errors.New("company verification/domain mapping error")
	}
	if len(coys) == 0 && h.c.Domain == domain {
		coys, _ = coyService.Find(map[string]interface{}{"tag": sub})
		if err != nil {
			return coy, errors.New("company verification/domain mapping error")
		}
	}
	if len(coys) == 0 {
		return coy, errors.New("company verification error")
	}
	return coys[0], nil
}

// SlitHost ... simplify Host parameters
func SlitHost(Host string) (subdomain string, domain string, port string) {
	re := regexp.MustCompile(`(?m)(?:(?P<subdomain>.*)\.)?(?P<domain>(?P<name>.+)\.(?P<extention>[^\:]+))(?::(?P<port>[0-9]+))?`)
	groupMap := make(map[string]int)
	for i, v := range re.SubexpNames() {
		groupMap[v] = i
	}
	matches := re.FindAllStringSubmatch(Host, -1)
	if len(matches) > 0 {
		subdomain = matches[0][groupMap["subdomain"]]
		domain = matches[0][groupMap["domain"]]
		port = matches[0][groupMap["port"]]
	}
	return
}

// http ClientInfo ...
func ClientInfo(r *http.Request) (data map[string]interface{}) {
	data = map[string]interface{}{}
	data["ip"] = strings.Split(r.Header.Get("X-Forwarded-For"), ",")[0]
	if len(fmt.Sprint(data["ip"])) == 0 {
		data["ip"] = strings.Split(r.RemoteAddr, ":")[0]
	}
	data["agent"] = r.Header.Get("user-agent")
	return data
}
