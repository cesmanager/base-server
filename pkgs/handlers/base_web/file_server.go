package base_web

import (
	"fmt"
	"net/http"
	"strings"

	"gitlab.com/cesmanager/base-server/core/base_domain"
	"gitlab.com/cesmanager/base-server/pkgs/base_blocs/wrap_sftp"
)

type fileServer struct {
	c base_domain.AppConfig
}

func NewFileServer(c base_domain.AppConfig) *fileServer {
	return &fileServer{c: c}
}

// ServeFile ...
func (h *fileServer) Serve(w http.ResponseWriter, r *http.Request) {
	if !strings.HasPrefix(r.URL.Path, "/") { //add prefix if needed
		r.URL.Path = fmt.Sprint("/", r.URL.Path)
	}
	//reject dot path
	pathParts := strings.Split(r.URL.Path, "/")
	for _, part := range pathParts {
		if strings.HasPrefix(part, ".") {
			http.Error(w, "403 Forbidden", http.StatusForbidden)
			return
		}
	}
	utils := Utilities(h.c, r)
	if sftp := utils.GetSftp(); sftp != nil {
		dir := wrap_sftp.Dir{}
		dir = *dir.SetUp(sftp, utils.GetCustom()["path"])
		http.FileServer(dir).ServeHTTP(w, r)
		return
	}
	http.FileServer(http.Dir(utils.GetCustom()["path"])).ServeHTTP(w, r)
}

// ServeFile ...
func (h *fileServer) ServeWeb(w http.ResponseWriter, r *http.Request) {
	utils := Utilities(h.c, r)
	http.FileServer(http.Dir(utils.GetCustom()["path"])).ServeHTTP(w, r)
}
