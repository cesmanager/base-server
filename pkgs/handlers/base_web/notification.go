package base_web

import (
	"fmt"
	"net/http"
	"runtime/debug"
	"strings"

	"gitlab.com/cesmanager/base-server/core/base_domain"
	"gitlab.com/cesmanager/base-server/pkgs/base_blocs"
	"gitlab.com/cesmanager/base-server/pkgs/base_repository"
)

type notification struct {
	c base_domain.AppConfig
}

func NewNotification(c base_domain.AppConfig) *notification {
	return &notification{c: c}
}

// get profile by id
func (h *notification) Get(w http.ResponseWriter, r *http.Request) {
	resp := NewResponse(w, r)
	defer func() {
		if err := recover(); err != nil {
			panic(fmt.Sprintf("%v\n\t%v", err, string(debug.Stack())))
		}
		resp.Execute(nil)
	}()
	db, close := base_repository.MustMySQL(h.c.DataBase[h.c.ActiveDB], r.Header.Get("coy-db"))
	defer close()
	req := ParseRequest(h.c, r)
	params := req.PayLoad()
	if _, ok := params["id"]; !ok {
		resp.SetError("error", "id is required")
		return
	}
	bloc := base_blocs.NewNotification(base_repository.NewNotification(db))
	data, err := bloc.Get(fmt.Sprint(params["id"]))
	if err != nil {
		resp.SetError("error", "Sorry an error occured")
		return
	}
	resp.SetBody(data)
}

func (h *notification) List(w http.ResponseWriter, r *http.Request) {
	resp := NewResponse(w, r)
	//response callback
	defer func() {
		resp.Execute(nil)
	}()
	db, close := base_repository.MustMySQL(h.c.DataBase[h.c.ActiveDB], r.Header.Get("coy-db"))
	defer close()

	req := ParseRequest(h.c, r)
	params := req.PayLoad()
	delete(params, "count")
	delete(params, "pages")
	page := "1"
	if _, ok := params["page"]; ok {
		page = fmt.Sprint(params["page"])
		delete(params, "page")
	}
	pageSize := "1"
	if _, ok := params["page_size"]; ok {
		pageSize = fmt.Sprint(params["page_size"])
		delete(params, "page_size")
	}

	bloc := base_blocs.NewNotification(base_repository.NewNotification(db))
	list, err := bloc.List(params, req.OrderBy, page, pageSize)
	if err != nil {
		resp.SetError("error", "unable to get list")
		return
	}
	resp.SetBody(list)
}

// Create a Profile
func (h *notification) Create(w http.ResponseWriter, r *http.Request) {
	resp := NewResponse(w, r)

	defer func() {
		if err := recover(); err != nil {
			panic(fmt.Sprintf("%v\n\t%v", err, string(debug.Stack())))
		}
		resp.Execute(nil)
	}()
	db, close := base_repository.MustMySQL(h.c.DataBase[h.c.ActiveDB], r.Header.Get("coy-db"))
	defer close()

	req := ParseRequest(h.c, r)
	params := req.PayLoad()
	if _, ok := params["title"]; !ok {
		resp.SetError("title_error", "title is required")
	}
	if _, ok := params["message"]; !ok {
		resp.SetError("message_error", "message is required")
	}
	if resp.IsError() {
		return
	}
	user, ok := UserContext(r.Context())
	if !ok {
		resp.SetError("error", "unable to get user context")
		return
	}
	params["created_by_id"] = user.ProfileID

	//begin a transaction
	tx := db.Begin()
	bloc_notification := base_blocs.NewNotification(base_repository.NewNotification(tx))
	notification, err := bloc_notification.Create(params)
	if err != nil {
		resp.SetError("error", "unable create notification")
		tx.Rollback()
		return
	}
	//notify
	bloc_notified := base_blocs.NewNotified(base_repository.NewNotified(tx))
	if user_groups, ok := params["to_user_groups"]; ok {
		err = bloc_notified.NotifyUserGroup(fmt.Sprint(notification.ID), strings.Split(fmt.Sprint(user_groups), ","))
		if err != nil {
			resp.SetError("error", "unable to notify group")
			tx.Rollback()
			return
		}
	} else if profiles, ok := params["to_profiles"]; ok {
		err = bloc_notified.NotifyProfiles(fmt.Sprint(notification.ID), strings.Split(fmt.Sprint(profiles), ","))
		if err != nil {
			resp.SetError("error", "unable to notify users")
			tx.Rollback()
			return
		}
	} else if _, ok := params["to_all"]; ok {
		err = bloc_notified.NotifyAll(fmt.Sprint(notification.ID))
		if err != nil {
			resp.SetError("error", "unable to notify all")
			tx.Rollback()
			return
		}
	} else {
		resp.SetError("error", "you must provide to_user_groups or to_profiles or to_all")
		tx.Rollback()
		return
	}
	//commit transaction
	tx.Commit()
	resp.SetBody(map[string]string{"msg": "notification complete"})
}

// Update a notification
func (h *notification) Update(w http.ResponseWriter, r *http.Request) {
	resp := NewResponse(w, r)

	defer func() {
		if err := recover(); err != nil {
			panic(fmt.Sprintf("%v\n\t%v", err, string(debug.Stack())))
		}
		resp.Execute(nil)
	}()
	db, close := base_repository.MustMySQL(h.c.DataBase[h.c.ActiveDB], r.Header.Get("coy-db"))
	defer close()

	req := ParseRequest(h.c, r)
	params := req.PayLoad()
	if _, ok := params["id"]; !ok {
		resp.SetError("error", "id is required")
		return
	}
	bloc := base_blocs.NewNotification(base_repository.NewNotification(db))

	data, err := bloc.Update(fmt.Sprint(params["id"]), params)
	if err != nil {
		resp.SetError("error", "notification update error")
		return
	}
	resp.SetBody(data)
}

// Update a notification
func (h *notification) Delete(w http.ResponseWriter, r *http.Request) {
	resp := NewResponse(w, r)

	defer func() {
		if err := recover(); err != nil {
			panic(fmt.Sprintf("%v\n\t%v", err, string(debug.Stack())))
		}
		resp.Execute(nil)
	}()
	db, close := base_repository.MustMySQL(h.c.DataBase[h.c.ActiveDB], r.Header.Get("coy-db"))
	defer close()

	req := ParseRequest(h.c, r)
	params := req.PayLoad()
	if _, ok := params["id"]; !ok {
		resp.SetError("error", "id is required")
		return
	}
	bloc := base_blocs.NewNotification(base_repository.NewNotification(db))

	err := bloc.Delete(strings.Split(fmt.Sprint(params["id"]), ","))
	if err != nil {
		resp.SetError("error", "delete failed")
		return
	}
	resp.SetBody(map[string]string{"msg": "notification deleted"})
}
