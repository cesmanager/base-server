package base_web

import (
	"encoding/json"
	"fmt"
	"mime/multipart"
	"net/http"
	"runtime/debug"
	"strconv"
	"strings"

	"gitlab.com/cesmanager/base-server/core/base_domain"
)

type fileRouter struct {
	c base_domain.AppConfig
}

func NewFileRouter(c base_domain.AppConfig) *fileRouter {
	return &fileRouter{c: c}
}

func (h *fileRouter) Upload(w http.ResponseWriter, r *http.Request) {
	resp := NewResponse(w, r)

	defer func() {
		if err := recover(); err != nil {
			panic(fmt.Sprintf("%v\n\t%v", err, string(debug.Stack())))
		}
		resp.Execute(nil)
	}()

	params := ParseRequest(h.c, r).PayLoad()
	mod := fmt.Sprintf("%v", params["mod"])
	group := fmt.Sprintf("%v", params["group"])
	prefix := fmt.Sprintf("%v", params["prefix"])
	recID, _ := strconv.Atoi(fmt.Sprintf("%v", params["rec_id"]))
	if len(mod) == 0 {
		resp.SetError("error", "mod is required")
		return
	}
	fhds := r.MultipartForm.File
	utils := Utilities(h.c, r)
	files := map[string][]string{
		"pix": {},
		"vid": {},
		"doc": {},
	}

	for _, fHd_group := range fhds {
		for _, fhd := range fHd_group {
			var file multipart.File
			file, err := fhd.Open()
			if nil != err {
				w.WriteHeader(http.StatusInternalServerError)
				resp.SetError("error", err.Error())
				return
			}
			switch mod {
			case "explorer":
				resp.SetError("error", "explorer under development")
				return
			default:
				if recID == 0 {
					resp.SetError("error", "rec_id is required")
					return
				}
				if len(group) == 0 {
					resp.SetError("error", "group is required")
					return
				}
				pt := ""
				switch group {
				case "pix":
					pt = utils.PixPath(mod, recID)
				case "vid":
					pt = utils.VidPath(mod, recID)
				case "doc":
					pt = utils.DocPath(mod, recID)
				}
				path, err := utils.UploadFile(file, pt, fmt.Sprint(prefix, fhd.Filename))
				url := strings.Replace(path, utils.GetPath(), "/static", 1)
				if err == nil {
					files[group] = append(files[group], url)
				}
			}
		}
	}
	resp.SetBody(map[string]interface{}{
		"files": files,
	})
}

func (h *fileRouter) RecFiles(w http.ResponseWriter, r *http.Request) {
	params := ParseRequest(h.c, r).PayLoad()
	mod := fmt.Sprintf("%v", params["mod"])
	utils := Utilities(h.c, r)
	// group := fmt.Sprintf("%v", data["group"])
	recID, _ := strconv.Atoi(fmt.Sprintf("%v", params["rec_id"]))
	if recID == 0 || len(mod) == 0 {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode("rec_id and mod is required")
		return
	}
	files := utils.RecFiles(mod, recID)
	json.NewEncoder(w).Encode(files)
}
