package base_web

import (
	"fmt"
	"net/http"
	"runtime/debug"

	"github.com/gorilla/mux"
	"gitlab.com/cesmanager/base-server/core/base_domain"
	"gitlab.com/cesmanager/base-server/pkgs/base_blocs"
	"gitlab.com/cesmanager/base-server/pkgs/base_repository"
)

type Account struct {
	c base_domain.AppConfig
}

func NewAccount(c base_domain.AppConfig) *Account {
	return &Account{c: c}
}

// get account by id
func (h *Account) Get(w http.ResponseWriter, r *http.Request) {
	resp := NewResponse(w, r)

	defer func() {
		if err := recover(); err != nil {
			panic(fmt.Sprintf("%v\n\t%v", err, string(debug.Stack())))
		}
		resp.Execute(nil)
	}()
	db, close := base_repository.MustMySQL(h.c.DataBase[h.c.ActiveDB], r.Header.Get("coy-db"))
	defer close()

	vars := mux.Vars(r)
	accountServ := base_blocs.NewAccount(base_repository.NewAccount(db))

	//check if id is provided
	id, ok := vars["id"]
	if !ok {
		//return all accounts related to the logged in user
		//get the user data.
		user_id := r.Header.Get("user_id")
		userServ := base_blocs.NewUser(base_repository.NewUser(db))
		user, err := userServ.Get(user_id)
		if err != nil {
			resp.SetError("error", err.Error())
			return
		}
		accounts, err := accountServ.Find(map[string]interface{}{"profile_id": user.ProfileID})
		if err != nil {
			resp.SetError("error", err.Error())
			return
		}
		resp.SetBody(accounts)
		return
	}
	account, err := accountServ.Get(id)
	if err != nil {
		resp.SetError("error", err.Error())
		return
	}
	resp.SetBody(account)
}

// Create a Account
func (h *Account) Create(w http.ResponseWriter, r *http.Request) {
	resp := NewResponse(w, r)

	defer func() {
		if err := recover(); err != nil {
			panic(fmt.Sprintf("%v\n\t%v", err, string(debug.Stack())))
		}
		resp.Execute(nil)
	}()
	db, close := base_repository.MustMySQL(h.c.DataBase[h.c.ActiveDB], r.Header.Get("coy-db"))
	defer close()

	params := ParseRequest(h.c, r).PayLoad()

	//do basic validation
	fails := h.VetParams(params)
	if len(fails) > 0 {
		for k, v := range fails {
			resp.SetError(k, v)
		}
		return
	}
	accountServ := base_blocs.NewAccount(base_repository.NewAccount(db))

	account, err := accountServ.Create(params)
	if err != nil {
		resp.SetCode(http.StatusBadRequest)
		resp.SetError("error", "account creation failed")
		return
	}
	resp.SetBody(account)
}

// Create a Account
func (h *Account) Update(w http.ResponseWriter, r *http.Request) {
	resp := NewResponse(w, r)

	defer func() {
		if err := recover(); err != nil {
			panic(fmt.Sprintf("%v\n\t%v", err, string(debug.Stack())))
		}
		resp.Execute(nil)
	}()
	db, close := base_repository.MustMySQL(h.c.DataBase[h.c.ActiveDB], r.Header.Get("coy-db"))
	defer close()

	params := ParseRequest(h.c, r).PayLoad()
	if _, ok := params["id"]; !ok {
		resp.SetError("error", "id is required")
		return
	}
	accountServ := base_blocs.NewAccount(base_repository.NewAccount(db))

	account, err := accountServ.Update(fmt.Sprint(params["id"]), params)
	if err != nil {
		resp.SetCode(http.StatusBadRequest)
		resp.SetError("error", "account update failed")
		return
	}
	resp.SetBody(account)
}

func (h *Account) List(w http.ResponseWriter, r *http.Request) {
	resp := NewResponse(w, r)
	//response callback
	defer func() {
		resp.Execute(nil)
	}()
	db, close := base_repository.MustMySQL(h.c.DataBase[h.c.ActiveDB], r.Header.Get("coy-db"))
	defer close()

	req := ParseRequest(h.c, r)
	params := req.PayLoad()
	//always delete count and pages so its no included in the query
	delete(params, "count")
	delete(params, "pages")
	page := "1"
	if _, ok := params["page"]; ok {
		page = fmt.Sprint(params["page"])
		delete(params, "page")
	}
	pageSize := "1"
	if _, ok := params["page_size"]; ok {
		pageSize = fmt.Sprint(params["page_size"])
		delete(params, "page_size")
	}

	accServ := base_blocs.NewAccount(base_repository.NewAccount(db))
	list, err := accServ.List(params, req.OrderBy, page, pageSize)
	if err != nil {
		resp.SetError("error", err.Error())
		return
	}
	resp.SetBody(list)
}

// get account by request parameter
func (h *Account) Find(w http.ResponseWriter, r *http.Request) {
	resp := NewResponse(w, r)

	defer func() {
		if err := recover(); err != nil {
			panic(fmt.Sprintf("%v\n\t%v", err, string(debug.Stack())))
		}
		resp.Execute(nil)
	}()
	db, close := base_repository.MustMySQL(h.c.DataBase[h.c.ActiveDB], r.Header.Get("coy-db"))
	defer close()

	accountServ := base_blocs.NewAccount(base_repository.NewAccount(db))

	params := ParseRequest(h.c, r).PayLoad()
	accounts, err := accountServ.Find(params)
	if err != nil {
		resp.SetError("error", err.Error())
		return
	}
	resp.SetBody(accounts)
}

func (h *Account) VetParams(params map[string]interface{}) map[string]string {
	msgs := map[string]string{}
	if _, ok := params["title"]; !ok {
		msgs["title_error"] = "title is required"
	}
	if _, ok := params["profile_id"]; !ok {
		msgs["profile_id_error"] = "Account owner is required"
	}
	if _, ok := params["currency_id"]; !ok {
		msgs["currency_id_error"] = "Currency is required"
	}
	return msgs
}
