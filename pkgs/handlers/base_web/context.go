package base_web

import (
	"context"
	"encoding/gob"
	"net/http"
	"strconv"

	"github.com/gorilla/sessions"
	"github.com/pkg/sftp"
	"gitlab.com/cesmanager/base-server/core/base_domain"
)

func init() {
	gob.Register(&base_domain.User{})
	gob.Register(&map[string]base_domain.UserGroupPermit{})
}

type contextKey string

var userKey contextKey = "session"
var configKey contextKey = "config"
var sessionKey contextKey = "session"
var permitsKey contextKey = "permits"
var sftpKey contextKey = "sftp"

// config context
func NewConfigContext(ctx context.Context, c *base_domain.AppConfig) context.Context {
	return context.WithValue(ctx, configKey, c)
}
func ConfigContext(ctx context.Context) (v *base_domain.AppConfig, ok bool) {
	v, ok = ctx.Value(configKey).(*base_domain.AppConfig)
	return
}

// permits context
func NewPermitsContext(ctx context.Context, c *map[string]base_domain.UserGroupPermit) context.Context {
	return context.WithValue(ctx, permitsKey, c)
}
func PermitsContext(ctx context.Context) (v *map[string]base_domain.UserGroupPermit, ok bool) {
	v, ok = ctx.Value(permitsKey).(*map[string]base_domain.UserGroupPermit)
	return
}

// user context
func NewUserContext(ctx context.Context, c *base_domain.User) context.Context {
	return context.WithValue(ctx, userKey, c)
}
func UserContext(ctx context.Context) (v *base_domain.User, ok bool) {
	v, ok = ctx.Value(userKey).(*base_domain.User)
	return
}

// session context
func NewSessionContext(ctx context.Context, c base_domain.AppConfig, r *http.Request) context.Context {
	utils := Utilities(c, r)
	store := NewFilesystemStore(utils.GetCustom()["session"], utils, []byte(c.SessionKey))
	expire, _ := strconv.Atoi(c.SessionExpire)
	store.Options = &sessions.Options{
		Path:     "/",
		HttpOnly: true,
		MaxAge:   expire,
	}
	store.MaxLength(4096 * 2)
	return context.WithValue(ctx, sessionKey, store)
}

func SessionContext(ctx context.Context) (v *SessionFileStore, ok bool) {
	v, ok = ctx.Value(sessionKey).(*SessionFileStore)
	return
}

// SFTP context
func NewSftpContext(ctx context.Context, c base_domain.AppConfig) (context.Context, func()) {
	utils := Utilities(c, nil)
	sftp, close := utils.ConnectSFTP(c.Sftp)
	return context.WithValue(ctx, sftpKey, sftp), close
}

func SftpContext(ctx context.Context) (v *sftp.Client, ok bool) {
	v, ok = ctx.Value(sftpKey).(*sftp.Client)
	return
}
