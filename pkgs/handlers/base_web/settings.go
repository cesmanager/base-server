package base_web

import (
	"fmt"
	"net/http"
	"path/filepath"
	"runtime/debug"

	"gitlab.com/cesmanager/base-server/core/base_domain"
	"gitlab.com/cesmanager/base-server/pkgs/base_blocs"
	"gitlab.com/cesmanager/base-server/pkgs/base_repository"
)

type Settings struct {
	c base_domain.AppConfig
}

func NewSettings(c base_domain.AppConfig) *Settings {
	return &Settings{c: c}
}

// get profile by id
func (h *Settings) Get(w http.ResponseWriter, r *http.Request) {
	resp := NewResponse(w, r)
	//response callback

	defer func() {
		if err := recover(); err != nil {
			panic(fmt.Sprintf("%v\n\t%v", err, string(debug.Stack())))
		}
		resp.Execute(nil)
	}()
	db, close := base_repository.MustMySQL(h.c.DataBase[h.c.ActiveDB], r.Header.Get("coy-db"))
	defer close()

	settingsServ := base_blocs.NewSettings(base_repository.NewSettings(db))
	settings, err := settingsServ.Get()
	if err != nil {
		resp.SetError("error", err.Error())
		return
	}
	// utils := Utilities(h.c, r)
	// //check if the record has a photo
	// logo := utils.GetPhoto(settingsServ.Mod, int(settings.ID))
	// if len(logo) > 0 {
	// 	settings.Logo = logo[0]
	// }
	resp.SetBody(settings)
}

func (h *Settings) Update(w http.ResponseWriter, r *http.Request) {
	resp := NewResponse(w, r)
	//response callback
	defer func() {
		if err := recover(); err != nil {
			panic(fmt.Sprintf("%v\n\t%v", err, string(debug.Stack())))
		}
		resp.Execute(nil)
	}()
	db, close := base_repository.MustMySQL(h.c.DataBase[h.c.ActiveDB], r.Header.Get("coy-db"))
	defer close()

	tx := db.Begin()
	req := ParseRequest(h.c, r)
	params := req.PayLoad()

	settingsServ := base_blocs.NewSettings(base_repository.NewSettings(tx))
	settings, err := settingsServ.Update(params)
	if err != nil {
		resp.SetError("error", err.Error())
		tx.Rollback()
		return
	}
	//upload photo
	utils := Utilities(h.c, r)
	if r.Form.Has("logo") {
		pt := utils.PixPath(*settingsServ.Mod, 1)
		//remove the previous photo if any
		if files, ok := utils.ReadDir(pt); ok == nil && len(files) > 0 {
			for _, file := range files {
				utils.Remove(pt + string(filepath.Separator) + file.Name())
			}
		}
		url, err := req.UploadFile("pix", *settingsServ.Mod, 1, "logo", "logo")
		if len(url) != 0 {
			settings.Photo = url
		}
		if err == nil {
			err := settingsServ.UpdateByInstance(&settings)
			if err != nil {
				resp.SetError("error", err.Error())
				tx.Rollback()
			}
			return
		}
	}
	tx.Commit()
	resp.SetBody(settings)
}
