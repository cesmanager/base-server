package base_web

import (
	"fmt"
	"net/http"
	"runtime/debug"

	"gitlab.com/cesmanager/base-server/core/base_domain"
	"gitlab.com/cesmanager/base-server/pkgs/base_blocs"
	"gitlab.com/cesmanager/base-server/pkgs/base_repository"
)

type mail struct {
	c base_domain.AppConfig
}

func NewMail(c base_domain.AppConfig) *mail {
	return &mail{c: c}
}

func (h *mail) Test(w http.ResponseWriter, r *http.Request) {
	resp := NewResponse(w, r)
	//response callback
	defer func() {
		if err := recover(); err != nil {
			panic(fmt.Sprintf("%v\n\t%v", err, string(debug.Stack())))
		}
		resp.Execute(nil)
	}()
	db, close := base_repository.MustMySQL(h.c.DataBase[h.c.ActiveDB], r.Header.Get("coy-db"))
	defer close()

	//get current user
	userServ := base_blocs.NewUser(base_repository.NewUser(db))
	u, err := userServ.Get(r.Header.Get("User_id"))
	if err != nil {
		resp.SetError("smtp_error", "unable to identify user")
		return
	}
	//get settings
	settingService := base_blocs.NewSettings(base_repository.NewSettings(db))
	settings, err := settingService.Get()
	if err != nil {
		resp.SetError("smtp_error", err.Error())
		return
	}
	mailclient := base_blocs.NewSMTP(&settings)
	mList := []string{}
	if u.Profile.Email != nil {
		mList = append(mList, *u.Profile.Email)
	}
	if u.Profile.AltEmail != nil {
		mList = append(mList, *u.Profile.AltEmail)
	}
	err = mailclient.Reciepents(mList).SetMsg("Test Email", "this email confirms that your smtp settings are configured correctly").Send()
	if err != nil {
		resp.SetError("smtp_error", err.Error())
		return
	}

	resp.SetBody(map[string]string{"smtp_success": "your smtp settings are configured correctly"})
}
