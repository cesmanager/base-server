package base_web

import (
	"encoding/base32"
	"fmt"
	"net/http"
	"os"
	"strings"
	"sync"

	"github.com/gorilla/securecookie"
	"github.com/gorilla/sessions"
	"gitlab.com/cesmanager/base-server/pkgs/base_blocs"
)

var fileMutex sync.RWMutex

type SessionFileStore struct {
	sessions.FilesystemStore
	path  string
	utils *base_blocs.Utilities
}

// See CookieStore.Get().
func (s *SessionFileStore) Get(r *http.Request, name string) (*sessions.Session, error) {
	return sessions.GetRegistry(r).Get(s, name)
}

// New returns a session for the given name without adding it to the registry.
//
// See CookieStore.New().
func (s *SessionFileStore) New(r *http.Request, name string) (*sessions.Session, error) {
	session := sessions.NewSession(s, name)
	opts := *s.Options
	session.Options = &opts
	session.IsNew = true
	var err error
	if c, errCookie := r.Cookie(name); errCookie == nil {
		err = securecookie.DecodeMulti(name, c.Value, &session.ID, s.Codecs...)
		if err == nil {
			err = s.load(session)
			if err == nil {
				session.IsNew = false
			}
		}
	}
	return session, err
}

// Save adds a single session to the response.
//
// If the Options.MaxAge of the session is <= 0 then the session file will be
// deleted from the store path. With this process it enforces the properly
// session cookie handling so no need to trust in the cookie management in the
// web browser.
func (s *SessionFileStore) Save(r *http.Request, w http.ResponseWriter,
	session *sessions.Session) error {
	// Delete if max-age is <= 0
	if session.Options.MaxAge <= 0 {
		if err := s.erase(session); err != nil {
			return err
		}
		http.SetCookie(w, sessions.NewCookie(session.Name(), "", session.Options))
		return nil
	}

	if session.ID == "" {
		// Because the ID is used in the filename, encode it to
		// use alphanumeric characters only.
		session.ID = strings.TrimRight(
			base32.StdEncoding.EncodeToString(
				securecookie.GenerateRandomKey(32)), "=")
	}
	if err := s.save(session); err != nil {
		return err
	}
	encoded, err := securecookie.EncodeMulti(session.Name(), session.ID,
		s.Codecs...)
	if err != nil {
		return err
	}
	http.SetCookie(w, sessions.NewCookie(session.Name(), encoded, session.Options))
	return nil
}

// save writes encoded session.Values to a file.
func (s *SessionFileStore) save(session *sessions.Session) error {
	encoded, err := securecookie.EncodeMulti(session.Name(), session.Values,
		s.Codecs...)
	if err != nil {
		return err
	}
	filename := fmt.Sprint(s.path, "/", "session_"+session.ID)
	fileMutex.Lock()
	defer fileMutex.Unlock()
	return s.utils.WriteFile(filename, []byte(encoded), 0600)
}

// load reads a file and decodes its content into session.Values.
func (s *SessionFileStore) load(session *sessions.Session) error {
	filename := fmt.Sprint(s.path, "/", "session_"+session.ID)
	fileMutex.RLock()
	defer fileMutex.RUnlock()
	fdata, err := s.utils.ReadFile(filename)
	if err != nil {
		return err
	}
	if err = securecookie.DecodeMulti(session.Name(), string(fdata),
		&session.Values, s.Codecs...); err != nil {
		return err
	}
	return nil
}

// delete session file
func (s *SessionFileStore) erase(session *sessions.Session) error {
	filename := fmt.Sprint(s.path, "/", "session_"+session.ID)

	fileMutex.RLock()
	defer fileMutex.RUnlock()

	err := s.utils.Remove(filename)
	return err
}

func NewFilesystemStore(path string, utils *base_blocs.Utilities, keyPairs ...[]byte) *SessionFileStore {
	if path == "" {
		path = os.TempDir()
	}
	fs := &SessionFileStore{
		FilesystemStore: sessions.FilesystemStore{
			Codecs: securecookie.CodecsFromPairs(keyPairs...),
			Options: &sessions.Options{
				Path:   "/",
				MaxAge: 86400 * 30,
			},
		},
		path:  path,
		utils: utils,
	}

	fs.MaxAge(fs.Options.MaxAge)
	return fs
}
