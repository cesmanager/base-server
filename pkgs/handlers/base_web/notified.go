package base_web

import (
	"fmt"
	"net/http"
	"runtime/debug"

	"gitlab.com/cesmanager/base-server/core/base_domain"
	"gitlab.com/cesmanager/base-server/pkgs/base_blocs"
	"gitlab.com/cesmanager/base-server/pkgs/base_repository"
)

type notified struct {
	c base_domain.AppConfig
}

func NewNotified(c base_domain.AppConfig) *notified {
	return &notified{c: c}
}

// get profile by id
func (h *notified) Read(w http.ResponseWriter, r *http.Request) {
	resp := NewResponse(w, r)
	defer func() {
		if err := recover(); err != nil {
			panic(fmt.Sprintf("%v\n\t%v", err, string(debug.Stack())))
		}
		resp.Execute(nil)
	}()
	db, close := base_repository.MustMySQL(h.c.DataBase[h.c.ActiveDB], r.Header.Get("coy-db"))
	defer close()
	params := ParseRequest(h.c, r).PayLoad()
	if _, ok := params["notification_id"]; !ok {
		resp.SetError("error", "notification_id is required")
		return
	}
	if _, ok := params["profile_id"]; !ok {
		resp.SetError("error", "profile_id is required")
		return
	}
	bloc := base_blocs.NewNotified(base_repository.NewNotified(db))
	data, err := bloc.Read(fmt.Sprint(params["notification_id"]), fmt.Sprint(params["profile_id"]))
	if err != nil {
		resp.SetError("error", "Sorry an error occured")
		return
	}
	resp.SetBody(data)
}

func (h *notified) List(w http.ResponseWriter, r *http.Request) {
	resp := NewResponse(w, r)
	//response callback
	defer func() {
		resp.Execute(nil)
	}()
	db, close := base_repository.MustMySQL(h.c.DataBase[h.c.ActiveDB], r.Header.Get("coy-db"))
	defer close()

	req := ParseRequest(h.c, r)
	params := req.PayLoad()
	delete(params, "count")
	delete(params, "pages")
	page := "1"
	if _, ok := params["page"]; ok {
		page = fmt.Sprint(params["page"])
		delete(params, "page")
	}
	pageSize := "1"
	if _, ok := params["page_size"]; ok {
		pageSize = fmt.Sprint(params["page_size"])
		delete(params, "page_size")
	}

	bloc := base_blocs.NewNotified(base_repository.NewNotified(db))
	list, err := bloc.List(params, req.OrderBy, page, pageSize)
	if err != nil {
		resp.SetError("error", "unable to get list")
		return
	}
	resp.SetBody(list)
}

// Update a notification
func (h *notified) Delete(w http.ResponseWriter, r *http.Request) {
	resp := NewResponse(w, r)

	defer func() {
		if err := recover(); err != nil {
			panic(fmt.Sprintf("%v\n\t%v", err, string(debug.Stack())))
		}
		resp.Execute(nil)
	}()
	db, close := base_repository.MustMySQL(h.c.DataBase[h.c.ActiveDB], r.Header.Get("coy-db"))
	defer close()

	params := ParseRequest(h.c, r).PayLoad()
	if _, ok := params["notification_id"]; !ok {
		resp.SetError("error", "notification_id is required")
		return
	}
	if _, ok := params["profile_id"]; !ok {
		resp.SetError("error", "profile_id is required")
		return
	}
	bloc := base_blocs.NewNotified(base_repository.NewNotified(db))
	err := bloc.Delete(fmt.Sprint(params["notification_id"]), fmt.Sprint(params["profile_id"]))
	if err != nil {
		resp.SetError("error", "delete failed")
		return
	}
	resp.SetBody(map[string]string{"msg": "notification deleted"})
}
