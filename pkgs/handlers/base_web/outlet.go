package base_web

import (
	"fmt"
	"net/http"
	"runtime/debug"

	"github.com/gorilla/mux"
	"gitlab.com/cesmanager/base-server/core/base_domain"
	"gitlab.com/cesmanager/base-server/pkgs/base_blocs"
	"gitlab.com/cesmanager/base-server/pkgs/base_repository"
)

type outlet struct {
	c base_domain.AppConfig
}

func NewOutlet(c base_domain.AppConfig) *outlet {
	return &outlet{c: c}
}

// get currency by id
func (h *outlet) Get(w http.ResponseWriter, r *http.Request) {
	resp := NewResponse(w, r)

	defer func() {
		if err := recover(); err != nil {
			panic(fmt.Sprintf("%v\n\t%v", err, string(debug.Stack())))
		}
		resp.Execute(nil)
	}()
	db, close := base_repository.MustMySQL(h.c.DataBase[h.c.ActiveDB], r.Header.Get("coy-db"))
	defer close()
	vars := mux.Vars(r)

	serv := base_blocs.NewOutlet(base_repository.NewOutlet(db))

	//check if id is provided
	id, ok := vars["id"]
	if !ok {
		resp.SetError("error", "id is required")
		return
	}
	data, err := serv.Get(id)

	if err != nil {
		resp.SetError("error", err.Error())
		return
	}
	resp.SetBody(data)
}

func (h *outlet) List(w http.ResponseWriter, r *http.Request) {
	resp := NewResponse(w, r)
	//response callback
	defer func() {
		resp.Execute(nil)
	}()
	db, close := base_repository.MustMySQL(h.c.DataBase[h.c.ActiveDB], r.Header.Get("coy-db"))
	defer close()

	req := ParseRequest(h.c, r)
	params := req.PayLoad()
	delete(params, "count")
	delete(params, "pages")
	page := "1"
	if _, ok := params["page"]; ok {
		page = fmt.Sprint(params["page"])
		delete(params, "page")
	}
	pageSize := "1"
	if _, ok := params["page_size"]; ok {
		pageSize = fmt.Sprint(params["page_size"])
		delete(params, "page_size")
	}

	serv := base_blocs.NewOutlet(base_repository.NewOutlet(db))
	data, err := serv.List(params, req.OrderBy, page, pageSize)
	if err != nil {
		resp.SetError("error", err.Error())
		return
	}
	resp.SetBody(data)
}

// get currency by id
func (h *outlet) Find(w http.ResponseWriter, r *http.Request) {
	resp := NewResponse(w, r)

	defer func() {
		if err := recover(); err != nil {
			panic(fmt.Sprintf("%v\n\t%v", err, string(debug.Stack())))
		}
		resp.Execute(nil)
	}()
	db, close := base_repository.MustMySQL(h.c.DataBase[h.c.ActiveDB], r.Header.Get("coy-db"))
	defer close()

	serv := base_blocs.NewOutlet(base_repository.NewOutlet(db))

	params := ParseRequest(h.c, r).PayLoad()
	data, err := serv.Find(params)
	if err != nil {
		resp.SetError("error", err.Error())
		return
	}
	resp.SetBody(data)
}

// Create a outlet
func (h *outlet) Create(w http.ResponseWriter, r *http.Request) {
	resp := NewResponse(w, r)

	defer func() {
		if err := recover(); err != nil {
			panic(fmt.Sprintf("%v\n\t%v", err, string(debug.Stack())))
		}
		resp.Execute(nil)
	}()
	db, close := base_repository.MustMySQL(h.c.DataBase[h.c.ActiveDB], r.Header.Get("coy-db"))
	defer close()

	params := ParseRequest(h.c, r).PayLoad()

	//do basic validation
	fails := h.vetParams(params)
	if len(fails) > 0 {
		for k, v := range fails {
			resp.SetError(k, v)
		}
		return
	}
	serv := base_blocs.NewOutlet(base_repository.NewOutlet(db))

	data, err := serv.Create(params)
	if err != nil {
		resp.SetCode(http.StatusBadRequest)
		resp.SetError("error", "outlet creation failed")
		return
	}
	resp.SetBody(data)
}

// Create a outlet
func (h *outlet) Update(w http.ResponseWriter, r *http.Request) {
	resp := NewResponse(w, r)

	defer func() {
		if err := recover(); err != nil {
			panic(fmt.Sprintf("%v\n\t%v", err, string(debug.Stack())))
		}
		resp.Execute(nil)
	}()
	db, close := base_repository.MustMySQL(h.c.DataBase[h.c.ActiveDB], r.Header.Get("coy-db"))
	defer close()

	params := ParseRequest(h.c, r).PayLoad()
	if _, ok := params["id"]; !ok {
		resp.SetError("error", "id is required")
		return
	}
	//do basic validation
	fails := h.vetParams(params)
	if len(fails) > 0 {
		for k, v := range fails {
			resp.SetError(k, v)
		}
		return
	}
	serv := base_blocs.NewOutlet(base_repository.NewOutlet(db))
	data, err := serv.Update(fmt.Sprint(params["id"]), params)
	if err != nil {
		resp.SetCode(http.StatusBadRequest)
		resp.SetError("error", "outlet update failed")
		return
	}
	resp.SetBody(data)
}

func (h *outlet) vetParams(params map[string]interface{}) map[string]string {
	var respErr = map[string]string{}
	if _, ok := params["title"]; !ok {
		respErr["title_error"] = "title is required"
	}
	if _, ok := params["profile_id"]; !ok {
		respErr["profile_id_error"] = "profile is required"
	}
	if _, ok := params["currency_id"]; !ok {
		respErr["currency_id_error"] = "currency_id is required"
	}
	return respErr
}
