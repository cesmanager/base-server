package base_web

import (
	"fmt"
	"net/http"
	"runtime/debug"

	"github.com/gorilla/mux"
	"gitlab.com/cesmanager/base-server/core/base_domain"
	"gitlab.com/cesmanager/base-server/pkgs/base_blocs"
	"gitlab.com/cesmanager/base-server/pkgs/base_repository"
	"gorm.io/gorm"
)

type Credit struct {
	c base_domain.AppConfig
}

func NewCredit(c base_domain.AppConfig) *Credit {
	return &Credit{c: c}
}

// get credit record by id
func (h *Credit) Get(w http.ResponseWriter, r *http.Request) {
	resp := NewResponse(w, r)

	defer func() {
		if err := recover(); err != nil {
			panic(fmt.Sprintf("%v\n\t%v", err, string(debug.Stack())))
		}
		resp.Execute(nil)
	}()
	db, close := base_repository.MustMySQL(h.c.DataBase[h.c.ActiveDB], r.Header.Get("coy-db"))
	defer close()
	vars := mux.Vars(r)

	creditServ := base_blocs.NewCredit(base_repository.NewCredit(db))

	//check if id is provided
	id, ok := vars["id"]
	if !ok {
		resp.SetError("error", "credit id is required")
		return
	}
	credit, err := creditServ.Get(id)

	if err != nil {
		resp.SetError("error", "Sorry an error occured")
		return
	}
	resp.SetBody(credit)
}

// Find credit record by request parameter
func (h *Credit) Find(w http.ResponseWriter, r *http.Request) {
	resp := NewResponse(w, r)
	defer func() {
		if err := recover(); err != nil {
			panic(fmt.Sprintf("%v\n\t%v", err, string(debug.Stack())))
		}
		resp.Execute(nil)
	}()
	db, close := base_repository.MustMySQL(h.c.DataBase[h.c.ActiveDB], r.Header.Get("coy-db"))
	defer close()

	creditServ := base_blocs.NewCredit(base_repository.NewCredit(db))

	params := ParseRequest(h.c, r).PayLoad()
	credits, err := creditServ.Find(params)
	if err != nil {
		resp.SetError("error", err.Error())
		return
	}
	resp.SetBody(credits)
}

// Create a Credit
func (h *Credit) Create(w http.ResponseWriter, r *http.Request) {
	resp := NewResponse(w, r)

	defer func() {
		if err := recover(); err != nil {
			panic(fmt.Sprintf("%v\n\t%v", err, string(debug.Stack())))
		}
		resp.Execute(nil)
	}()
	db, close := base_repository.MustMySQL(h.c.DataBase[h.c.ActiveDB], r.Header.Get("coy-db"))
	defer close()

	params := ParseRequest(h.c, r).PayLoad()

	//do basic validation
	fails := h.VetParams(params, []string{"title", "account_id", "amount", "creator_id", "ip", "creditor_id"})
	if len(fails) > 0 {
		for k, v := range fails {
			resp.SetError(k, v)
		}
		return
	}
	creditServ := base_blocs.NewCredit(base_repository.NewCredit(db))

	credit, err := creditServ.Create(params, base_blocs.NewAccount(base_repository.NewAccount(db)))
	if err != nil {
		resp.SetError("error", err.Error())
		return
	}
	resp.SetBody(credit)
}

// Create a Credit
func (h *Credit) Post(w http.ResponseWriter, r *http.Request) {
	resp := NewResponse(w, r)

	defer func() {
		if err := recover(); err != nil {
			panic(fmt.Sprintf("%v\n\t%v", err, string(debug.Stack())))
		}
		resp.Execute(nil)
	}()
	db, close := base_repository.MustMySQL(h.c.DataBase[h.c.ActiveDB], r.Header.Get("coy-db"))
	defer close()

	params := ParseRequest(h.c, r).PayLoad()
	if _, ok := params["id"]; !ok {
		resp.SetError("id_error", "id is required")
		return
	}
	//get the user data.
	user_id := r.Header.Get("user_id")
	userServ := base_blocs.NewUser(base_repository.NewUser(db))
	user, err := userServ.Get(user_id)
	if err != nil {
		resp.SetError("error", "unable to identify poster")
		return
	}
	//post the credit in a transaction
	db.Transaction(func(tx *gorm.DB) error {
		creditServ := base_blocs.NewCredit(base_repository.NewCredit(tx))
		credit, err := creditServ.Post(fmt.Sprint(params["id"]), fmt.Sprint(user.ProfileID), base_blocs.NewAccount(base_repository.NewAccount(tx)))
		if err != nil {
			resp.SetError("error", err.Error())
			tx.Rollback()
			return err
		}
		resp.SetBody(credit)
		return nil
	})
}

func (h *Credit) VetParams(params map[string]interface{}, required []string) map[string]string {
	msgs := map[string]string{}
	for _, r := range required {
		switch r {
		case "id":
			if _, ok := params["id"]; !ok {
				msgs["id_error"] = "id is required"
			}
		case "title":
			if _, ok := params["title"]; !ok {
				msgs["title_error"] = "title is required"
			}
		case "account_id":
			if _, ok := params["account_id"]; ok {
				msgs["account_id_error"] = "account_id is required"
			}
		case "amount":
			if _, ok := params["amount"]; ok {
				msgs["amount_error"] = "amount is required"
			}
		case "creator_id":
			if _, ok := params["creator_id"]; ok {
				msgs["creator_id_error"] = "creator_id is required"
			}
		case "ip":
			if _, ok := params["ip"]; ok {
				msgs["ip_error"] = "ip is required"
			}
		case "creditor_id":
			if _, ok := params["creditor_id"]; ok {
				msgs["creditor_id_error"] = "creditor_id is required"
			}
		}
	}
	return msgs
}
