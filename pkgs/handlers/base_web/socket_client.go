package base_web

import (
	"net/http"
	"time"

	"gitlab.com/cesmanager/base-server/core/base_domain"
	"gitlab.com/cesmanager/base-server/pkgs/base_blocs"

	"github.com/gorilla/websocket"
	uuid "github.com/satori/go.uuid"
)

const (
	// Time allowed to write a message to the peer.
	writeWait = 10 * time.Second

	// Time allowed to read the next pong message from the peer.
	pongWait = 60 * time.Second

	// Send pings to peer with this period. Must be less than pongWait.
	pingPeriod = (pongWait * 9) / 10

	// Maximum message size allowed from peer.
	maxMessageSize = 4096
)

//message struct/format
// type message struct {
// 	CoyID   int
// 	Mod     string
// 	Content interface{}
// }

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
}

// Client is a middleman between the websocket connection and the hub.
type Client struct {
	hub    *Hub
	id     string          //unique id for every client
	CoyTag string          //company tag
	Tag    string          //connection tag used for grouping connection
	socket *websocket.Conn // The websocket connection.
	send   chan []byte     // Buffered channel of outbound messages.
}

// readPump pumps messages from the websocket connection to the hub.
//
// The application runs readPump in a per-connection goroutine. The application
// ensures that there is at most one reader on a connection by executing all
// reads from this goroutine.
func (c *Client) readPump(utils *base_blocs.Utilities) {
	defer func() {
		c.hub.unregister <- c
		c.socket.Close()
	}()
	c.socket.SetReadLimit(maxMessageSize)
	c.socket.SetReadDeadline(time.Now().Add(pongWait))
	c.socket.SetPongHandler(func(string) error { c.socket.SetReadDeadline(time.Now().Add(pongWait)); return nil })
	for {
		_, msg, err := c.socket.ReadMessage()
		if err != nil {
			utils.Log(err, base_blocs.RunTimeError)
			break
		}
		//to set mode id
		if len(c.Tag) == 0 {
			c.Tag = string(msg)
			continue
		}

		c.hub.broadcast <- broadcast{
			msg:    msg,
			client: c,
		}
	}
}

// writePump pumps messages from the hub to the websocket connection.
//
// A goroutine running writePump is started for each connection. The
// application ensures that there is at most one writer to a connection by
// executing all writes from this goroutine.
func (c *Client) writePump(utils *base_blocs.Utilities) {
	ticker := time.NewTicker(pingPeriod)
	defer func() {
		ticker.Stop()
		c.socket.Close()
	}()
	for {
		select {
		case message, ok := <-c.send:
			c.socket.SetWriteDeadline(time.Now().Add(writeWait))
			if !ok {
				// The hub closed the channel.
				c.socket.WriteMessage(websocket.CloseMessage, []byte{})
				return
			}

			w, err := c.socket.NextWriter(websocket.TextMessage)
			if err != nil {
				utils.Log(err, base_blocs.RunTimeError)
				return
			}
			w.Write(message)

			// Add queued chat messages to the current websocket message.
			n := len(c.send)
			for i := 0; i < n; i++ {
				_, err = w.Write([]byte{'\n'})
				if err != nil {
					utils.Log(err, base_blocs.RunTimeError)
					return
				}
				_, err = w.Write(<-c.send)
				if err != nil {
					utils.Log(err, base_blocs.RunTimeError)
					return
				}
			}

			if err := w.Close(); err != nil {
				utils.Log(err, base_blocs.RunTimeError)
				return
			}

		case <-ticker.C:
			c.socket.SetWriteDeadline(time.Now().Add(writeWait))
			if err := c.socket.WriteMessage(websocket.PingMessage, nil); err != nil {
				utils.Log(err, base_blocs.RunTimeError)
				return
			}
		}
	}
}

// ServeWs handles websocket requests from the peer.
func ServeWs(hub *Hub, config base_domain.AppConfig, w http.ResponseWriter, r *http.Request) {
	utils := Utilities(config, r)
	m := NewDomainManager(config, r)
	coy, err := m.CheckDomain()
	if err != nil {
		utils.Log(err.Error(), base_blocs.RunTimeError)
		return
	}
	//origin validation pending.
	upgrader.CheckOrigin = func(r *http.Request) bool {
		return true
	}
	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		utils.Log(err.Error(), base_blocs.RunTimeError)
		return
	}
	client := &Client{id: uuid.NewV4().String(), hub: hub, socket: conn, CoyTag: coy.Tag, send: make(chan []byte, 256)}
	client.hub.register <- client
	if coy.Status != 0 {
		client.hub.unregister <- client
		return
	}
	go client.writePump(utils)
	go client.readPump(utils)
}
