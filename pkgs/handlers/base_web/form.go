package base_web

import (
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/cesmanager/base-server/core/base_domain"
	"gitlab.com/cesmanager/base-server/pkgs/base_blocs"
	"gitlab.com/cesmanager/base-server/pkgs/base_repository"
)

type form struct {
	c base_domain.AppConfig
}

func NewForm(c base_domain.AppConfig) *form {
	return &form{c: c}
}

func (h *form) List(w http.ResponseWriter, r *http.Request) {
	resp := NewResponse(w, r)
	//response callback
	defer func() {
		resp.Execute(nil)
	}()
	db, close := base_repository.MustMySQL(h.c.DataBase[h.c.ActiveDB], r.Header.Get("coy-db"))
	defer close()

	req := ParseRequest(h.c, r)
	params := req.PayLoad()
	delete(params, "count")
	delete(params, "pages")
	page := "1"
	if _, ok := params["page"]; ok {
		page = fmt.Sprint(params["page"])
		delete(params, "page")
	}
	pageSize := "1"
	if _, ok := params["page_size"]; ok {
		pageSize = fmt.Sprint(params["page_size"])
		delete(params, "page_size")
	}

	serv := base_blocs.NewForm(base_repository.NewForm(db))
	data, err := serv.List(params, req.OrderBy, page, pageSize)
	if err != nil {
		resp.SetError("error", err.Error())
		return
	}
	resp.SetBody(data)
}

func (h *form) Create(w http.ResponseWriter, r *http.Request) {
	resp := NewResponse(w, r)
	defer func() {
		resp.Execute(nil)
	}()
	db, close := base_repository.MustMySQL(h.c.DataBase[h.c.ActiveDB], r.Header.Get("coy-db"))
	defer close()

	params := ParseRequest(h.c, r).PayLoad()
	if _, ok := params["title"]; !ok {
		resp.SetError("title_error", "title is required")
		return
	}
	formBloc := base_blocs.NewForm(base_repository.NewForm(db))
	form, err := formBloc.Create(params)
	if err != nil {
		resp.SetError("error", err.Error())
		return
	}
	resp.SetBody(form)
}

func (h *form) Update(w http.ResponseWriter, r *http.Request) {
	resp := NewResponse(w, r)
	defer func() {
		resp.Execute(nil)
	}()
	db, close := base_repository.MustMySQL(h.c.DataBase[h.c.ActiveDB], r.Header.Get("coy-db"))
	defer close()

	params := ParseRequest(h.c, r).PayLoad()
	if _, ok := params["id"]; !ok {
		resp.SetError("error", "id is required")
		return
	}
	formBloc := base_blocs.NewForm(base_repository.NewForm(db))
	form, err := formBloc.Update(fmt.Sprint(params["id"]), params)
	if err != nil {
		resp.SetError("error", err.Error())
		return
	}
	resp.SetBody(form)
}

func (h *form) Get(w http.ResponseWriter, r *http.Request) {
	resp := NewResponse(w, r)
	defer func() {
		resp.Execute(nil)
	}()
	db, close := base_repository.MustMySQL(h.c.DataBase[h.c.ActiveDB], r.Header.Get("coy-db"))
	defer close()

	param := mux.Vars(r)
	if _, ok := param["id"]; !ok {
		resp.SetError("error", "id is required")
		return
	}
	formBloc := base_blocs.NewForm(base_repository.NewForm(db))
	form, err := formBloc.Get(fmt.Sprint(param["id"]))
	if err != nil {
		resp.SetError("error", err.Error())
		return
	}
	resp.SetBody(form)
}

func (h *form) Find(w http.ResponseWriter, r *http.Request) {
	resp := NewResponse(w, r)
	defer func() {
		resp.Execute(nil)
	}()
	db, close := base_repository.MustMySQL(h.c.DataBase[h.c.ActiveDB], r.Header.Get("coy-db"))
	defer close()

	formBloc := base_blocs.NewForm(base_repository.NewForm(db))
	forms, err := formBloc.Find(ParseRequest(h.c, r).PayLoad())
	if err != nil {
		resp.SetError("error", err.Error())
		return
	}
	resp.SetBody(forms)
}
