package base_web

import (
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"runtime/debug"

	"github.com/gorilla/mux"
	"gitlab.com/cesmanager/base-server/core/base_domain"
	"gitlab.com/cesmanager/base-server/pkgs/base_blocs"
	"gitlab.com/cesmanager/base-server/pkgs/base_repository"
)

type GeoLocation struct {
	c base_domain.AppConfig
}

func NewGeoLocation(c base_domain.AppConfig) *GeoLocation {
	return &GeoLocation{c: c}
}

// get geoLocation by id
func (h *GeoLocation) Get(w http.ResponseWriter, r *http.Request) {
	resp := NewResponse(w, r)

	defer func() {
		if err := recover(); err != nil {
			panic(fmt.Sprintf("%v\n\t%v", err, string(debug.Stack())))
		}
		resp.Execute(nil)
	}()
	db, close := base_repository.MustMySQL(h.c.DataBase[h.c.ActiveDB], r.Header.Get("coy-db"))
	defer close()
	vars := mux.Vars(r)

	geoLocationServ := base_blocs.NewGeoLocation(base_repository.NewGeoLocation(db))

	//check if id is provided
	id, ok := vars["id"]
	if !ok {
		//return all geoLocations related to the logged in user
		//get the user data.
		user_id := r.Header.Get("user_id")
		userServ := base_blocs.NewUser(base_repository.NewUser(db))
		user, err := userServ.Get(user_id)
		if err != nil {
			resp.SetError("error", err.Error())
			return
		}
		geoLocations, err := geoLocationServ.Find(map[string]interface{}{"profile_id": user.ProfileID}, nil, nil)
		if err != nil {
			resp.SetError("error", err.Error())
			return
		}
		resp.SetBody(geoLocations)
		return
	}
	geoLocation, err := geoLocationServ.Get(id)
	if err != nil {
		resp.SetError("error", err.Error())
		return
	}
	resp.SetBody(geoLocation)
}

func (h *GeoLocation) List(w http.ResponseWriter, r *http.Request) {
	resp := NewResponse(w, r)
	//response callback
	defer func() {
		resp.Execute(nil)
	}()
	db, close := base_repository.MustMySQL(h.c.DataBase[h.c.ActiveDB], r.Header.Get("coy-db"))
	defer close()

	req := ParseRequest(h.c, r)
	params := req.PayLoad()
	delete(params, "count")
	delete(params, "pages")
	page := "1"
	if _, ok := params["page"]; ok {
		page = fmt.Sprint(params["page"])
		delete(params, "page")
	}
	pageSize := "1"
	if _, ok := params["page_size"]; ok {
		pageSize = fmt.Sprint(params["page_size"])
		delete(params, "page_size")
	}

	listServ := base_blocs.NewGeoLocation(base_repository.NewGeoLocation(db))
	list, err := listServ.List(params, req.OrderBy, page, pageSize)
	if err != nil {
		resp.SetError("error", err.Error())
		return
	}
	resp.SetBody(list)
}

// get geoLocation by request parameter
func (h *GeoLocation) Find(w http.ResponseWriter, r *http.Request) {
	resp := NewResponse(w, r)

	defer func() {
		if err := recover(); err != nil {
			panic(fmt.Sprintf("%v\n\t%v", err, string(debug.Stack())))
		}
		resp.Execute(nil)
	}()
	db, close := base_repository.MustMySQL(h.c.DataBase[h.c.ActiveDB], r.Header.Get("coy-db"))
	defer close()

	geoLocationServ := base_blocs.NewGeoLocation(base_repository.NewGeoLocation(db))

	//get parameters
	req := ParseRequest(h.c, r)
	params := req.PayLoad()
	geoLocations, err := geoLocationServ.Find(params, &req.ColumnOperator, &req.OrderBy)
	if err != nil {
		resp.SetError("error", err.Error())
		return
	}
	resp.SetBody(geoLocations)
}

// Create a GeoLocation
func (h *GeoLocation) Create(w http.ResponseWriter, r *http.Request) {
	resp := NewResponse(w, r)

	defer func() {
		if err := recover(); err != nil {
			panic(fmt.Sprintf("%v\n\t%v", err, string(debug.Stack())))
		}
		resp.Execute(nil)
	}()
	db, close := base_repository.MustMySQL(h.c.DataBase[h.c.ActiveDB], r.Header.Get("coy-db"))
	defer close()

	params := ParseRequest(h.c, r).PayLoad()

	//do basic validation
	fails := h.VetParams(params)
	if len(fails) > 0 {
		for k, v := range fails {
			resp.SetError(k, v)
		}
		return
	}
	geoLocationServ := base_blocs.NewGeoLocation(base_repository.NewGeoLocation(db))

	geoLocation, err := geoLocationServ.Create(params)
	if err != nil {
		resp.SetCode(http.StatusBadRequest)
		resp.SetError("error", "geoLocation creation failed")
		return
	}
	resp.SetBody(geoLocation)
}

// Create a GeoLocation
func (h *GeoLocation) Update(w http.ResponseWriter, r *http.Request) {
	resp := NewResponse(w, r)

	defer func() {
		if err := recover(); err != nil {
			panic(fmt.Sprintf("%v\n\t%v", err, string(debug.Stack())))
		}
		resp.Execute(nil)
	}()
	db, close := base_repository.MustMySQL(h.c.DataBase[h.c.ActiveDB], r.Header.Get("coy-db"))
	defer close()

	params := ParseRequest(h.c, r).PayLoad()
	if _, ok := params["id"]; !ok {
		resp.SetError("error", "id is required")
		return
	}
	geoLocationServ := base_blocs.NewGeoLocation(base_repository.NewGeoLocation(db))

	geoLocation, err := geoLocationServ.Update(fmt.Sprint(params["id"]), params)
	if err != nil {
		resp.SetCode(http.StatusBadRequest)
		resp.SetError("error", "geoLocation update failed")
		return
	}
	resp.SetBody(geoLocation)
}

// TimeZones
func (h *GeoLocation) TimeZones(w http.ResponseWriter, r *http.Request) {
	resp := NewResponse(w, r)
	defer func() {
		if err := recover(); err != nil {
			panic(fmt.Sprintf("%v\n\t%v", err, string(debug.Stack())))
		}
		resp.Execute(nil)
	}()
	_timezones, err := os.ReadFile("timezones.json")
	if err != nil {
		resp.SetError("error", "no timezone file found")
		return
	}
	var timezones []base_domain.TimeZone
	err = json.Unmarshal(_timezones, &timezones)
	if err != nil {
		resp.SetError("error", err.Error())
		return
	}
	data := []map[string]string{}
	for _, timezone := range timezones {
		for _, utc := range timezone.Utc {
			text := fmt.Sprint("(UTC", timezone.Offset, ")")
			if timezone.Offset >= 0 {
				text = fmt.Sprint("(UTC+", timezone.Offset, ")")
			}
			data = append(data, map[string]string{"key": utc, "value": fmt.Sprint(text, " ", timezone.Value, " - ", utc)})
		}
	}
	resp.SetBody(data)
}

func (h *GeoLocation) VetParams(params map[string]interface{}) map[string]string {
	msgs := map[string]string{}
	if _, ok := params["name"]; !ok {
		msgs["name_error"] = "name is required"
	}
	return msgs
}
