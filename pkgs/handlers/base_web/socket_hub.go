package base_web

import (
	"fmt"

	"gitlab.com/cesmanager/base-server/core/base_domain"
	"gitlab.com/cesmanager/base-server/pkgs/base_blocs"
)

var (
	clientCnt = 0
)

// Hub maintains the set of active clients and broadcasts messages to the
// clients.
type Hub struct {
	// Registered clients.
	clients map[*Client]bool

	// Inbound messages from the clients.
	broadcast chan broadcast

	// Register requests from the clients.
	register chan *Client

	// Unregister requests from clients.
	unregister chan *Client
}

type broadcast struct {
	msg    []byte
	client *Client
}

//NewHub ...
func NewHub() *Hub {
	return &Hub{
		broadcast:  make(chan broadcast),
		register:   make(chan *Client),
		unregister: make(chan *Client),
		clients:    make(map[*Client]bool),
	}
}

//Run ... start the socket routine
func (h *Hub) Run(config base_domain.AppConfig) {
	utils := Utilities(config, nil)
	for {
		select {
		case client := <-h.register:
			h.clients[client] = true
			utils.SetTag(client.CoyTag)
			clientCnt++
			utils.Log(fmt.Sprintf("%v Clients connected", clientCnt), base_blocs.StdLog)
		case client := <-h.unregister:
			if _, ok := h.clients[client]; ok {
				utils.SetTag(client.CoyTag)
				clientCnt--
				utils.Log(fmt.Sprintf("%v Clients connected", clientCnt), base_blocs.StdLog)
				delete(h.clients, client)
				close(client.send)
			}
		case msg := <-h.broadcast:
			for client := range h.clients {
				if client.Tag != msg.client.Tag || msg.client.Tag != client.Tag {
					continue
				}

				if len(fmt.Sprintf("%v", msg.msg)) == 0 {
					continue
				}
				select {
				case client.send <- msg.msg:
				default:
					close(client.send)
					delete(h.clients, client)
				}
			}
		}
	}
}
