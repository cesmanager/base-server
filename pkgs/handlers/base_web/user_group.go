package base_web

import (
	"fmt"
	"net/http"
	"runtime/debug"

	"github.com/gorilla/mux"
	"gitlab.com/cesmanager/base-server/core/base_domain"
	"gitlab.com/cesmanager/base-server/pkgs/base_blocs"
	"gitlab.com/cesmanager/base-server/pkgs/base_repository"
)

type UserGroup struct {
	c base_domain.AppConfig
}

func NewUserGroup(c base_domain.AppConfig) *UserGroup {
	return &UserGroup{c: c}
}

// Create a user group
func (h *UserGroup) Create(w http.ResponseWriter, r *http.Request) {
	resp := NewResponse(w, r)
	defer func() {
		if err := recover(); err != nil {
			panic(fmt.Sprintf("%v\n\t%v", err, string(debug.Stack())))
		}
		resp.Execute(nil)
	}()
	db, close := base_repository.MustMySQL(h.c.DataBase[h.c.ActiveDB], r.Header.Get("coy-db"))
	defer close()

	params := ParseRequest(h.c, r).PayLoad()

	if _, ok := params["title"]; !ok {
		resp.SetError("title_error", "title is required")
		resp.SetCode(http.StatusBadRequest)
		return
	}
	//start transaction
	tx := db.Begin()
	ugsrv := base_blocs.NewUserGroup(base_repository.NewUserGroup(db))
	userGroup, err := ugsrv.Create(params)
	if err != nil {
		tx.Rollback()
		resp.SetError("error", err.Error())
		return
	}
	//update the module permissions
	if _p, ok := params["permits"]; ok {
		ugpsrv := base_blocs.NewUserGroupPermit(base_repository.NewUserGroupPermit(tx))
		if permits, ok := _p.([]interface{}); ok {
			for _, _permit := range permits {
				if permit, ok := _permit.(map[string]interface{}); ok {
					_, err := ugpsrv.Save(fmt.Sprint(userGroup.ID), permit)
					if err != nil {
						tx.Rollback()
						resp.SetError("error", err.Error())
						return
					}
				}
			}
		}
	}
	tx.Commit()
	resp.SetBody(userGroup)
}

// update user group
func (h *UserGroup) Update(w http.ResponseWriter, r *http.Request) {
	resp := NewResponse(w, r)

	defer func() {
		if err := recover(); err != nil {
			panic(fmt.Sprintf("%v\n\t%v", err, string(debug.Stack())))
		}
		resp.Execute(nil)
	}()
	db, close := base_repository.MustMySQL(h.c.DataBase[h.c.ActiveDB], r.Header.Get("coy-db"))
	defer close()

	req := ParseRequest(h.c, r)
	params := req.PayLoad()
	if _, ok := params["id"]; !ok {
		resp.SetError("error", "id is required")
		return
	}
	//start transaction
	tx := db.Begin()
	//update the module permissions
	if _p, ok := params["permits"]; ok {
		ugpsrv := base_blocs.NewUserGroupPermit(base_repository.NewUserGroupPermit(tx))
		if permits, ok := _p.([]interface{}); ok {
			for _, _permit := range permits {
				if permit, ok := _permit.(map[string]interface{}); ok {
					_, err := ugpsrv.Save(fmt.Sprint(params["id"]), permit)
					if err != nil {
						tx.Rollback()
						resp.SetError("error", err.Error())
						return
					}
				}
			}
		}
	}

	ugsrv := base_blocs.NewUserGroup(base_repository.NewUserGroup(tx))
	_, err := ugsrv.Update(fmt.Sprint(params["id"]), params)
	if err != nil {
		tx.Rollback()
		resp.SetError("error", err.Error())
		return
	}
	//fetch the whole user group
	userGroup, err := ugsrv.Get(fmt.Sprint(params["id"]))
	if err != nil {
		tx.Rollback()
		resp.SetError("error", err.Error())
		return
	}
	tx.Commit()
	resp.SetBody(userGroup)
}

// get user_group by id
func (h *UserGroup) Get(w http.ResponseWriter, r *http.Request) {
	resp := NewResponse(w, r)

	defer func() {
		if err := recover(); err != nil {
			panic(fmt.Sprintf("%v\n\t%v", err, string(debug.Stack())))
		}
		resp.Execute(nil)
	}()
	db, close := base_repository.MustMySQL(h.c.DataBase[h.c.ActiveDB], r.Header.Get("coy-db"))
	defer close()
	//get parameters
	params := mux.Vars(r)

	//check if id is provided
	if _, ok := params["id"]; !ok {
		resp.SetError("error", "id is required")
		return
	}

	ugsrv := base_blocs.NewUserGroup(base_repository.NewUserGroup(db))
	userGroup, err := ugsrv.Get(fmt.Sprint(params["id"]))

	if err != nil {
		resp.SetError("error", err.Error())
		return
	}
	resp.SetBody(userGroup)
}

func (h *UserGroup) List(w http.ResponseWriter, r *http.Request) {
	resp := NewResponse(w, r)
	//response callback
	defer func() {
		resp.Execute(nil)
	}()
	db, close := base_repository.MustMySQL(h.c.DataBase[h.c.ActiveDB], r.Header.Get("coy-db"))
	defer close()

	params := ParseRequest(h.c, r).PayLoad()
	delete(params, "count")
	delete(params, "pages")
	page := "1"
	if _, ok := params["page"]; ok {
		page = fmt.Sprint(params["page"])
		delete(params, "page")
	}
	pageSize := "1"
	if _, ok := params["page_size"]; ok {
		pageSize = fmt.Sprint(params["page_size"])
		delete(params, "page_size")
	}
	order := map[string]interface{}{}

	qtServ := base_blocs.NewUserGroup(base_repository.NewUserGroup(db))
	list, err := qtServ.List(params, order, page, pageSize)
	if err != nil {
		resp.SetError("error", err.Error())
		return
	}
	resp.SetBody(list)
}

// get profile by id
func (h *UserGroup) Find(w http.ResponseWriter, r *http.Request) {
	resp := NewResponse(w, r)
	defer func() {
		if err := recover(); err != nil {
			panic(fmt.Sprintf("%v\n\t%v", err, string(debug.Stack())))
		}
		resp.Execute(nil)
	}()
	db, close := base_repository.MustMySQL(h.c.DataBase[h.c.ActiveDB], r.Header.Get("coy-db"))
	defer close()

	ugServ := base_blocs.NewUserGroup(base_repository.NewUserGroup(db))

	params := ParseRequest(h.c, r).PayLoad()
	data, err := ugServ.Find(params)
	if err != nil {
		resp.SetError("error", err.Error())
		return
	}
	resp.SetBody(data)
}

// permit user
func (h *UserGroup) MyPermit(w http.ResponseWriter, r *http.Request) {
	resp := NewResponse(w, r)
	defer func() {
		if err := recover(); err != nil {
			panic(fmt.Sprintf("%v\n\t%v", err, string(debug.Stack())))
		}
		resp.Execute(nil)
	}()
	db, close := base_repository.MustMySQL(h.c.DataBase[h.c.ActiveDB], r.Header.Get("coy-db"))
	defer close()

	user, ok := UserContext(r.Context())
	if !ok {
		resp.SetError("error", "unable to determine user")
		return
	}

	ugsrv := base_blocs.NewUserGroup(base_repository.NewUserGroup(db))
	userGroup, err := ugsrv.Get(fmt.Sprint(user.UserGroupID))
	if err != nil {
		resp.SetError("error", err.Error())
		return
	}
	resp.SetBody(userGroup)
}
