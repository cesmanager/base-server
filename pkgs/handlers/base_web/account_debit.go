package base_web

import (
	"fmt"
	"net/http"
	"runtime/debug"

	"github.com/gorilla/mux"
	"gitlab.com/cesmanager/base-server/core/base_domain"
	"gitlab.com/cesmanager/base-server/pkgs/base_blocs"
	"gitlab.com/cesmanager/base-server/pkgs/base_repository"
	"gorm.io/gorm"
)

type Debit struct {
	c base_domain.AppConfig
}

func NewDebit(c base_domain.AppConfig) *Debit {
	return &Debit{c: c}
}

// get debit record by id
func (h *Debit) Get(w http.ResponseWriter, r *http.Request) {
	resp := NewResponse(w, r)

	defer func() {
		if err := recover(); err != nil {
			panic(fmt.Sprintf("%v\n\t%v", err, string(debug.Stack())))
		}
		resp.Execute(nil)
	}()
	db, close := base_repository.MustMySQL(h.c.DataBase[h.c.ActiveDB], r.Header.Get("coy-db"))
	defer close()

	vars := mux.Vars(r)
	debitServ := base_blocs.NewDebit(base_repository.NewDebit(db))

	//check if id is provided
	id, ok := vars["id"]
	if !ok {
		resp.SetError("error", "debit id is required")
		return
	}
	debit, err := debitServ.Get(id)

	if err != nil {
		resp.SetError("error", "Sorry an error occured")
		return
	}
	resp.SetBody(debit)
}

// Find debit record by request parameter
func (h *Debit) Find(w http.ResponseWriter, r *http.Request) {
	resp := NewResponse(w, r)

	defer func() {
		if err := recover(); err != nil {
			panic(fmt.Sprintf("%v\n\t%v", err, string(debug.Stack())))
		}
		resp.Execute(nil)
	}()
	db, close := base_repository.MustMySQL(h.c.DataBase[h.c.ActiveDB], r.Header.Get("coy-db"))
	defer close()

	debitServ := base_blocs.NewDebit(base_repository.NewDebit(db))

	params := ParseRequest(h.c, r).PayLoad()
	debits, err := debitServ.Find(params)
	if err != nil {
		resp.SetError("error", err.Error())
		return
	}
	resp.SetBody(debits)
}

// Create a Debit
func (h *Debit) Create(w http.ResponseWriter, r *http.Request) {
	resp := NewResponse(w, r)

	defer func() {
		if err := recover(); err != nil {
			panic(fmt.Sprintf("%v\n\t%v", err, string(debug.Stack())))
		}
		resp.Execute(nil)
	}()
	db, close := base_repository.MustMySQL(h.c.DataBase[h.c.ActiveDB], r.Header.Get("coy-db"))
	defer close()

	params := ParseRequest(h.c, r).PayLoad()

	//do basic validation
	fails := h.VetParams(params, []string{"title", "account_id", "amount", "creator_id", "ip", "debitor_id"})
	if len(fails) > 0 {
		for k, v := range fails {
			resp.SetError(k, v)
		}
		return
	}
	debitServ := base_blocs.NewDebit(base_repository.NewDebit(db))

	debit, err := debitServ.Create(params, base_blocs.NewAccount(base_repository.NewAccount(db)))
	if err != nil {
		resp.SetError("error", err.Error())
		return
	}
	resp.SetBody(debit)
}

// Create a Debit
func (h *Debit) Post(w http.ResponseWriter, r *http.Request) {
	resp := NewResponse(w, r)

	defer func() {
		if err := recover(); err != nil {
			panic(fmt.Sprintf("%v\n\t%v", err, string(debug.Stack())))
		}
		resp.Execute(nil)
	}()
	db, close := base_repository.MustMySQL(h.c.DataBase[h.c.ActiveDB], r.Header.Get("coy-db"))
	defer close()

	params := ParseRequest(h.c, r).PayLoad()
	if _, ok := params["id"]; !ok {
		resp.SetError("id_error", "id is required")
		return
	}
	//get the user data.
	user_id := r.Header.Get("user_id")
	userServ := base_blocs.NewUser(base_repository.NewUser(db))
	user, err := userServ.Get(user_id)
	if err != nil {
		resp.SetError("error", "unable to identify poster")
		return
	}
	//post the debit in a transaction
	db.Transaction(func(tx *gorm.DB) error {
		debitServ := base_blocs.NewDebit(base_repository.NewDebit(tx))
		debit, err := debitServ.Post(fmt.Sprint(params["id"]), fmt.Sprint(user.ProfileID), base_blocs.NewAccount(base_repository.NewAccount(tx)))
		if err != nil {
			resp.SetError("error", err.Error())
			tx.Rollback()
			return err
		}
		resp.SetBody(debit)
		return nil
	})
}

func (h *Debit) VetParams(params map[string]interface{}, required []string) map[string]string {
	msgs := map[string]string{}
	for _, r := range required {
		switch r {
		case "id":
			if _, ok := params["id"]; !ok {
				msgs["id_error"] = "id is required"
			}
		case "title":
			if _, ok := params["title"]; !ok {
				msgs["title_error"] = "title is required"
			}
		case "account_id":
			if _, ok := params["account_id"]; ok {
				msgs["account_id_error"] = "account_id is required"
			}
		case "amount":
			if _, ok := params["amount"]; ok {
				msgs["amount_error"] = "amount is required"
			}
		case "creator_id":
			if _, ok := params["creator_id"]; ok {
				msgs["creator_id_error"] = "creator_id is required"
			}
		case "ip":
			if _, ok := params["ip"]; ok {
				msgs["ip_error"] = "ip is required"
			}
		case "debtor_id":
			if _, ok := params["debtor_id"]; ok {
				msgs["debtor_id_error"] = "debtor_id is required"
			}
		}
	}
	return msgs
}
