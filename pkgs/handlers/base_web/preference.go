package base_web

import (
	"fmt"
	"net/http"
	"runtime/debug"

	"github.com/gorilla/mux"
	"gitlab.com/cesmanager/base-server/core/base_domain"
	"gitlab.com/cesmanager/base-server/pkgs/base_blocs"
	"gitlab.com/cesmanager/base-server/pkgs/base_repository"
)

type Preference struct {
	c base_domain.AppConfig
}

func NewPreference(c base_domain.AppConfig) *Preference {
	return &Preference{c: c}
}

func (h *Preference) Get(w http.ResponseWriter, r *http.Request) {
	resp := NewResponse(w, r)
	defer func() {
		if err := recover(); err != nil {
			panic(fmt.Sprintf("%v\n\t%v", err, string(debug.Stack())))
		}
		resp.Execute(nil)
	}()
	db, close := base_repository.MustMySQL(h.c.DataBase[h.c.ActiveDB], r.Header.Get("coy-db"))
	defer close()

	vars := mux.Vars(r)
	profile_id := vars["profile_id"]
	bloc := base_blocs.NewPreference(base_repository.NewPreference(db))
	preferences, err := bloc.Get(profile_id)
	if err != nil {
		resp.SetError("error", "unable to get preference(s)")
		return
	}
	resp.SetBody(preferences)
}

func (h *Preference) Find(w http.ResponseWriter, r *http.Request) {
	resp := NewResponse(w, r)
	defer func() {
		if err := recover(); err != nil {
			panic(fmt.Sprintf("%v\n\t%v", err, string(debug.Stack())))
		}
		resp.Execute(nil)
	}()
	db, close := base_repository.MustMySQL(h.c.DataBase[h.c.ActiveDB], r.Header.Get("coy-db"))
	defer close()

	req := ParseRequest(h.c, r)
	params := req.PayLoad()

	if _, ok := params["profile_id"]; !ok {
		resp.SetError("error", "profile_id is required")
		return
	}
	if _, ok := params["key"]; !ok {
		resp.SetError("error", "key is required")
		return
	}
	bloc := base_blocs.NewPreference(base_repository.NewPreference(db))
	preferences, err := bloc.Find(fmt.Sprint(params["profile_id"]), fmt.Sprint(params["key"]))
	if err != nil {
		resp.SetError("error", "unable to get preference(s)")
		return
	}
	resp.SetBody(preferences)
}

func (h *Preference) Store(w http.ResponseWriter, r *http.Request) {
	resp := NewResponse(w, r)
	defer func() {
		if err := recover(); err != nil {
			panic(fmt.Sprintf("%v\n\t%v", err, string(debug.Stack())))
		}
		resp.Execute(nil)
	}()
	db, close := base_repository.MustMySQL(h.c.DataBase[h.c.ActiveDB], r.Header.Get("coy-db"))
	defer close()

	req := ParseRequest(h.c, r)
	params := req.PayLoad()
	if _, ok := params["profile_id"]; !ok {
		resp.SetError("error", "profile_id is required")
		return
	}
	profile_id := fmt.Sprint(params["profile_id"])
	delete(params, "profile_id")
	bloc := base_blocs.NewPreference(base_repository.NewPreference(db))
	preferences, err := bloc.Store(profile_id, params)
	if err != nil {
		resp.SetError("error", "unable to save preference(s)")
		return
	}
	resp.SetBody(preferences)
}
