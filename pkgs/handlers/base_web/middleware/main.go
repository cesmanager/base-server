package middleware

import (
	"net/http"

	"gitlab.com/cesmanager/base-server/core/base_domain"
	"gitlab.com/cesmanager/base-server/pkgs/base_blocs"
	"gitlab.com/cesmanager/base-server/pkgs/handlers/base_web"
)

// Middleware ...
type middlewares struct {
	c base_domain.AppConfig
}

func New(c base_domain.AppConfig) *middlewares {
	return &middlewares{c: c}
}

// Middleware ...
type Middleware func(http.HandlerFunc, base_domain.AppConfig) http.HandlerFunc

// Chain applies middlewares to a http.HandlerFunc
func (m *middlewares) Chain(f http.HandlerFunc, middlewares ...Middleware) http.HandlerFunc {
	for _, mw := range middlewares {
		f = mw(f, m.c)
	}
	//capture fatal errors
	return func(w http.ResponseWriter, r *http.Request) {
		defer func() {
			if err := recover(); err != nil {
				resp := base_web.NewResponse(w, r)
				utils := base_web.Utilities(m.c, r)
				utils.Log(err, base_blocs.PanicError)
				resp.SetCode(http.StatusInternalServerError)
				resp.SetError("error", "sorry, an internal server error occured. kindly contact the service provider to resolve this issue.")
				resp.Execute(nil)
			}
		}()
		ctx := r.Context()
		ctx = base_web.NewConfigContext(ctx, &m.c) //attach config
		r = r.WithContext(ctx)
		//handle cors
		w.Header().Set("Access-Control-Allow-Origin", r.Header.Get("Origin")) //origin validation pending.
		w.Header().Set("Access-Control-Allow-Credentials", "true")
		w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
		w.Header().Set("Access-Control-Allow-Headers", "Origin, Cookie, Cache-Control, Content-Type, X-CSRF-TOKEN, Accept, Authorization, X-XSRF-TOKEN, Access-Control-Allow-Origin, "+m.c.TokenKey)
		if (*r).Method == "OPTIONS" {
			return
		}
		ctx = r.Context()
		ctx, close := base_web.NewSftpContext(ctx, m.c) //attach sftp
		defer close()                                   //close the sftp connection if it was opened
		//call with attached contexts
		f(w, r.WithContext(ctx))
	}
}

// chaining functions
func (m *middlewares) Auth() Middleware {
	return Auth(false, false)
}
func (m *middlewares) OptAuth() Middleware {
	return Auth(false, true)
}
func (m *middlewares) VetDomain() Middleware {
	return VetDomain()
}
func (m *middlewares) AuthRoot() Middleware {
	return Auth(true, false)
}
