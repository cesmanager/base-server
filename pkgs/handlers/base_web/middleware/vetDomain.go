package middleware

import (
	"fmt"
	"net/http"

	"gitlab.com/cesmanager/base-server/core/base_domain"
	"gitlab.com/cesmanager/base-server/pkgs/handlers/base_web"
)

// VetDomain ...
func VetDomain() Middleware {
	// Create a new Middleware
	return func(f http.HandlerFunc, c base_domain.AppConfig) http.HandlerFunc {
		// Define the http.HandlerFunc
		return func(w http.ResponseWriter, r *http.Request) {
			resp := base_web.NewResponse(w, r)
			m := base_web.NewDomainManager(c, r)
			coy, err := m.CheckDomain()
			if err != nil {
				resp.SetCode(http.StatusUnauthorized)
				resp.SetError("error", "sorry a domain error occured")
				resp.Execute(nil)
				return
			}
			if coy.Status == 2 {
				resp.SetCode(http.StatusUnauthorized)
				resp.SetError("error", "company access disabled")
				resp.Execute(nil)
				return
			}
			if coy.Status == 3 {
				resp.SetCode(http.StatusUnauthorized)
				resp.SetError("error", "ongoing maintenance")
				resp.Execute(nil)
				return
			}
			r.Header.Set("coy-id", fmt.Sprint(coy.ID))
			r.Header.Set("coy-db", c.DataBase[c.ActiveDB].DBPrefix+coy.Tag)
			r.Header.Set("coy-tag", coy.Tag)
			r.Header.Set("coy-domain", coy.Domain)
			r.Header.Set("coy-features", coy.Features)
			r.Header.Set("coy-root", fmt.Sprint(coy.Root))
			//reattach session
			ctx := r.Context()
			ctx = base_web.NewSessionContext(ctx, c, r) //attach session store
			// Call the next middleware/handler in chain
			f(w, r.WithContext(ctx))
		}
	}
}
