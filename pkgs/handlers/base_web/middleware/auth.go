package middleware

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"

	"gitlab.com/cesmanager/base-server/core/base_domain"
	"gitlab.com/cesmanager/base-server/pkgs/handlers/base_web"
)

// Auth ...
func Auth(root bool, optional bool) Middleware {
	// Create a new Middleware
	return func(f http.HandlerFunc, c base_domain.AppConfig) http.HandlerFunc {
		// Define the http.HandlerFunc
		return func(w http.ResponseWriter, r *http.Request) {
			//check if its a root company
			if root {
				root, err := strconv.ParseBool(r.Header.Get("coy-root"))
				if err != nil {
					w.WriteHeader(http.StatusBadRequest)
					json.NewEncoder(w).Encode(map[string]interface{}{"error": "root validation error"})
					return
				}
				if !root {
					w.WriteHeader(http.StatusUnauthorized)
					json.NewEncoder(w).Encode(map[string]interface{}{"error": "root access denied"})
					return
				}
			}
			//session
			sesHandle := base_web.NewSession(c)
			user, _, err := sesHandle.ActiveUser(w, r)
			if err != nil {
				if !optional { //check if authentication is optional for this route
					resp := base_web.NewResponse(w, r)
					resp.SetCode(http.StatusForbidden)
					resp.SetError("error", err.Error())
					resp.Execute(nil)
					return
				}
			} else {
				permis, err := sesHandle.Permits(w, r)
				if err != nil {
					resp := base_web.NewResponse(w, r)
					resp.SetCode(http.StatusBadRequest)
					resp.SetError("error", err.Error())
					resp.Execute(nil)
					return
				}
				ctx := r.Context()
				ctx = base_web.NewUserContext(ctx, &user)
				ctx = base_web.NewPermitsContext(ctx, &permis)
				r = r.WithContext(ctx)
				//to be removed
				r.Header.Set("user_id", fmt.Sprintf("%v", user.ID))
			}
			// Call the next middleware/handler in chain
			f(w, r)
		}
	}
}
