package base_web

import (
	"fmt"
	"io"
	"net/http"
	"net/url"
	"os/exec"
	"runtime/debug"
	"strings"

	"gitlab.com/cesmanager/base-server/core/base_domain"
	"gitlab.com/cesmanager/base-server/pkgs/base_blocs"

	"github.com/gorilla/mux"
)

type customization struct {
	c base_domain.AppConfig
}

func NewCustomization(c base_domain.AppConfig) *customization {
	return &customization{c: c}
}

// CustomizedRoute - this navigates the user to the addon customized for them.
func (h *customization) Navigate(w http.ResponseWriter, r *http.Request) {
	resp := NewResponse(w, r)
	utils := Utilities(h.c, r)
	defer func() {
		if err := recover(); err != nil {
			panic(fmt.Sprintf("%v\n\t%v", err, string(debug.Stack())))
		}
	}()

	//get url params
	vars := mux.Vars(r)
	//load addon from config folder in companies directory
	addon, err := utils.LoadAddon(vars["addon"])
	if err != nil {
		resp.SetCode(http.StatusNotFound).SetError("error", err.Error()).Execute(nil)
		return
	}
	if !addon.Active {
		resp.SetCode(http.StatusNotFound).SetError("error", "addon is down for maintenance").Execute(nil)
		return
	}
	if addon.Auth {
		//session
		sesHandle := NewSession(h.c)
		user, _, err := sesHandle.ActiveUser(w, r)
		if err != nil {
			resp.SetCode(http.StatusNotAcceptable).SetError("error", err.Error()).Execute(nil)
			return
		}
		r.Header.Set("user_id", fmt.Sprint(user.ID))
		r.Header.Set("token-key", h.c.TokenKey)
	}

	switch addon.Scheme {
	case "cmd":
		/**
		* This section needs to be reviewed
		 */
		//parse the form
		err = r.ParseForm()
		if err != nil {
			resp.SetCode(http.StatusInternalServerError)
			resp.SetError("error", err.Error())
			resp.Execute(nil)
			return
		}
		posts := ""
		for k, v := range r.PostForm {
			posts = prepend(posts, "&") + k + "=" + url.QueryEscape(strings.Join(v, ""))
		}
		//call the custom addon in cmd mode
		rw, err := exec.Command(utils.GetCustom()["exec"]+"/"+vars["addon"]+"/cmb/main", r.Header.Get("coy-id"), posts).Output()
		if err != nil {
			resp.SetCode(http.StatusInternalServerError)
			resp.SetError("error", err.Error())
			resp.Execute(nil)
			return
		}
		//wrte back the response
		w.Write(rw)
	case "http", "https":
		//call the custom addon in http mode
		port := ""
		if len(addon.Port) > 0 {
			port = ":" + addon.Port
		}
		ln := strings.Split(r.RequestURI, "/"+vars["addon"]+"/")
		url := fmt.Sprintf("%s://%s%s/%s", addon.Scheme, addon.Domain, port, ln[1])

		proxyReq, err := http.NewRequest(r.Method, url, r.Body)
		if err != nil {
			utils.Log(fmt.Sprintf("%v\n\t%v", err.Error(), string(debug.Stack())), base_blocs.RunTimeError)
			resp.SetCode(http.StatusBadGateway).SetError("error", "addon is unreacheable").Execute(nil)
			return
		}
		proxyReq.Header = r.Header

		client := &http.Client{}

		proxyRes, err := client.Do(proxyReq)
		if err != nil {
			utils.Log(fmt.Sprintf("%v\n\t%v", err.Error(), string(debug.Stack())), base_blocs.RunTimeError)
			resp.SetCode(http.StatusBadGateway).SetError("error", "addon is unreacheable").Execute(nil)
			return
		}
		defer proxyRes.Body.Close()
		//set all the returned hearders
		for k, v := range proxyRes.Header {
			for _, _v := range v {
				w.Header().Set(k, _v)
			}
		}
		//read the status code
		w.WriteHeader(proxyRes.StatusCode)
		//read the body
		body, err := io.ReadAll(proxyRes.Body)
		if err != nil {
			utils.Log(fmt.Sprintf("%v\n\t%v", err.Error(), string(debug.Stack())), base_blocs.RunTimeError)
			resp.SetCode(http.StatusBadGateway).SetError("error", "unable to process addon response").Execute(nil)
			return
		}
		//wrte back the response
		w.Write(body)
	}
}

func prepend(param string, sep string) string {
	if len(param) > 0 {
		return param + sep
	}
	return ""
}
