package base_web

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"strconv"
	"strings"

	"gitlab.com/cesmanager/base-server/core/base_domain"
	"gitlab.com/cesmanager/base-server/pkgs/base_blocs"
	"gitlab.com/cesmanager/base-server/pkgs/base_repository"
)

type session struct {
	c    base_domain.AppConfig
	name string
}

func NewSession(c base_domain.AppConfig) *session {
	return &session{
		c:    c,
		name: "Cookie-Name",
	}
}

// authenticate a user
func (h *session) Authenticate(w http.ResponseWriter, r *http.Request) bool {
	_, _, err := h.ActiveUser(w, r)
	return err != nil
}

// set session
func (h *session) SetAll(w http.ResponseWriter, r *http.Request, data map[string]interface{}) error {
	store, ok := SessionContext(r.Context())
	if !ok {
		return fmt.Errorf("no session store found")
	}
	session, err := store.Get(r, h.name)
	if err != nil {
		return err
	}
	for k, v := range data {
		session.Values[k] = v
	}
	return session.Save(r, w)
}

// drop session
func (h *session) Drop(w http.ResponseWriter, r *http.Request) error {
	store, ok := SessionContext(r.Context())
	if !ok {
		return fmt.Errorf("no session store found")
	}
	session, err := store.Get(r, h.name)
	if err != nil {
		return err
	}
	session.Options.MaxAge = -1
	return session.Save(r, w)
}

// authenticate a user
func (h *session) ActiveUser(w http.ResponseWriter, r *http.Request) (user base_domain.User, token string, err error) {
	defer func() (base_domain.User, string, error) {
		return user, token, err
	}()
	store, ok := SessionContext(r.Context())
	if !ok {
		err = fmt.Errorf("no session store found")
		return
	}
	//try to get the session
	session, err := store.Get(r, h.name)
	if err != nil {
		//save it to have the file
		err = store.Save(r, w, session)
		if err != nil {
			err = fmt.Errorf("unable to save session")
			return
		}
	}
	//initialize access service
	db, close := base_repository.MustMySQL(h.c.DataBase[h.c.ActiveDB], r.Header.Get("coy-db"))
	defer close()
	accessServ := base_blocs.NewAccess(base_repository.NewAccess(db))

	msg := "access denied"
	token = strings.TrimSpace(r.Header.Get(h.c.TokenKey))
	auth, ok := session.Values["auth"]
	if !ok && len(token) == 0 { //user has never logged in
		err = errors.New(msg)
		return
	}
	authicated, _ := strconv.ParseBool(fmt.Sprint(auth))
	if authicated {
		//user is logged in
		ruser, err1 := json.Marshal(session.Values["user"])
		if err1 != nil {
			err = errors.New(msg)
			return
		}
		json.Unmarshal(ruser, &user)
		token = fmt.Sprint(session.Values[h.c.TokenKey])
		//ensure that token is still valid
		_, err = accessServ.Get(token)
		if err != nil {
			//drop the session
			err = h.Drop(w, r)
			return
		}
		return
	}
	if !authicated && len(token) != 0 {
		acc, err1 := accessServ.Get(token)
		if err1 != nil {
			err = err1
			return
		}
		//get the users permission
		ugServ := base_blocs.NewUserGroup(base_repository.NewUserGroup(db))
		ug, err1 := ugServ.Get(fmt.Sprint(user.UserGroupID))
		if err1 != nil {
			err = err1
			return
		}
		user = (*acc.User)
		session.Values["auth"] = true
		session.Values["user"] = user
		session.Values["permits"] = &ug.MapPermits().MappedPermits
		session.Values[h.c.TokenKey] = token
		err = session.Save(r, w)
		if err != nil {
			return
		}
	}
	err = errors.New(msg)
	return
}

// get users permits store in session
func (h *session) Permits(w http.ResponseWriter, r *http.Request) (permits map[string]base_domain.UserGroupPermit, err error) {
	store, ok := SessionContext(r.Context())
	if !ok {
		err = fmt.Errorf("no session store found")
		return
	}
	//try to get the session
	session, err := store.Get(r, h.name)
	if err != nil {
		//save it to have the file
		err = store.Save(r, w, session)
		if err != nil {
			err = fmt.Errorf("unable to save session")
			return
		}
	}
	msg := "access denied"
	auth, ok := session.Values["auth"]
	if !ok { //user has never logged in
		err = errors.New(msg)
		return
	}
	authicated, _ := strconv.ParseBool(fmt.Sprint(auth))
	if !authicated {
		err = fmt.Errorf(msg)
		return
	}
	//user is logged in
	_permits, err := json.Marshal(session.Values["permits"])
	if err != nil {
		err = errors.New(msg)
		return
	}
	err = json.Unmarshal(_permits, &permits)
	if err != nil {
		err = errors.New("unable to load permits")
		return
	}
	return
}
