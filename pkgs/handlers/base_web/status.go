package base_web

import (
	"fmt"
	"net/http"
	"runtime/debug"

	"github.com/gorilla/mux"
	"gitlab.com/cesmanager/base-server/core/base_domain"
	"gitlab.com/cesmanager/base-server/pkgs/base_blocs"
	"gitlab.com/cesmanager/base-server/pkgs/base_repository"
)

type status struct {
	c base_domain.AppConfig
}

func NewStatus(c base_domain.AppConfig) *status {
	return &status{c: c}
}

// get currency by id
func (h *status) Get(w http.ResponseWriter, r *http.Request) {
	resp := NewResponse(w, r)

	defer func() {
		if err := recover(); err != nil {
			panic(fmt.Sprintf("%v\n\t%v", err, string(debug.Stack())))
		}
		resp.Execute(nil)
	}()
	db, close := base_repository.MustMySQL(h.c.DataBase[h.c.ActiveDB], r.Header.Get("coy-db"))
	defer close()

	vars := mux.Vars(r)
	//check if id is provided
	id, ok := vars["id"]
	if !ok {
		resp.SetError("error", "id is required")
		return
	}
	bloc := base_blocs.NewStatus(base_repository.NewStatus(db))

	data, err := bloc.Get(id)

	if err != nil {
		resp.SetError("error", err.Error())
		return
	}
	resp.SetBody(data)
}

func (h *status) List(w http.ResponseWriter, r *http.Request) {
	resp := NewResponse(w, r)
	//response callback
	defer func() {
		resp.Execute(nil)
	}()
	db, close := base_repository.MustMySQL(h.c.DataBase[h.c.ActiveDB], r.Header.Get("coy-db"))
	defer close()

	req := ParseRequest(h.c, r)
	params, page, size := req.PayLoadList()
	if _, ok := params["type"]; !ok {
		resp.SetError("error", "type is required")
		return
	}
	bloc := base_blocs.NewStatus(base_repository.NewStatus(db))
	list, err := bloc.List(params, req.OrderBy, page, size)
	if err != nil {
		resp.SetError("error", err.Error())
		return
	}
	resp.SetBody(list)
}

// get currency by id
func (h *status) Find(w http.ResponseWriter, r *http.Request) {
	resp := NewResponse(w, r)
	defer func() {
		if err := recover(); err != nil {
			panic(fmt.Sprintf("%v\n\t%v", err, string(debug.Stack())))
		}
		resp.Execute(nil)
	}()
	db, close := base_repository.MustMySQL(h.c.DataBase[h.c.ActiveDB], r.Header.Get("coy-db"))
	defer close()

	bloc := base_blocs.NewStatus(base_repository.NewStatus(db))
	params := ParseRequest(h.c, r).PayLoad()
	if _, ok := params["type"]; !ok {
		resp.SetError("error", "type is required")
		return
	}
	data, err := bloc.Find(params)
	if err != nil {
		resp.SetError("error", err.Error())
		return
	}
	resp.SetBody(data)
}

// Create a status
func (h *status) Create(w http.ResponseWriter, r *http.Request) {
	resp := NewResponse(w, r)
	defer func() {
		if err := recover(); err != nil {
			panic(fmt.Sprintf("%v\n\t%v", err, string(debug.Stack())))
		}
		resp.Execute(nil)
	}()
	db, close := base_repository.MustMySQL(h.c.DataBase[h.c.ActiveDB], r.Header.Get("coy-db"))
	defer close()

	params := ParseRequest(h.c, r).PayLoad()
	if _, ok := params["title"]; !ok {
		resp.SetError("title_error", "title is required")
		return
	}
	if _, ok := params["type"]; !ok {
		resp.SetError("type_error", "type is required")
		return
	}
	bloc := base_blocs.NewStatus(base_repository.NewStatus(db))
	data, err := bloc.Create(params)
	if err != nil {
		resp.SetError("error", err)
		return
	}
	resp.SetBody(data)
}

// Create a status
func (h *status) Update(w http.ResponseWriter, r *http.Request) {
	resp := NewResponse(w, r)

	defer func() {
		if err := recover(); err != nil {
			panic(fmt.Sprintf("%v\n\t%v", err, string(debug.Stack())))
		}
		resp.Execute(nil)
	}()
	db, close := base_repository.MustMySQL(h.c.DataBase[h.c.ActiveDB], r.Header.Get("coy-db"))
	defer close()

	params := ParseRequest(h.c, r).PayLoad()
	if _, ok := params["id"]; !ok {
		resp.SetError("error", "id is required")
		return
	}
	if _, ok := params["type"]; !ok {
		resp.SetError("type_error", "type is required")
		return
	}
	bloc := base_blocs.NewStatus(base_repository.NewStatus(db))
	data, err := bloc.Update(fmt.Sprint(params["id"]), params)
	if err != nil {
		resp.SetError("error", err)
		return
	}
	resp.SetBody(data)
}

func (h *status) Delete(w http.ResponseWriter, r *http.Request) {
	resp := NewResponse(w, r)

	defer func() {
		if err := recover(); err != nil {
			panic(fmt.Sprintf("%v\n\t%v", err, string(debug.Stack())))
		}
		resp.Execute(nil)
	}()
	db, close := base_repository.MustMySQL(h.c.DataBase[h.c.ActiveDB], r.Header.Get("coy-db"))
	defer close()

	//get parameters
	params := ParseRequest(h.c, r).PayLoad()
	if _, ok := params["id"]; !ok {
		resp.SetError("error", "id is required")
		return
	}
	bloc := base_blocs.NewStatus(base_repository.NewStatus(db))

	err := bloc.Delete(fmt.Sprint(params["id"]))

	if err != nil {
		resp.SetError("error", err)
		return
	}
	resp.SetBody(map[string]interface{}{"message": "deleted"})
}
