package base_web

import (
	"fmt"
	"net/http"

	"gitlab.com/cesmanager/base-server/core/base_domain"
	"gitlab.com/cesmanager/base-server/pkgs/base_blocs"
	"gitlab.com/cesmanager/base-server/pkgs/base_repository"
)

type fileManager struct {
	c base_domain.AppConfig
}

func NewFileManager(c base_domain.AppConfig) *fileManager {
	return &fileManager{c: c}
}

func (h *fileManager) Dir(w http.ResponseWriter, r *http.Request) {
	resp := NewResponse(w, r)
	defer func() {
		resp.Execute(nil)
	}()
	params := ParseRequest(h.c, r).PayLoad()
	if _, ok := params["folder"]; !ok {
		resp.SetError("error", "folder is required")
		return
	}
	fm := base_blocs.NewFileManager(Utilities(h.c, r), nil)
	//check permit
	err := resp.CheckPermit(*fm.Mod, "list")
	if err != nil {
		resp.SetError("error", err.Error())
		return
	}
	// err = resp.CheckPermit(*fm.Mod, "root")
	// if err != nil
	files, err := fm.Dir(fmt.Sprint(params["folder"]))
	if err != nil {
		resp.SetCleanError("error", err.Error())
		return
	}
	resp.SetBody(files)
}
func (h *fileManager) Upload(w http.ResponseWriter, r *http.Request) {
	resp := NewResponse(w, r)
	defer func() {
		resp.Execute(nil)
	}()
	db, close := base_repository.MustMySQL(h.c.DataBase[h.c.ActiveDB], r.Header.Get("coy-db"))
	defer close()
	user, ok := UserContext(r.Context())
	if !ok {
		resp.SetError("error", "unable to find user")
	}
	req := ParseRequest(h.c, r)
	params := req.PayLoad()
	if _, ok := params["folder"]; !ok {
		resp.SetError("error", "folder is required")
		return
	}
	tx := db.Begin()
	fm := base_blocs.NewFileManager(Utilities(h.c, r), base_repository.NewFileInfo(tx))
	//check permit
	err := resp.CheckPermit(*fm.Mod, "create")
	if err != nil {
		tx.Rollback()
		resp.SetError("error", err.Error())
		return
	}

	if file, fileHeader, err := req.FormFile("file"); err == nil {
		err = fm.Upload(file, fileHeader.Filename, fmt.Sprint(params["folder"]), fmt.Sprint(user.ID))
		if err != nil {
			tx.Rollback()
			resp.SetCleanError("error", err.Error())
			return
		}
	}
	tx.Commit()
	resp.SetBody(map[string]string{"msg": "upload successful"})
}

func (h *fileManager) New(w http.ResponseWriter, r *http.Request) {
	resp := NewResponse(w, r)
	defer func() {
		resp.Execute(nil)
	}()
	db, close := base_repository.MustMySQL(h.c.DataBase[h.c.ActiveDB], r.Header.Get("coy-db"))
	defer close()
	user, ok := UserContext(r.Context())
	if !ok {
		resp.SetError("error", "unable to find user")
	}
	params := ParseRequest(h.c, r).PayLoad()
	if _, ok := params["name"]; !ok {
		resp.SetError("name_error", "name is required")
	}
	if _, ok := params["path"]; !ok {
		resp.SetError("path_error", "path is required")
	}
	if _, ok := params["is_dir"]; !ok {
		resp.SetError("is_dir_error", "is_dir is required")
	}
	if resp.IsError() {
		return
	}
	tx := db.Begin()
	fm := base_blocs.NewFileManager(Utilities(h.c, r), base_repository.NewFileInfo(tx))
	//check permit
	err := resp.CheckPermit(*fm.Mod, "create")
	if err != nil {
		tx.Rollback()
		resp.SetError("error", err.Error())
		return
	}
	err = fm.New(fmt.Sprint(user.ProfileID), params)
	if err != nil {
		tx.Rollback()
		resp.SetCleanError("error", err.Error())
		return
	}
	tx.Commit()
	resp.SetBody(map[string]string{"msg": "rename successful"})
}
func (h *fileManager) Rename(w http.ResponseWriter, r *http.Request) {
	resp := NewResponse(w, r)
	defer func() {
		resp.Execute(nil)
	}()
	db, close := base_repository.MustMySQL(h.c.DataBase[h.c.ActiveDB], r.Header.Get("coy-db"))
	defer close()
	params := ParseRequest(h.c, r).PayLoad()
	if _, ok := params["old"]; !ok {
		resp.SetError("old_error", "old is required")
	}
	if _, ok := params["new"]; !ok {
		resp.SetError("new_error", "new is required")
	}
	if resp.IsError() {
		return
	}
	tx := db.Begin()
	fm := base_blocs.NewFileManager(Utilities(h.c, r), base_repository.NewFileInfo(tx))
	//check permit
	err := resp.CheckPermit(*fm.Mod, "update")
	if err != nil {
		tx.Rollback()
		resp.SetError("error", err.Error())
		return
	}
	err = fm.Rename(fmt.Sprint(params["old"]), fmt.Sprint(params["new"]))
	if err != nil {
		tx.Rollback()
		resp.SetCleanError("error", err.Error())
		return
	}
	tx.Commit()
	resp.SetBody(map[string]string{"msg": "rename successful"})
}

func (h *fileManager) Delete(w http.ResponseWriter, r *http.Request) {
	resp := NewResponse(w, r)
	defer func() {
		resp.Execute(nil)
	}()
	db, close := base_repository.MustMySQL(h.c.DataBase[h.c.ActiveDB], r.Header.Get("coy-db"))
	defer close()
	params := ParseRequest(h.c, r).PayLoad()
	if _, ok := params["paths"]; !ok {
		resp.SetError("error", "paths is required")
		return
	}
	tx := db.Begin()
	fm := base_blocs.NewFileManager(Utilities(h.c, r), base_repository.NewFileInfo(tx))
	//check permit
	err := resp.CheckPermit(*fm.Mod, "delete")
	if err != nil {
		tx.Rollback()
		resp.SetError("error", err.Error())
		return
	}
	if paths, ok := params["paths"].([]interface{}); ok {
		for _, path := range paths {
			err := fm.Delete(fmt.Sprint(path))
			if err != nil {
				tx.Rollback()
				resp.SetCleanError("error", err.Error())
				return
			}
		}
	} else {
		err := fm.Delete(fmt.Sprint(params["paths"]))
		if err != nil {
			tx.Rollback()
			resp.SetCleanError("error", err.Error())
			return
		}
	}
	tx.Commit()
	resp.SetBody(map[string]string{"msg": "delete successful"})
}
