package base_web

import (
	"fmt"
	"math/rand"
	"net/http"
	"runtime/debug"
	"strconv"
	"time"

	"github.com/gorilla/mux"
	"gitlab.com/cesmanager/base-server/core/base_domain"
	"gitlab.com/cesmanager/base-server/pkgs/base_blocs"
	"gitlab.com/cesmanager/base-server/pkgs/base_repository"
	samgotemplate "gitlab.com/sammybigboy440/samgo-template"
	"golang.org/x/crypto/bcrypt"
)

type User struct {
	c base_domain.AppConfig
}

func NewUser(c base_domain.AppConfig) *User {
	return &User{c: c}
}

func (h *User) List(w http.ResponseWriter, r *http.Request) {
	resp := NewResponse(w, r)
	//response callback
	defer func() {
		resp.Execute(nil)
	}()
	db, close := base_repository.MustMySQL(h.c.DataBase[h.c.ActiveDB], r.Header.Get("coy-db"))
	defer close()

	params := ParseRequest(h.c, r).PayLoad()
	//always delete count and pages so its no included in the query
	delete(params, "count")
	delete(params, "pages")
	page := "1"
	if _, ok := params["page"]; ok {
		page = fmt.Sprint(params["page"])
		delete(params, "page")
	}
	pageSize := "1"
	if _, ok := params["page_size"]; ok {
		pageSize = fmt.Sprint(params["page_size"])
		delete(params, "page_size")
	}
	order := map[string]interface{}{}
	qtServ := base_blocs.NewUser(base_repository.NewUser(db))
	//get current user
	_user, ok := UserContext(r.Context())
	if !ok {
		resp.SetError("error", "unable to get user context")
		return
	}
	if resp.CheckSelfPermit(*qtServ.Mod) {
		params["id"] = _user.ID
	}
	list, err := qtServ.List(params, order, page, pageSize)
	if err != nil {
		resp.SetError("error", err.Error())
		return
	}
	resp.SetBody(list)
}

// get user_group by id
func (h *User) Get(w http.ResponseWriter, r *http.Request) {
	resp := NewResponse(w, r)

	defer func() {
		if err := recover(); err != nil {
			panic(fmt.Sprintf("%v\n\t%v", err, string(debug.Stack())))
		}
		resp.Execute(nil)
	}()
	db, close := base_repository.MustMySQL(h.c.DataBase[h.c.ActiveDB], r.Header.Get("coy-db"))
	defer close()
	//get parameters
	params := mux.Vars(r)

	//check if id is provided
	if _, ok := params["id"]; !ok {
		resp.SetError("error", "id is required")
		return
	}

	userServ := base_blocs.NewUser(base_repository.NewUser(db))
	user, err := userServ.Get(fmt.Sprint(params["id"]))

	if err != nil {
		resp.SetError("error", err.Error())
		return
	}
	//remove the password
	user.Pword = ""
	resp.SetBody(user)
}

func (h *User) Create(w http.ResponseWriter, r *http.Request) {
	resp := NewResponse(w, r)
	var token string
	//response callback
	defer func() {
		if err := recover(); err != nil {
			panic(fmt.Sprintf("%v\n\t%v", err, string(debug.Stack())))
		}
		resp.Execute(&token)
	}()
	db, close := base_repository.MustMySQL(h.c.DataBase[h.c.ActiveDB], r.Header.Get("coy-db"))
	defer close()

	params := ParseRequest(h.c, r).PayLoad()
	if _, ok := params["uname"]; !ok {
		resp.SetError("uname_error", "username is required")
	}
	if _, ok := params["pword"]; !ok {
		resp.SetError("pword_error", "password is required")
	}
	if _, ok := params["profile_id"]; !ok {
		resp.SetError("profile_id_error", "profile is required")
	}
	if _, ok := params["status"]; !ok {
		resp.SetError("status_error", "status is required")
	}
	if resp.IsError() {
		resp.SetCode(http.StatusBadRequest)
		return
	}
	//begin a transaction
	tx := db.Begin()
	//check if the user exists
	userServ := base_blocs.NewUser(base_repository.NewUser(tx))
	musers, err := userServ.Find(map[string]interface{}{"uname": params["uname"]})
	if err != nil {
		resp.SetCode(http.StatusInternalServerError)
		resp.SetError("uname_error", "username error")
		return
	}
	if len(musers) > 0 {
		resp.SetCode(http.StatusBadRequest)
		resp.SetError("uname_error", "username is registered to another user")
		return
	}
	//check if the profile exists
	profileServ := base_blocs.NewProfile(base_repository.NewProfile(tx))
	profile, err := profileServ.Get(fmt.Sprint(params["profile_id"]))
	if err != nil || profile.ID == 0 {
		resp.SetError("profile_id_error", "profile error")
		return
	}

	//generate verification code
	rand := rand.New(rand.NewSource(time.Now().UnixNano()))
	params["vcode"] = strconv.Itoa(rand.Intn(999999-100000) + 100000)
	//create the user
	user, err := userServ.Create(params)
	if err != nil {
		resp.SetError("error", err.Error())
		tx.Rollback()
		return
	}
	outletID := ""
	//get the profile outlet
	if profile.OutletID != nil {
		outletID = fmt.Sprint((*profile.OutletID))
	}
	if outletID == "" {
		outletID = "1"
	}
	//send out email
	outletServ := base_blocs.NewOutlet(base_repository.NewOutlet(tx))
	outlet, err := outletServ.Get(outletID)
	if err != nil {
		resp.SetError("error", err.Error())
		tx.Rollback()
		return
	}
	settingService := base_blocs.NewSettings(base_repository.NewSettings(tx))
	settings, err := settingService.Get()
	if err != nil {
		resp.SetError("error", err.Error())
		tx.Rollback()
		return
	}
	utils := Utilities(h.c, r)
	//commit transaction
	tx.Commit()
	resp.SetBody(user)
	//send the verification email
	if profile.Email != nil {
		go h.sendMail(outlet.VerificationTemplate, settings, params, utils, *profile.Email)
	}
	if profile.AltEmail != nil {
		go h.sendMail(outlet.VerificationTemplate, settings, params, utils, *profile.AltEmail)
	}
}

// update user
func (h *User) Update(w http.ResponseWriter, r *http.Request) {
	resp := NewResponse(w, r)
	defer func() {
		if err := recover(); err != nil {
			panic(fmt.Sprintf("%v\n\t%v", err, string(debug.Stack())))
		}
		resp.Execute(nil)
	}()
	db, close := base_repository.MustMySQL(h.c.DataBase[h.c.ActiveDB], r.Header.Get("coy-db"))
	defer close()

	params := ParseRequest(h.c, r).PayLoad()
	//ensure profile is not changed in update
	delete(params, "profile_id")
	if _, ok := params["id"]; !ok {
		resp.SetError("id_error", "id is required")
	}
	if _, ok := params["uname"]; !ok {
		resp.SetError("uname_error", "username is required")
	}
	if _, ok := params["status"]; !ok {
		resp.SetError("status_error", "status is required")
	}
	if resp.IsError() {
		resp.SetCode(http.StatusBadRequest)
		return
	}
	//get current user
	_user, ok := UserContext(r.Context())
	if !ok {
		resp.SetError("error", "unable to get user context")
		return
	}
	//delete password key if its empty
	if _, ok := params["pword"]; ok && len(fmt.Sprint(params["pword"])) == 0 {
		delete(params, "pword")
	}
	userServ := base_blocs.NewUser(base_repository.NewUser(db))
	if resp.CheckSelfPermit(*userServ.Mod) {
		if fmt.Sprint(params["id"]) != fmt.Sprint(_user.ID) {
			resp.SetError("error", "you can only update your user record")
			return
		}
	}
	if _, ok := params["pword"]; ok {
		if _, ok := params["opword"]; !ok {
			resp.SetError("opword_error", "you must provide current password")
			return
		}
		_user, err := userServ.Get(fmt.Sprint(params["id"]))
		if err != nil {
			resp.SetCode(http.StatusUnauthorized)
			resp.SetError("error", "unable to get user record")
			return
		}
		err = bcrypt.CompareHashAndPassword([]byte(_user.Pword), []byte(fmt.Sprint(params["opword"])))
		if err != nil {
			resp.SetCode(http.StatusUnauthorized)
			resp.SetError("opword_error", "incorrect password")
			return
		}
	}

	//check if the username exist for another user
	musers, err := userServ.Find(map[string]interface{}{"id": map[string]interface{}{
		"val": params["id"], "con": "!=",
	}, "uname": params["uname"]})
	if err != nil {
		resp.SetCode(http.StatusInternalServerError)
		resp.SetError("uname_error", "username error")
		return
	}
	if len(musers) > 0 {
		resp.SetCode(http.StatusBadRequest)
		resp.SetError("uname_error", "username is registered to another user")
		return
	}
	//update the user
	user, err := userServ.Update(fmt.Sprint(params["id"]), params)
	if err != nil {
		resp.SetError("error", err.Error())
		return
	}
	user.Pword = "" //clean the encrypted password out
	resp.SetBody(user)
}

func (h *User) sendMail(template string, s base_domain.Settings, params map[string]interface{}, utils *base_blocs.Utilities, emails ...string) {
	//make the template
	st := samgotemplate.NewEngine(template, &params)
	msg, _ := st.Execute()
	//log the error
	m := base_blocs.NewSMTP(&s)
	err := m.Reciepents(emails).SetMsg("Account Verification", msg).Send()
	if err != nil {
		utils.Log(err.Error(), base_blocs.StdLog)
	}
}
