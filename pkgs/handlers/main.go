package routers

import (
	"fmt"
	"net/http"

	"gitlab.com/cesmanager/base-server/core/base_domain"
	"gitlab.com/cesmanager/base-server/pkgs/base_blocs"
	"gitlab.com/cesmanager/base-server/pkgs/handlers/base_web"
	"gitlab.com/cesmanager/base-server/pkgs/handlers/base_web/middleware"

	"github.com/gorilla/mux"
)

// Prepare all handlers
func Web(config base_domain.AppConfig) *mux.Router {
	//general socket
	hub := base_web.NewHub()
	go hub.Run(config)
	//initialize the midleware with session
	mws := middleware.New(config)
	//load the handlers
	account := base_web.NewAccount(config)
	auth := base_web.NewAuthentication(config)
	category := base_web.NewCategory(config)
	coy := base_web.NewCoy(config)
	currency := base_web.NewCurrency(config)
	customization := base_web.NewCustomization(config)
	fileRouter := base_web.NewFileRouter(config)
	fileServer := base_web.NewFileServer(config)
	fileManager := base_web.NewFileManager(config)
	form := base_web.NewForm(config)
	formRecord := base_web.NewFormRecord(config)
	geoLocation := base_web.NewGeoLocation(config)
	mail := base_web.NewMail(config)
	notification := base_web.NewNotification(config)
	notified := base_web.NewNotified(config)
	outlet := base_web.NewOutlet(config)
	profile := base_web.NewProfile(config)
	preference := base_web.NewPreference(config)
	user := base_web.NewUser(config)
	user_group := base_web.NewUserGroup(config)
	status := base_web.NewStatus(config)
	settings := base_web.NewSettings(config)

	r := mux.NewRouter()
	//Company
	r.HandleFunc("/api/coy", mws.Chain(coy.List, mws.AuthRoot(), mws.VetDomain())).Methods(http.MethodGet, http.MethodOptions)
	r.HandleFunc("/api/coy", mws.Chain(coy.Create, mws.AuthRoot(), mws.VetDomain())).Methods(http.MethodPost, http.MethodOptions)
	r.HandleFunc("/api/coy", mws.Chain(coy.Update, mws.AuthRoot(), mws.VetDomain())).Methods(http.MethodPut, http.MethodOptions)
	r.HandleFunc("/api/coy/info", mws.Chain(coy.Info)).Methods(http.MethodGet, http.MethodOptions)
	r.HandleFunc("/api/coy/setup", mws.Chain(coy.Setup)).Methods(http.MethodPost, http.MethodOptions)
	r.HandleFunc("/api/coy/features", mws.Chain(coy.Features, mws.AuthRoot(), mws.VetDomain())).Methods(http.MethodGet, http.MethodOptions)
	r.HandleFunc("/api/coy/{id:[0-9]+}", mws.Chain(coy.Get, mws.AuthRoot(), mws.VetDomain())).Methods(http.MethodGet, http.MethodOptions)
	r.HandleFunc("/api/coy/sync-feature", mws.Chain(coy.SyncFeature, mws.AuthRoot(), mws.VetDomain())).Methods(http.MethodGet, http.MethodOptions)
	r.HandleFunc("/api/coy/active-features", mws.Chain(coy.ActiveFeatures, mws.Auth(), mws.VetDomain())).Methods(http.MethodGet, http.MethodOptions)

	//authentication
	r.HandleFunc("/api/auth/signin", mws.Chain(auth.SignIn, mws.VetDomain())).Methods(http.MethodPost, http.MethodOptions)
	r.HandleFunc("/api/auth/signup", mws.Chain(auth.SignUp, mws.VetDomain())).Methods(http.MethodPost, http.MethodOptions)
	r.HandleFunc("/api/auth/verify", mws.Chain(auth.Verification, mws.VetDomain())).Methods(http.MethodPost, http.MethodOptions)
	r.HandleFunc("/api/auth/signout", mws.Chain(auth.Logout, mws.VetDomain())).Methods(http.MethodGet, http.MethodOptions)
	r.HandleFunc("/api/auth/vetlogin", mws.Chain(auth.VetLogin, mws.Auth(), mws.VetDomain())).Methods(http.MethodPost, http.MethodOptions)

	//user
	r.HandleFunc("/api/user", mws.Chain(user.List, mws.Auth(), mws.VetDomain())).Methods(http.MethodGet, http.MethodOptions)
	r.HandleFunc("/api/user", mws.Chain(user.Create, mws.Auth(), mws.VetDomain())).Methods(http.MethodPost, http.MethodOptions)
	r.HandleFunc("/api/user", mws.Chain(user.Update, mws.Auth(), mws.VetDomain())).Methods(http.MethodPut, http.MethodOptions)
	r.HandleFunc("/api/user/{id:[0-9]+}", mws.Chain(user.Get, mws.Auth(), mws.VetDomain())).Methods(http.MethodGet, http.MethodOptions)

	//user group
	r.HandleFunc("/api/user_group", mws.Chain(user_group.List, mws.Auth(), mws.VetDomain())).Methods(http.MethodGet, http.MethodOptions)
	r.HandleFunc("/api/user_group", mws.Chain(user_group.Create, mws.Auth(), mws.VetDomain())).Methods(http.MethodPost, http.MethodOptions)
	r.HandleFunc("/api/user_group", mws.Chain(user_group.Update, mws.Auth(), mws.VetDomain())).Methods(http.MethodPut, http.MethodOptions)
	r.HandleFunc("/api/user_group/find", mws.Chain(user_group.Find, mws.Auth(), mws.VetDomain())).Methods(http.MethodGet, http.MethodPost, http.MethodOptions)
	r.HandleFunc("/api/user_group/permit", mws.Chain(user_group.MyPermit, mws.Auth(), mws.VetDomain())).Methods(http.MethodGet, http.MethodPost, http.MethodOptions)
	r.HandleFunc("/api/user_group/{id:[0-9]+}", mws.Chain(user_group.Get, mws.Auth(), mws.VetDomain())).Methods(http.MethodGet, http.MethodOptions)

	//Profile
	r.HandleFunc("/api/profile", mws.Chain(profile.List, mws.Auth(), mws.VetDomain())).Methods(http.MethodGet, http.MethodOptions)
	r.HandleFunc("/api/profile", mws.Chain(profile.Create, mws.Auth(), mws.VetDomain())).Methods(http.MethodPost, http.MethodOptions)
	r.HandleFunc("/api/profile", mws.Chain(profile.Update, mws.Auth(), mws.VetDomain())).Methods(http.MethodPut, http.MethodOptions)
	r.HandleFunc("/api/profile/me", mws.Chain(profile.Get, mws.Auth(), mws.VetDomain())).Methods(http.MethodGet, http.MethodOptions)
	r.HandleFunc("/api/profile/find", mws.Chain(profile.Find, mws.Auth(), mws.VetDomain())).Methods(http.MethodGet, http.MethodPost, http.MethodOptions)
	r.HandleFunc("/api/profile/{id:[0-9]+}", mws.Chain(profile.Get, mws.Auth(), mws.VetDomain())).Methods(http.MethodGet, http.MethodOptions)

	//Preference
	r.HandleFunc("/api/preference", mws.Chain(preference.Store, mws.Auth(), mws.VetDomain())).Methods(http.MethodPut, http.MethodOptions)
	r.HandleFunc("/api/preference", mws.Chain(preference.Find, mws.Auth(), mws.VetDomain())).Methods(http.MethodGet, http.MethodPost, http.MethodOptions)
	r.HandleFunc("/api/preference/{profile_id:[0-9]+}", mws.Chain(preference.Get, mws.Auth(), mws.VetDomain())).Methods(http.MethodGet, http.MethodOptions)

	//Accounts
	r.HandleFunc("/api/account", mws.Chain(account.List, mws.Auth(), mws.VetDomain())).Methods(http.MethodGet, http.MethodOptions)
	r.HandleFunc("/api/account", mws.Chain(profile.Create, mws.Auth(), mws.VetDomain())).Methods(http.MethodPost, http.MethodOptions)
	r.HandleFunc("/api/account", mws.Chain(profile.Update, mws.Auth(), mws.VetDomain())).Methods(http.MethodPut, http.MethodOptions)
	r.HandleFunc("/api/account/me", mws.Chain(account.Get, mws.Auth(), mws.VetDomain())).Methods(http.MethodGet, http.MethodOptions)
	r.HandleFunc("/api/account/find", mws.Chain(account.Find, mws.Auth(), mws.VetDomain())).Methods(http.MethodGet, http.MethodPost, http.MethodOptions)
	r.HandleFunc("/api/account/{id:[0-9]+}", mws.Chain(account.Get, mws.Auth(), mws.VetDomain())).Methods(http.MethodGet, http.MethodOptions)

	//Currency
	r.HandleFunc("/api/currency", mws.Chain(currency.List, mws.Auth(), mws.VetDomain())).Methods(http.MethodGet, http.MethodOptions)
	r.HandleFunc("/api/currency", mws.Chain(currency.Create, mws.Auth(), mws.VetDomain())).Methods(http.MethodPost, http.MethodOptions)
	r.HandleFunc("/api/currency", mws.Chain(currency.Update, mws.Auth(), mws.VetDomain())).Methods(http.MethodPut, http.MethodOptions)
	r.HandleFunc("/api/currency/find", mws.Chain(currency.Find, mws.Auth(), mws.VetDomain())).Methods(http.MethodGet, http.MethodPost, http.MethodOptions)
	r.HandleFunc("/api/currency/{id:[0-9]+}", mws.Chain(currency.Get, mws.Auth(), mws.VetDomain())).Methods(http.MethodGet, http.MethodOptions)

	//Countries
	r.HandleFunc("/api/country", mws.Chain(geoLocation.List, mws.Auth(), mws.VetDomain())).Methods(http.MethodGet, http.MethodOptions)
	r.HandleFunc("/api/country", mws.Chain(geoLocation.Create, mws.Auth(), mws.VetDomain())).Methods(http.MethodPost, http.MethodOptions)
	r.HandleFunc("/api/country", mws.Chain(geoLocation.Update, mws.Auth(), mws.VetDomain())).Methods(http.MethodPut, http.MethodOptions)
	r.HandleFunc("/api/country/find", mws.Chain(geoLocation.Find, mws.Auth(), mws.VetDomain())).Methods(http.MethodGet, http.MethodPost, http.MethodOptions)
	r.HandleFunc("/api/country/{id:[0-9]+}", mws.Chain(geoLocation.Get, mws.Auth(), mws.VetDomain())).Methods(http.MethodGet, http.MethodOptions)
	r.HandleFunc("/api/country/timezones", mws.Chain(geoLocation.TimeZones, mws.VetDomain())).Methods(http.MethodGet, http.MethodOptions)

	//Categories
	r.HandleFunc("/api/category", mws.Chain(category.List, mws.Auth(), mws.VetDomain())).Methods(http.MethodGet, http.MethodOptions)
	r.HandleFunc("/api/category", mws.Chain(category.Create, mws.Auth(), mws.VetDomain())).Methods(http.MethodPost, http.MethodOptions)
	r.HandleFunc("/api/category", mws.Chain(category.Update, mws.Auth(), mws.VetDomain())).Methods(http.MethodPut, http.MethodOptions)
	r.HandleFunc("/api/category", mws.Chain(category.Delete, mws.Auth(), mws.VetDomain())).Methods(http.MethodDelete, http.MethodOptions)
	r.HandleFunc("/api/category/find", mws.Chain(category.Find, mws.Auth(), mws.VetDomain())).Methods(http.MethodGet, http.MethodPost, http.MethodOptions)
	r.HandleFunc("/api/category/{id:[0-9]+}", mws.Chain(category.Get, mws.Auth(), mws.VetDomain())).Methods(http.MethodGet, http.MethodOptions)

	//form route
	r.HandleFunc("/api/e-form", mws.Chain(form.List, mws.Auth(), mws.VetDomain())).Methods(http.MethodGet, http.MethodOptions)
	r.HandleFunc("/api/e-form", mws.Chain(form.Create, mws.Auth(), mws.VetDomain())).Methods(http.MethodPost, http.MethodOptions)
	r.HandleFunc("/api/e-form", mws.Chain(form.Update, mws.Auth(), mws.VetDomain())).Methods(http.MethodPut, http.MethodOptions)
	r.HandleFunc("/api/e-form/find", mws.Chain(form.Find, mws.VetDomain())).Methods(http.MethodPost, http.MethodOptions)
	r.HandleFunc("/api/e-form/{id:[0-9]+}", mws.Chain(form.Get, mws.VetDomain())).Methods(http.MethodGet, http.MethodOptions)

	//form record route
	r.HandleFunc("/api/e-form-record", mws.Chain(formRecord.List, mws.Auth(), mws.VetDomain())).Methods(http.MethodGet, http.MethodOptions)
	r.HandleFunc("/api/e-form-record", mws.Chain(formRecord.Create, mws.OptAuth(), mws.VetDomain())).Methods(http.MethodPost, http.MethodOptions)
	r.HandleFunc("/api/e-form-record", mws.Chain(formRecord.Update, mws.Auth(), mws.VetDomain())).Methods(http.MethodPut, http.MethodOptions)
	r.HandleFunc("/api/e-form-record/find", mws.Chain(formRecord.Find, mws.Auth(), mws.VetDomain())).Methods(http.MethodPost, http.MethodOptions)
	r.HandleFunc("/api/e-form-record/status", mws.Chain(formRecord.Status, mws.Auth(), mws.VetDomain())).Methods(http.MethodGet, http.MethodOptions)
	r.HandleFunc("/api/e-form-record/{id:[0-9]+}", mws.Chain(formRecord.Get, mws.Auth(), mws.VetDomain())).Methods(http.MethodGet, http.MethodOptions)

	//file manager
	r.HandleFunc("/api/file-manager", mws.Chain(fileManager.Dir, mws.Auth(), mws.VetDomain())).Methods(http.MethodPost, http.MethodOptions)
	r.HandleFunc("/api/file-manager", mws.Chain(fileManager.Upload, mws.Auth(), mws.VetDomain())).Methods(http.MethodPut, http.MethodOptions)
	r.HandleFunc("/api/file-manager/new", mws.Chain(fileManager.New, mws.Auth(), mws.VetDomain())).Methods(http.MethodPost, http.MethodOptions)
	r.HandleFunc("/api/file-manager/delete", mws.Chain(fileManager.Delete, mws.Auth(), mws.VetDomain())).Methods(http.MethodPost, http.MethodOptions)
	r.HandleFunc("/api/file-manager/rename", mws.Chain(fileManager.Rename, mws.Auth(), mws.VetDomain())).Methods(http.MethodPost, http.MethodOptions)

	//Outlet
	r.HandleFunc("/api/outlet", mws.Chain(outlet.List, mws.Auth(), mws.VetDomain())).Methods(http.MethodGet, http.MethodOptions)
	r.HandleFunc("/api/outlet", mws.Chain(outlet.Create, mws.Auth(), mws.VetDomain())).Methods(http.MethodPost, http.MethodOptions)
	r.HandleFunc("/api/outlet", mws.Chain(outlet.Update, mws.Auth(), mws.VetDomain())).Methods(http.MethodPut, http.MethodOptions)
	r.HandleFunc("/api/outlet/find", mws.Chain(outlet.Find, mws.Auth(), mws.VetDomain())).Methods(http.MethodGet, http.MethodPost, http.MethodOptions)
	r.HandleFunc("/api/outlet/{id:[0-9]+}", mws.Chain(outlet.Get, mws.Auth(), mws.VetDomain())).Methods(http.MethodGet, http.MethodOptions)

	//notification
	r.HandleFunc("/api/notification", mws.Chain(notification.List, mws.VetDomain())).Methods(http.MethodGet, http.MethodOptions)
	r.HandleFunc("/api/notification", mws.Chain(notification.Create, mws.Auth(), mws.VetDomain())).Methods(http.MethodPost, http.MethodOptions)
	r.HandleFunc("/api/notification", mws.Chain(notification.Update, mws.Auth(), mws.VetDomain())).Methods(http.MethodPut, http.MethodOptions)
	r.HandleFunc("/api/notification", mws.Chain(notification.Delete, mws.Auth(), mws.VetDomain())).Methods(http.MethodDelete, http.MethodOptions)
	r.HandleFunc("/api/notification/{id:[0-9]+}", mws.Chain(notification.Get, mws.VetDomain())).Methods(http.MethodGet, http.MethodOptions)

	//notified
	r.HandleFunc("/api/notified", mws.Chain(notified.List, mws.VetDomain())).Methods(http.MethodGet, http.MethodOptions)
	r.HandleFunc("/api/notified", mws.Chain(notified.Delete, mws.Auth(), mws.VetDomain())).Methods(http.MethodDelete, http.MethodOptions)
	r.HandleFunc("/api/notified", mws.Chain(notified.Read, mws.VetDomain())).Methods(http.MethodPost, http.MethodOptions)

	//Status
	r.HandleFunc("/api/status", mws.Chain(status.List, mws.VetDomain())).Methods(http.MethodGet, http.MethodOptions)
	r.HandleFunc("/api/status", mws.Chain(status.Create, mws.Auth(), mws.VetDomain())).Methods(http.MethodPost, http.MethodOptions)
	r.HandleFunc("/api/status", mws.Chain(status.Update, mws.Auth(), mws.VetDomain())).Methods(http.MethodPut, http.MethodOptions)
	r.HandleFunc("/api/status", mws.Chain(status.Delete, mws.Auth(), mws.VetDomain())).Methods(http.MethodDelete, http.MethodOptions)
	r.HandleFunc("/api/status/find", mws.Chain(status.Find, mws.VetDomain())).Methods(http.MethodGet, http.MethodPost, http.MethodOptions)
	r.HandleFunc("/api/status/{id:[0-9]+}", mws.Chain(status.Get, mws.VetDomain())).Methods(http.MethodGet, http.MethodOptions)

	//Settings
	r.HandleFunc("/api/settings", mws.Chain(settings.Get, mws.Auth(), mws.VetDomain())).Methods(http.MethodGet, http.MethodOptions)
	r.HandleFunc("/api/settings", mws.Chain(settings.Update, mws.Auth(), mws.VetDomain())).Methods(http.MethodPut, http.MethodOptions)
	r.HandleFunc("/api/settings/test-smtp", mws.Chain(mail.Test, mws.Auth(), mws.VetDomain())).Methods(http.MethodGet, http.MethodOptions)

	//file manager routes
	r.HandleFunc("/api/file_manager/upload", mws.Chain(fileRouter.Upload, mws.Auth(), mws.VetDomain())).Methods(http.MethodPost, http.MethodPut, http.MethodOptions)
	r.HandleFunc("/api/file_manager/record_files", mws.Chain(fileRouter.RecFiles, mws.Auth(), mws.VetDomain())).Methods(http.MethodPost, http.MethodOptions)

	//handle that sends custom requests to a customized module
	r.PathPrefix("/api/custom/{addon}").HandlerFunc(mws.Chain(customization.Navigate, mws.VetDomain()))
	//serve static files
	r.PathPrefix("/api/static").Handler(http.StripPrefix("/api/static", mws.Chain(fileServer.Serve, mws.VetDomain())))

	//serve the base_web
	r.PathPrefix("/base_web").Handler(http.StripPrefix("/base_web", mws.Chain(fileServer.Serve, mws.VetDomain())))

	//handle system socket requests
	r.HandleFunc("/api/ws", mws.Chain(func(w http.ResponseWriter, r *http.Request) {
		base_web.ServeWs(hub, config, w, r)
	}, mws.VetDomain()))

	//run server
	host := "{subdomain}." + config.Domain
	r.Host(host)
	utils := base_blocs.NewUtils(config, nil)
	utils.Log(fmt.Sprint("Host: ", host), base_blocs.StdLog)
	return r
}
