package base_repository

import (
	"fmt"
	"strconv"

	"gitlab.com/cesmanager/base-server/core/base_domain"
	"gitlab.com/cesmanager/base-server/core/base_port"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

type coyInfo struct {
	db *gorm.DB
}

func NewCoyInfo(db *gorm.DB) base_port.CoyInfo {
	loadstatus := !db.Migrator().HasTable(&base_domain.Status{})
	db.AutoMigrate(&base_domain.Feature{})
	//migrate coyinfo table
	db.AutoMigrate(&base_domain.CoyInfo{})
	if loadstatus {
		status := base_domain.Status{}
		for _, s := range status.BaseInitialize() {
			db.Create(&s)
		}
	}

	return &coyInfo{
		db: db,
	}
}
func NewCCoyInfo(db *gorm.DB) base_port.CoyInfo {
	return &coyInfo{
		db: db,
	}
}

func (r *coyInfo) Get(id string) (base_domain.CoyInfo, error) {
	var coy base_domain.CoyInfo
	resp := r.db.Preload(clause.Associations).Where("id = ?", id).Find(&coy)
	if resp.Error != nil {
		return coy, resp.Error
	}
	return coy, nil
}

func (r *coyInfo) Store(coy *base_domain.CoyInfo) error {
	resp := r.db.Create(coy)
	if resp.Error != nil {
		return resp.Error
	}
	return nil
}

func (r *coyInfo) Update(coy *base_domain.CoyInfo) error {
	resp := r.db.Model(base_domain.CoyInfo{}).Where("id = ?", coy.ID).Updates(coy)
	if resp.Error != nil {
		return resp.Error
	}
	return nil
}

func (r *coyInfo) List(param map[string]interface{}, order map[string]interface{}, page int, pageSize int) (base_domain.List, error) {
	var items []base_domain.CoyInfo
	list := base_domain.List{Page: page, PageSize: pageSize, Items: []interface{}{}}
	db := r.db
	db = db.Scopes(Where(param))
	//get count
	err := db.Model(&items).Count(&list.Count).Error
	if err != nil {
		return list, err
	}
	db = db.Preload(clause.Associations)
	for column, desc := range order {
		d, _ := strconv.ParseBool(fmt.Sprint(desc))
		db = db.Order(clause.OrderByColumn{Column: clause.Column{Name: column}, Desc: d})
	}
	err = db.Scopes(Paginate(page, pageSize)).Find(&items).Error
	if err != nil {
		return list, err
	}
	for _, item := range items {
		list.Items = append(list.Items, item)
	}
	return list, err
}

func (r *coyInfo) Find(param map[string]interface{}) ([]base_domain.CoyInfo, error) {
	var coys []base_domain.CoyInfo
	db := r.db
	resp := db.Scopes(Where(param)).Find(&coys)
	if resp.Error != nil {
		return coys, resp.Error
	}
	return coys, nil
}
