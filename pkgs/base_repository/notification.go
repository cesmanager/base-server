package base_repository

import (
	"gitlab.com/cesmanager/base-server/core/base_domain"
	"gitlab.com/cesmanager/base-server/core/base_port"
	"gorm.io/gorm"
)

type notification struct {
	db *gorm.DB
}

func NewNotification(db *gorm.DB) base_port.Notification {
	return &notification{
		db: db,
	}
}

func (r *notification) Get(id string) (base_domain.Notification, error) {
	var model base_domain.Notification
	err := r.db.Where("id = ?", id).
		Preload("CreatedBy", func(db *gorm.DB) *gorm.DB {
			return db.Select("id", "fname", "mname", "lname")
		}).Find(&model).Error
	return model, err
}

func (r *notification) Store(model *base_domain.Notification) error {
	return r.db.Save(model).Error
}

func (r *notification) Update(model *base_domain.Notification) error {
	resp := r.db.Model(base_domain.Notification{}).Where("id = ?", model.ID).Updates(model)
	if resp.Error != nil {
		return resp.Error
	}
	return nil
}

func (r *notification) List(params map[string]interface{}, order map[string]interface{}, page int, pageSize int) (base_domain.List, error) {
	var items []base_domain.Notification
	list := base_domain.List{Page: page, PageSize: pageSize, Items: []interface{}{}}
	db := r.db
	err := db.Model(&items).Count(&list.Count).Error
	if err != nil {
		return list, err
	}
	err = db.Scopes(Order(&order), Paginate(page, pageSize)).
		Preload("CreatedBy", func(db *gorm.DB) *gorm.DB {
			return db.Select("id", "fname", "mname", "lname")
		}).Find(&items).Error
	if err != nil {
		return list, err
	}
	for _, item := range items {
		list.Items = append(list.Items, item)
	}
	return list, err
}

func (r *notification) Delete(id []string) error {
	return r.db.Unscoped().Delete(&base_domain.Notification{}, id).Error
}
