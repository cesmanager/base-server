package base_repository

import (
	"fmt"

	"gitlab.com/cesmanager/base-server/core/base_domain"
	"gitlab.com/cesmanager/base-server/core/base_port"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

type eventMember struct {
	db *gorm.DB
}

func NewEventMember(db *gorm.DB) base_port.EventMemeber {
	return &eventMember{
		db: db,
	}
}
func (r *eventMember) Read(eventID string, profileID string) (base_domain.Notified, error) {
	var model base_domain.Notified
	err := r.db.Where("event_id = ? AND profile_id = ?", eventID, profileID).Preload(clause.Associations).Find(&model).Error
	return model, err
}
func (r *eventMember) InviteAll(eventID string) error {
	return r.InviteCustom(r.db.ToSQL(func(tx *gorm.DB) *gorm.DB {
		return r.db.Model(base_domain.Profile{}).
			Select(fmt.Sprint("'", eventID, "' AS event_id"), "`profile_id`, '0' AS `host`, '0' AS `cohost`, NOW(), NOW()")
	}))
}
func (r *eventMember) InviteProfiles(eventID string, profileID []string, host bool, cohost bool) error {
	_host := 0
	_cohost := 0
	if host {
		_host = 1
	}
	if cohost {
		_cohost = 1
	}
	return r.InviteCustom(r.db.ToSQL(func(tx *gorm.DB) *gorm.DB {
		return r.db.Model(base_domain.Profile{}).
			Select(fmt.Sprint("'", eventID, "' AS event_id"), "`profile_id`", fmt.Sprint("'", _host, "' AS `host`"), fmt.Sprint("'", _cohost, "' AS `cohost`"), "NOW(), NOW()").
			Where("id IN ?", profileID)
	}))
}
func (r *eventMember) InviteUserGroup(eventID string, userGroupID []string) error {
	return r.InviteCustom(r.db.ToSQL(func(tx *gorm.DB) *gorm.DB {
		return r.db.Model(base_domain.User{}).
			Distinct(fmt.Sprint("'", eventID, "' AS event_id"), "`profile_id`, '0' AS `host`, '0' AS `cohost`, NOW(), NOW()").
			Where("user_group_id IN ?", userGroupID)
	}))
}
func (r *eventMember) InviteCustom(query string) error {
	stmt := &gorm.Statement{DB: r.db}
	stmt.Parse(&base_domain.EventMember{})
	event_member_tbl := stmt.Schema.Table
	err := r.db.Exec(
		fmt.Sprint("INSERT INTO `", event_member_tbl, "` (`event_id`, `profile_id`, `host`, `cohost`, `created_at`, `updated_at`) ?"),
		query,
	).Error
	return err
}
func (r *eventMember) Update(model *base_domain.EventMember) error {
	return r.db.Model(base_domain.EventMember{}).
		Where("event_id = ? AND profile_id = ?", model.EventID, model.ProfileID).
		Updates(model).Error
}
func (r *eventMember) List(params map[string]interface{}, order map[string]interface{}, page int, pageSize int) (base_domain.List, error) {
	var items []base_domain.EventMember
	list := base_domain.List{Page: page, PageSize: pageSize, Items: []interface{}{}}
	db := r.db.Scopes(Where(params))
	err := db.Model(&items).Count(&list.Count).Error
	if err != nil {
		return list, err
	}
	err = db.Scopes(Order(&order), Paginate(page, pageSize)).
		Preload("Event", func(db *gorm.DB) *gorm.DB {
			return db.Select("id", "title", "LEFT(message, 100) AS message")
		}).Find(&items).Error
	if err != nil {
		return list, err
	}
	for _, item := range items {
		list.Items = append(list.Items, item)
	}
	return list, err
}

// to be fixed
func (r *eventMember) Find(params map[string]interface{}) ([]base_domain.EventMember, error) {
	items := []base_domain.EventMember{}
	err := r.db.Preload(clause.Associations).InnerJoins("Event").Scopes(Where(params)).Find(&items).Error
	return items, err
}
func (r *eventMember) Delete(eventID string, profileID string) error {
	return r.db.Where("event_id = ? AND profile_id = ?", eventID, profileID).Delete(&base_domain.EventMember{}).Error
}
