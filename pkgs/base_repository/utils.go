package base_repository

import (
	"fmt"
	"regexp"
	"strconv"
	"strings"

	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

func Paginate(page int, limit int) func(db *gorm.DB) *gorm.DB {
	return func(db *gorm.DB) *gorm.DB {
		if page <= 0 {
			page = 1
		}
		if limit <= 0 {
			page = 10
		}

		offset := (page - 1) * limit
		return db.Offset(offset).Limit(limit)
	}
}

func LikeSearch(text string, columns ...string) func(db *gorm.DB) *gorm.DB {
	re, _ := regexp.Compile(`[^ [:alnum:]]`)
	text = fmt.Sprint("%", re.ReplaceAllString(text, ""), "%")
	return func(db *gorm.DB) *gorm.DB {
		if text == "%%" {
			return db
		}
		for i, col := range columns {
			if i == 0 {
				db = db.Where(fmt.Sprint(col, " LIKE ?"), text)
			} else {
				db = db.Or(fmt.Sprint(col, " LIKE ?"), text)
			}
		}
		return db
	}
}

func FullTextSearch(text string, columns ...string) func(db *gorm.DB) *gorm.DB {
	var output strings.Builder
	pChar := ' '
	for _, char := range text {
		if char == ' ' && pChar == ' ' { // Replace double spaces with a single space
			continue
		}
		output.WriteRune(char)
		pChar = char
	}
	text = output.String()
	re := regexp.MustCompile(`[+-<>~]?[ [:alnum:]]+`)
	text = strings.Join(re.FindAllString(text, -1), " ")
	mode := "WITH QUERY EXPANSION"
	if mtc, _ := regexp.MatchString(`[+-<>~*]`, text); mtc {
		mode = "IN BOOLEAN MODE"
	}
	return func(db *gorm.DB) *gorm.DB {
		if len(text) == 0 {
			return db
		}
		return db.Where(fmt.Sprint("MATCH (", strings.Join(columns, ","), ") AGAINST ('", text, "' ", mode, ")"))
	}
}

func Where(params map[string]interface{}) func(db *gorm.DB) *gorm.DB {
	return func(db *gorm.DB) *gorm.DB {
		for k, v := range params {
			//handle find with condition
			if vc, ok := v.(map[string]interface{}); ok {
				if _, ok := vc["val"]; !ok {
					db = db.Where(k + " IS NULL")
					continue
				}
				if _, ok := vc["con"]; !ok {
					vc["con"] = "="
				}
				switch vc["con"] {
				case "in", "not in", "IN", "NOT IN":
					db = db.Where(fmt.Sprint(k, " ", vc["con"], " (?)"), vc["val"])
				default:
					db = db.Where(fmt.Sprint(k, " ", vc["con"], " ?"), vc["val"])
				}
				continue
			}
			fmt.Println(strings.ToLower(fmt.Sprint(v)))
			switch strings.ToLower(fmt.Sprint(v)) {
			case "null", "nil", fmt.Sprint(nil):
				db = db.Where(k + " IS NULL")
			default:
				db = db.Where(k+" = ?", v)
			}
		}
		return db
	}
}
func Preload(preload interface{}) func(db *gorm.DB) *gorm.DB {
	return func(db *gorm.DB) *gorm.DB {
		if pre, ok := preload.(string); ok {
			db = db.Preload(pre)
		}
		if pre, ok := preload.([]string); ok {
			for _, v := range pre {
				db = db.Preload(v)
			}
		}
		if pre, ok := preload.(map[string]interface{}); ok {
			for k, v := range pre {
				if vc, ok := v.(map[string]interface{}); ok {
					if _, ok := vc["val"]; !ok {
						db = db.Where(k + " IS NULL")
						continue
					}
					if _, ok := vc["con"]; !ok {
						vc["con"] = "="
					}
					switch vc["con"] {
					case "in", "not in", "IN", "NOT IN":
						db = db.Preload(k, func(db *gorm.DB) *gorm.DB {
							return db.Where(fmt.Sprint(" `", vc["col"], "` ",
								vc["con"], " (",
								strings.Replace(fmt.Sprint("'", vc["val"], "'"), ",", "','", -1), ")"),
							)
						})
					default:
						db = db.Preload(k, func(db *gorm.DB) *gorm.DB {
							return db.Where(fmt.Sprint(vc["col"], " ", vc["con"], " ?"), vc["val"])
						})
					}
					continue
				}
			}
		}
		return db
	}
}

func Order(order *map[string]interface{}) func(db *gorm.DB) *gorm.DB {
	return func(db *gorm.DB) *gorm.DB {
		if order == nil {
			return db
		}
		for column, desc := range *order {
			if column == desc {
				db.Order(desc)
				continue
			}
			d, _ := strconv.ParseBool(fmt.Sprint(desc))
			db = db.Order(clause.OrderByColumn{Column: clause.Column{Name: column}, Desc: d})
		}
		return db
	}
}

func TableName(db *gorm.DB, model interface{}) (string, error) {
	//make raw statement
	stmt := &gorm.Statement{DB: db}
	//select all attempt
	err := stmt.Parse(&model)
	return stmt.Schema.Table, err
}
