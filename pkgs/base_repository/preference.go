package base_repository

import (
	"gitlab.com/cesmanager/base-server/core/base_domain"
	"gitlab.com/cesmanager/base-server/core/base_port"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

type preference struct {
	db *gorm.DB
}

func NewPreference(db *gorm.DB) base_port.Preference {
	return &preference{
		db: db,
	}
}

func (r *preference) Find(profile_id string, key string) (base_domain.Preference, error) {
	var pref base_domain.Preference
	err := r.db.Preload(clause.Associations).
		Where("profile_id = ? AND key = ?", profile_id, key).
		Find(&pref).Error
	return pref, err
}

func (r *preference) Get(profile_id string) ([]base_domain.Preference, error) {
	var prefs []base_domain.Preference
	err := r.db.Where("profile_id = ?", profile_id).Find(&prefs).Error
	return prefs, err
}

func (r *preference) Store(preferences *[]base_domain.Preference) error {
	return r.db.Save(preferences).Error
}
