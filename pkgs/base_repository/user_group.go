package base_repository

import (
	"fmt"
	"strconv"
	"strings"

	"gitlab.com/cesmanager/base-server/core/base_domain"
	"gitlab.com/cesmanager/base-server/core/base_port"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

type userGroup struct {
	db *gorm.DB
}

func NewUserGroup(db *gorm.DB) base_port.UserGroup {
	return &userGroup{db: db}
}

func (r *userGroup) Get(id string) (base_domain.UserGroup, error) {
	var userGroup base_domain.UserGroup
	err := r.db.Preload(clause.Associations).Where("id = ?", id).Find(&userGroup).Error
	return userGroup, err
}
func (r *userGroup) Store(userGroup *base_domain.UserGroup) error {
	return r.db.Create(userGroup).Error
}
func (r *userGroup) Update(userGroup *base_domain.UserGroup) error {
	return r.db.Model(base_domain.UserGroup{}).Select("*").Where("id = ?", userGroup.ID).Updates(userGroup).Error
}

func (r *userGroup) List(param map[string]interface{}, order map[string]interface{}, page int, pageSize int) (base_domain.List, error) {
	var items []base_domain.UserGroup
	list := base_domain.List{Page: page, PageSize: pageSize, Items: []interface{}{}}
	db := r.db
	for k, v := range param {
		switch strings.ToLower(fmt.Sprint(v)) {
		case "null", "nil":
			db = db.Where(k + " IS NULL")
		default:
			db = db.Where(k+" = ?", v)
		}
	}
	err := db.Model(&items).Count(&list.Count).Error
	if err != nil {
		return list, err
	}
	db = db.Preload(clause.Associations)
	for column, desc := range order {
		d, _ := strconv.ParseBool(fmt.Sprint(desc))
		db = db.Order(clause.OrderByColumn{Column: clause.Column{Name: column}, Desc: d})
	}
	err = db.Scopes(Paginate(page, pageSize)).Find(&items).Error
	if err != nil {
		return list, err
	}
	for _, item := range items {
		list.Items = append(list.Items, item)
	}

	return list, err
}

func (r *userGroup) GetByUser(user_id string) (base_domain.UserGroup, error) {
	//get the user first
	user_repo := NewUser(r.db)
	user, err := user_repo.Get(user_id)
	if err != nil {
		return base_domain.UserGroup{}, err
	}
	return r.Get(fmt.Sprint(user.UserGroupID))
}

func (r *userGroup) Find(param map[string]interface{}) ([]base_domain.UserGroup, error) {
	var users []base_domain.UserGroup
	db := r.db
	for k, v := range param {
		switch strings.ToLower(fmt.Sprint(v)) {
		case "null", "nil":
			db = r.db.Where(k + " IS NULL")
		default:
			db = r.db.Where(k+" = ?", v)
		}
	}
	err := db.Preload(clause.Associations).Find(&users).Error
	return users, err
}
