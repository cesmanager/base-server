package base_repository

import (
	"errors"
	"fmt"
	"strings"

	"gitlab.com/cesmanager/base-server/core/base_domain"
	"gitlab.com/cesmanager/base-server/core/base_port"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

type userGroupPermit struct {
	db *gorm.DB
}

func NewUserGroupPermit(db *gorm.DB) base_port.UserGroupPermit {
	return &userGroupPermit{db: db}
}

func (r *userGroupPermit) Store(userGroupPermit *base_domain.UserGroupPermit) error {
	return r.db.Clauses(clause.OnConflict{
		UpdateAll: true,
	}).Create(userGroupPermit).Error
}

func (r *userGroupPermit) Find(param map[string]interface{}) ([]base_domain.UserGroupPermit, error) {
	var data []base_domain.UserGroupPermit
	db := r.db
	for k, v := range param {
		switch strings.ToLower(fmt.Sprint(v)) {
		case "null", "nil":
			db = r.db.Where(k + " IS NULL")
		default:
			db = r.db.Where(k+" = ?", v)
		}
	}
	err := db.Preload(clause.Associations).Find(&data).Error
	return data, err
}

func (r *userGroupPermit) GetPermit(user_id string, mod string) (base_domain.UserGroupPermit, error) {
	//get the user first
	user_repo := NewUser(r.db)
	user, err := user_repo.Get(user_id)
	if err != nil {
		return base_domain.UserGroupPermit{}, err
	}
	permits, err := r.Find(map[string]interface{}{"user_group_id": user.UserGroupID, "mod": mod})
	if err != nil {
		return base_domain.UserGroupPermit{}, err
	}
	if len(permits) == 0 {
		return base_domain.UserGroupPermit{}, errors.New("no permission found")
	}
	return permits[0], nil
}
