package base_repository

import (
	"context"
	"fmt"
	"net"

	sql "github.com/go-sql-driver/mysql"
	"gitlab.com/cesmanager/base-server/core/base_domain"
	"golang.org/x/crypto/ssh"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
)

type SSHDialer struct {
	client *ssh.Client
}

func (s *SSHDialer) DialContext(_ context.Context, addr string) (net.Conn, error) {
	return s.client.Dial("tcp", addr)
}

func MySQL(dbcon base_domain.DB, dbName string) (db *gorm.DB, close func(), err error) {
	//connect to baseDB if none is specified
	if dbName == "" {
		//try to create the baseDB
		MakeSchema(dbcon, dbcon.BaseDB)
		db, err = requestConnection(dbcon, dbcon.BaseDB)
	} else {
		db, err = requestConnection(dbcon, dbName)
	}
	close = func() {
		//defer close
		_db, _ := db.DB()
		_db.Close()
	}
	return
}
func MustMySQL(dbcon base_domain.DB, dbName string) (db *gorm.DB, close func()) {
	db, close, err := MySQL(dbcon, dbName)
	if err != nil {
		panic(err.Error())
	}
	return
}

func requestConnection(dbcon base_domain.DB, dbName string) (*gorm.DB, error) {
	proto := "tcp" //set protocal
	if dbcon.SShTag != "" {
		proto = fmt.Sprint(dbcon.SShTag, "+", proto) //use new protocal for ssh
	}
	dsn := fmt.Sprintf("%s:%s@%s(%s:%s)/%s?charset=utf8&parseTime=True&loc=Local",
		dbcon.User, dbcon.Password, proto, dbcon.Host, dbcon.Port, dbName)
	opts := mysql.Config{
		DSN:                       dsn,   // data source name
		DefaultStringSize:         256,   // default size for string fields
		DisableDatetimePrecision:  true,  // disable datetime precision, which not supported before MySQL 5.6
		DontSupportRenameIndex:    true,  // drop & create when rename index, rename index not supported before MySQL 5.7, MariaDB
		DontSupportRenameColumn:   false, // `change` when rename column, rename column not supported before MySQL 8, MariaDB
		SkipInitializeWithVersion: false, // auto configure based on currently MySQL version
	}
	logmode := logger.Default.LogMode(logger.Error)
	if dbcon.Debug {
		logmode = logger.Default.LogMode(logger.Info)
	}
	// refer https://github.com/go-sql-driver/mysql#dsn-data-source-name for details
	db, err := gorm.Open(mysql.New(opts), &gorm.Config{
		SkipDefaultTransaction: true,
		Logger:                 logmode,
	})
	if err != nil {
		return db, err
	}
	return db, nil
}

// MakeSchema ...
func MakeSchema(dbcon base_domain.DB, name string) error {
	db, err := requestConnection(dbcon, "")
	if err != nil {
		return err
	}
	connection, err := db.DB()
	if err != nil {
		return err
	}
	defer connection.Close()

	statement := "CREATE DATABASE IF NOT EXISTS  `" + name + "`  /*!40100 COLLATE 'utf8mb4_general_ci' */;"
	db.Exec(statement)
	return nil
}

func RemoveSchema(dbcon base_domain.DB, name string) error {
	db, err := requestConnection(dbcon, "")
	if err != nil {
		return err
	}
	connection, err := db.DB()
	if err != nil {
		return err
	}
	defer connection.Close()

	statement := "DROP DATABASE `" + name + "`;"
	resp := db.Exec(statement)
	if resp.Error != nil {
		return resp.Error
	}
	return nil
}

// register an ssh dailer for database
func RegisterDBSSH(ssh *ssh.Client, SShTag string) {
	sql.RegisterDialContext(SShTag+"+tcp", (&SSHDialer{ssh}).DialContext)
}
