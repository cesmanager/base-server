package base_repository

import (
	"fmt"
	"strings"

	"gitlab.com/cesmanager/base-server/core/base_domain"
	"gitlab.com/cesmanager/base-server/core/base_port"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

type debit struct {
	db *gorm.DB
}

func NewDebit(db *gorm.DB) base_port.Debit {
	return &debit{
		db: db,
	}
}

func (r *debit) Get(id string) (base_domain.Debit, error) {
	var debit base_domain.Debit
	resp := r.db.Preload(clause.Associations).Where("id = ?", id).Find(&debit)
	if resp.Error != nil {
		return debit, resp.Error
	}
	return debit, nil
}

func (r *debit) Store(debit *base_domain.Debit) error {
	resp := r.db.Create(debit)
	if resp.Error != nil {
		return resp.Error
	}
	return nil
}

func (r *debit) Update(debit *base_domain.Debit) error {
	resp := r.db.Model(base_domain.Debit{}).Where("id = ?", debit.ID).Updates(debit)
	if resp.Error != nil {
		return resp.Error
	}
	return nil
}

func (r *debit) Find(param map[string]interface{}) ([]base_domain.Debit, error) {
	var debits []base_domain.Debit
	db := r.db
	for k, v := range param {
		switch strings.ToLower(fmt.Sprint(v)) {
		case "null", "nil":
			db = r.db.Where(k + " IS NULL")
		default:
			db = r.db.Where(k+" = ?", v)
		}
	}
	resp := db.Find(&debits)
	if resp.Error != nil {
		return debits, resp.Error
	}
	return debits, nil
}
