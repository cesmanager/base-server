package base_repository

import (
	"fmt"

	"gitlab.com/cesmanager/base-server/core/base_domain"
	"gitlab.com/cesmanager/base-server/core/base_port"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

type notified struct {
	db *gorm.DB
}

func NewNotified(db *gorm.DB) base_port.Notified {
	return &notified{
		db: db,
	}
}

func (r *notified) Read(notificationID string, profileID string) (base_domain.Notified, error) {
	var model base_domain.Notified
	err := r.db.Where("notification_id = ? AND profile_id = ?", notificationID, profileID).Preload(clause.Associations).Find(&model).Error
	return model, err
}

func (r *notified) NotifyAll(notificationID string) error {
	return r.NotifyCustom(r.db.ToSQL(func(tx *gorm.DB) *gorm.DB {
		return r.db.Model(base_domain.Profile{}).
			Select(fmt.Sprint("'", notificationID, "' AS `notification_id`"), "id AS `profile_id`, NOW(), NOW()")
	}))
}

func (r *notified) NotifyProfiles(notificationID string, profileID []string) error {
	return r.NotifyCustom(r.db.ToSQL(func(tx *gorm.DB) *gorm.DB {
		return r.db.Model(base_domain.Profile{}).
			Select(fmt.Sprint("'", notificationID, "' AS `notification_id`"), "id AS `profile_id`, NOW(), NOW()").
			Where("id IN ?", profileID)
	}))
}

func (r *notified) NotifyUserGroup(notificationID string, userGroupID []string) error {
	return r.NotifyCustom(r.db.ToSQL(func(tx *gorm.DB) *gorm.DB {
		return r.db.Model(base_domain.User{}).
			Distinct(fmt.Sprint("'", notificationID, "' AS notification_id"), "profile_id, NOW(), NOW()").
			Where("user_group_id IN ?", userGroupID)
	}))
}

func (r *notified) NotifyCustom(query string) error {
	stmt := &gorm.Statement{DB: r.db}
	stmt.Parse(&base_domain.Notified{})
	notified_tbl := stmt.Schema.Table
	return r.db.Exec(
		fmt.Sprint("INSERT INTO `", notified_tbl, "` (`notification_id`, `profile_id`, `created_at`, `updated_at`) ?"),
		query,
	).Error
}

func (r *notified) Update(model *base_domain.Notified) error {
	return r.db.Model(base_domain.Notified{}).
		Where("notification_id = ? AND profile_id = ?", model.NotificationID, model.ProfileID).
		Updates(model).Error
}

func (r *notified) List(params map[string]interface{}, order map[string]interface{}, page int, pageSize int) (base_domain.List, error) {
	var items []base_domain.Notified
	list := base_domain.List{Page: page, PageSize: pageSize, Items: []interface{}{}}
	db := r.db.Scopes(Where(params))
	err := db.Model(&items).Count(&list.Count).Error
	if err != nil {
		return list, err
	}
	err = db.Scopes(Order(&order), Paginate(page, pageSize)).
		Preload("Notification", func(db *gorm.DB) *gorm.DB {
			return db.Select("id", "title", "LEFT(message, 100) AS message")
		}).Find(&items).Error
	if err != nil {
		return list, err
	}
	for _, item := range items {
		list.Items = append(list.Items, item)
	}
	return list, err
}

func (r *notified) Delete(notificationID string, profileID string) error {
	return r.db.Where("notification_id = ? AND profile_id = ?", notificationID, profileID).Delete(&base_domain.Notified{}).Error
}
