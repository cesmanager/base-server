package base_repository

import (
	"gitlab.com/cesmanager/base-server/core/base_domain"
	"gitlab.com/cesmanager/base-server/core/base_port"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

type settings struct {
	db *gorm.DB
}

func NewSettings(db *gorm.DB) base_port.Settings {
	return &settings{
		db: db,
	}
}

func (r *settings) Get() (base_domain.Settings, error) {
	var m base_domain.Settings
	resp := r.db.Preload(clause.Associations).Where("id = 1").Find(&m)
	if resp.Error != nil {
		return m, resp.Error
	}
	return m, nil
}

func (r *settings) Store(settings *base_domain.Settings) error {
	resp := r.db.Create(settings)
	if resp.Error != nil {
		return resp.Error
	}
	return nil
}
func (r *settings) Update(m *base_domain.Settings) error {
	resp := r.db.Model(base_domain.Settings{}).Where("id = ?", 1).Updates(m)
	if resp.Error != nil {
		return resp.Error
	}
	return nil
}
