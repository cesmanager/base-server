package base_repository

import (
	"gitlab.com/cesmanager/base-server/core/base_domain"
	"gitlab.com/cesmanager/base-server/core/base_port"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

type event struct {
	db *gorm.DB
}

func NewEvent(db *gorm.DB) base_port.Event {
	return &event{
		db: db,
	}
}

func (r *event) Get(id string) (base_domain.Event, error) {
	data := base_domain.Event{}
	r.db.Preload(clause.Associations).First(&data, "id = ?", id)
	return data, nil
}

func (r *event) Save(model *base_domain.Event) error {
	return r.db.Select("*").Save(model).Error
}

func (r *event) List(params map[string]interface{}, order map[string]interface{}, page int, limit int) (base_domain.List, error) {
	var items []base_domain.Event
	list := base_domain.List{Page: page, PageSize: limit, Items: []interface{}{}}
	db := r.db.Scopes(Where(params))
	err := db.Model(&items).Count(&list.Count).Error
	if err != nil {
		return list, err
	}
	err = db.Preload(clause.Associations).Scopes(Order(&order), Paginate(page, limit)).Find(&items).Error
	if err != nil {
		return list, err
	}
	for _, item := range items {
		list.Items = append(list.Items, item)
	}
	return list, err
}

func (r *event) Find(params map[string]interface{}) ([]base_domain.Event, error) {
	items := []base_domain.Event{}
	err := r.db.Preload(clause.Associations).Scopes(Where(params)).Find(&items).Error
	return items, err
}

func (r *event) Delete(id []string) error {
	return r.db.Unscoped().Delete(&base_domain.Event{}, id).Error
}
