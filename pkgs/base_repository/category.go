package base_repository

import (
	"fmt"
	"strconv"
	"strings"

	"gitlab.com/cesmanager/base-server/core/base_domain"
	"gitlab.com/cesmanager/base-server/core/base_port"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

type category struct {
	db *gorm.DB
}

func NewCategory(db *gorm.DB) base_port.Category {
	return &category{
		db: db,
	}
}

func (r *category) Get(id string) (base_domain.Category, error) {
	var category base_domain.Category
	catTable, err := TableName(r.db, category)
	if err != nil {
		return category, err
	}
	resp := r.db.Joins("Parent").Where("`"+catTable+"`.`id` = ?", id).Find(&category)
	if resp.Error != nil {
		return category, resp.Error
	}
	err = r.db.Model(&category).Where("parent_id = ?", id).Count(&category.Cnt).Error
	if err != nil {
		return category, err
	}
	return category, nil
}
func (r *category) Store(category *base_domain.Category) error {
	resp := r.db.Create(category)
	if resp.Error != nil {
		return resp.Error
	}
	return nil
}

func (r *category) Update(category *base_domain.Category) error {
	resp := r.db.Model(base_domain.Category{}).Where("id = ?", category.ID).Updates(category)
	if resp.Error != nil {
		return resp.Error
	}
	return nil
}

func (r *category) List(param map[string]interface{}, order map[string]interface{}, page int, pageSize int) (base_domain.List, error) {

	type Category struct {
		base_domain.Category
		Cnt int64 `json:"cnt"` //count of children
	}
	var items []Category
	list := base_domain.List{Page: page, PageSize: pageSize, Items: []interface{}{}}
	db := r.db
	for k, v := range param {
		switch strings.ToLower(fmt.Sprint(v)) {
		case "null", "nil":
			db = db.Where(k + " IS NULL")
		default:
			db = db.Where(k+" = ?", v)
		}
	}
	err := db.Model(&items).Count(&list.Count).Error
	if err != nil {
		return list, err
	}
	db = db.Preload(clause.Associations)
	for column, desc := range order {
		d, _ := strconv.ParseBool(fmt.Sprint(desc))
		db = db.Order(clause.OrderByColumn{Column: clause.Column{Name: column}, Desc: d})
	}

	catTable, err := TableName(r.db, base_domain.Category{})
	if err != nil {
		return list, err
	}
	cnt := r.db.Table(catTable + " AS ccat").Select("COUNT(*)").Where("ccat.parent_id = cat.id")
	err = db.Table(catTable+" AS cat").Scopes(Paginate(page, pageSize)).Select("cat.*, (?) AS cnt", cnt).Find(&items).Error
	if err != nil {
		return list, err
	}
	for _, item := range items {
		list.Items = append(list.Items, item)
	}

	return list, err
}

func (r *category) Find(param map[string]interface{}) ([]base_domain.Category, error) {
	var categorys []base_domain.Category
	db := r.db
	resp := db.Scopes(Where(param)).Find(&categorys)
	if resp.Error != nil {
		return categorys, resp.Error
	}
	return categorys, nil
}

func (r *category) Count(param map[string]interface{}, operators *map[string]string) (int64, error) {
	var count int64
	db := r.db
	for k, v := range param {
		switch strings.ToLower(fmt.Sprint(v)) {
		case "null", "nil":
			db = db.Where(k + " IS NULL")
		default:
			operator := "="
			if operators != nil {
				if op, ok := (*operators)[k]; ok {
					operator = op
				}
			}
			db = db.Where(k+" "+operator+" ?", v)
		}
	}
	resp := db.Model(&base_domain.Category{}).Count(&count)
	if resp.Error != nil {
		return count, resp.Error
	}
	return count, nil
}

func (r *category) Delete(param map[string]interface{}) error {
	db := r.db
	for k, v := range param {
		switch strings.ToLower(fmt.Sprint(v)) {
		case "null", "nil":
			db = db.Where(k + " IS NULL")
		default:
			db = db.Where(k+" = ?", v)
		}
	}
	return db.Delete(&base_domain.Category{}).Error
}
