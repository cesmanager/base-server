package base_repository

import (
	"errors"
	"fmt"
	"strconv"
	"strings"

	"gitlab.com/cesmanager/base-server/core/base_domain"
	"gitlab.com/cesmanager/base-server/core/base_port"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

type outlet struct {
	db *gorm.DB
}

func NewOutlet(db *gorm.DB) base_port.Outlet {
	return &outlet{
		db: db,
	}
}

func (r *outlet) Get(id string) (base_domain.Outlet, error) {
	var outlet base_domain.Outlet
	resp := r.db.Preload(clause.Associations).Where("id = ?", id).Find(&outlet)
	if resp.Error != nil {
		return outlet, resp.Error
	}
	if outlet.ID == 0 {
		return outlet, errors.New("No outlet with id = " + id)
	}
	return outlet, nil
}
func (r *outlet) Store(outlet *base_domain.Outlet) error {
	resp := r.db.Create(outlet)
	if resp.Error != nil {
		return resp.Error
	}
	return nil
}
func (r *outlet) Update(outlet *base_domain.Outlet) error {
	resp := r.db.Model(base_domain.Outlet{}).Where("id = ?", outlet.ID).Updates(outlet)
	if resp.Error != nil {
		return resp.Error
	}
	return nil
}

func (r *outlet) List(param map[string]interface{}, order map[string]interface{}, page int, pageSize int) (base_domain.List, error) {
	var items []base_domain.Outlet
	list := base_domain.List{Page: page, PageSize: pageSize, Items: []interface{}{}}
	db := r.db
	for k, v := range param {
		switch strings.ToLower(fmt.Sprint(v)) {
		case "null", "nil":
			db = db.Where(k + " IS NULL")
		default:
			db = db.Where(k+" = ?", v)
		}
	}
	err := db.Model(&items).Count(&list.Count).Error
	if err != nil {
		return list, err
	}
	db = db.Preload(clause.Associations)
	for column, desc := range order {
		d, _ := strconv.ParseBool(fmt.Sprint(desc))
		db = db.Order(clause.OrderByColumn{Column: clause.Column{Name: column}, Desc: d})
	}
	err = db.Scopes(Paginate(page, pageSize)).Find(&items).Error
	if err != nil {
		return list, err
	}
	for _, item := range items {
		list.Items = append(list.Items, item)
	}

	return list, err
}

func (r *outlet) Find(param map[string]interface{}) ([]base_domain.Outlet, error) {
	var outlets []base_domain.Outlet
	db := r.db
	for k, v := range param {
		switch strings.ToLower(fmt.Sprint(v)) {
		case "null", "nil":
			db = r.db.Where(k + " IS NULL")
		default:
			db = r.db.Where(k+" = ?", v)
		}
	}
	resp := db.Find(&outlets)
	if resp.Error != nil {
		return outlets, resp.Error
	}
	return outlets, nil
}
