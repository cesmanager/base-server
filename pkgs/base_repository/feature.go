package base_repository

import (
	"fmt"
	"strconv"
	"strings"

	"gitlab.com/cesmanager/base-server/core/base_domain"
	"gitlab.com/cesmanager/base-server/core/base_port"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

type feature struct {
	db *gorm.DB
}

func NewFeature(db *gorm.DB) base_port.Feature {
	return &feature{
		db: db,
	}
}

func (r *feature) Get(id string) (base_domain.Feature, error) {
	var feature base_domain.Feature
	resp := r.db.Preload(clause.Associations).Where("`"+feature.TableName()+"`.`id` = ?", id).Find(&feature)
	if resp.Error != nil {
		return feature, resp.Error
	}
	err := r.db.Model(&feature).Where("parent_id = ?", id).Count(&feature.Cnt).Error
	if err != nil {
		return feature, err
	}
	return feature, nil
}
func (r *feature) Create(feature *base_domain.Feature) error {
	resp := r.db.Create(feature)
	if resp.Error != nil {
		return resp.Error
	}
	return nil
}

func (r *feature) Update(feature *base_domain.Feature) error {
	resp := r.db.Model(base_domain.Feature{}).Where("id = ?", feature.ID).Updates(feature)
	if resp.Error != nil {
		return resp.Error
	}
	return nil
}

func (r *feature) List(param map[string]interface{}, order map[string]interface{}, page int, pageSize int) (base_domain.List, error) {

	type Feature struct {
		base_domain.Feature
		Cnt int64 `json:"cnt"` //count of children
	}
	var items []Feature
	list := base_domain.List{Page: page, PageSize: pageSize, Items: []interface{}{}}
	db := r.db
	for k, v := range param {
		switch strings.ToLower(fmt.Sprint(v)) {
		case "null", "nil":
			db = db.Where(k + " IS NULL")
		default:
			db = db.Where(k+" = ?", v)
		}
	}
	err := db.Model(&items).Count(&list.Count).Error
	if err != nil {
		return list, err
	}
	db = db.Preload(clause.Associations)
	for column, desc := range order {
		d, _ := strconv.ParseBool(fmt.Sprint(desc))
		db = db.Order(clause.OrderByColumn{Column: clause.Column{Name: column}, Desc: d})
	}

	cnt := r.db.Table((&Feature{}).TableName() + " AS ccat").Select("COUNT(*)").Where("ccat.parent_id = cat.id")
	err = db.Table((&Feature{}).TableName()+" AS cat").Scopes(Paginate(page, pageSize)).Select("cat.*, (?) AS cnt", cnt).Find(&items).Error
	if err != nil {
		return list, err
	}
	for _, item := range items {
		list.Items = append(list.Items, item)
	}

	return list, err
}

func (r *feature) Find(param map[string]interface{}) ([]base_domain.Feature, error) {
	var categorys []base_domain.Feature
	db := r.db
	resp := db.Scopes(Where(param)).Preload(clause.Associations).Find(&categorys)
	if resp.Error != nil {
		return categorys, resp.Error
	}
	return categorys, nil
}

func (r *feature) FindWithPreload(param map[string]interface{}, preload interface{}) ([]base_domain.Feature, error) {
	var features []base_domain.Feature
	db := r.db
	db = db.Scopes(Where(param), Preload(preload))
	resp := db.Find(&features)
	if resp.Error != nil {
		return features, resp.Error
	}
	return features, nil
}

func (r *feature) Count(param map[string]interface{}, operators *map[string]string) (int64, error) {
	var count int64
	db := r.db
	for k, v := range param {
		switch strings.ToLower(fmt.Sprint(v)) {
		case "null", "nil":
			db = db.Where(k + " IS NULL")
		default:
			operator := "="
			if operators != nil {
				if op, ok := (*operators)[k]; ok {
					operator = op
				}
			}
			db = db.Where(k+" "+operator+" ?", v)
		}
	}
	resp := db.Model(&base_domain.Feature{}).Count(&count)
	if resp.Error != nil {
		return count, resp.Error
	}
	return count, nil
}

func (r *feature) Delete(param map[string]interface{}) error {
	db := r.db
	if len(param) == 0 {
		rs := db.Exec(fmt.Sprint("TRUNCATE TABLE ", base_domain.Feature{}.TableName()))
		return rs.Error
	}
	for k, v := range param {
		switch strings.ToLower(fmt.Sprint(v)) {
		case "null", "nil":
			db = db.Where(k + " IS NULL")
		default:
			db = db.Where(k+" = ?", v)
		}
	}
	return db.Delete(&base_domain.Feature{}).Error
}
