package base_repository

import (
	"fmt"
	"strconv"
	"strings"

	"gitlab.com/cesmanager/base-server/core/base_domain"
	"gitlab.com/cesmanager/base-server/core/base_port"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

type geoLocation struct {
	db *gorm.DB
}

func NewGeoLocation(db *gorm.DB) base_port.GeoLocation {
	return &geoLocation{
		db: db,
	}
}

func (r *geoLocation) Get(id string) (base_domain.GeoLocation, error) {
	var geoLocation base_domain.GeoLocation
	err := r.db.Preload(clause.Associations).First(&geoLocation, id).Error
	if err != nil {
		return geoLocation, err
	}
	return geoLocation, nil
}

func (r *geoLocation) Store(geoLocation *base_domain.GeoLocation) error {
	return r.db.Create(geoLocation).Error
}

func (r *geoLocation) Update(geoLocation *base_domain.GeoLocation) error {
	return r.db.Model(&geoLocation).Updates(geoLocation).Error
}

func (r *geoLocation) List(param map[string]interface{}, order map[string]interface{}, page int, pageSize int) (base_domain.List, error) {
	var items []base_domain.GeoLocation
	list := base_domain.List{Page: page, PageSize: pageSize, Items: []interface{}{}}
	db := r.db
	for k, v := range param {
		switch strings.ToLower(fmt.Sprint(v)) {
		case "null", "nil":
			db = db.Where(k + " IS NULL")
		default:
			db = db.Where(k+" = ?", v)
		}
	}
	err := db.Model(&items).Count(&list.Count).Error
	if err != nil {
		return list, err
	}
	db = db.Preload(clause.Associations)
	for column, desc := range order {
		d, _ := strconv.ParseBool(fmt.Sprint(desc))
		db = db.Order(clause.OrderByColumn{Column: clause.Column{Name: column}, Desc: d})
	}
	err = db.Scopes(Paginate(page, pageSize)).Find(&items).Error
	if err != nil {
		return list, err
	}
	for _, item := range items {
		list.Items = append(list.Items, item)
	}

	return list, err
}

func (r *geoLocation) Find(param map[string]interface{}, order *map[string]interface{}) ([]base_domain.GeoLocation, error) {
	var geoLocations []base_domain.GeoLocation
	db := r.db
	db = db.Scopes(Where(param))
	db = db.Scopes(Order(order))
	resp := db.Find(&geoLocations)
	return geoLocations, resp.Error
}

func (r *geoLocation) Count(param map[string]interface{}) (int64, error) {
	var count int64
	db := r.db
	db = db.Scopes(Where(param))
	resp := db.Count(&count)
	if resp.Error != nil {
		return count, resp.Error
	}
	return count, nil
}
