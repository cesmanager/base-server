package base_repository

import (
	"fmt"
	"strings"

	"gitlab.com/cesmanager/base-server/core/base_domain"
	"gitlab.com/cesmanager/base-server/core/base_port"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

type currency struct {
	db *gorm.DB
}

func NewCurrency(db *gorm.DB) base_port.Currency {
	return &currency{
		db: db,
	}
}

func (r *currency) Get(id string) (base_domain.Currency, error) {
	var currency base_domain.Currency
	resp := r.db.Preload(clause.Associations).Where("id = ?", id).Find(&currency)
	if resp.Error != nil {
		return currency, resp.Error
	}
	return currency, nil
}
func (r *currency) Store(currency *base_domain.Currency) error {
	resp := r.db.Create(currency)
	if resp.Error != nil {
		return resp.Error
	}
	return nil
}
func (r *currency) Update(currency *base_domain.Currency) error {
	resp := r.db.Model(base_domain.Currency{}).Where("id = ?", currency.ID).Updates(currency)
	if resp.Error != nil {
		return resp.Error
	}
	return nil
}

func (r *currency) List(search string, order map[string]interface{}, page int, pageSize int) (base_domain.List, error) {
	var items []base_domain.Currency
	list := base_domain.List{Page: page, PageSize: pageSize, Items: []interface{}{}}
	db := LikeSearch(search, "name", "unit_name")(r.db)
	err := db.Model(&items).Count(&list.Count).Error
	if err != nil {
		return list, err
	}
	err = db.Preload(clause.Associations).Scopes(Order(&order), Paginate(page, pageSize)).Find(&items).Error
	if err != nil {
		return list, err
	}
	for _, item := range items {
		list.Items = append(list.Items, item)
	}

	return list, err
}

func (r *currency) Find(param map[string]interface{}) ([]base_domain.Currency, error) {
	var currencys []base_domain.Currency
	db := r.db
	for k, v := range param {
		switch strings.ToLower(fmt.Sprint(v)) {
		case "null", "nil":
			db = r.db.Where(k + " IS NULL")
		default:
			db = r.db.Where(k+" = ?", v)
		}
	}
	resp := db.Find(&currencys)
	if resp.Error != nil {
		return currencys, resp.Error
	}
	return currencys, nil
}
