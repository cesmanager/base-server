package base_repository

import (
	"fmt"
	"strings"

	"gitlab.com/cesmanager/base-server/core/base_domain"
	"gitlab.com/cesmanager/base-server/core/base_port"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

type formRecord struct {
	db *gorm.DB
}

func NewFormRecord(db *gorm.DB) base_port.FormRecordPort {
	return &formRecord{
		db: db,
	}
}
func (r *formRecord) Get(id string) (base_domain.FormRecord, error) {
	record := base_domain.FormRecord{}
	r.db.Preload(clause.Associations).First(&record, "id = ?", id)
	return record, nil
}
func (r *formRecord) Store(model *base_domain.FormRecord) error {
	return r.db.Create(model).Error
}
func (r *formRecord) Update(model *base_domain.FormRecord) error {
	return r.db.Save(model).Error
}
func (r *formRecord) List(param map[string]interface{}, order map[string]interface{}, page int, pageSize int) (base_domain.List, error) {
	var items []base_domain.FormRecord
	list := base_domain.List{Page: page, PageSize: pageSize, Items: []interface{}{}}
	db := r.db
	err := db.Model(&items).Count(&list.Count).Error
	if err != nil {
		return list, err
	}
	err = db.Preload(clause.Associations).Scopes(Where(param), Order(&order), Paginate(page, pageSize)).Find(&items).Error
	if err != nil {
		return list, err
	}
	for _, item := range items {
		list.Items = append(list.Items, item)
	}
	return list, err
}
func (r *formRecord) Find(params map[string]interface{}) ([]base_domain.FormRecord, error) {
	db := r.db
	for k, v := range params {
		switch strings.ToLower(fmt.Sprint(v)) {
		case "null", "nil":
			db = db.Where(k + " IS NULL")
		default:
			db = db.Where(k+" = ?", v)
		}
	}
	records := []base_domain.FormRecord{}
	err := db.Preload(clause.Associations).Find(&records).Error
	return records, err
}

func (r *formRecord) Delete(params map[string]interface{}) error {
	db := r.db
	for k, v := range params {
		switch strings.ToLower(fmt.Sprint(v)) {
		case "null", "nil":
			db = db.Where(k + " IS NULL")
		default:
			db = db.Where(k+" = ?", v)
		}
	}
	return db.Delete(&base_domain.FormRecord{}).Error
}
