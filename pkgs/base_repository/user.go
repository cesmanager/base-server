package base_repository

import (
	"gitlab.com/cesmanager/base-server/core/base_domain"
	"gitlab.com/cesmanager/base-server/core/base_port"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

type user struct {
	db *gorm.DB
}

func NewUser(db *gorm.DB) base_port.User {
	return &user{
		db: db,
	}
}

func (r *user) Get(id string) (base_domain.User, error) {
	var user base_domain.User
	db := r.db.Preload(clause.Associations)
	resp := db.Where("id = ?", id).Find(&user)
	if resp.Error != nil {
		return user, resp.Error
	}
	return user, nil
}
func (r *user) Store(user *base_domain.User) error {
	resp := r.db.Create(user)
	if resp.Error != nil {
		return resp.Error
	}
	return nil
}
func (r *user) Update(user *base_domain.User) error {
	resp := r.db.Model(base_domain.User{}).Where("id = ?", user.ID).Updates(user)
	if resp.Error != nil {
		return resp.Error
	}
	return nil
}

func (r *user) List(param map[string]interface{}, order map[string]interface{}, page int, pageSize int) (base_domain.List, error) {
	var items []base_domain.User
	list := base_domain.List{Page: page, PageSize: pageSize, Items: []interface{}{}}
	db := r.db
	err := db.Model(&items).Count(&list.Count).Error
	if err != nil {
		return list, err
	}
	err = db.Preload(clause.Associations).Scopes(Where(param), Order(&order), Paginate(page, pageSize)).Find(&items).Error
	if err != nil {
		return list, err
	}
	for _, item := range items {
		list.Items = append(list.Items, item)
	}

	return list, err
}

func (r *user) Find(param map[string]interface{}) ([]base_domain.User, error) {
	var users []base_domain.User
	resp := r.db.Preload(clause.Associations).Scopes(Where(param)).Find(&users)
	if resp.Error != nil {
		return users, resp.Error
	}
	return users, nil
}
