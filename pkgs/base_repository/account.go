package base_repository

import (
	"fmt"
	"strings"

	"gitlab.com/cesmanager/base-server/core/base_domain"
	"gitlab.com/cesmanager/base-server/core/base_port"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

type account struct {
	db *gorm.DB
}

func NewAccount(db *gorm.DB) base_port.Account {
	return &account{
		db: db,
	}
}

func (r *account) Get(id string) (base_domain.Account, error) {
	var account base_domain.Account
	resp := r.db.Preload(clause.Associations).Where("id = ?", id).Find(&account)
	if resp.Error != nil {
		return account, resp.Error
	}
	return account, nil
}
func (r *account) Store(account *base_domain.Account) error {
	resp := r.db.Create(account)
	if resp.Error != nil {
		return resp.Error
	}
	return nil
}

func (r *account) Update(account *base_domain.Account) error {
	resp := r.db.Model(base_domain.Account{}).Where("id = ?", account.ID).Updates(account)
	if resp.Error != nil {
		return resp.Error
	}
	return nil
}

func (r *account) Lock(id string) error {
	var account base_domain.Account
	//find and lock the account's record
	resp := r.db.Clauses(clause.Locking{Strength: "UPDATE"}).Where("id = ?", id).Find(&account)
	if resp.Error != nil {
		return resp.Error
	}
	return nil
}

func (r *account) List(param map[string]interface{}, order map[string]interface{}, page int, pageSize int) (base_domain.List, error) {
	var items []base_domain.Account
	list := base_domain.List{Page: page, PageSize: pageSize, Items: []interface{}{}}
	db := r.db
	for k, v := range param {
		switch strings.ToLower(fmt.Sprint(v)) {
		case "null", "nil":
			db = db.Where(k + " IS NULL")
		default:
			db = db.Where(k+" = ?", v)
		}
	}
	err := db.Model(&items).Count(&list.Count).Error
	if err != nil {
		return list, err
	}
	db = db.Preload(clause.Associations).Scopes(Order(&order))
	err = db.Scopes(Paginate(page, pageSize)).Find(&items).Error
	if err != nil {
		return list, err
	}
	for _, item := range items {
		list.Items = append(list.Items, item)
	}

	return list, err
}

func (r *account) Find(param map[string]interface{}) ([]base_domain.Account, error) {
	var accounts []base_domain.Account
	db := r.db
	for k, v := range param {
		switch strings.ToLower(fmt.Sprint(v)) {
		case "null", "nil":
			db = r.db.Where(k + " IS NULL")
		default:
			db = r.db.Where(k+" = ?", v)
		}
	}
	resp := db.Find(&accounts)
	if resp.Error != nil {
		return accounts, resp.Error
	}
	return accounts, nil
}
