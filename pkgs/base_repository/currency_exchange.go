package base_repository

import (
	"fmt"
	"strings"

	"gitlab.com/cesmanager/base-server/core/base_domain"
	"gitlab.com/cesmanager/base-server/core/base_port"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

type currencyExchange struct {
	db *gorm.DB
}

func NewCurrencyExchange(db *gorm.DB) base_port.CurrencyExchange {
	return &currencyExchange{
		db: db,
	}
}

func (r *currencyExchange) Get(id string) (base_domain.CurrencyExchange, error) {
	var currencyExchange base_domain.CurrencyExchange
	resp := r.db.Preload(clause.Associations).Where("id = ?", id).Find(&currencyExchange)
	if resp.Error != nil {
		return currencyExchange, resp.Error
	}
	return currencyExchange, nil
}
func (r *currencyExchange) Store(currencyExchange *base_domain.CurrencyExchange) error {
	resp := r.db.Create(currencyExchange)
	if resp.Error != nil {
		return resp.Error
	}
	return nil
}
func (r *currencyExchange) Update(currencyExchange *base_domain.CurrencyExchange) error {
	resp := r.db.Model(base_domain.CurrencyExchange{}).Where("id = ?", currencyExchange.ID).Updates(currencyExchange)
	if resp.Error != nil {
		return resp.Error
	}
	return nil
}

func (r *currencyExchange) Find(param map[string]interface{}) ([]base_domain.CurrencyExchange, error) {
	var currencyExchanges []base_domain.CurrencyExchange
	db := r.db
	for k, v := range param {
		switch strings.ToLower(fmt.Sprint(v)) {
		case "null", "nil":
			db = r.db.Where(k + " IS NULL")
		default:
			db = r.db.Where(k+" = ?", v)
		}
	}
	resp := db.Find(&currencyExchanges)
	if resp.Error != nil {
		return currencyExchanges, resp.Error
	}
	return currencyExchanges, nil
}
