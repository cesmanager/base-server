package base_repository

import (
	"gitlab.com/cesmanager/base-server/core/base_domain"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

type fileInfo struct {
	db *gorm.DB
}

func NewFileInfo(db *gorm.DB) *fileInfo {
	return &fileInfo{
		db: db,
	}
}
func (r *fileInfo) Get(path string) (base_domain.FileInfo, error) {
	record := base_domain.FileInfo{}
	r.db.First(&record, "path = ?", path)
	return record, nil
}
func (r *fileInfo) Store(model *base_domain.FileInfo) error {
	return r.db.Create(model).Error
}
func (r *fileInfo) Update(path string, model *base_domain.FileInfo) error {
	return r.db.Where("path = ?", path).Updates(model).Error
}

func (r *fileInfo) Delete(path string) error {
	record := base_domain.FileInfo{}
	return r.db.Where("path = ?", path).Delete(&record).Error
}
func (r *fileInfo) Find(param map[string]interface{}) ([]base_domain.FileInfo, error) {
	var items []base_domain.FileInfo
	db := r.db
	err := db.Preload(clause.Associations).Scopes(Where(param)).Find(&items).Error
	return items, err
}
