package base_repository

import (
	"gitlab.com/cesmanager/base-server/core/base_domain"
	"gitlab.com/cesmanager/base-server/core/base_port"
	"gorm.io/gorm"
)

type status struct {
	db *gorm.DB
}

func NewStatus(db *gorm.DB) base_port.Status {
	return &status{
		db: db,
	}
}

func (r *status) Get(id string) (base_domain.Status, error) {
	var status base_domain.Status
	err := r.db.Where("id = ?", id).Find(&status).Error
	return status, err
}

func (r *status) Store(status *base_domain.Status) error {
	return r.db.Create(status).Error
}

func (r *status) Update(status *base_domain.Status) error {
	return r.db.Model(base_domain.Status{}).Where("id = ?", status.ID).Updates(status).Error
}

func (r *status) List(param map[string]interface{}, order map[string]interface{}, page int, pageSize int) (base_domain.List, error) {
	var items []base_domain.Status
	list := base_domain.List{Page: page, PageSize: pageSize, Items: []interface{}{}}
	db := r.db.Scopes(Where(param))
	err := db.Model(&items).Count(&list.Count).Error
	if err != nil {
		return list, err
	}
	err = db.Scopes(Order(&order), Paginate(page, pageSize)).Find(&items).Error
	if err != nil {
		return list, err
	}
	for _, item := range items {
		list.Items = append(list.Items, item)
	}
	return list, err
}

func (r *status) Find(param map[string]interface{}) ([]base_domain.Status, error) {
	var statuss []base_domain.Status
	err := r.db.Scopes(Where(param)).Find(&statuss).Error
	return statuss, err
}

func (r *status) Delete(id string) error {
	return r.db.Delete(&base_domain.Status{}, id).Error
}
