package base_repository

import (
	"fmt"
	"strings"

	"gitlab.com/cesmanager/base-server/core/base_domain"
	"gitlab.com/cesmanager/base-server/core/base_port"
	"gorm.io/gorm"
)

type access struct {
	db *gorm.DB
}

func NewAccess(db *gorm.DB) base_port.Access {
	return &access{
		db: db,
	}
}

func (r *access) Get(token string) (base_domain.Access, error) {
	var access base_domain.Access
	resp := r.db.Preload("User.Profile").Where("token = ?", token).Find(&access)
	if resp.Error != nil {
		return access, resp.Error
	}
	return access, nil
}

func (r *access) Store(access *base_domain.Access) error {
	resp := r.db.Create(access)
	if resp.Error != nil {
		return resp.Error
	}
	return nil
}
func (r *access) Update(access *base_domain.Access) error {
	resp := r.db.Model(base_domain.User{}).Where("token = ?", access.Token).Updates(access)
	if resp.Error != nil {
		return resp.Error
	}
	return nil
}

func (r *access) Find(param map[string]interface{}) ([]base_domain.Access, error) {
	var access []base_domain.Access
	db := r.db
	for k, v := range param {
		switch strings.ToLower(fmt.Sprint(v)) {
		case "null", "nil":
			db = r.db.Where(k + " IS NULL")
		default:
			db = r.db.Where(k+" = ?", v)
		}
	}
	resp := db.Find(&access)
	if resp.Error != nil {
		return access, resp.Error
	}
	return access, nil
}

func (r *access) Delete(token string) error {
	var access base_domain.Access
	resp := r.db.Where("token = ?", token).Delete(&access)
	if resp.Error != nil {
		return resp.Error
	}
	return nil
}
