package base_repository

import (
	"fmt"
	"strings"

	"gitlab.com/cesmanager/base-server/core/base_domain"
	"gitlab.com/cesmanager/base-server/core/base_port"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

type formRecordEntry struct {
	db *gorm.DB
}

func NewFormRecordEntry(db *gorm.DB) base_port.FormRecordEntryPort {
	return &formRecordEntry{
		db: db,
	}
}
func (r *formRecordEntry) Find(params map[string]interface{}) ([]base_domain.FormRecordEntry, error) {
	db := r.db
	for k, v := range params {
		switch strings.ToLower(fmt.Sprint(v)) {
		case "null", "nil":
			db = db.Where(k + " IS NULL")
		default:
			db = db.Where(k+" = ?", v)
		}
	}
	records := []base_domain.FormRecordEntry{}
	err := db.Preload(clause.Associations).Find(&records).Error
	return records, err
}

func (r *formRecordEntry) Store(model *base_domain.FormRecordEntry) error {
	return r.db.Clauses(clause.OnConflict{
		UpdateAll: true,
	}).Create(&model).Error
}

func (r *formRecordEntry) Delete(params map[string]interface{}) error {
	db := r.db
	for k, v := range params {
		switch strings.ToLower(fmt.Sprint(v)) {
		case "null", "nil":
			db = db.Where(k + " IS NULL")
		default:
			db = db.Where(k+" = ?", v)
		}
	}
	return db.Delete(&base_domain.FormRecord{}).Error
}
