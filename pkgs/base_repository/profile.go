package base_repository

import (
	"fmt"

	"gitlab.com/cesmanager/base-server/core/base_domain"
	"gitlab.com/cesmanager/base-server/core/base_port"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

type profile struct {
	db *gorm.DB
}

func NewProfile(db *gorm.DB) base_port.Profile {
	return &profile{
		db: db,
	}
}

func (r *profile) Get(id string) (base_domain.Profile, error) {
	var profile base_domain.Profile
	resp := r.db.Preload(clause.Associations).Where("id = ?", id).Find(&profile)
	if resp.Error != nil {
		return profile, resp.Error
	}
	return profile, nil
}
func (r *profile) Store(profile *base_domain.Profile) error {
	resp := r.db.Create(profile)
	if resp.Error != nil {
		return resp.Error
	}
	return nil
}
func (r *profile) Update(profile *base_domain.Profile) error {
	resp := r.db.Model(base_domain.Profile{}).Where("id = ?", profile.ID).Updates(profile)
	if resp.Error != nil {
		return resp.Error
	}
	return nil
}

func (r *profile) List(params map[string]interface{}, order map[string]interface{}, page int, pageSize int) (base_domain.List, error) {
	var items []base_domain.Profile
	list := base_domain.List{Page: page, PageSize: pageSize, Items: []interface{}{}}
	//switch to fulltext search later
	search := ""
	if _search, ok := params["search"]; ok {
		search = fmt.Sprint(_search)
		delete(params, "search")
	}
	db := r.db.Scopes(
		FullTextSearch(search, "email", "alt_email", "fname", "lname", "mname", "bname"),
		Where(params),
	)
	err := db.Model(&items).Count(&list.Count).Error
	if err != nil {
		return list, err
	}
	err = db.Preload(clause.Associations).Scopes(Order(&order), Paginate(page, pageSize)).Find(&items).Error
	if err != nil {
		return list, err
	}
	for _, item := range items {
		list.Items = append(list.Items, item)
	}
	return list, err
}

func (r *profile) Find(params map[string]interface{}) ([]base_domain.Profile, error) {
	var profiles []base_domain.Profile
	resp := r.db.Scopes(Where(params)).Find(&profiles)
	if resp.Error != nil {
		return profiles, resp.Error
	}
	return profiles, nil
}

func (r *profile) Delete(id string) error {
	return r.db.Delete(&base_domain.Profile{}, id).Error
}
