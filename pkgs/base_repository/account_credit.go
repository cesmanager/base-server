package base_repository

import (
	"fmt"
	"strings"

	"gitlab.com/cesmanager/base-server/core/base_domain"
	"gitlab.com/cesmanager/base-server/core/base_port"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

type credit struct {
	db *gorm.DB
}

func NewCredit(db *gorm.DB) base_port.Credit {
	return &credit{
		db: db,
	}
}

func (r *credit) Get(id string) (base_domain.Credit, error) {
	var credit base_domain.Credit
	resp := r.db.Preload(clause.Associations).Where("id = ?", id).Find(&credit)
	if resp.Error != nil {
		return credit, resp.Error
	}
	return credit, nil
}
func (r *credit) Store(credit *base_domain.Credit) error {
	resp := r.db.Create(credit)
	if resp.Error != nil {
		return resp.Error
	}
	return nil
}

func (r *credit) Update(credit *base_domain.Credit) error {
	resp := r.db.Model(base_domain.Credit{}).Where("id = ?", credit.ID).Updates(credit)
	if resp.Error != nil {
		return resp.Error
	}
	return nil
}

func (r *credit) Find(param map[string]interface{}) ([]base_domain.Credit, error) {
	var credits []base_domain.Credit
	db := r.db
	for k, v := range param {
		switch strings.ToLower(fmt.Sprint(v)) {
		case "null", "nil":
			db = r.db.Where(k + " IS NULL")
		default:
			db = r.db.Where(k+" = ?", v)
		}
	}
	resp := db.Find(&credits)
	if resp.Error != nil {
		return credits, resp.Error
	}
	return credits, nil
}
