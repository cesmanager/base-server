package base_blocs

import (
	"errors"
	"fmt"
	"strconv"

	"gitlab.com/cesmanager/base-server/core/base_domain"
	"gitlab.com/cesmanager/base-server/core/base_port"
)

type feature struct {
	base_domain.Feature
	port base_port.Feature
}

func NewFeature(port base_port.Feature) *feature {
	mod := "feature"
	return &feature{
		port: port,
		Feature: base_domain.Feature{
			Name:    "Feature",
			Mod:     &mod,
			Actions: "list,create,update,delete",
		},
	}
}

func (srv *feature) Get(id string) (base_domain.Feature, error) {
	feature, err := srv.port.Get(id)
	if err != nil {
		return base_domain.Feature{}, errors.New("unable to get feature")
	}
	if feature.ID == 0 {
		return base_domain.Feature{}, errors.New("no feature found")
	}
	return feature, nil
}

func (srv *feature) List(param map[string]interface{}, order map[string]interface{}, page string, pageSize string) (base_domain.List, error) {
	pg, _ := strconv.Atoi(page)
	pgs, _ := strconv.Atoi(pageSize)
	list, err := srv.port.List(param, order, pg, pgs)
	if err != nil {
		return list, errors.New("unable to get list")
	}
	return list, nil
}

func (srv *feature) Find(param map[string]interface{}, operator *map[string]interface{}) ([]base_domain.Feature, error) {
	ParamOperator(&param, operator)
	feature, err := srv.port.Find(param)
	if err != nil {
		return feature, errors.New("unable to get feature(s)")
	}
	return feature, nil
}

// create the geolocation. Always call this in a transaction
func (srv *feature) CreateByInstance(feature base_domain.Feature) (base_domain.Feature, error) {
	if feature.ParentID != nil {
		//find the parents
		parent, err := srv.Get(fmt.Sprint(*feature.ParentID))
		if err != nil {
			return feature, err
		}
		//get the parents children
		gs, err := srv.Find(map[string]interface{}{"parent_id": *feature.ParentID}, nil)
		if err != nil {
			return feature, err
		}
		feature.GroupTag = fmt.Sprintf("%v-%v", parent.GroupTag, len(gs)+1)
		feature.Title = fmt.Sprintf("%v>%v", parent.Title, feature.Name)
	}
	if err := srv.port.Create(&feature); err != nil {
		return base_domain.Feature{}, errors.New("unable to create feature")
	}
	if feature.ParentID == nil {
		feature.Title = feature.Name
		feature.GroupTag = fmt.Sprint(feature.ID)
		//update it
		if err := srv.port.Update(&feature); err != nil {
			return base_domain.Feature{}, errors.New("unable to update group tag")
		}
	}
	return feature, nil
}

func (srv *feature) SyncFeature() error {
	//drop all old records "TRUNCATE TABLE Categories;"
	err := srv.port.Delete(map[string]interface{}{})
	if err != nil {
		return err
	}
	//groups
	features := []base_domain.Feature{
		{
			ID: 1, Name: "Administration",
			Icon: "fas fa-cogs", Menu: true, Active: true,
		},
		{
			ID: 2, Name: "Finance",
			Icon: "fas fa-briefcase", Menu: true, Active: true,
		},
		{
			ID: 3, Name: "e-Document",
			Icon: "fas fa-file-pdf", Menu: true, Active: true,
		},
	}
	features = append(features,
		//Administration
		NewCoyInfo(nil).Feature.SetID(100).SetParent(features[0].ID).IsRoot().IsActive(true).SetIcon("fas fa-building"),
		NewSettings(nil).Feature.IsActive(true).SetParent(features[0].ID).IsActive(true).SetIcon("fas fa-cogs"),
		NewProfile(nil).Feature.SetParent(features[0].ID).IsActive(true).SetIcon("fas fa-user-cog"),
		NewUser(nil).Feature.SetParent(features[0].ID).IsActive(true).SetIcon("fas fa-user-lock"),
		NewUserGroup(nil).Feature.SetParent(features[0].ID).IsActive(true).SetIcon("fas fa-user-shield"),
		NewCurrency(nil).Feature.SetParent(features[0].ID).IsActive(true).SetIcon("fas fa-money-bill"),
		NewCurrencyExchange(nil).Feature.SetParent(features[0].ID).IsActive(true).SetIcon("fas fa-exchange-alt"),
		NewOutlet(nil).Feature.SetParent(features[0].ID).IsActive(true).SetIcon("fas fa-dot-circle"),
		NewGeoLocation(nil).Feature.SetParent(features[0].ID).IsActive(true).SetIcon("fas fa-map"),
		NewCategory(nil).Feature.SetParent(features[0].ID).IsActive(true).SetIcon("fas fa-sitemap"),
		NewStatus(nil).Feature.SetParent(features[0].ID).IsActive(true).SetIcon("fas fa-hourglass-start"),
		NewFileManager(nil, nil).Feature.SetParent(features[0].ID).IsActive(true).SetIcon("fas fa-file-alt"),
		//Finance
		NewAccount(nil).Feature.SetParent(features[1].ID).IsActive(true).SetIcon("fas fa-piggy-bank"),
		//e-Documents
		NewForm(nil).Feature.SetParent(features[2].ID).IsActive(true).SetIcon("fas fa-file-alt"),
		NewFormRecord(nil).Feature.SetParent(features[2].ID).IsActive(true),
	)

	for _, v := range features {
		_, err := srv.CreateByInstance(v)
		if err != nil {
			return err
		}
	}
	return nil
}

// mod must not be sent from the client for security reasons
func (srv *feature) ActiveFeatures(mods string, root bool) (features []base_domain.Feature, err error) {
	//load all modules for root company
	features, err = srv.port.FindWithPreload(map[string]interface{}{"parent_id": nil}, map[string]interface{}{
		"Features": map[string]interface{}{"col": "mod", "val": mods, "con": "IN"},
	})
	if err != nil {
		return features, errors.New("unable to get feature(s)")
	}
	return
}
