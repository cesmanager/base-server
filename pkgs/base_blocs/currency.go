package base_blocs

import (
	"errors"
	"fmt"
	"strconv"

	"gitlab.com/cesmanager/base-server/core/base_domain"
	"gitlab.com/cesmanager/base-server/core/base_port"
)

type currency struct {
	base_domain.Feature
	port base_port.Currency
}

func NewCurrency(port base_port.Currency) *currency {
	mod := "currency"
	return &currency{
		port: port,
		Feature: base_domain.Feature{
			Name: "Currency",
			Mod:  &mod,
			// Menu:    true,
			Actions: "list,create,update,delete",
		},
	}
}

func (srv *currency) Get(id string) (base_domain.Currency, error) {
	currency, err := srv.port.Get(id)
	if err != nil {
		return base_domain.Currency{}, errors.New("unable to get currency")
	}
	return currency, nil
}

func (srv *currency) List(search string, order map[string]interface{}, page string, pageSize string) (base_domain.List, error) {
	pg, _ := strconv.Atoi(page)
	pgs, _ := strconv.Atoi(pageSize)
	list, err := srv.port.List(search, order, pg, pgs)
	if err != nil {
		return list, errors.New("unable to get list")
	}
	return list, nil
}

func (srv *currency) Find(param map[string]interface{}) ([]base_domain.Currency, error) {
	currency, err := srv.port.Find(param)
	if err != nil {
		return currency, errors.New("unable to get currency")
	}
	return currency, nil
}

func (srv *currency) Create(params map[string]interface{}) (base_domain.Currency, error) {
	var currency = base_domain.Currency{}
	srv.updateFields(&currency, params)
	return srv.CreateByInstance(currency)
}

func (srv *currency) CreateByInstance(currency base_domain.Currency) (base_domain.Currency, error) {
	if err := srv.port.Store(&currency); err != nil {
		return currency, errors.New("unable to create currency")
	}
	return currency, nil
}
func (srv *currency) Update(id string, params map[string]interface{}) (base_domain.Currency, error) {
	currency, err := srv.Get(id)
	if err != nil {
		return currency, err
	}
	srv.updateFields(&currency, params)
	if err := srv.port.Update(&currency); err != nil {
		return base_domain.Currency{}, errors.New("unable to create currency")
	}
	return currency, nil
}

func (srv *currency) updateFields(currency *base_domain.Currency, params map[string]interface{}) {
	if name, ok := params["name"]; ok {
		currency.Name = fmt.Sprint(name)
	}
	if photo, ok := params["photo"]; ok {
		currency.Photo = fmt.Sprint(photo)
	}
	if symbol, ok := params["symbol"]; ok {
		currency.Symbol = fmt.Sprint(symbol)
	}
	if unit_name, ok := params["unit_name"]; ok {
		currency.UnitName = fmt.Sprint(unit_name)
	}
	if unit_symbol, ok := params["unit_symbol"]; ok {
		currency.UnitSymbol = fmt.Sprint(unit_symbol)
	}
	if active, ok := params["active"]; ok {
		currency.Active, _ = strconv.ParseBool(fmt.Sprint(active))
	}
}
