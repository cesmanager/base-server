package base_blocs

import (
	"errors"
	"fmt"
	"strconv"

	"gitlab.com/cesmanager/base-server/core/base_domain"
	"gitlab.com/cesmanager/base-server/core/base_port"
)

type outlet struct {
	base_domain.Feature
	port base_port.Outlet
}

func NewOutlet(port base_port.Outlet) *outlet {
	mod := "outlet"
	return &outlet{
		port: port,
		Feature: base_domain.Feature{
			Name:    "Outlet",
			Mod:     &mod,
			Menu:    true,
			Actions: "list,create,update,delete",
		},
	}
}

func (srv *outlet) Get(id string) (base_domain.Outlet, error) {
	outlet, err := srv.port.Get(id)
	if err != nil {
		return outlet, errors.New("unable to get outlet")
	}
	return outlet, nil
}

func (srv *outlet) List(param map[string]interface{}, order map[string]interface{}, page string, pageSize string) (base_domain.List, error) {
	pg, _ := strconv.Atoi(page)
	pgs, _ := strconv.Atoi(pageSize)
	list, err := srv.port.List(param, order, pg, pgs)
	if err != nil {
		return list, errors.New("unable to get list")
	}
	return list, nil
}

func (srv *outlet) Find(param map[string]interface{}) ([]base_domain.Outlet, error) {
	outlet, err := srv.port.Find(param)
	if err != nil {
		return outlet, errors.New("unable to get outlet")
	}
	return outlet, nil
}

func (srv *outlet) Create(params map[string]interface{}) (base_domain.Outlet, error) {
	var outlet = base_domain.Outlet{}
	srv.updateFields(&outlet, params)
	if err := srv.port.Store(&outlet); err != nil {
		return base_domain.Outlet{}, errors.New("unable to create outlet")
	}
	return outlet, nil
}

func (srv *outlet) CreateByInstance(outlet base_domain.Outlet) (base_domain.Outlet, error) {
	if err := srv.port.Store(&outlet); err != nil {
		return base_domain.Outlet{}, errors.New("unable to create outlet")
	}
	return outlet, nil
}
func (srv *outlet) Update(id string, params map[string]interface{}) (base_domain.Outlet, error) {
	outlet, err := srv.Get(id)
	if err != nil {
		return outlet, err
	}
	srv.updateFields(&outlet, params)
	if err := srv.port.Update(&outlet); err != nil {
		return base_domain.Outlet{}, errors.New("unable to create outlet")
	}
	return outlet, nil
}

func (srv *outlet) updateFields(outlet *base_domain.Outlet, params map[string]interface{}) {
	if title, ok := params["title"]; ok {
		outlet.Title = fmt.Sprint(title)
	}
	if profile_id, ok := params["profile_id"]; ok {
		u64, _ := strconv.ParseUint(fmt.Sprint(profile_id), 10, 32)
		outlet.ProfileID = uint(u64)
	}
	if customer_id, ok := params["customer_id"]; ok {
		u64, _ := strconv.ParseUint(fmt.Sprint(customer_id), 10, 32)
		_u64 := uint(u64)
		outlet.CustomerID = &_u64
	}
	if currency_id, ok := params["currency_id"]; ok {
		u64, _ := strconv.ParseUint(fmt.Sprint(currency_id), 10, 32)
		outlet.CurrencyID = uint(u64)
	}
	if tax_account_id, ok := params["tax_account_id"]; ok {
		u64, _ := strconv.ParseUint(fmt.Sprint(tax_account_id), 10, 32)
		_u64 := uint(u64)
		outlet.TaxAccountID = &_u64
	}
	if invoice_category_id, ok := params["invoice_category_id"]; ok {
		u64, _ := strconv.ParseUint(fmt.Sprint(invoice_category_id), 10, 32)
		_u64 := uint(u64)
		outlet.InvoiceCategoryID = &_u64
	}
	if verification_template, ok := params["verification_template"]; ok {
		outlet.VerificationTemplate = fmt.Sprint(verification_template)
	}
	if recovery_template, ok := params["recovery_template"]; ok {
		outlet.RecoveryTemplate = fmt.Sprint(recovery_template)
	}
}
