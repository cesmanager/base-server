package base_blocs

import (
	"strconv"

	"gitlab.com/cesmanager/base-server/core/base_domain"
	"gitlab.com/cesmanager/base-server/core/base_port"
)

type notified struct {
	base_domain.Feature
	port base_port.Notified
}

func NewNotified(port base_port.Notified) *notified {
	mod := "notified"
	return &notified{
		port: port,
		Feature: base_domain.Feature{
			Name:    "Notified",
			Mod:     &mod,
			Actions: "list,delete",
		},
	}
}

func (srv *notified) Read(notificationID string, profileID string) (base_domain.Notified, error) {
	data, err := srv.port.Read(notificationID, profileID)
	if err != nil {
		return data, err
	}
	//mark as read
	data.Seen = true
	srv.port.Update(&data)
	return data, nil
}

func (srv *notified) NotifyAll(notificationID string) error {
	return srv.port.NotifyAll(notificationID)
}

func (srv *notified) NotifyProfiles(notificationID string, profileID []string) error {
	return srv.port.NotifyProfiles(notificationID, profileID)
}

func (srv *notified) NotifyUserGroup(notificationID string, userGroupID []string) error {
	return srv.port.NotifyUserGroup(notificationID, userGroupID)
}

func (srv *notified) List(params map[string]interface{}, order map[string]interface{}, page string, pageSize string) (base_domain.List, error) {
	pg, _ := strconv.Atoi(page)
	pgs, _ := strconv.Atoi(pageSize)
	return srv.port.List(params, order, pg, pgs)
}

func (srv *notified) Delete(notificationID string, profileID string) error {
	return srv.port.Delete(notificationID, profileID)
}
