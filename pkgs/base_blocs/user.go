package base_blocs

import (
	"errors"
	"fmt"
	"strconv"

	"gitlab.com/cesmanager/base-server/core/base_domain"
	"gitlab.com/cesmanager/base-server/core/base_port"
	"golang.org/x/crypto/bcrypt"
)

type user struct {
	base_domain.Feature
	port base_port.User
}

func NewUser(port base_port.User) *user {
	mod := "user"
	return &user{
		port: port,
		Feature: base_domain.Feature{
			Name:    "User",
			Mod:     &mod,
			Menu:    true,
			Actions: "list,create,update,delete",
		},
	}
}

func (srv *user) Get(id string) (base_domain.User, error) {
	user, err := srv.port.Get(id)
	if err != nil {
		return user, errors.New("unable to get user")
	}
	if user.ID == 0 {
		return user, errors.New("no user found")
	}
	return user, nil
}

func (srv *user) List(param map[string]interface{}, order map[string]interface{}, page string, pageSize string) (base_domain.List, error) {
	pg, _ := strconv.Atoi(page)
	pgs, _ := strconv.Atoi(pageSize)
	list, err := srv.port.List(param, order, pg, pgs)
	if err != nil {
		return list, errors.New("unable to get list")
	}
	return list, nil
}

func (srv *user) Find(param map[string]interface{}) ([]base_domain.User, error) {
	user, err := srv.port.Find(param)
	if err != nil {
		return user, errors.New("unable to get user")
	}
	return user, nil
}

func (srv *user) Create(params map[string]interface{}) (base_domain.User, error) {
	var user = base_domain.User{}
	srv.updateFields(&user, params)
	err := srv.port.Store(&user)
	if err != nil {
		return user, errors.New("unable to create user")
	}
	return user, nil
}
func (srv *user) CreateByInstance(user base_domain.User) (base_domain.User, error) {
	err := srv.port.Store(&user)
	if err != nil {
		return user, errors.New("unable to create user")
	}
	return user, nil
}
func (srv *user) Update(id string, params map[string]interface{}) (base_domain.User, error) {
	user, err := srv.Get(id)
	if err != nil {
		return user, err
	}
	srv.updateFields(&user, params)
	if err := srv.port.Update(&user); err != nil {
		return user, errors.New("unable to create user")
	}
	return user, nil
}

func (srv *user) updateFields(user *base_domain.User, params map[string]interface{}) error {
	if uname, ok := params["uname"]; ok {
		user.Uname = fmt.Sprintf("%v", uname)
	}
	if pword, ok := params["pword"]; ok {
		//encrypt password
		bytes, err := bcrypt.GenerateFromPassword([]byte(fmt.Sprintf("%v", pword)), bcrypt.DefaultCost)
		if err != nil {
			return errors.New("unable to encrypt password")
		}
		user.Pword = string(bytes)
	}
	if vcode, ok := params["vcode"]; ok {
		//update vcode and message the user
		user.Vcode = fmt.Sprintf("%v", vcode)
	}
	if profileID, ok := params["profile_id"]; ok {
		u64, _ := strconv.ParseUint(fmt.Sprintf("%v", profileID), 10, 32)
		user.ProfileID = uint(u64)
	}
	if userGroup, ok := params["user_group_id"]; ok {
		u64, _ := strconv.ParseUint(fmt.Sprintf("%v", userGroup), 10, 32)
		user.UserGroupID = uint(u64)
	}
	if _status, ok := params["status"]; ok {
		status, _ := strconv.Atoi(fmt.Sprintf("%v", _status))
		user.Status = uint(status)
	}
	return nil
}
