package base_blocs

import (
	"bytes"
	"fmt"
	"os"
	"os/exec"
	"time"

	"gitlab.com/cesmanager/base-server/core/base_domain"
	"gitlab.com/cesmanager/base-server/pkgs/base_blocs/wrap_sftp"
)

// BackUp Database ...
func BackUp(c base_domain.AppConfig, tag string) error {
	if tag == "" {
		return fmt.Errorf("company tag is required")
	}
	utils := NewUtils(c, &tag)
	sftp, close := utils.ConnectSFTP(c.Sftp)
	if sftp != nil {
		defer close()
		utils.SetSftp(sftp)
	}
	dbName := c.DataBase[c.ActiveDB].DBPrefix + tag
	dumpfile := utils.CorrectPath(fmt.Sprint(utils.DBDir(), "/", dbName, "_", time.Now().Unix(), "_dump.sql"))
	//creat the dump file
	var dump wrap_sftp.File
	dump, err := utils.Create(dumpfile)
	if err != nil {
		utils.Log(fmt.Sprintf("%v, unable to create dump file >> %v", err, dumpfile), BackUpError)
		return err
	}
	defer func() {
		dump.Close()
		if err != nil {
			utils.Remove(dumpfile)
		}
	}()
	if c.DataBase[c.ActiveDB].SShTag == "" { //if database is local
		//create the password file
		pfile := fmt.Sprint(dbName, "_password.cnf")
		err = utils.WriteFile(pfile, []byte(fmt.Sprint(
			"[mysqldump]\n",
			"password=", "\""+c.DataBase[c.ActiveDB].Password+"\"",
		)), os.ModePerm)
		if err != nil {
			utils.Log(fmt.Sprintf("%v, unable to create password file >> %v", err, pfile), BackUpError)
			return err
		}
		//remove file after
		defer utils.Remove(pfile)
		cmd := exec.Command("mysqldump",
			fmt.Sprint("--defaults-extra-file=", pfile),
			"-u", c.DataBase[c.ActiveDB].User,
			"--add-drop-database",
			"--add-drop-table",
			"--add-drop-trigger",
			"--opt",
			"--databases", dbName,
		)
		var Stderr bytes.Buffer
		cmd.Stderr = &Stderr
		cmd.Stdout = dump
		err := cmd.Run()
		if err != nil {
			utils.Log("Error executing local mysqldump", BackUpError)
			return err
		}
		return nil
	}
	//run this section when db is hosted in a remote server
	ssh := utils.ConnectSSH(c.DataBase[c.ActiveDB].SShTag)
	if ssh != nil {
		defer ssh.Close()
	}
	dbSftp := utils.ConnectSFTPWith(ssh)
	if dbSftp != nil {
		defer dbSftp.Close()
	}
	_utils := NewUtils(c, &tag).SetPrefSftp(c.DataBase[c.ActiveDB].SShTag).SetSftp(dbSftp)
	pfile := _utils.CorrectPath(fmt.Sprint(_utils.DBDir(), "/", dbName, "_password.cnf"))
	err = _utils.WriteFile(pfile, []byte(fmt.Sprint(
		"[mysqldump]\n",
		"password=", "\""+c.DataBase[c.ActiveDB].Password+"\"",
	)), os.ModePerm)
	if err != nil {
		utils.Log(fmt.Sprintf("%v, unable to create password file >> %v", err, pfile), BackUpError)
		return err
	}
	//remove file after
	defer sftp.Remove(pfile)

	sess, err := ssh.NewSession()
	if err != nil {
		dump.Close()
		utils.Remove(dumpfile)
		utils.Log("Unable to start ssh session", BackUpError)
		return err
	}
	defer sess.Close()
	var Stderr bytes.Buffer
	sess.Stderr = &Stderr
	sess.Stdout = dump
	cmd := fmt.Sprint("mysqldump",
		" --defaults-extra-file=\"", pfile, "\"",
		" -u ", c.DataBase[c.ActiveDB].User,
		" --add-drop-database",
		" --add-drop-table",
		" --add-drop-trigger",
		" --opt",
		" --databases ", dbName,
	)
	err = sess.Run(cmd)
	if err != nil {
		utils.Log("Error executing remote mysqldump", BackUpError)
		return err
	}
	return nil
}
