package wrap_sftp

import (
	"io"
	"net/http"
	"os"
	"path"

	"github.com/pkg/sftp"
)

//a custom file interface for mixing sftp and os file system
type File interface {
	Chmod(mode os.FileMode) error
	Truncate(size int64) error
	Close() error
	Name() string
	Read(b []byte) (int, error)
	ReadAt(b []byte, off int64) (int, error)
	Stat() (os.FileInfo, error)
	ReadFrom(r io.Reader) (int64, error)
	Seek(offset int64, whence int) (int64, error)
	Sync() error
	Chown(uid, gid int) error
	Write(b []byte) (int, error)
	WriteAt(b []byte, off int64) (written int, err error)
}

// type File struct {
//     *file // os specific
// }

// func (*os.File).Chdir() error
// func (*os.File).Chmod(mode fs.FileMode) error
// func (*os.File).Chown(uid int, gid int) error
// func (*os.File).Close() error
// func (*os.File).Fd() uintptr
// func (*os.File).Name() string
// func (*os.File).Read(b []byte) (n int, err error)
// func (*os.File).ReadAt(b []byte, off int64) (n int, err error)
// func (*os.File).ReadDir(n int) ([]fs.DirEntry, error)
// func (*os.File).ReadFrom(r io.Reader) (n int64, err error)
// func (*os.File).Readdir(n int) ([]fs.FileInfo, error)
// func (*os.File).Readdirnames(n int) (names []string, err error)
// func (*os.File).Seek(offset int64, whence int) (ret int64, err error)
// func (*os.File).SetDeadline(t time.Time) error
// func (*os.File).SetReadDeadline(t time.Time) error
// func (*os.File).SetWriteDeadline(t time.Time) error
// func (*os.File).Stat() (fs.FileInfo, error)
// func (*os.File).Sync() error
// func (*os.File).SyscallConn() (syscall.RawConn, error)
// func (*os.File).Truncate(size int64) error
// func (*os.File).Write(b []byte) (n int, err error)
// func (*os.File).WriteAt(b []byte, off int64) (n int, err error)
// func (*os.File).WriteString(s string) (n int, err error)

//custom file for sftp connection
type HttpFile struct {
	*sftp.File
	*sftp.Client
}

func (f *HttpFile) Readdir(count int) ([]os.FileInfo, error) {
	return f.Client.ReadDir(f.File.Name())
}
func (f *HttpFile) Close() error {
	return f.File.Close()
}
func (f *HttpFile) Read(b []byte) (int, error) {
	n, e := f.File.Read(b)
	return n, e
}

// func (f HttpFile) WriteTo(w io.Writer) (written int64, err error) {
// 	return f.File.WriteTo(w)
// }
func (f *HttpFile) Seek(offset int64, whence int) (int64, error) {
	return f.File.Seek(offset, whence)
}
func (f *HttpFile) Stat() (os.FileInfo, error) {
	return f.File.Stat()
}

type Dir struct {
	*sftp.Client
	base string
}

func (m Dir) SetUp(sftp *sftp.Client, base string) *Dir {
	m.Client = sftp
	m.base = base
	return &m
}

func (m Dir) Open(name string) (http.File, error) {
	var file *HttpFile
	if m.base == "" {
		m.base = "."
	}
	fullName := m.base + path.Clean("/"+name)
	f, err := m.Client.Open(fullName)
	if err != nil {
		return file, err
	}
	return &HttpFile{Client: m.Client, File: f}, nil
}
