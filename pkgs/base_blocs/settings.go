package base_blocs

import (
	"encoding/json"
	"errors"
	"fmt"
	"strconv"
	"strings"

	"gitlab.com/cesmanager/base-server/core/base_domain"
	"gitlab.com/cesmanager/base-server/core/base_port"
)

type Settings struct {
	base_domain.Feature
	port base_port.Settings
}

func NewSettings(port base_port.Settings) *Settings {
	mod := "settings"
	return &Settings{
		port: port,
		Feature: base_domain.Feature{
			Name:    "Settings",
			Mod:     &mod,
			Menu:    true,
			Actions: "save",
		},
	}
}

// Fetch Statuss
func (srv *Settings) Get() (base_domain.Settings, error) {
	s, err := srv.port.Get()
	if err != nil {
		return s, errors.New("unable to get settings")
	}
	s.SMTPPass = strings.TrimSpace(s.SMTPPass)
	s.SMTPServer = strings.TrimSpace(s.SMTPServer)
	s.SMTPUser = strings.TrimSpace(s.SMTPUser)
	return s, nil
}
func (srv *Settings) CreateByInstance(settings base_domain.Settings) (base_domain.Settings, error) {
	if err := srv.port.Store(&settings); err != nil {
		return settings, errors.New("unable to create settings")
	}
	return settings, nil
}

// Update setting by instance
func (srv *Settings) UpdateByInstance(settings *base_domain.Settings) error {
	if err := srv.port.Update(settings); err != nil {
		return errors.New("unable to update settings")
	}
	return nil
}

// Update Status title
func (srv *Settings) Update(params map[string]interface{}) (base_domain.Settings, error) {
	s, err := srv.Get()
	if err != nil {
		return s, err
	}
	srv.updateFields(&s, params)
	if err := srv.port.Update(&s); err != nil {
		return s, errors.New("unable to update settings")
	}
	return s, nil
}

func (srv *Settings) updateFields(setting *base_domain.Settings, params map[string]interface{}) error {
	_prm, _ := json.Marshal(params)
	json.Unmarshal(_prm, &setting)
	if data, ok := params["profile_active"]; ok {
		u64, _ := strconv.ParseUint(fmt.Sprint(data), 10, 32)
		setting.ProfileActive = uint(u64)
	}
	if data, ok := params["profile_suspended"]; ok {
		u64, _ := strconv.ParseUint(fmt.Sprint(data), 10, 32)
		setting.ProfileDisabled = uint(u64)
	}
	if data, ok := params["profile_disabled"]; ok {
		u64, _ := strconv.ParseUint(fmt.Sprint(data), 10, 32)
		setting.ProfileSuspended = uint(u64)
	}
	if data, ok := params["user_active"]; ok {
		u64, _ := strconv.ParseUint(fmt.Sprint(data), 10, 32)
		setting.UserActive = uint(u64)
	}
	if data, ok := params["user_disabled"]; ok {
		u64, _ := strconv.ParseUint(fmt.Sprint(data), 10, 32)
		setting.UserDisabled = uint(u64)
	}
	if port, ok := params["smtp_port"]; ok {
		setting.SMTPPort, _ = strconv.Atoi(fmt.Sprint(port))
	}
	return nil
}
