package base_blocs

import (
	"errors"
	"fmt"
	"strconv"

	"gitlab.com/cesmanager/base-server/core/base_domain"
	"gitlab.com/cesmanager/base-server/core/base_port"
)

type userGroup struct {
	base_domain.Feature
	port base_port.UserGroup
}

func NewUserGroup(port base_port.UserGroup) *userGroup {
	mod := "user_group"
	return &userGroup{
		port: port,
		Feature: base_domain.Feature{
			Name:    "User Group",
			Mod:     &mod,
			Menu:    true,
			Actions: "list,create,update,delete",
		},
	}
}

func (srv *userGroup) Get(id string) (base_domain.UserGroup, error) {
	group, err := srv.port.Get(id)
	if err != nil {
		return group, errors.New("unable to get group")
	}
	if group.ID == 0 {
		return group, errors.New("no group found")
	}
	return group, nil
}

func (srv *userGroup) List(param map[string]interface{}, order map[string]interface{}, page string, pageSize string) (base_domain.List, error) {
	pg, _ := strconv.Atoi(page)
	pgs, _ := strconv.Atoi(pageSize)
	list, err := srv.port.List(param, order, pg, pgs)
	if err != nil {
		return list, errors.New("unable to get list")
	}
	return list, nil
}

func (srv *userGroup) Find(param map[string]interface{}) ([]base_domain.UserGroup, error) {
	group, err := srv.port.Find(param)
	if err != nil {
		return group, errors.New("unable to get group")
	}
	return group, nil
}

func (srv *userGroup) Create(params map[string]interface{}) (base_domain.UserGroup, error) {
	var group = base_domain.UserGroup{}
	srv.updateFields(&group, params)
	return srv.CreateByInstance(group)
}
func (srv *userGroup) CreateByInstance(group base_domain.UserGroup) (base_domain.UserGroup, error) {
	err := srv.port.Store(&group)
	if err != nil {
		return group, errors.New("unable to create group")
	}
	return group, nil
}
func (srv *userGroup) Update(id string, params map[string]interface{}) (base_domain.UserGroup, error) {
	group, err := srv.Get(id)
	if err != nil {
		return group, err
	}
	srv.updateFields(&group, params)
	if err := srv.port.Update(&group); err != nil {
		return group, errors.New("unable to create group")
	}
	return group, nil
}

func (srv *userGroup) VetPermit(user_id string, mod string, action string) (bool, error) {
	group, err := srv.port.GetByUser(user_id)
	if err != nil {
		return false, errors.New("permission error")
	}
	if group.SuperGroup {
		return true, nil
	}
	group.MapPermits()
	if permit, ok := (*group.MappedPermits)[mod]; ok {
		if _, ok := (*permit.MappedAction)[action]; ok {
			return true, nil
		}
	}
	return false, nil
}

func (srv *userGroup) updateFields(group *base_domain.UserGroup, params map[string]interface{}) error {
	if title, ok := params["title"]; ok {
		group.Title = fmt.Sprint(title)
	}
	if description, ok := params["description"]; ok {
		group.Description = fmt.Sprint(description)
	}
	if tag, ok := params["tag"]; ok {
		group.Tag = fmt.Sprint(tag)
	}
	if super_group, ok := params["super_group"]; ok {
		group.SuperGroup, _ = strconv.ParseBool(fmt.Sprint(super_group))
	}
	if active, ok := params["active"]; ok {
		group.Active, _ = strconv.ParseBool(fmt.Sprint(active))
	}
	return nil
}
