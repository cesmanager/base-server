package service

//Need ...
// var (
// 	Need utils.Need
// )

// func prep() {
// 	Need.Copy(&s.Need)
// 	utils.Custom = Need.Custom
// 	c.DBname = Need.DBname
// }

// //Create a ItemService
// func Create(
// 	title string,
// 	code string, unitprice string, capacity string,
// 	days string, hours string, minutes string, seconds string,
// 	categoryID string, active string) r.Response {
// 	//set the database name for connection
// 	prep()

// 	var ItemService s.ItemService
// 	var Item s.Item
// 	var response r.Response
// 	var msgs []interface{}

// 	if len(title) < 1 {
// 		msg := "title is required"
// 		msgs = append(msgs, map[string]string{"title": msg})
// 	}
// 	if len(unitprice) < 1 {
// 		msg := "Price is required"
// 		msgs = append(msgs, map[string]string{"user": msg})
// 	}

// 	if len(msgs) < 1 {
// 		//item definition

// 		Item.Title = title
// 		Item.Code = code

// 		/**
// 		* Remeber to set this to the cumpany default currency from settings later
// 		 */
// 		Item.CurrencyID = 1

// 		Item.ItemType = 2
// 		//connect to db
// 		db := c.DBConn()
// 		connection, _ := db.DB()
// 		defer connection.Close()

// 		//begin a transaction
// 		tx := db.Begin()
// 		if tx.Error != nil {
// 			msg := "Error while starting a transaction"
// 			msgs = append(msgs, msg)
// 		}
// 		_, err := Item.Add(tx)
// 		if err != nil {
// 			tx.Rollback()
// 			msg := "Unable to create Item"
// 			msgs = append(msgs, msg)
// 		} else {
// 			//ItemService definition
// 			UnitPrice, _ := strconv.ParseFloat(unitprice, 32)
// 			ItemService.UnitPrice = float32(UnitPrice)

// 			cty, _ := strconv.ParseFloat(capacity, 32)
// 			ItemService.Capacity = float32(cty)

// 			//generate the duration
// 			ItemService.ItemID = Item.ID
// 			var Duration util.Duration
// 			Duration.Assign(days, hours, minutes, seconds)
// 			ItemService.Duration = &Duration
// 			Active, _ := strconv.Atoi(active)
// 			ItemService.Active = int8(Active)
// 			_, err = ItemService.Add(tx)
// 			if err != nil {
// 				tx.Rollback()
// 				msg := "Unable to create ItemService"
// 				msgs = append(msgs, msg)
// 			} else {
// 				msg := "Service Created"
// 				response.Status = true
// 				msgs = append(msgs, msg)
// 				tx.Commit()
// 			}
// 		}

// 	}
// 	response.Body = msgs
// 	return response
// }

// //Update ItemService title
// func Update(id string, set map[string]interface{}) r.Response {
// 	//set the database name for connection
// 	prep()

// 	var ItemService s.ItemService
// 	var response r.Response
// 	var msgs []interface{}

// 	if len(id) < 1 {
// 		msg := "Service id is required"
// 		msgs = append(msgs, msg)
// 	}
// 	if len(msgs) < 0 {
// 		were := []utils.SQLWhere{{Query: "id = ?", Value: id}}
// 		db := c.DBConn()
// 		connection, _ := db.DB()
// 		defer connection.Close()
// 		_, err := ItemService.Update(db, set, were)
// 		if err != nil {
// 			msg := "Unable to Update Service"
// 			utils.Log(msg+" Reason:"+err.Error(), 1)
// 			msgs = append(msgs, msg)
// 		} else {
// 			response.Status = true
// 			msg := "Service updated"
// 			msgs = append(msgs, msg)
// 		}
// 	}
// 	response.Body = msgs
// 	return response
// }

// //Fetch ItemServices
// func Fetch(param []map[string]interface{}) r.Response {
// 	//set the database name for connection
// 	prep()

// 	var ItemService s.ItemService
// 	var response r.Response
// 	var msgs []interface{}

// 	var where []utils.SQLWhere
// 	for _, v := range param {
// 		where = append(where, utils.SQLWhere{Query: fmt.Sprintf("%v", v["q"]), Value: v["v"]})
// 	}
// 	db := c.DBConn()
// 	connection, _ := db.DB()
// 	defer connection.Close()
// 	ItemServices, _ := ItemService.Fetch(db, where)
// 	if len(ItemServices) < 1 {
// 		msg := "No ItemServices found"
// 		msgs = append(msgs, msg)
// 	} else {
// 		response.Status = true
// 		msgs = append(msgs, ItemServices)
// 	}
// 	response.Body = msgs
// 	return response
// }
