package base_blocs

import (
	"errors"
	"fmt"
	"strconv"
	"strings"

	"gitlab.com/cesmanager/base-server/core/base_domain"
	"gitlab.com/cesmanager/base-server/core/base_port"
)

type category struct {
	base_domain.Feature
	port base_port.Category
}

func NewCategory(port base_port.Category) *category {
	mod := "category"
	return &category{
		port: port,
		Feature: base_domain.Feature{
			Name:    "Category",
			Mod:     &mod,
			Actions: "list,create,update,delete",
		},
	}
}

func (srv *category) Get(id string) (base_domain.Category, error) {
	category, err := srv.port.Get(id)
	if err != nil {
		return base_domain.Category{}, errors.New("unable to get category")
	}
	if category.ID == 0 {
		return base_domain.Category{}, errors.New("no category found")
	}
	return category, nil
}

func (srv *category) List(param map[string]interface{}, order map[string]interface{}, page string, pageSize string) (base_domain.List, error) {
	pg, _ := strconv.Atoi(page)
	pgs, _ := strconv.Atoi(pageSize)
	list, err := srv.port.List(param, order, pg, pgs)
	if err != nil {
		return list, errors.New("unable to get list")
	}
	return list, nil
}

func (srv *category) Find(param map[string]interface{}, operator *map[string]interface{}) ([]base_domain.Category, error) {
	ParamOperator(&param, operator)
	category, err := srv.port.Find(param)
	if err != nil {
		return category, errors.New("unable to get category(s)")
	}
	return category, nil
}

// create the geolocation. Always call this in a transaction
func (srv *category) Create(params map[string]interface{}) (base_domain.Category, error) {
	var category = base_domain.Category{}
	srv.updateFields(&category, params)
	return srv.CreateByInstance(category)
}

// create the geolocation. Always call this in a transaction
func (srv *category) CreateByInstance(category base_domain.Category) (base_domain.Category, error) {
	if category.ParentID != nil {
		//find the parents
		parent, err := srv.Get(fmt.Sprint(*category.ParentID))
		if err != nil {
			return category, err
		}
		//get the parents children
		gs, err := srv.Find(map[string]interface{}{"parent_id": *category.ParentID}, nil)
		if err != nil {
			return category, err
		}
		category.GroupTag = fmt.Sprintf("%v-%v", parent.GroupTag, len(gs)+1)
		category.Title = fmt.Sprintf("%v>%v", parent.Title, category.Name)
	}
	if err := srv.port.Store(&category); err != nil {
		return base_domain.Category{}, errors.New("unable to create category")
	}
	if category.ParentID == nil {
		category.Title = category.Name
		category.GroupTag = fmt.Sprint(category.ID)
		//update it
		if err := srv.port.Update(&category); err != nil {
			return base_domain.Category{}, errors.New("unable to update group tag")
		}
	}
	return category, nil
}

// update the category. Recommend calling this in a transaction
func (srv *category) Update(id string, params map[string]interface{}) (base_domain.Category, error) {
	category, err := srv.Get(id)
	if err != nil {
		return category, err
	}
	//get the old group tag
	oldGroupTag := category.GroupTag
	//get old name
	oldName := category.Name
	//get old title
	oldTitle := category.Title
	//get the old parent
	oldParentID := (*category.ParentID)
	srv.updateFields(&category, params)
	if oldParentID != *category.ParentID || oldName != category.Name {
		//find the new parent
		parent, err := srv.Get(fmt.Sprint((*category.ParentID)))
		if err != nil {
			return category, err
		}
		//if new parent, count new parents children and update group tag
		if oldParentID != parent.ID {
			count, err := srv.port.Count(map[string]interface{}{"parent_id": parent.ID}, nil)
			if err != nil {
				return category, err
			}
			category.GroupTag = fmt.Sprintf("%v-%v", parent.GroupTag, count+1)
		}
		category.Title = fmt.Sprintf("%v>%v", parent.Title, category.Name)
		//update it
		if err := srv.port.Update(&category); err != nil {
			return category, errors.New("unable to update old category group")
		}
		//find all the children and update their group tag
		subs, err := srv.port.Find(map[string]interface{}{
			"group_tag": map[string]interface{}{"val": oldGroupTag + "-%", "con": "LIKE"}},
		)
		if err != nil {
			return category, err
		}
		//update all of its subs
		for _, sub := range subs {
			sub.GroupTag = strings.Replace(sub.GroupTag, oldGroupTag+"-", category.GroupTag+"-", 1)
			sub.Title = strings.Replace(sub.Title, oldTitle+">", category.Title+">", 1)
			if err := srv.port.Update(&sub); err != nil {
				return category, errors.New("unable to update all sub category")
			}
		}

		//arrange old group
		if oldParentID != 0 && oldParentID != *category.ParentID {
			//find its old parent
			oldParent, err := srv.Get(fmt.Sprint(oldParentID))
			if err != nil {
				return category, err
			}
			//get old group mates
			old_groupmates, err := srv.Find(map[string]interface{}{"parent_id": oldParentID}, nil)
			if err != nil {
				return category, err
			}
			//for each of them
			for i := 0; i < len(old_groupmates); i++ {
				gl := old_groupmates[i]
				oldmGroupTag := gl.GroupTag //get the old group tag
				newmGroupTag := fmt.Sprintf("%v-%v", oldParent.GroupTag, i+1)
				//update with new value
				gl.GroupTag = newmGroupTag
				if err := srv.port.Update(&category); err != nil {
					return category, errors.New("unable to update old category group")
				}
				//find all of its subs and update their group tag
				subs, err := srv.port.Find(map[string]interface{}{
					"group_tag": map[string]interface{}{"val": oldmGroupTag + "-%", "con": "LIKE"}},
				)
				if err != nil {
					return category, err
				}
				//update all of its subs
				for _, sub := range subs {
					sub.GroupTag = strings.Replace(sub.GroupTag, oldmGroupTag, newmGroupTag, 1)
					if err := srv.port.Update(&sub); err != nil {
						return base_domain.Category{}, errors.New("unable to update all sub category")
					}
				}
			}
		}
	} else {
		if err := srv.port.Update(&category); err != nil {
			return category, errors.New("unable to update old category group")
		}
	}
	return category, nil
}

// update the geolocation. Recommend calling this in a transaction
func (srv *category) Delete(id string) error {
	category, err := srv.Get(id)
	if err != nil {
		return err
	}
	//find all of the category children and update their group tag
	cnt, err := srv.port.Count(map[string]interface{}{"group_tag": category.GroupTag + "-%"}, &map[string]string{"group_tag": "LIKE"})
	if err != nil {
		return err
	}
	if cnt != 0 {
		return errors.New("category must have no child node")
	}
	//delete the category
	return srv.port.Delete(map[string]interface{}{"id": category.ID})
}

func (srv *category) updateFields(category *base_domain.Category, params map[string]interface{}) {
	if name, ok := params["name"]; ok {
		category.Name = fmt.Sprint(name)
	}
	if parent_id, ok := params["parent_id"]; ok {
		if parent_id != "" {
			u64, _ := strconv.ParseUint(fmt.Sprint(parent_id), 10, 32)
			_u64 := uint(u64)
			category.ParentID = &_u64
		} else {
			category.ParentID = nil
		}
	}
}
