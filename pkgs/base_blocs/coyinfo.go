package base_blocs

import (
	"errors"
	"fmt"
	"strconv"

	"gitlab.com/cesmanager/base-server/core/base_domain"
	"gitlab.com/cesmanager/base-server/core/base_port"
)

type coyinfo struct {
	base_domain.Feature
	port base_port.CoyInfo
}

func NewCoyInfo(port base_port.CoyInfo) *coyinfo {
	mod := "coy"
	return &coyinfo{
		port: port,
		Feature: base_domain.Feature{
			Name:    "Company",
			Mod:     &mod,
			Menu:    true,
			Actions: "list,create,update",
		},
	}
}

func (srv *coyinfo) Get(id string) (base_domain.CoyInfo, error) {
	coy, err := srv.port.Get(id)
	if err != nil {
		return coy, errors.New("unable to get company")
	}
	return coy, nil
}

func (srv *coyinfo) List(param map[string]interface{}, order map[string]interface{}, page string, pageSize string) (base_domain.List, error) {
	pg, _ := strconv.Atoi(page)
	pgs, _ := strconv.Atoi(pageSize)
	list, err := srv.port.List(param, order, pg, pgs)
	if err != nil {
		return list, errors.New("unable to get list")
	}
	return list, nil
}

//Create company
func (srv *coyinfo) Create(params map[string]interface{}) (coy base_domain.CoyInfo, err error) {
	if err := srv.requiredParams(params); err != nil {
		return coy, err
	}
	srv.updateFields(&coy, params)
	//ensure domain is not already registered
	coys, err := srv.port.Find(map[string]interface{}{
		"tag": coy.Tag,
	})
	if err != nil {
		return coy, errors.New("tag verification error")
	}
	//check if error already exist
	if len(coys) > 0 {
		return coy, errors.New("tag already exist")
	}

	err = srv.port.Store(&coy)
	if err != nil {
		return coy, errors.New("company creation failed")
	}
	return coy, nil
}

func (srv *coyinfo) Update(id string, params map[string]interface{}) (base_domain.CoyInfo, error) {
	coy, err := srv.Get(id)
	if err != nil {
		return coy, err
	}
	srv.updateFields(&coy, params)
	if err := srv.port.Update(&coy); err != nil {
		return coy, errors.New("unable to update company")
	}
	return coy, nil
}

func (srv *coyinfo) Find(params map[string]interface{}) ([]base_domain.CoyInfo, error) {
	coys, err := srv.port.Find(params)
	if err != nil {
		return coys, errors.New("unable to get company info")
	}
	return coys, nil
}
func (srv *coyinfo) requiredParams(params map[string]interface{}) error {
	if _, ok := params["tag"]; !ok {
		return errors.New("tag is required")
	}
	if _, ok := params["domain"]; !ok {
		return errors.New("domain is required")
	}
	return nil
}

func (srv *coyinfo) updateFields(coy *base_domain.CoyInfo, params map[string]interface{}) error {
	if name, ok := params["name"]; ok {
		coy.Name = fmt.Sprint(name)
	}
	if typ, ok := params["type_id"]; ok && typ != "null" && typ != "" {
		v, _ := strconv.Atoi(fmt.Sprint(typ))
		coy.TypeID = &v
	}
	if status, ok := params["status"]; ok {
		v, _ := strconv.Atoi(fmt.Sprint(status))
		coy.Status = int8(v)
	}
	if tag, ok := params["tag"]; ok {
		coy.Tag = fmt.Sprint(tag)
	}
	if features, ok := params["features"]; ok {
		coy.Features = fmt.Sprint(features)
	}
	if contact, ok := params["contact"]; ok {
		u64, _ := strconv.ParseUint(fmt.Sprint(contact), 10, 32)
		_u64 := uint(u64)
		coy.Contact = &_u64
	}
	if domain, ok := params["domain"]; ok {
		coy.Domain = fmt.Sprint(domain)
	}
	if root, ok := params["root"]; ok {
		v, _ := strconv.ParseBool(fmt.Sprint(root))
		coy.Root = v
	}
	return nil
}
