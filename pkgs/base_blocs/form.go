package base_blocs

import (
	"encoding/json"
	"errors"
	"fmt"
	"strconv"

	"gitlab.com/cesmanager/base-server/core/base_domain"
	"gitlab.com/cesmanager/base-server/core/base_port"
)

type form struct {
	port base_port.FormPort
	base_domain.Feature
}

func NewForm(port base_port.FormPort) *form {
	mod := "eform"
	return &form{port: port,
		Feature: base_domain.Feature{
			Name:    "e-Form",
			Mod:     &mod,
			Menu:    true,
			Actions: "list,create,update,delete",
		},
	}
}

func (b *form) Get(id string) (base_domain.Form, error) {
	rec, err := b.port.Get(id)
	if err != nil {
		return rec, err
	}
	if rec.ID == 0 {
		return rec, errors.New("no form found")
	}
	//get the mapped version of the data
	json.Unmarshal([]byte(rec.Data), &rec.MapedData)
	rec.Data = ""
	return rec, err
}

func (srv *form) List(param map[string]interface{}, order map[string]interface{}, page string, pageSize string) (base_domain.List, error) {
	pg, _ := strconv.Atoi(page)
	pgs, _ := strconv.Atoi(pageSize)
	list, err := srv.port.List(param, order, pg, pgs)
	if err != nil {
		return list, errors.New("unable to get list")
	}
	return list, nil
}

func (b *form) Create(params map[string]interface{}) (base_domain.Form, error) {
	rec := base_domain.Form{}
	b.parse(&rec, params)
	err := b.port.Store(&rec)
	return rec, err
}

func (b *form) Update(id string, params map[string]interface{}) (base_domain.Form, error) {
	rec, err := b.Get(id)
	if err != nil {
		return rec, err
	}
	b.parse(&rec, params)
	err = b.port.Update(&rec)
	return rec, err
}

func (b *form) Find(params map[string]interface{}) ([]base_domain.Form, error) {
	recs, err := b.port.Find(params)
	if err == nil {
		for i := range recs {
			json.Unmarshal([]byte(recs[i].Data), &recs[i].MapedData)
			recs[i].Data = ""
		}
	}
	return recs, err
}

func (b *form) parse(rec *base_domain.Form, params map[string]interface{}) {
	if _, ok := params["title"]; ok {
		rec.Title = fmt.Sprint(params["title"])
	}
	if _, ok := params["description"]; ok {
		rec.Description = fmt.Sprint(params["description"])
	}
	if _, ok := params["ref"]; ok {
		rec.Ref = fmt.Sprint(params["ref"])
	}
	if _, ok := params["document_required"]; ok {
		rec.DocumentRequired, _ = strconv.ParseBool(fmt.Sprint(params["document_required"]))
	}
	if _, ok := params["data"]; ok {
		data, err := json.Marshal(params["data"])
		if err == nil {
			rec.Data = string(data)
		}
	}
	if _category, ok := params["category_id"]; ok && _category != nil && fmt.Sprint(_category) != "" {
		u64, _ := strconv.ParseUint(fmt.Sprint(_category), 10, 32)
		_u64 := uint(u64)
		rec.CategoryID = &_u64
	}
}
