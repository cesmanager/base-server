package base_blocs

import (
	"errors"
	"fmt"
	"strconv"

	"gitlab.com/cesmanager/base-server/core/base_domain"
	"gitlab.com/cesmanager/base-server/core/base_port"
)

type currencyExchange struct {
	base_domain.Feature
	port base_port.CurrencyExchange
}

func NewCurrencyExchange(port base_port.CurrencyExchange) *currencyExchange {
	mod := "currency_exchange"
	return &currencyExchange{
		port: port,
		Feature: base_domain.Feature{
			Name:    "Exchange Rates",
			Mod:     &mod,
			Actions: "list,create,update,delete",
		},
	}
}

func (srv *currencyExchange) Get(id string) (base_domain.CurrencyExchange, error) {
	currencyExchange, err := srv.port.Get(id)
	if err != nil {
		return base_domain.CurrencyExchange{}, errors.New("unable to get currencyExchange")
	}
	return currencyExchange, nil
}

func (srv *currencyExchange) Find(param map[string]interface{}) ([]base_domain.CurrencyExchange, error) {
	currencyExchange, err := srv.port.Find(param)
	if err != nil {
		return currencyExchange, errors.New("unable to get currencyExchange")
	}
	return currencyExchange, nil
}

func (srv *currencyExchange) Create(params map[string]interface{}) (base_domain.CurrencyExchange, error) {
	var exchange = base_domain.CurrencyExchange{}
	srv.updateFields(&exchange, params)
	return srv.CreateByInstance(exchange)
}

func (srv *currencyExchange) CreateByInstance(exchange base_domain.CurrencyExchange) (base_domain.CurrencyExchange, error) {
	if err := srv.port.Store(&exchange); err != nil {
		return exchange, errors.New("unable to create currencyExchange")
	}
	return exchange, nil
}
func (srv *currencyExchange) Update(id string, params map[string]interface{}) (base_domain.CurrencyExchange, error) {
	currencyExchange, err := srv.Get(id)
	if err != nil {
		return currencyExchange, err
	}
	srv.updateFields(&currencyExchange, params)
	if err := srv.port.Update(&currencyExchange); err != nil {
		return base_domain.CurrencyExchange{}, errors.New("unable to create currencyExchange")
	}
	return currencyExchange, nil
}

func (srv *currencyExchange) updateFields(currencyExchange *base_domain.CurrencyExchange, params map[string]interface{}) {
	if title, ok := params["title"]; ok {
		currencyExchange.Title = fmt.Sprint(title)
	}
	if currency_id, ok := params["currency_id"]; ok {
		u64, _ := strconv.ParseUint(fmt.Sprint(currency_id), 10, 32)
		currencyExchange.CurrencyID = uint(u64)
	}
	if to_id, ok := params["to_id"]; ok {
		u64, _ := strconv.ParseUint(fmt.Sprint(to_id), 10, 32)
		currencyExchange.ToID = uint(u64)
	}
	if ex_value, ok := params["ex_value"]; ok {
		f64, _ := strconv.ParseFloat(fmt.Sprint(ex_value), 32)
		currencyExchange.ExValue = float32(f64)
	}
}
