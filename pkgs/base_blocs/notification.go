package base_blocs

import (
	"errors"
	"fmt"
	"strconv"

	"gitlab.com/cesmanager/base-server/core/base_domain"
	"gitlab.com/cesmanager/base-server/core/base_port"
)

type notification struct {
	base_domain.Feature
	port base_port.Notification
}

func NewNotification(port base_port.Notification) *notification {
	mod := "notification"
	return &notification{
		port: port,
		Feature: base_domain.Feature{
			Name:    "Notification",
			Mod:     &mod,
			Menu:    true,
			Actions: "list,create,update,delete",
		},
	}
}

func (srv *notification) Get(id string) (base_domain.Notification, error) {
	data, err := srv.port.Get(id)
	if err != nil {
		return data, errors.New("unable to get notification")
	}
	return data, nil
}

func (srv *notification) Create(params map[string]interface{}) (base_domain.Notification, error) {
	var data = base_domain.Notification{}
	srv.updateFields(&data, params)
	err := srv.port.Store(&data)
	return data, err
}

func (srv *notification) Update(id string, params map[string]interface{}) (base_domain.Notification, error) {
	data, err := srv.Get(id)
	if err != nil {
		return data, err
	}
	if data.ID == 0 {
		return data, fmt.Errorf("no notification found")
	}
	srv.updateFields(&data, params)
	err = srv.port.Update(&data)
	return data, err
}

func (srv *notification) List(params map[string]interface{}, order map[string]interface{}, page string, pageSize string) (base_domain.List, error) {
	pg, _ := strconv.Atoi(page)
	pgs, _ := strconv.Atoi(pageSize)
	list, err := srv.port.List(params, order, pg, pgs)
	if err != nil {
		return list, errors.New("unable to get list")
	}
	return list, nil
}

func (srv *notification) Delete(ids []string) error {
	err := srv.port.Delete(ids)
	if err != nil {
		return errors.New("notification delete failed")
	}
	return nil
}

func (srv *notification) updateFields(model *base_domain.Notification, params map[string]interface{}) {
	if title, ok := params["title"]; ok {
		model.Title = fmt.Sprint(title)
	}
	if message, ok := params["message"]; ok {
		model.Message = fmt.Sprint(message)
	}
	if created_by_id, ok := params["created_by_id"]; ok && created_by_id != "null" && fmt.Sprint(created_by_id) != "" {
		u64, _ := strconv.ParseUint(fmt.Sprint(created_by_id), 10, 32)
		_u64 := uint(u64)
		model.CreatedByID = &_u64
	}
	if tag, ok := params["tag"]; ok && tag != "null" {
		f := fmt.Sprint(tag)
		model.Tag = &f
	}
}
