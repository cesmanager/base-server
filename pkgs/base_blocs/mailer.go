package base_blocs

import (
	"fmt"
	"net/smtp"
	"strings"

	"gitlab.com/cesmanager/base-server/core/base_domain"
)

// SMTPMail ...
type SMTPMail struct {
	from     string
	password string
	host     string
	port     int
	to       []string
	cc       []string
	subject  string
	msg      []byte
}

func NewSMTP(s *base_domain.Settings) SMTPMail {
	return SMTPMail{
		host:     s.SMTPServer,
		port:     s.SMTPPort,
		from:     s.SMTPUser,
		password: s.SMTPPass,
	}
}

// Sender ...
func (e *SMTPMail) Sender(from string, password string) *SMTPMail {
	e.from = from
	e.password = password
	return e
}

// Reciepents ...
func (e *SMTPMail) Reciepents(to []string) *SMTPMail {
	for _, email := range to {
		if len(email) > 0 {
			e.to = append(e.to, email)
		}
	}
	return e
}

// CCAll ...
func (e *SMTPMail) CCAll() *SMTPMail {
	e.cc = e.to
	return e
}

// SetCC ...
func (e *SMTPMail) SetCC(cc []string) *SMTPMail {
	for _, email := range cc {
		if len(email) > 0 {
			e.cc = append(e.cc, email)
		}
	}
	return e
}

// SetServer ...
func (e *SMTPMail) SetServer(host string, port int) *SMTPMail {
	return e.SetHost(host).SetPort(port)
}

// SetPort ...
func (e *SMTPMail) SetPort(port int) *SMTPMail {
	e.port = port
	return e
}

// SetHost ...
func (e *SMTPMail) SetHost(host string) *SMTPMail {
	e.host = host
	return e
}

// SetMsg ...
func (e *SMTPMail) SetMsg(subject string, msg string) *SMTPMail {
	e.msg = []byte(msg)
	e.subject = subject
	return e
}

// Send ...
func (e *SMTPMail) Send() error {
	auth := smtp.PlainAuth("", e.from, e.password, e.host)
	msg := ""
	if len(e.cc) > 0 {
		msg = fmt.Sprint("To: ", strings.Join(e.cc, ","), "\r\n")
	}
	if len(e.subject) > 0 {
		msg = fmt.Sprint("Subject: ", e.subject, "\r\n\r\n")
	}
	e.msg = []byte(msg + string(e.msg))
	if e.password == "" {
		return e.sendMail(fmt.Sprintf("%v:%v", e.host, e.port), e.from, e.to, e.msg)
	}
	return smtp.SendMail(fmt.Sprintf("%v:%v", e.host, e.port), auth, e.from, e.to, e.msg)
}

// QuikSend ...
func (e *SMTPMail) QuikSend(host string, port int, from string, password string, to []string,
	cc []string, subject string, msg string) error {
	e.SetServer(host, port).Sender(from, password).Reciepents(to).SetMsg(subject, msg)
	e.cc = cc
	return e.Send()
}

// send mail without authentication
func (e *SMTPMail) sendMail(addr string, from string, to []string, msg []byte) error {
	//connect to smtp
	c, err := smtp.Dial(addr)
	if err != nil {
		return err
	}
	defer c.Close()

	r := strings.NewReplacer("\r\n", "", "\r", "", "\n", "", "%0a", "", "%0d", "")
	if err = c.Mail(r.Replace(from)); err != nil {
		return err
	}
	for i := range to {
		to[i] = r.Replace(to[i])
		if err = c.Rcpt(to[i]); err != nil {
			return err
		}
	}

	w, err := c.Data()
	if err != nil {
		return err
	}

	_, err = w.Write([]byte(msg))
	if err != nil {
		return err
	}
	err = w.Close()
	if err != nil {
		return err
	}
	return c.Quit()
}
