package invoice

// //Need ...
// var (
// 	Need utils.Need
// )

// func prep() {
// 	Need.Copy(&s.Need)
// 	utils.Custom = Need.Custom
// 	c.DBname = Need.DBname
// }

// //Create a Invoice
// func Create(dsc string, discount string,
// 	tax string, rawTotal string, total string,
// 	accountID string, cashierID string, categoryID string) r.Response {

// 	prep()

// 	var (
// 		Invoice  s.Invoice
// 		response r.Response
// 		msgs     []interface{}
// 	)

// 	if len(rawTotal) < 1 {
// 		msg := "Lump sum is required"
// 		utils.Log(msg, 1)
// 		msgs = append(msgs, map[string]string{"raw_total": msg})
// 	}
// 	if len(total) < 1 {
// 		msg := "Total Amount is required"
// 		utils.Log(msg, 1)
// 		msgs = append(msgs, map[string]string{"total": msg})
// 	}
// 	if len(accountID) < 1 {
// 		msg := "Buyers account is required"
// 		utils.Log(msg, 1)
// 		msgs = append(msgs, map[string]string{"account_id": msg})
// 	}

// 	if len(cashierID) < 1 {
// 		msg := "Cashier ID is required"
// 		utils.Log(msg, 1)
// 		msgs = append(msgs, map[string]string{"cashier_id": msg})
// 	}
// 	if len(categoryID) < 1 {
// 		msg := "Invoice Category is required"
// 		utils.Log(msg, 1)
// 		msgs = append(msgs, map[string]string{"category_id": msg})
// 	}

// 	if len(msgs) < 1 {
// 		db := c.DBConn()
// 		connection, _ := db.DB()
// 		defer connection.Close()

// 		Dsc, _ := strconv.ParseFloat(dsc, 32)
// 		Invoice.Dsc = float32(Dsc)

// 		Discount, _ := strconv.ParseFloat(discount, 32)
// 		Invoice.Discount = float32(Discount)

// 		Tax, _ := strconv.ParseFloat(tax, 32)
// 		Invoice.Tax = float32(Tax)

// 		RawTotal, _ := strconv.ParseFloat(rawTotal, 32)
// 		Invoice.RawTotal = float32(RawTotal)

// 		Total, _ := strconv.ParseFloat(total, 32)
// 		Invoice.Discount = float32(Total)

// 		u64, _ := strconv.Atoi(accountID)
// 		Invoice.AccountID = uint(u64)

// 		// ExFrom, _ := strconv.ParseFloat(exFrom, 32)
// 		// Invoice.ExFrom = float32(ExFrom)

// 		// ExTo, _ := strconv.ParseFloat(exTo, 32)
// 		// Invoice.ExTo = float32(ExTo)

// 		u64, _ = strconv.Atoi(cashierID)
// 		Invoice.CashierID = uint(u64)

// 		u64, _ = strconv.Atoi(categoryID)
// 		Invoice.CategoryID = uint(u64)

// 		_, err := InAppCreate(db, Invoice.Dsc, Invoice.Discount,
// 			Invoice.Tax, Invoice.RawTotal, Invoice.Total,
// 			Invoice.AccountID, Invoice.CashierID, Invoice.CategoryID)
// 		if err != nil {
// 			msg := "Unable to create Invoice"
// 			utils.Log(msg, 1)
// 			msgs = append(msgs, msg)
// 		} else {
// 			msg := "Invoice Created"
// 			log.Printf(msg)
// 			response.Status = true
// 			msgs = append(msgs, msg)
// 		}
// 	}
// 	response.Body = msgs
// 	return response
// }

// //InAppCreate ...
// func InAppCreate(db *gorm.DB, dsc float32, discount float32,
// 	tax float32, rawTotal float32, total float32,
// 	accountID uint, cashierID uint, categoryID uint) (s.Invoice, error) {

// 	prep()

// 	var (
// 		Invoice s.Invoice
// 	)

// 	Invoice.Dsc = dsc

// 	Invoice.Discount = discount

// 	Invoice.Tax = tax

// 	Invoice.RawTotal = rawTotal

// 	Invoice.Discount = total

// 	Invoice.AccountID = accountID

// 	Invoice.CashierID = cashierID

// 	Invoice.CategoryID = categoryID

// 	_, err := Invoice.Add(db)
// 	if err != nil {
// 		return Invoice, err
// 	}
// 	return Invoice, nil
// }

// //Post ...
// func Post(id string, PostByID string) r.Response {

// 	prep()

// 	var (
// 		response r.Response
// 		msgs     []interface{}
// 	)

// 	if len(id) < 1 {
// 		msg := "Invoice ID is required"
// 		utils.Log(msg, 1)
// 		msgs = append(msgs, map[string]string{"ex_to": msg})
// 	}
// 	if len(PostByID) < 1 {
// 		msg := "current user id is required"
// 		utils.Log(msg, 1)
// 		msgs = append(msgs, map[string]string{"post_by": msg})
// 	}

// 	db := c.DBConn()
// 	connection, _ := db.DB()
// 	defer connection.Close()
// 	//begin transaction
// 	tx := db.Begin()

// 	if tx.Error != nil {
// 		msg := "Unable to begin transaction"
// 		utils.Log(msg+" Reason:"+tx.Error.Error(), 1)
// 		msgs = append(msgs, msg)
// 	} else {
// 		_, err := InAppPost(tx, id, PostByID)
// 		if err != nil {
// 			msg := "Unable to post invoice"
// 			utils.Log(msg+" Reason:"+err.Error(), 1)
// 			msgs = append(msgs, msg)
// 			tx.Rollback()
// 		} else {
// 			msg := "Invoice Posted"
// 			response.Status = true
// 			msgs = append(msgs, msg)
// 			tx.Commit()
// 		}
// 	}

// 	response.Body = msgs
// 	return response
// }

// //InAppPost ...
// func InAppPost(tx *gorm.DB, id string, PostByID string) (s.Invoice, error) {

// 	prep()

// 	var (
// 		Account s.Account
// 		Invoice s.Invoice
// 	)
// 	var where []utils.SQLWhere
// 	where = append(where, utils.SQLWhere{Query: "id = ?", Value: id})
// 	Invoices, _ := Invoice.Fetch(tx, where)
// 	if len(Invoices) < 1 {
// 		msg := "Invalid Invoice number"
// 		return Invoice, errors.New(msg)
// 	}
// 	invoice := Invoices[0]

// 	var param1 []utils.SQLWhere
// 	param1 = append(param1, utils.SQLWhere{Query: "id = ?", Value: invoice.AccountID})
// 	param1 = append(param1, utils.SQLWhere{Query: "balance > ?", Value: invoice.Total})
// 	accounts, err := Account.Fetch(tx, param1)
// 	if err != nil {
// 		return Invoice, err
// 	}
// 	if len(accounts) == 0 {
// 		return Invoice, errors.New("insufficient balance")
// 	}

// 	//debit the users Account
// 	amount, _ := strconv.ParseFloat(fmt.Sprintf("%f", invoice.Total), 64)
// 	var were []utils.SQLWhere
// 	were = append(were, utils.SQLWhere{Query: "id = ?", Value: invoice.AccountID})

// 	err = Account.Debit(tx, amount, were)
// 	if err != nil {
// 		tx.Rollback()
// 		msg := "Unable to debit User"
// 		utils.Log(msg+" Reason:"+err.Error(), 1)
// 		return Invoice, errors.New(msg)
// 	}
// 	return Invoice, nil
// }

// //Fetch Invoice
// func Fetch(param []map[string]interface{}) r.Response {
// 	//set the database name for connection
// 	prep()

// 	var Invoice s.Invoice
// 	var response r.Response
// 	var msgs []interface{}

// 	var where []utils.SQLWhere
// 	for _, v := range param {
// 		where = append(where, utils.SQLWhere{Query: fmt.Sprintf("%v", v["q"]), Value: v["v"]})
// 	}
// 	db := c.DBConn()
// 	connection, _ := db.DB()
// 	defer connection.Close()
// 	Invoices, _ := Invoice.Fetch(db, where)
// 	if len(Invoices) < 1 {
// 		msg := "No Invoices found"
// 		msgs = append(msgs, msg)
// 	} else {
// 		response.Status = true
// 		msgs = append(msgs, Invoices)
// 	}
// 	response.Body = msgs
// 	return response
// }
