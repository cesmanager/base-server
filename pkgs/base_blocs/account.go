package base_blocs

import (
	"errors"
	"fmt"
	"strconv"

	"gitlab.com/cesmanager/base-server/core/base_domain"
	"gitlab.com/cesmanager/base-server/core/base_port"
)

type account struct {
	base_domain.Feature
	port base_port.Account
}

func NewAccount(port base_port.Account) *account {
	mod := "account"
	return &account{
		port: port,
		Feature: base_domain.Feature{
			Name:    "Account",
			Mod:     &mod,
			Menu:    true,
			Actions: "list,create,update,delete,credit,debit",
		},
	}
}

func (srv *account) Get(id string) (base_domain.Account, error) {
	account, err := srv.port.Get(id)
	if err != nil {
		return base_domain.Account{}, errors.New("unable to get account")
	}
	if account.ID == 0 {
		return base_domain.Account{}, errors.New("no account found")
	}
	return account, nil
}

func (srv *account) List(param map[string]interface{}, order map[string]interface{}, page string, pageSize string) (base_domain.List, error) {
	pg, _ := strconv.Atoi(page)
	pgs, _ := strconv.Atoi(pageSize)
	list, err := srv.port.List(param, order, pg, pgs)
	if err != nil {
		return list, errors.New("unable to get list")
	}
	return list, nil
}

func (srv *account) Find(param map[string]interface{}) ([]base_domain.Account, error) {
	account, err := srv.port.Find(param)
	if err != nil {
		return account, errors.New("unable to get account")
	}
	return account, nil
}

func (srv *account) Create(params map[string]interface{}) (base_domain.Account, error) {
	var account = base_domain.Account{}
	srv.updateFields(&account, params)
	if err := srv.port.Store(&account); err != nil {
		return base_domain.Account{}, errors.New("unable to create account")
	}
	return account, nil
}

func (srv *account) Update(id string, params map[string]interface{}) (base_domain.Account, error) {
	account, err := srv.Get(id)
	if err != nil {
		return account, err
	}
	delete(params, "currency_id") //currency of an account can't be changed
	delete(params, "profile_id")  //profile_id of an account can't be changed
	srv.updateFields(&account, params)
	if err := srv.port.Update(&account); err != nil {
		return base_domain.Account{}, errors.New("unable to create account")
	}
	return account, nil
}

//PostAmount post amount to an account. Remember to always call this method in a transaction
func (srv *account) PostAmount(id string, amount string) (base_domain.Account, error) {
	srv.port.Lock(id)           //lock the record
	account, err := srv.Get(id) //fetch it
	if err != nil {
		return account, err
	}
	amt, err := strconv.ParseFloat(amount, 64)
	if err != nil {
		return account, err
	}
	if amt >= 0 {
		account.Balance += amt
	} else {
		if (account.Balance + account.Overdraft) < amt {
			return account, errors.New("insufficient balance")
		}
		account.Balance += amt
	}

	if err := srv.port.Update(&account); err != nil {
		return account, errors.New("unable to create account")
	}
	return account, nil
}

func (srv *account) updateFields(account *base_domain.Account, params map[string]interface{}) {
	if title, ok := params["title"]; ok {
		account.Title = fmt.Sprint(title)
	}
	if _category, ok := params["category_id"]; ok {
		u64, _ := strconv.ParseUint(fmt.Sprint(_category), 10, 32)
		_u64 := uint(u64)
		account.CategoryID = &_u64
	}
	if currency, ok := params["currency_id"]; ok {
		u64, _ := strconv.ParseUint(fmt.Sprint(currency), 10, 32)
		account.CurrencyID = uint(u64)
	}
	if overdraft, ok := params["overdraft"]; ok {
		account.Overdraft, _ = strconv.ParseFloat(fmt.Sprint(overdraft), 64)
	}
	if profile, ok := params["profile_id"]; ok {
		u64, _ := strconv.ParseUint(fmt.Sprint(profile), 10, 32)
		account.ProfileID = uint(u64)
	}
	if _active, ok := params["active"]; ok {
		active, _ := strconv.Atoi(fmt.Sprint(_active))
		account.Active = int8(active)
	}
}
