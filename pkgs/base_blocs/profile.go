package base_blocs

import (
	"errors"
	"fmt"
	"strconv"
	"time"

	"gitlab.com/cesmanager/base-server/core/base_domain"
	"gitlab.com/cesmanager/base-server/core/base_port"
)

type profile struct {
	base_domain.Feature
	port base_port.Profile
}

func NewProfile(port base_port.Profile) *profile {
	mod := "profile"
	return &profile{
		port: port,
		Feature: base_domain.Feature{
			Name:    "Profile",
			Mod:     &mod,
			Menu:    true,
			Actions: "list,create,update,delete",
		},
	}
}

func (srv *profile) Get(id string) (base_domain.Profile, error) {
	profile, err := srv.port.Get(id)
	if err != nil {
		return base_domain.Profile{}, errors.New("unable to get profile")
	}
	return profile, nil
}

func (srv *profile) List(params map[string]interface{}, order map[string]interface{}, page string, pageSize string) (base_domain.List, error) {
	pg, _ := strconv.Atoi(page)
	pgs, _ := strconv.Atoi(pageSize)
	list, err := srv.port.List(params, order, pg, pgs)
	if err != nil {
		return list, errors.New("unable to get list")
	}
	return list, nil
}

func (srv *profile) Find(param map[string]interface{}) ([]base_domain.Profile, error) {
	profile, err := srv.port.Find(param)
	if err != nil {
		return profile, errors.New("unable to get profile")
	}
	return profile, nil
}

func (srv *profile) Create(params map[string]interface{}) (base_domain.Profile, error) {
	var profile = base_domain.Profile{}
	srv.updateFields(&profile, params)
	return srv.CreateByInstance(profile)
}

func (srv *profile) CreateByInstance(profile base_domain.Profile) (base_domain.Profile, error) {
	if err := srv.port.Store(&profile); err != nil {
		return base_domain.Profile{}, errors.New("unable to create profile")
	}
	return profile, nil
}
func (srv *profile) Update(id string, params map[string]interface{}) (base_domain.Profile, error) {
	profile, err := srv.Get(id)
	if err != nil {
		return profile, err
	}
	if profile.ID == 0 {
		return profile, fmt.Errorf("no profile found")
	}
	srv.updateFields(&profile, params)
	return srv.UpdateByInstance(profile)
}

func (srv *profile) UpdateByInstance(profile base_domain.Profile) (base_domain.Profile, error) {
	if err := srv.port.Update(&profile); err != nil {
		return base_domain.Profile{}, errors.New("unable to update profile")
	}
	return profile, nil
}

func (srv *profile) Delete(id string) error {
	err := srv.port.Delete(id)
	if err != nil {
		return errors.New("unable to delete")
	}
	return nil
}

func (srv *profile) updateFields(model *base_domain.Profile, params map[string]interface{}) {
	if email, ok := params["email"]; ok && email != "null" {
		f := fmt.Sprint(email)
		model.Email = &f
	}
	if alt_email, ok := params["alt_email"]; ok && alt_email != "null" {
		f := fmt.Sprint(alt_email)
		model.AltEmail = &f
	}
	if mobile, ok := params["mobile"]; ok {
		model.Mobile = fmt.Sprint(mobile)
	}
	if phone, ok := params["phone"]; ok {
		model.Phone = fmt.Sprint(phone)
	}
	if fname, ok := params["fname"]; ok {
		model.Fname = fmt.Sprint(fname)
	}
	if lname, ok := params["lname"]; ok {
		model.Lname = fmt.Sprint(lname)
	}
	if mname, ok := params["mname"]; ok {
		model.Mname = fmt.Sprint(mname)
	}
	if bname, ok := params["bname"]; ok {
		model.Bname = fmt.Sprint(bname)
	}
	if birthday, ok := params["birthday"]; ok && len(fmt.Sprint(birthday)) == len("2006-01-02") {
		dt, _ := time.Parse("2006-01-02", fmt.Sprint(birthday))
		model.Birthday = &dt
	}
	if address1, ok := params["address1"]; ok {
		model.Address1 = fmt.Sprint(address1)
	}
	if address2, ok := params["address2"]; ok {
		model.Address2 = fmt.Sprint(address2)
	}
	if city, ok := params["city"]; ok {
		model.City = fmt.Sprint(city)
	}
	if zipcode, ok := params["zipcode"]; ok {
		model.ZipCode = fmt.Sprint(zipcode)
	}
	if state, ok := params["state"]; ok {
		model.State = fmt.Sprint(state)
	}
	if gender_id, ok := params["gender_id"]; ok && gender_id != "null" && fmt.Sprint(gender_id) != "" {
		u64, _ := strconv.ParseUint(fmt.Sprint(gender_id), 10, 32)
		_u64 := uint(u64)
		model.GenderID = &_u64
	}
	if country, ok := params["country_id"]; ok && country != "null" && fmt.Sprint(country) != "" {
		u64, _ := strconv.ParseUint(fmt.Sprint(country), 10, 32)
		_u64 := uint(u64)
		model.CountryID = &_u64
	}
	if _status, ok := params["status"]; ok {
		status, _ := strconv.Atoi(fmt.Sprint(_status))
		model.Status = uint(status)
	}
	if _type, ok := params["type"]; ok {
		type_, _ := strconv.Atoi(fmt.Sprint(_type))
		model.Type = int8(type_)
	}
	if _category, ok := params["category_id"]; ok && _category != "null" && fmt.Sprint(_category) != "" {
		u64, _ := strconv.ParseUint(fmt.Sprint(_category), 10, 32)
		_u64 := uint(u64)
		model.CategoryID = &_u64
	}
	if _outlet, ok := params["outlet_id"]; ok && _outlet != "null" && fmt.Sprint(_outlet) != "" {
		u64, _ := strconv.ParseUint(fmt.Sprint(_outlet), 10, 32)
		_u64 := uint(u64)
		model.OutletID = &_u64
	}
}
