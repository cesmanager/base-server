package base_blocs

import (
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"

	"gitlab.com/cesmanager/base-server/core/base_domain"
	"gitlab.com/cesmanager/base-server/core/base_port"
)

var FilesMod = "files"

type fileManager struct {
	base_domain.Feature
	utils *Utilities
	port  base_port.FileInfoPort
}

func NewFileManager(utils *Utilities, port base_port.FileInfoPort) *fileManager {
	return &fileManager{
		utils: utils,
		port:  port,
		Feature: base_domain.Feature{
			Name:    "File Manager",
			Mod:     &FilesMod,
			Actions: "list,create,update,delete,root,log,exec,database",
			Menu:    true,
		},
	}
}

// Read a directory
func (srv *fileManager) Dir(dir string) (files []base_domain.FileInfo, err error) {
	err = srv.checkPath(dir)
	if err != nil {
		return
	}
	_fs, err := srv.utils.ReadDir(fmt.Sprint(srv.utils.CoyDir(), dir))
	if err != nil {
		return
	}
	for _, f := range _fs {
		files = append(files, base_domain.FileInfo{
			Name:      f.Name(),
			Path:      fmt.Sprint(dir, "/", f.Name()),
			IsDir:     f.IsDir(),
			Mode:      f.Mode().String(),
			UpdatedAt: f.ModTime(),
		})
	}
	return
}

// Read a directory
func (srv *fileManager) Read(path string) (file io.Reader, err error) {
	err = srv.checkPath(path)
	if err != nil {
		return
	}
	file, err = srv.utils.OpenFile(fmt.Sprint(srv.utils.CoyDir(), path), (os.O_RDONLY), os.FileMode(int(0666)))
	return
}

// New file or directory
func (srv *fileManager) New(owner string, params map[string]interface{}) (err error) {
	isDir, err := strconv.ParseBool(fmt.Sprint(params["is_dir"]))
	if err != nil {
		return
	}
	name := fmt.Sprint(params["name"])
	err = srv.checkPath(name)
	if err != nil {
		return
	}
	u64, _ := strconv.ParseUint(owner, 10, 32)
	_owner := uint(u64)
	file := base_domain.FileInfo{
		Name:    name,
		Path:    fmt.Sprint(params["path"]),
		IsDir:   isDir,
		OwnerID: &_owner,
	}
	path := fmt.Sprint(srv.utils.CoyDir(), fmt.Sprint(params["path"]))
	if isDir { //make directory
		err = srv.port.Store(&file)
		if err != nil {
			return
		}
		err = srv.utils.MkdirAll(path, 0776)
		return
	}
	//make file
	err = srv.port.Store(&file)
	if err != nil {
		return
	}
	f, err := srv.utils.Create(path)
	if err != nil {
		return
	}
	f.Close()
	return
}

// New file or directory
func (srv *fileManager) Upload(file io.Reader, fileName string, folder string, owner string) (err error) {
	path := fmt.Sprint(folder, "/", fileName)
	u64, _ := strconv.ParseUint(owner, 10, 32)
	_owner := uint(u64)
	fileInfo := base_domain.FileInfo{
		Name:    fileName,
		Path:    path,
		OwnerID: &_owner,
	}
	err = srv.port.Store(&fileInfo)
	if err != nil {
		return
	}
	//make file
	_, err = srv.utils.UploadFile(file, fmt.Sprint(srv.utils.CoyDir(), folder), fileName)
	return
}

// Rename file or directory
func (srv *fileManager) Rename(oldPath string, newPath string) (err error) {
	oldPath = srv.utils.CorrectPath(oldPath)
	newPath = srv.utils.CorrectPath(newPath)
	if oldPath == newPath {
		return fmt.Errorf("same new and old name")
	}
	err = srv.checkPath(oldPath)
	if err != nil {
		return
	}
	err = srv.checkPath(newPath)
	if err != nil {
		return
	}
	fs, err := srv.port.Find(map[string]interface{}{"path": map[string]interface{}{
		"val": fmt.Sprint(oldPath, "%"), "con": "like",
	}})
	if err != nil {
		return
	}
	if (len(fs) > 0 && fs[0].System) || len(fs) == 0 { //ensure its not a system record
		return fmt.Errorf("you can't rename system record(s)")
	}
	for _, f := range fs {
		fPath := f.Path
		f.Path = strings.Replace(fPath, oldPath, newPath, 1)
		err = srv.port.Update(fPath, &f)
		if err != nil {
			return
		}
	}
	oldPath = fmt.Sprint(srv.utils.CoyDir(), oldPath)
	newPath = fmt.Sprint(srv.utils.CoyDir(), newPath)
	err = srv.utils.Rename(oldPath, newPath)
	return
}

// Delete file or directory
func (srv *fileManager) Delete(path string) (err error) {
	path = srv.utils.CorrectPath(path)
	err = srv.checkPath(path)
	if err != nil {
		return
	}
	fs, err := srv.port.Find(map[string]interface{}{"path": path})
	if err != nil {
		return
	}
	if (len(fs) > 0 && fs[0].System) || len(fs) == 0 { //ensure its not a system record
		return fmt.Errorf("you can't delete system record(s)")
	}
	err = srv.port.Delete(path)
	if err != nil {
		return
	}
	err = srv.utils.Remove(fmt.Sprint(srv.utils.CoyDir(), path))
	return
}

// Copy file or directory
func (srv *fileManager) Copy(path string) (err error) {
	path = srv.utils.CorrectPath(path)
	err = srv.checkPath(path)
	if err != nil {
		return
	}
	fs, err := srv.port.Find(map[string]interface{}{"path": path})
	if err != nil {
		return
	}
	if (len(fs) > 0 && fs[0].System) || len(fs) == 0 { //ensure its not a system record
		return fmt.Errorf("you can't delete system record(s)")
	}
	err = srv.port.Delete(path)
	if err != nil {
		return
	}
	err = srv.utils.Remove(fmt.Sprint(srv.utils.CoyDir(), path))
	return
}
func (srv *fileManager) checkPath(name string) error {
	for _, part := range strings.Split(name, "/") {
		if strings.HasPrefix(part, ".") {
			return fmt.Errorf("invalid file name")
		}
	}
	return nil
}
