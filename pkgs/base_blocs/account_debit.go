package base_blocs

import (
	"errors"
	"fmt"
	"strconv"

	"gitlab.com/cesmanager/base-server/core/base_domain"
	"gitlab.com/cesmanager/base-server/core/base_port"
)

type debit struct {
	base_domain.Feature
	port base_port.Debit
}

func NewDebit(port base_port.Debit) *debit {
	return &debit{
		port: port,
		Feature: base_domain.Feature{
			Name:    NewAccount(nil).Name,
			Mod:     NewAccount(nil).Mod,
			Actions: NewAccount(nil).Actions,
		},
	}
}

func (srv *debit) Create(params map[string]interface{}, accountSrv *account) (base_domain.Debit, error) {
	var debit = base_domain.Debit{}
	err := srv.requiredParams(params, []string{"title", "account_id", "amount", "creator_id", "ip", "creditor_id"})
	if err != nil {
		return debit, err
	}
	srv.updateFields(&debit, params)
	account, err := accountSrv.Get(fmt.Sprint(debit.Transaction.AccountID))
	if err != nil {
		return debit, err
	}
	debtor, err := accountSrv.Get(fmt.Sprint(debit.Transaction.CreatorID))
	if err != nil {
		return debit, err
	}
	if account.CurrencyID != debtor.CurrencyID {
		return debit, errors.New("incompatible currency match")
	}
	if err := srv.port.Store(&debit); err != nil {
		return debit, errors.New("unable to create debit")
	}
	return debit, nil
}

func (srv *debit) Get(id string) (base_domain.Debit, error) {
	debit, err := srv.port.Get(id)
	if err != nil {
		return debit, errors.New("unable to get debit")
	}
	if debit.ID == 0 {
		return debit, errors.New("no debit with requested id found")
	}
	return debit, nil
}

func (srv *debit) Find(param map[string]interface{}) ([]base_domain.Debit, error) {
	debit, err := srv.port.Find(param)
	if err != nil {
		return debit, errors.New("unable to get debit(s)")
	}
	return debit, nil
}

// Post a debit. Always call this function with a transaction db connection
func (srv *debit) Post(id string, poster_id string, accountSrv *account) (base_domain.Debit, error) {
	//find the debit
	debit, err := srv.Get(id)
	if err != nil {
		return debit, err
	}
	//post the amount to the company account
	_, err = accountSrv.PostAmount(fmt.Sprint(debit.Transaction.AccountID), fmt.Sprint((debit.Transaction.Amount * -1)))
	if err != nil {
		return debit, err
	}
	//post the amount to the receivers account
	_, err = accountSrv.PostAmount(fmt.Sprint(debit.DebtorID), fmt.Sprint((debit.Transaction.Amount * -1)))
	if err != nil {
		return debit, err
	}
	//mark this transaction as posted
	u64, _ := strconv.ParseUint(fmt.Sprint(poster_id), 10, 32)
	_u64 := uint(u64)
	debit.Transaction.PosterID = &_u64
	debit.Transaction.Posted = true

	if err := srv.port.Update(&debit); err != nil {
		return debit, errors.New("unable to post debit")
	}
	return debit, nil
}

func (srv *debit) requiredParams(params map[string]interface{}, required []string) error {
	for _, r := range required {
		switch r {
		case "title":
			return errors.New("title is required")
		case "account_id":
			return errors.New("account_id is required")
		case "amount":
			return errors.New("amount is required")
		case "creator_id":
			return errors.New("creator_id is required")
		case "ip":
			return errors.New("ip is required")
		case "debtor_id":
			return errors.New("debtor_id is required")
		}
	}
	return nil
}
func (srv *debit) updateFields(debit *base_domain.Debit, params map[string]interface{}) {
	if title, ok := params["title"]; ok {
		debit.Transaction.Title = fmt.Sprint(title)
	}
	if account_id, ok := params["account_id"]; ok {
		u64, _ := strconv.ParseUint(fmt.Sprint(account_id), 10, 32)
		debit.Transaction.AccountID = uint(u64)
	}
	if amount, ok := params["amount"]; ok {
		debit.Transaction.Amount, _ = strconv.ParseFloat(fmt.Sprint(amount), 64)
	}
	if comment, ok := params["comment"]; ok {
		debit.Transaction.Comment = fmt.Sprint(comment)
	}
	if _category, ok := params["category_id"]; ok {
		u64, _ := strconv.ParseUint(fmt.Sprintf("%v", _category), 10, 32)
		_u64 := uint(u64)
		debit.Transaction.CategoryID = &_u64
	}
	if creator_id, ok := params["creator_id"]; ok {
		u64, _ := strconv.ParseUint(fmt.Sprint(creator_id), 10, 32)
		debit.Transaction.CreatorID = uint(u64)
	}
	if poster_id, ok := params["poster_id"]; ok {
		u64, _ := strconv.ParseUint(fmt.Sprint(poster_id), 10, 32)
		_u64 := uint(u64)
		debit.Transaction.PosterID = &_u64
	}
	if ip, ok := params["ip"]; ok {
		debit.Transaction.IP = fmt.Sprint(ip)
	}
	if transactor_name, ok := params["transactor_name"]; ok {
		debit.Transaction.TransactorName = fmt.Sprint(transactor_name)
	}
	if transactor_address1, ok := params["transactor_address1"]; ok {
		debit.Transaction.TransactorAddress1 = fmt.Sprint(transactor_address1)
	}
	if transactor_address2, ok := params["transactor_address2"]; ok {
		debit.Transaction.TransactorAddress2 = fmt.Sprint(transactor_address2)
	}
	if transactor_city, ok := params["transactor_city"]; ok {
		debit.Transaction.TransactorCity = fmt.Sprint(transactor_city)
	}
	if transactor_state, ok := params["transactor_state"]; ok {
		debit.Transaction.TransactorState = fmt.Sprint(transactor_state)
	}
	if transactor_country, ok := params["transactor_country"]; ok {
		debit.Transaction.TransactorCountry = fmt.Sprint(transactor_country)
	}
	if debtor_id, ok := params["debtor_id"]; ok {
		u64, _ := strconv.ParseUint(fmt.Sprint(debtor_id), 10, 32)
		debit.DebtorID = uint(u64)
	}
}
