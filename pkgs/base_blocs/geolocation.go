package base_blocs

import (
	"errors"
	"fmt"
	"strconv"
	"strings"

	"gitlab.com/cesmanager/base-server/core/base_domain"
	"gitlab.com/cesmanager/base-server/core/base_port"
)

type geoLocation struct {
	base_domain.Feature
	port base_port.GeoLocation
}

func NewGeoLocation(port base_port.GeoLocation) *geoLocation {
	mod := "map_location"
	return &geoLocation{
		port: port,
		Feature: base_domain.Feature{
			Name:    "Map Location",
			Mod:     &mod,
			Menu:    true,
			Actions: "list,create,update,delete",
		},
	}
}

func (srv *geoLocation) Get(id string) (base_domain.GeoLocation, error) {
	geoLocation, err := srv.port.Get(id)
	if err != nil {
		return geoLocation, errors.New("unable to get geoLocation")
	}
	if geoLocation.ID == 0 {
		return geoLocation, errors.New("no geoLocation found")
	}
	return geoLocation, nil
}

func (srv *geoLocation) List(param map[string]interface{}, order map[string]interface{}, page string, pageSize string) (base_domain.List, error) {
	pg, _ := strconv.Atoi(page)
	pgs, _ := strconv.Atoi(pageSize)
	list, err := srv.port.List(param, order, pg, pgs)
	if err != nil {
		return list, errors.New("unable to get list")
	}
	return list, nil
}

func (srv *geoLocation) Find(param map[string]interface{}, operator *map[string]interface{}, order *map[string]interface{}) ([]base_domain.GeoLocation, error) {
	ParamOperator(&param, operator)
	geoLocation, err := srv.port.Find(param, order)
	if err != nil {
		return geoLocation, errors.New("unable to get geoLocation(s)")
	}
	return geoLocation, nil
}

//create the geolocation. Always call this in a transaction
func (srv *geoLocation) Create(params map[string]interface{}) (base_domain.GeoLocation, error) {
	var geoLocation = base_domain.GeoLocation{}
	srv.updateFields(&geoLocation, params)
	if geoLocation.ParentID != nil {
		//find the parents
		parent, err := srv.Get(fmt.Sprint((*geoLocation.ParentID)))
		if err != nil {
			return geoLocation, err
		}
		//get the parents children
		gs, err := srv.Find(map[string]interface{}{"parent_id": (*geoLocation.ParentID)}, nil, nil)
		if err != nil {
			return geoLocation, err
		}
		geoLocation.GroupTag = fmt.Sprintf("%v-%v", parent.GroupTag, len(gs)+1)
		geoLocation.Type = int8(strings.Count(geoLocation.GroupTag, "-") + 1)
	}
	if err := srv.port.Store(&geoLocation); err != nil {
		return base_domain.GeoLocation{}, errors.New("unable to create geoLocation")
	}
	if geoLocation.ParentID == nil {
		geoLocation.GroupTag = fmt.Sprint(geoLocation.ID)
		geoLocation.Type = 1
		//update it
		if err := srv.port.Update(&geoLocation); err != nil {
			return base_domain.GeoLocation{}, errors.New("unable to update group tag")
		}
	}
	return geoLocation, nil
}

//update the geolocation. Recommend calling this in a transaction
func (srv *geoLocation) Update(id string, params map[string]interface{}) (base_domain.GeoLocation, error) {
	geoLocation, err := srv.Get(id)
	if err != nil {
		return geoLocation, err
	}
	//get the old group tag
	oldGroupTag := geoLocation.GroupTag
	//get the old group tag
	oldParentID := (*geoLocation.ParentID)
	srv.updateFields(&geoLocation, params)
	if &oldParentID != geoLocation.ParentID {
		//find the new parent
		parent, err := srv.Get(fmt.Sprint((*geoLocation.ParentID)))
		if err != nil {
			return geoLocation, err
		}
		//count new parents children
		count, err := srv.port.Count(map[string]interface{}{"parent_id": (*geoLocation.ParentID)})
		if err != nil {
			return geoLocation, err
		}
		newGroupTag := fmt.Sprintf("%v-%v", parent.GroupTag, count+1)
		geoLocation.GroupTag = newGroupTag
		geoLocation.Type = int8(strings.Count(newGroupTag, "-") + 1)
		//update it
		if err := srv.port.Update(&geoLocation); err != nil {
			return geoLocation, errors.New("unable to update old geoLocation group")
		}
		//find all of the geolocations subs and update their group tag
		subs, err := srv.port.Find(map[string]interface{}{
			"group_tag": map[string]interface{}{"con": "LIKE", "val": oldGroupTag + "-%"},
		}, nil)
		if err != nil {
			return geoLocation, err
		}
		//update all of its subs
		for _, sub := range subs {
			sub.GroupTag = strings.Replace(sub.GroupTag, oldGroupTag, newGroupTag, 1)
			sub.Type = int8(strings.Count(sub.GroupTag, "-") + 1)
			if err := srv.port.Update(&sub); err != nil {
				return base_domain.GeoLocation{}, errors.New("unable to update all sub geoLocation")
			}
		}

		//arrange old group
		if oldParentID != 0 {
			//find its old parent
			oldParent, err := srv.Get(fmt.Sprint((*geoLocation.ParentID)))
			if err != nil {
				return geoLocation, err
			}
			//get old group mates
			old_groupmates, err := srv.Find(map[string]interface{}{"parent_id": oldParentID}, nil, nil)
			if err != nil {
				return geoLocation, err
			}
			//for each of them
			for i := 0; i < len(old_groupmates); i++ {
				gl := old_groupmates[i]
				oldmGroupTag := gl.GroupTag //get the old group tag
				newmGroupTag := fmt.Sprintf("%v-%v", oldParent.GroupTag, i+1)
				//update with new value
				gl.GroupTag = newmGroupTag
				if err := srv.port.Update(&geoLocation); err != nil {
					return geoLocation, errors.New("unable to update old geoLocation group")
				}
				//find all of its subs and update their group tag
				subs, err := srv.port.Find(map[string]interface{}{
					"group_tag": map[string]interface{}{"con": "LIKE", "val": oldmGroupTag + "-%"},
				}, nil)
				if err != nil {
					return geoLocation, err
				}
				//update all of its subs
				for _, sub := range subs {
					sub.GroupTag = strings.Replace(sub.GroupTag, oldmGroupTag, newmGroupTag, 1)
					if err := srv.port.Update(&sub); err != nil {
						return base_domain.GeoLocation{}, errors.New("unable to update all sub geoLocation")
					}
				}
			}
		}
	} else {
		if err := srv.port.Update(&geoLocation); err != nil {
			return geoLocation, errors.New("unable to update old geoLocation group")
		}
	}
	return geoLocation, nil
}

//update the geolocation. Recommend calling this in a transaction
func (srv *geoLocation) UpdateInstance(geoLocation base_domain.GeoLocation) (base_domain.GeoLocation, error) {
	if err := srv.port.Update(&geoLocation); err != nil {
		return geoLocation, errors.New("unable to update old geoLocation group")
	}
	return geoLocation, nil
}

func (srv *geoLocation) updateFields(geoLocation *base_domain.GeoLocation, params map[string]interface{}) {
	if name, ok := params["name"]; ok {
		geoLocation.Name = fmt.Sprint(name)
	}
	if title, ok := params["title"]; ok {
		geoLocation.Title = fmt.Sprint(title)
	}
	if photo, ok := params["photo"]; ok {
		geoLocation.Photo = fmt.Sprint(photo)
	}
	if alpha_code_2, ok := params["alpha_code_2"]; ok {
		geoLocation.AlphaCode2 = fmt.Sprint(alpha_code_2)
	}
	if alpha_code_3, ok := params["alpha_code_3"]; ok {
		geoLocation.AlphaCode3 = fmt.Sprint(alpha_code_3)
	}
	if call_code, ok := params["call_code"]; ok {
		geoLocation.CallCode = fmt.Sprint(call_code)
	}
	if capital, ok := params["capital"]; ok {
		geoLocation.Capital = fmt.Sprint(capital)
	}
	if parent_id, ok := params["parent_id"]; ok {
		u64, _ := strconv.ParseUint(fmt.Sprint(parent_id), 10, 32)
		_u64 := uint(u64)
		geoLocation.ParentID = &_u64
	}
}
