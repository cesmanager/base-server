package base_blocs

import (
	"encoding/json"
	"errors"
	"fmt"
	"strconv"

	"gitlab.com/cesmanager/base-server/core/base_domain"
	"gitlab.com/cesmanager/base-server/core/base_port"
)

type formRecord struct {
	port base_port.FormRecordPort
	base_domain.Feature
}

func NewFormRecord(port base_port.FormRecordPort) *formRecord {
	mod := "eform-rec"
	return &formRecord{
		port: port,
		Feature: base_domain.Feature{
			Name:    "e-Form Record",
			Mod:     &mod,
			Actions: "list,create,update,delete",
		},
	}
}

func (b *formRecord) Get(id string) (base_domain.FormRecord, error) {
	rec, err := b.port.Get(id)
	if err != nil {
		return rec, err
	}
	if rec.ID == 0 {
		return rec, errors.New("no formRecord found")
	}
	//get the mapped version of the data
	json.Unmarshal([]byte(rec.Form.Data), &rec.Form.MapedData)
	rec.Form.Data = ""
	return rec, err
}

func (srv *formRecord) List(param map[string]interface{}, order map[string]interface{}, page string, pageSize string) (base_domain.List, error) {
	pg, _ := strconv.Atoi(page)
	pgs, _ := strconv.Atoi(pageSize)
	list, err := srv.port.List(param, order, pg, pgs)
	if err != nil {
		return list, errors.New("unable to get list")
	}
	return list, nil
}

func (b *formRecord) Create(params map[string]interface{}, entrysrv *formRecordEntry) (base_domain.FormRecord, error) {
	params["status"] = "Pending Review" //default status
	rec := base_domain.FormRecord{}
	b.parse(&rec, params)
	err := b.port.Store(&rec)
	if err != nil {
		return rec, errors.New("unable to submit form, try again")
	}
	//delete the field unique for form records
	delete(params, "form_id")
	delete(params, "outlet_id")
	delete(params, "profile_id")
	delete(params, "reviewd_by_id")
	delete(params, "status")
	delete(params, "comment")
	for n, v := range params {
		entry, err := entrysrv.Save(map[string]interface{}{
			"name":           n,
			"value":          v,
			"form_record_id": rec.ID,
		})
		if err != nil {
			return rec, fmt.Errorf("unable to record %v", n)
		}
		rec.Entries = append(rec.Entries, entry)
	}
	return rec, nil
}

func (b *formRecord) Update(id string, params map[string]interface{}, ProfileID uint, entrysrv *formRecordEntry) (base_domain.FormRecord, error) {
	rec, err := b.Get(id)
	if err != nil {
		return rec, err
	}
	if ProfileID == *rec.ProfileID {
		switch rec.Status {
		case "Pending Review", "Incomplete":
		default:
			return rec, errors.New("you don't have the permission to make this changes")
		}
	}
	b.parse(&rec, params)
	err = b.port.Update(&rec)
	if err != nil {
		return rec, err
	}
	delete(params, "id")
	delete(params, "form_id")
	delete(params, "outlet_id")
	delete(params, "profile_id")
	delete(params, "reviewd_by_id")
	delete(params, "status")
	delete(params, "comment")
	for n, v := range params {
		entry, err := entrysrv.Save(map[string]interface{}{
			"name":           n,
			"value":          v,
			"form_record_id": rec.ID,
		})
		if err != nil {
			return rec, err
		}
		rec.Entries = append(rec.Entries, entry)
	}
	return b.Get(id)
}

func (b *formRecord) Find(params map[string]interface{}) ([]base_domain.FormRecord, error) {
	return b.port.Find(params)
}

func (b *formRecord) Delete(params map[string]interface{}) error {
	return b.port.Delete(params)
}

func (b *formRecord) parse(rec *base_domain.FormRecord, params map[string]interface{}) {
	if _, ok := params["form_id"]; ok {
		u64, _ := strconv.ParseUint(fmt.Sprint(params["form_id"]), 10, 64)
		rec.FormID = uint(u64)
	}
	if _, ok := params["outlet_id"]; ok {
		u64, _ := strconv.ParseUint(fmt.Sprint(params["outlet_id"]), 10, 64)
		_u64 := uint(u64)
		rec.OutletID = &_u64
	}
	if _, ok := params["profile_id"]; ok {
		u64, _ := strconv.ParseUint(fmt.Sprint(params["profile_id"]), 10, 64)
		_u64 := uint(u64)
		rec.ProfileID = &_u64
	}
	if _, ok := params["reviewd_by_id"]; ok {
		u64, _ := strconv.ParseUint(fmt.Sprint(params["reviewd_by_id"]), 10, 64)
		_u64 := uint(u64)
		rec.ReviewdByID = &_u64
	}
	if _, ok := params["status"]; ok {
		rec.Status = fmt.Sprint(params["status"])
	}
	if _, ok := params["comment"]; ok {
		rec.Comment = fmt.Sprint(params["comment"])
	}
}
