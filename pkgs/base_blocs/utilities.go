package base_blocs

import (
	"encoding/base64"
	"errors"
	"fmt"
	"io"
	"io/fs"
	"log"
	"math/rand"
	"net"
	"os"
	"path/filepath"
	"regexp"
	"runtime/debug"
	"strings"
	"time"

	"github.com/pkg/sftp"
	"gitlab.com/cesmanager/base-server/core/base_domain"
	"gitlab.com/cesmanager/base-server/pkgs/base_blocs/wrap_sftp"
	"golang.org/x/crypto/ssh"
	"golang.org/x/crypto/ssh/knownhosts"
	"gopkg.in/yaml.v3"
)

const charset = `abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789`

var seededRand *rand.Rand = rand.New(rand.NewSource(time.Now().UnixNano()))

// error log types
type logType string

var (
	StdLog         logType = "StdLog"
	RunTimeError   logType = "RunTimeError"
	SQLError       logType = "SQLError"
	PanicError     logType = "PanicError"
	MigrationError logType = "MigrationError"
	BackUpError    logType = "BackUpError"
	SMTPError      logType = "SMTPError"
)

type Utilities struct {
	config    base_domain.AppConfig
	coy_tag   *string
	pathrex   *regexp.Regexp
	pref_sftp string
	sftp      *sftp.Client
}

func NewUtils(config base_domain.AppConfig, tag *string) *Utilities {
	return &Utilities{
		config:  config,
		coy_tag: tag,
		pathrex: regexp.MustCompile(`[\\\/]+`),
	}
}

// Log - Log error from this user
func (srv *Utilities) Log(message interface{}, ltype logType) {
	//every month will have its log file
	now := time.Now()
	file := fmt.Sprintf("log-%v-%02d.log", now.Year(), int(now.Month()))
	if srv.coy_tag != nil {
		file = fmt.Sprintf("%v/%v", srv.GetCustom()["log"], file)
	}
	f, err := srv.OpenFile(file, os.O_APPEND|os.O_RDWR|os.O_CREATE, 0666)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()
	//log the error normally
	log.Println("["+ltype+"]", ":", message)
	//log to the file
	log.SetOutput(f)
	log.Println("["+ltype+"]", ":", message)
	//revert back to its previous state
	log.SetOutput(os.Stdout)
}

// GetCustom ...
func (srv *Utilities) GetCustom() map[string]string {
	coyPath := srv.CoyDir()
	custom := map[string]string{}
	custom["dir"] = srv.CorrectPath(coyPath)
	custom["log"] = srv.CorrectPath(coyPath + "/" + "log")
	custom["exec"] = srv.CorrectPath(coyPath + "/" + srv.config.Exec)
	custom["temp_path"] = srv.CorrectPath(coyPath + "/" + srv.config.TempPath)
	custom["path"] = srv.CorrectPath(coyPath + "/" + srv.config.Path)
	custom["session"] = srv.CorrectPath(coyPath + "/" + "session")
	custom["config"] = srv.CorrectPath(custom["path"] + "/" + srv.config.Config)
	custom[FilesMod] = srv.CorrectPath(custom["path"] + "/" + FilesMod)
	for _, path := range custom {
		srv.MkdirAll(path, 0776)
	}
	return custom
}

func (srv *Utilities) CorrectPath(path string) string {
	path = srv.pathrex.ReplaceAllString(path, "/")
	return path
}

// Set the company id ...
func (srv *Utilities) SetTag(tag interface{}) *Utilities {
	coy_tag := fmt.Sprint(tag)
	srv.coy_tag = &coy_tag
	return srv
}

// Set the company id ...
func (srv *Utilities) SetSftp(sftp *sftp.Client) *Utilities {
	srv.sftp = sftp
	return srv
}
func (srv *Utilities) GetSftp() *sftp.Client {
	return srv.sftp
}

// Set prefared sftp tag ...
func (srv *Utilities) SetPrefSftp(pref_sftp string) *Utilities {
	srv.pref_sftp = pref_sftp
	return srv
}
func (srv *Utilities) getPrefSftp() string {
	if srv.pref_sftp == "" {
		return srv.config.Sftp
	}
	return srv.pref_sftp
}

// All Load Addons ...
func (srv *Utilities) AllAddons() (addons map[string]base_domain.Addon, err error) {
	if srv.coy_tag == nil {
		return addons, errors.New("no company defined")
	}
	// var mod mod.Module
	// defer the closing of our jsonFile so that we can parse it later  on
	addonPath := srv.CorrectPath(srv.GetCustom()["config"] + "/" + "addons.yml")
	addonByte, err := srv.ReadFile(addonPath)
	if err != nil {
		srv.Log(err.Error(), RunTimeError)
		if os.IsNotExist(err) {
			//create the file
			srv.Create(addonPath)
		}
		srv.Log(fmt.Sprint(err, string(debug.Stack())), RunTimeError)
		return addons, errors.New("unable to read addon file")
	}
	err = yaml.Unmarshal(addonByte, &addons)
	return
}

// Load Addons ...
func (srv *Utilities) LoadAddon(key string) (addon base_domain.Addon, err error) {
	addons, err := srv.AllAddons()
	if err != nil {
		return base_domain.Addon{}, err
	}
	var ok bool
	if addon, ok = addons[key]; !ok {
		return addon, errors.New("addon does not exist")
	}
	return addon, nil
}

// GetPhoto ...
func (srv *Utilities) GetPhoto(mod string, id interface{}) (paths []string) {
	folder := srv.PixPath(mod, id)
	path := strings.Replace(folder, srv.GetPath(), "/api/static", 1)
	if files, err := srv.ReadDir(folder); err == nil && len(files) > 0 {
		for _, file := range files {
			paths = append(paths, fmt.Sprint(path, "/", file.Name()))
		}
	}
	return paths
}

// Get all files associated to a record ...
func (srv *Utilities) RecFiles(mod string, id int) (recFiles map[string][]string) {
	recFiles = map[string][]string{}
	folder := srv.PixPath(mod, id)
	if files, ok := srv.ReadDir(folder); ok == nil && len(files) > 0 {
		path := strings.Replace(strings.Replace(folder, srv.GetPath(), "/api/static", 1), "/", "/", -1)
		recFiles["pix"] = []string{}
		for _, file := range files {
			// recFiles["pix"] = append(recFiles["pix"], subdomain+"."+srv.config.Domain+port+path+"/"+file)
			recFiles["pix"] = append(recFiles["pix"], fmt.Sprint(path, "/", file.Name()))
		}
	}
	folder = srv.DocPath(mod, id)
	if files, ok := srv.ReadDir(folder); ok == nil && len(files) > 0 {
		path := strings.Replace(strings.Replace(folder, srv.GetPath(), "/api/static", 1), "/", "/", -1)
		recFiles["doc"] = []string{}
		for _, file := range files {
			// recFiles["doc"] = append(recFiles["doc"], subdomain+"."+srv.config.Domain+port+path+"/"+file)
			recFiles["doc"] = append(recFiles["doc"], fmt.Sprint(path, "/", file.Name()))
		}
	}
	folder = srv.VidPath(mod, id)
	if files, ok := srv.ReadDir(folder); ok == nil && len(files) > 0 {
		path := strings.Replace(strings.Replace(folder, srv.GetPath(), "/api/static", 1), "/", "/", -1)
		recFiles["vid"] = []string{}
		for _, file := range files {
			// recFiles["vid"] = append(recFiles["vid"], subdomain+"."+srv.config.Domain+port+path+"/"+file)
			recFiles["vid"] = append(recFiles["vid"], fmt.Sprint(path, "/", file.Name()))
		}
	}
	return recFiles
}

// UploadFile ...
func (srv *Utilities) UploadFile(src io.Reader, path string, fileName string) (fullPath string, err error) {
	err = srv.MkdirAll(srv.CorrectPath(path), 0776)
	if err != nil {
		srv.Log(err.Error(), RunTimeError)
		return fullPath, errors.New("unable to create folder")
	}
	fileName = srv.CorrectPath(path + "/" + fileName)
	dst, err := srv.Create(fileName)
	if err != nil {
		srv.Log(err.Error(), RunTimeError)
		return fullPath, errors.New("unable to create file")
	}
	defer dst.Close()
	_, err = io.Copy(dst, src)
	if err != nil {
		srv.Log(err.Error(), RunTimeError)
		return fullPath, errors.New("unable to process file")
	}
	//build the files url
	fullPath = strings.Replace(fileName, srv.GetPath(), "/static", 1)
	// url = strings.Replace(path, "/", "/", -1)
	return
}

func (srv *Utilities) CoyDir() string {
	if srv.coy_tag == nil {
		panic(errors.New("CoyDir can only be used if tag is set"))
	}
	coydir := srv.config.CustomPath + "/" + *srv.coy_tag
	if srv.sftp != nil {
		coydir = srv.config.SSH[srv.getPrefSftp()].Path + "/" + *srv.coy_tag
		// srv.sftp.
	}
	return srv.CorrectPath(coydir)
}

func (srv *Utilities) GetPath() string {
	return srv.CorrectPath(srv.CoyDir() + srv.config.Path)
}

func (srv *Utilities) DBDir() string {
	path := srv.CorrectPath(srv.CoyDir() + "/" + "database")
	srv.MkdirAll(path, 0776)
	return path
}

// PixPath ...
func (srv *Utilities) PixPath(mod string, id interface{}) string {
	return srv.buildPath(mod, id, "pix")
}

// VidPath ...
func (srv *Utilities) VidPath(mod string, id interface{}) string {
	return srv.buildPath(mod, id, "vid")
}

// DocPath ...
func (srv *Utilities) DocPath(mod string, id interface{}) string {
	return srv.buildPath(mod, id, "doc")
}

func (srv *Utilities) buildPath(mod string, id interface{}, typ string) string {
	coyPath := srv.GetPath()
	switch typ {
	case "pix":
		return coyPath + "/" + mod + "/" + fmt.Sprintf("%v", id) + "/" + srv.config.PicPath
	case "vid":
		return coyPath + "/" + mod + "/" + fmt.Sprintf("%v", id) + "/" + srv.config.VideoPath
	case "doc":
		return coyPath + "/" + mod + "/" + fmt.Sprintf("%v", id) + "/" + srv.config.VideoPath
	}
	return coyPath
}

// returns the proper path sepertor on current OS ...
func (srv *Utilities) GetIP() string {
	addrs, err := net.InterfaceAddrs()
	if err != nil {
		return ""
	}
	for _, address := range addrs {
		// check the address type and if it is not a loopback the display it
		if ipnet, ok := address.(*net.IPNet); ok && !ipnet.IP.IsLoopback() {
			if ipnet.IP.To4() != nil {
				return ipnet.IP.String()
			}
		}
	}
	return "127.0.0.1"
}
func (srv *Utilities) MkdirAll(path string, mode fs.FileMode) error {
	if len(strings.TrimSpace(srv.getPrefSftp())) != 0 && srv.sftp != nil {
		err := srv.sftp.MkdirAll(path)
		if err != nil {
			return err
		}
		srv.sftp.Chmod(path, mode)
	}
	return os.MkdirAll(path, mode)
}
func (srv *Utilities) OpenFile(name string, flag int, perm fs.FileMode) (wrap_sftp.File, error) {
	if len(strings.TrimSpace(srv.getPrefSftp())) != 0 && srv.sftp != nil {
		file, err := srv.sftp.OpenFile(name, flag)
		if err != nil {
			return file, err
		}
		srv.sftp.Chmod(name, perm)
		return file, nil
	}
	return os.OpenFile(name, flag, perm)
}
func (srv *Utilities) Rename(oldpath string, newpath string) error {
	if len(strings.TrimSpace(srv.getPrefSftp())) != 0 && srv.sftp != nil {
		return srv.sftp.Rename(oldpath, newpath)
	}
	return os.Rename(oldpath, newpath)
}
func (srv *Utilities) Create(name string) (wrap_sftp.File, error) {
	if len(strings.TrimSpace(srv.getPrefSftp())) != 0 && srv.sftp != nil {
		return srv.sftp.Create(name)
	}
	return os.Create(name)
}
func (srv *Utilities) Remove(name string) error {
	if len(strings.TrimSpace(srv.getPrefSftp())) != 0 && srv.sftp != nil {
		return srv.sftp.Remove(name)
	}
	return os.Remove(name)
}
func (srv *Utilities) ReadFile(name string) ([]byte, error) {
	if len(strings.TrimSpace(srv.getPrefSftp())) != 0 && srv.sftp != nil {
		f, err := srv.sftp.OpenFile(name, (os.O_RDONLY))
		if err != nil {
			fmt.Fprintf(os.Stderr, "Unable to open remote file: %v\n", err)
			return []byte{}, err
		}
		defer f.Close()

		var size int
		if info, err := f.Stat(); err == nil {
			size64 := info.Size()
			if int64(int(size64)) == size64 {
				size = int(size64)
			}
		}
		size++ // one byte for final read at EOF

		// If a file claims a small size, read at least 512 bytes.
		// In particular, files in Linux's /proc claim size 0 but
		// then do not work right if read in small pieces,
		// so an initial read of 1 byte would not work correctly.
		if size < 512 {
			size = 512
		}

		data := make([]byte, 0, size)
		for {
			if len(data) >= cap(data) {
				d := append(data[:cap(data)], 0)
				data = d[:len(data)]
			}
			n, err := f.Read(data[len(data):cap(data)])
			data = data[:len(data)+n]
			if err != nil {
				if err == io.EOF {
					err = nil
				}
				return data, err
			}
		}
	}
	return os.ReadFile(name)
}

// ReadDir ...
func (srv *Utilities) ReadDir(path string) (files []fs.FileInfo, err error) {
	// var _fs []fs.FileInfo
	if len(strings.TrimSpace(srv.getPrefSftp())) != 0 && srv.sftp != nil {
		files, err = srv.sftp.ReadDir(path)
		if err != nil {
			return files, err
		}
	} else {
		f, err := os.Open(path)
		if err != nil {
			return files, err
		}
		files, err = f.Readdir(-1)
		f.Close()
		if err != nil {
			return files, err
		}
	}
	// for _, file := range _fs {
	// 	files = append(files, file.Name())
	// }
	return
}

func (srv *Utilities) WriteFile(name string, data []byte, perm fs.FileMode) error {
	if len(strings.TrimSpace(srv.getPrefSftp())) != 0 && srv.sftp != nil {
		f, err := srv.sftp.OpenFile(name, (os.O_RDWR | os.O_CREATE | os.O_TRUNC))
		if err != nil {
			return err
		}
		defer f.Close()
		_, err = f.Write(data)
		return err
	}
	return os.WriteFile(name, data, perm)
}

// connect to ssh
func (srv *Utilities) ConnectSSH(ssh_tag string) *ssh.Client {
	if cred, ok := srv.config.SSH[ssh_tag]; ok {
		// When there's a non empty password add the password AuthMethod
		if cred.Password == "" {
			log.Panicln(errors.New("ssh password is required"))
		}
		//clean key value
		cred.Key = strings.ReplaceAll(strings.TrimSpace(cred.Key), "*", "")
		if cred.Key == "" {
			//get user home diectory
			//try this path for key if not provided
			user_home, _ := os.UserHomeDir()
			cred.Key = filepath.Join(user_home, ".ssh", "id_rsa")
		}
		//read the key
		keyBuff, err := os.ReadFile(cred.Key)
		if err != nil {
			log.Panicln(err)
		}
		key, err := ssh.ParsePrivateKey(keyBuff)
		if err != nil {
			log.Panicln(err)
		}
		var keyErr *knownhosts.KeyError
		errCallBack := func(e error) {
			if e != nil {
				log.Panicln(err)
			}
		}
		//get the key directory
		keyDir, _ := filepath.Split(cred.Key)
		known_hosts := filepath.Join(keyDir, "known_hosts")
		createKnownHosts := func() {
			f, e := os.OpenFile(known_hosts, os.O_CREATE, 0600)
			errCallBack(e)
			f.Close()
		}
		checkKnownHosts := func() ssh.HostKeyCallback {
			createKnownHosts()
			kh, e := knownhosts.New(known_hosts)
			errCallBack(e)
			return kh
		}
		addHostKey := func(host string, remote net.Addr, pubKey ssh.PublicKey) error {
			// add host key if host is not found in known_hosts, error object is return, if nil then connection proceeds,
			// if not nil then connection stops.
			f, fErr := os.OpenFile(known_hosts, os.O_APPEND|os.O_WRONLY, 0600)
			// f, fErr := os.OpenFile(host, os.O_APPEND|os.O_WRONLY, 0600)
			if fErr != nil {
				return fErr
			}
			defer f.Close()

			knownHosts := knownhosts.Normalize(remote.String())
			_, fileErr := f.WriteString(fmt.Sprintln(knownhosts.Line([]string{knownHosts}, pubKey)))
			return fileErr
		}
		keyString := func(k ssh.PublicKey) string {
			// e.g. "ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTY...."
			return k.Type() + " " + base64.StdEncoding.EncodeToString(k.Marshal())
		}
		// The client configuration with configuration option to use the ssh-agent
		sshConfig := &ssh.ClientConfig{
			User: cred.User,
			Auth: []ssh.AuthMethod{
				ssh.PublicKeys(key),
				ssh.Password(cred.Password),
			},
			HostKeyCallback: ssh.HostKeyCallback(func(host string, remote net.Addr, pubKey ssh.PublicKey) error {
				kh := checkKnownHosts()
				hErr := kh(host, remote, pubKey)
				// Reference: https://blog.golang.org/go1.13-errors
				// To understand what errors.As is.
				if errors.As(hErr, &keyErr) && len(keyErr.Want) > 0 {
					// Reference: https://www.godoc.org/golang.org/x/crypto/ssh/knownhosts#KeyError
					// if keyErr.Want slice is empty then host is unknown, if keyErr.Want is not empty
					// and if host is known then there is key mismatch the connection is then rejected.
					log.Printf("WARNING: %v is not a key of %s, either a MiTM attack or %s has reconfigured the host pub key.\n", keyString(pubKey), host, host)
					return keyErr
				} else if errors.As(hErr, &keyErr) && len(keyErr.Want) == 0 {
					// host key not found in known_hosts then give a warning and continue to connect.
					log.Printf("WARNING: %s is not trusted, adding this key: %s to known_hosts file.\n", host, keyString(pubKey))
					return addHostKey(host, remote, pubKey)
				}
				log.Printf("Pub key exists for %s.", host)
				return nil
			}),
		}
		sshConfig = &ssh.ClientConfig{
			User: cred.User,
			Auth: []ssh.AuthMethod{
				ssh.PublicKeys(key),
				ssh.Password(cred.Password),
			},
			HostKeyCallback: ssh.InsecureIgnoreHostKey(),
		}

		// Connect to the SSH Server
		ssh, err := ssh.Dial("tcp", fmt.Sprintf("%s:%s", cred.Host, cred.Port), sshConfig)
		if err != nil {
			log.Panicln(err)
		}
		return ssh
	}
	return nil
}

func (srv *Utilities) KeepSSHAlive(ssh *ssh.Client, CountMax int, done <-chan bool) error {
	if ssh == nil {
		return nil
	}
	const keepAliveInterval = time.Minute * 30
	t := time.NewTicker(keepAliveInterval)
	defer t.Stop()

	n := 0
	for {
		select {
		case <-t.C:
			if _, _, err := ssh.SendRequest("keepalive@golang.org", true, nil); err != nil {
				n++
				if n >= CountMax {
					ssh.Close()
					return nil
				}
			} else {
				n = 0
			}
		case <-done:
			return nil
		}
	}
}

// ConnectSFTP
func (srv *Utilities) ConnectSFTP(ssh_tag string) (*sftp.Client, func()) {
	close := func() {}
	ssh := srv.ConnectSSH(ssh_tag)
	if ssh == nil {
		return nil, close
	}
	// Create new SFTP client
	sftp, err := sftp.NewClient(ssh)
	if err != nil {
		ssh.Close()
		log.Panicln(err)
	}
	close = func() {
		sftp.Close()
		ssh.Close()
	}
	return sftp, close
}
func (srv *Utilities) ConnectSFTPWith(ssh *ssh.Client) *sftp.Client {
	// Create new SFTP client
	sftp, err := sftp.NewClient(ssh)
	if err != nil {
		log.Panicln(err)
	}
	return sftp
}

/*
*Functions
 */

// OSReadDir ...
func OSReadDir(root string) ([]string, error) {
	var files []string
	f, err := os.Open(root)
	if err != nil {
		return files, err
	}
	fileInfo, err := f.Readdir(-1)
	f.Close()
	if err != nil {
		return files, err
	}
	for _, file := range fileInfo {
		files = append(files, file.Name())
	}
	return files, nil
}

func LoadConfig(configPath *string) (config base_domain.AppConfig, err error) {
	data, err := os.ReadFile(*(configPath))
	if err != nil {
		return config, err
	}
	err = yaml.Unmarshal(data, &config)
	if err != nil {
		return config, err
	}
	//config file error conditions

	//PicPath picture path
	config.PicPath = "pix"
	//VideoPath video path
	config.VideoPath = "vidz"
	//DocPath Documnet path
	config.DocPath = "docs"
	//Config Documnet path
	config.Config = "/" + "config"
	return config, nil
}

// validate the config to ensure requred fields are provided
func ValidateConfig(config base_domain.AppConfig) error {
	def := "****"
	if config.Domain == "" || config.Domain == def {
		return errors.New("invalid config parameter : domain")
	}
	if config.Port == "" || config.Port == def {
		return errors.New("invalid config parameter : port")
	}
	if config.CustomPath == "" || config.CustomPath == def {
		return errors.New("invalid config parameter : root_path")
	}
	if config.ActiveDB == "" || config.ActiveDB == def {
		return errors.New("invalid config parameter : active_db")
	}
	if db, ok := config.DataBase[config.ActiveDB]; ok {
		if db.Host == "" || config.Port == def {
			return errors.New(fmt.Sprint("invalid config parameter : db['", config.ActiveDB, "'].host"))
		}
		if db.User == "" || config.Port == def {
			return errors.New(fmt.Sprint("invalid config parameter : db['", config.ActiveDB, "'].user"))
		}
		if db.Password == "" || config.Port == def {
			return errors.New(fmt.Sprint("invalid config parameter : db['", config.ActiveDB, "'].password"))
		}
		if db.Port == "" || config.Port == def {
			return errors.New(fmt.Sprint("invalid config parameter : db['", config.ActiveDB, "'].port"))
		}
	} else {
		return errors.New(fmt.Sprint("invalid config parameter : db['", config.ActiveDB, "'] doest exist in db list"))
	}
	if _, ok := config.SSH[config.Sftp]; ok && len(strings.TrimSpace(config.SSH[config.Sftp].Path)) == 0 {
		return errors.New(fmt.Sprint("invalid config parameter : ssh['", config.Sftp, "'].path"))
	}
	return nil
}

// RandomCharset ...
func RandomCharset(length int, charset string) string {
	b := make([]byte, length)
	for i := range b {
		b[i] = charset[seededRand.Intn(len(charset))]
	}
	return string(b)
}

// RandomString ...
func RandomString(length int) string {
	return RandomCharset(length, charset)
}

// ParamOperator -- map param to operators
func ParamOperator(param *map[string]interface{}, operator *map[string]interface{}) {
	if operator != nil && param != nil {
		for k := range *param {
			if v, ok := (*operator)[k]; ok {
				(*param)[k] = map[string]interface{}{"con": v, "val": (*param)[k]}
			}
		}
	}
}
