package base_blocs

import (
	"errors"
	"fmt"
	"strconv"

	"gitlab.com/cesmanager/base-server/core/base_domain"
	"gitlab.com/cesmanager/base-server/core/base_port"
)

type credit struct {
	base_domain.Feature
	port base_port.Credit
}

func NewCredit(port base_port.Credit) *credit {
	return &credit{
		port: port,
		Feature: base_domain.Feature{
			Name:    NewAccount(nil).Name,
			Mod:     NewAccount(nil).Mod,
			Actions: NewAccount(nil).Actions,
		},
	}
}

func (srv *credit) Create(params map[string]interface{}, accountSrv *account) (base_domain.Credit, error) {
	var credit = base_domain.Credit{}
	err := srv.requiredParams(params, []string{"title", "account_id", "amount", "creator_id", "ip", "creditor_id"})
	if err != nil {
		return credit, err
	}
	srv.updateFields(&credit, params)
	account, err := accountSrv.Get(fmt.Sprint(credit.Transaction.AccountID))
	if err != nil {
		return credit, err
	}
	creditor, err := accountSrv.Get(fmt.Sprint(credit.Transaction.CreatorID))
	if err != nil {
		return credit, err
	}
	if account.CurrencyID != creditor.CurrencyID {
		return credit, errors.New("incompatible currency match")
	}
	if err := srv.port.Store(&credit); err != nil {
		return credit, errors.New("unable to create credit")
	}
	return credit, nil
}

func (srv *credit) Get(id string) (base_domain.Credit, error) {
	credit, err := srv.port.Get(id)
	if err != nil {
		return credit, errors.New("unable to get credit")
	}
	if credit.ID == 0 {
		return credit, errors.New("no credit with requested id found")
	}
	return credit, nil
}

func (srv *credit) Find(param map[string]interface{}) ([]base_domain.Credit, error) {
	credit, err := srv.port.Find(param)
	if err != nil {
		return credit, errors.New("unable to get credit(s)")
	}
	return credit, nil
}

// Post a credit. Always call this function with a transaction db connection
func (srv *credit) Post(id string, poster_id string, accountSrv *account) (base_domain.Credit, error) {
	//find the credit
	credit, err := srv.Get(id)
	if err != nil {
		return credit, err
	}
	//post the amount to the company account
	_, err = accountSrv.PostAmount(fmt.Sprint(credit.Transaction.AccountID), fmt.Sprint(credit.Transaction.Amount))
	if err != nil {
		return credit, err
	}
	//post the amount to the receivers account
	_, err = accountSrv.PostAmount(fmt.Sprint(credit.CreditorID), fmt.Sprint(credit.Transaction.Amount))
	if err != nil {
		return credit, err
	}
	//mark this transaction as posted
	u64, _ := strconv.ParseUint(fmt.Sprint(poster_id), 10, 32)
	_u64 := uint(u64)
	credit.Transaction.PosterID = &_u64
	credit.Transaction.Posted = true

	if err := srv.port.Update(&credit); err != nil {
		return credit, errors.New("unable to post credit")
	}
	//find the updated record and reurn it
	return credit, nil
}

func (srv *credit) requiredParams(params map[string]interface{}, required []string) error {
	for _, r := range required {
		switch r {
		case "title":
			if _, ok := params[r]; !ok {
				return errors.New("title is required")
			}
		case "account_id":
			if _, ok := params[r]; !ok {
				return errors.New("account_id is required")
			}
		case "amount":
			if _, ok := params[r]; !ok {
				return errors.New("amount is required")
			}
		case "creator_id":
			if _, ok := params[r]; !ok {
				return errors.New("creator_id is required")
			}
		case "ip":
			if _, ok := params[r]; !ok {
				return errors.New("ip is required")
			}
		case "creditor_id":
			if _, ok := params[r]; !ok {
				return errors.New("creditor_id is required")
			}
		}
	}
	return nil
}
func (srv *credit) updateFields(credit *base_domain.Credit, params map[string]interface{}) {
	if title, ok := params["title"]; ok {
		credit.Transaction.Title = fmt.Sprint(title)
	}
	if account_id, ok := params["account_id"]; ok {
		u64, _ := strconv.ParseUint(fmt.Sprint(account_id), 10, 32)
		credit.Transaction.AccountID = uint(u64)
	}
	if amount, ok := params["amount"]; ok {
		credit.Transaction.Amount, _ = strconv.ParseFloat(fmt.Sprint(amount), 64)
	}
	if comment, ok := params["comment"]; ok {
		credit.Transaction.Comment = fmt.Sprint(comment)
	}
	if _category, ok := params["category_id"]; ok {
		u64, _ := strconv.ParseUint(fmt.Sprintf("%v", _category), 10, 32)
		_u64 := uint(u64)
		credit.Transaction.CategoryID = &_u64
	}
	if creator_id, ok := params["creator_id"]; ok {
		u64, _ := strconv.ParseUint(fmt.Sprint(creator_id), 10, 32)
		credit.Transaction.CreatorID = uint(u64)
	}
	if ip, ok := params["ip"]; ok {
		credit.Transaction.IP = fmt.Sprint(ip)
	}
	if transactor_name, ok := params["transactor_name"]; ok {
		credit.Transaction.TransactorName = fmt.Sprint(transactor_name)
	}
	if transactor_address1, ok := params["transactor_address1"]; ok {
		credit.Transaction.TransactorAddress1 = fmt.Sprint(transactor_address1)
	}
	if transactor_address2, ok := params["transactor_address2"]; ok {
		credit.Transaction.TransactorAddress2 = fmt.Sprint(transactor_address2)
	}
	if transactor_city, ok := params["transactor_city"]; ok {
		credit.Transaction.TransactorCity = fmt.Sprint(transactor_city)
	}
	if transactor_state, ok := params["transactor_state"]; ok {
		credit.Transaction.TransactorState = fmt.Sprint(transactor_state)
	}
	if transactor_country, ok := params["transactor_country"]; ok {
		credit.Transaction.TransactorCountry = fmt.Sprint(transactor_country)
	}
	if creditor_id, ok := params["creditor_id"]; ok {
		u64, _ := strconv.ParseUint(fmt.Sprint(creditor_id), 10, 32)
		credit.CreditorID = uint(u64)
	}
}
