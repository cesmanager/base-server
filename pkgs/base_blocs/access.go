package base_blocs

import (
	"errors"
	"fmt"
	"strconv"
	"time"

	"gitlab.com/cesmanager/base-server/core/base_domain"
	"gitlab.com/cesmanager/base-server/core/base_port"
)

type access struct {
	base_domain.Feature
	port base_port.Access
}

func NewAccess(port base_port.Access) *access {
	return &access{
		port: port,
	}
}

//Get Access ...
func (s *access) Get(token string) (base_domain.Access, error) {
	access, err := s.port.Get(token)
	if err != nil {
		return access, err
	}
	if access.UserID == 0 {
		return access, errors.New("no access found")
	}
	return access, nil
}

//Log Access ...
func (s *access) Log(userID string, params map[string]interface{}) base_domain.Access {
	id, _ := strconv.Atoi(userID)

	access := base_domain.Access{
		UserID: uint(id),
		Token:  RandomString(32),
		Expiry: time.Now().AddDate(1, 0, 0), //expire after one year
	}
	s.updateFields(&access, params)
	for {
		err := s.port.Store(&access)
		if err == nil {
			break
		}
	}
	return access
}

//mark the last access ...
func (s *access) Update(token string, params map[string]interface{}) error {
	access, err := s.port.Get(token)
	s.updateFields(&access, params)
	if err != nil {
		return err
	}
	access.UpdatedAt = time.Now()
	err = s.port.Update(&access)
	if err != nil {
		return err
	}
	return nil
}

//DelAccess ...
func (s *access) Delete(token string) error {
	if len(token) == 0 {
		return errors.New("no token found")
	}
	err := s.port.Delete(token)
	if err != nil {
		return err
	}
	return nil
}

//GetAccess ...
func (s *access) updateFields(access *base_domain.Access, params map[string]interface{}) {
	if _, ok := params["ip"]; ok {
		access.IP = fmt.Sprint(params["ip"])
	}
	if _, ok := params["agent"]; ok {
		access.Agent = fmt.Sprint(params["agent"])
	}
}
