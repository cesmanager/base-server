package base_blocs

import (
	"errors"
	"fmt"
	"strconv"

	"gitlab.com/cesmanager/base-server/core/base_domain"
	"gitlab.com/cesmanager/base-server/core/base_port"
)

type preference struct {
	base_domain.Feature
	port base_port.Preference
}

func NewPreference(port base_port.Preference) *preference {
	mod := "preference"
	return &preference{
		port: port,
		Feature: base_domain.Feature{
			Name:    "Preference",
			Mod:     &mod,
			Actions: "list,store",
		},
	}
}

func (srv *preference) Find(profile_id string, key string) (base_domain.Preference, error) {
	profile, err := srv.port.Find(profile_id, key)
	if err != nil {
		return profile, errors.New("unable to get profile")
	}
	return profile, nil
}

func (srv *preference) Get(profile_id string) ([]base_domain.Preference, error) {
	profiles, err := srv.port.Get(profile_id)
	if err != nil {
		return profiles, errors.New("unable to get Preference")
	}
	return profiles, nil
}

func (srv *preference) Store(profile_id string, params map[string]interface{}) ([]base_domain.Preference, error) {
	_profile_id, _ := strconv.ParseUint(profile_id, 10, 32)
	delete(params, "profile_id")
	var preferences = []base_domain.Preference{}
	for k, v := range params {
		preferences = append(preferences, base_domain.Preference{
			ProfileID: uint(_profile_id),
			Key:       k,
			Value:     fmt.Sprint(v),
		})
	}

	err := srv.port.Store(&preferences)
	if err != nil {
		return preferences, errors.New("unable to save preferences")
	}
	return preferences, nil
}
