package base_blocs

import (
	"errors"
	"fmt"
	"strconv"
	"time"

	"gitlab.com/cesmanager/base-server/core/base_domain"
	"gitlab.com/cesmanager/base-server/core/base_port"
)

type event struct {
	base_domain.Feature
	port base_port.Event
}

func NewEvent(port base_port.Event) *event {
	mod := "event"
	return &event{
		port: port,
		Feature: base_domain.Feature{
			Name:    "Event",
			Mod:     &mod,
			Menu:    true,
			Actions: "list,create,update,delete",
		},
	}
}

func (srv *event) Get(id string) (base_domain.Event, error) {
	data, err := srv.port.Get(id)
	if err != nil {
		return data, errors.New("unable to get event")
	}
	return data, nil
}

func (srv *event) Create(params map[string]interface{}) (base_domain.Event, error) {
	var data = base_domain.Event{}
	srv.updateFields(&data, params)
	err := srv.port.Save(&data)
	return data, err
}

func (srv *event) Update(id string, params map[string]interface{}) (base_domain.Event, error) {
	data, err := srv.Get(id)
	if err != nil {
		return data, err
	}
	if data.ID == 0 {
		return data, fmt.Errorf("no event found")
	}
	srv.updateFields(&data, params)
	err = srv.port.Save(&data)
	return data, err
}

func (srv *event) List(params map[string]interface{}, order map[string]interface{}, page string, pageSize string) (base_domain.List, error) {
	pg, _ := strconv.Atoi(page)
	pgs, _ := strconv.Atoi(pageSize)
	list, err := srv.port.List(params, order, pg, pgs)
	if err != nil {
		return list, errors.New("unable to get list")
	}
	return list, nil
}

func (srv *event) Delete(ids []string) error {
	err := srv.port.Delete(ids)
	if err != nil {
		return errors.New("event delete failed")
	}
	return nil
}

func (srv *event) updateFields(model *base_domain.Event, params map[string]interface{}) error {
	if title, ok := params["title"]; ok {
		model.Title = fmt.Sprint(title)
	}
	if description, ok := params["description"]; ok {
		model.Description = fmt.Sprint(description)
	}
	if created_by_id, ok := params["created_by_id"]; ok && created_by_id != "null" && fmt.Sprint(created_by_id) != "" {
		u64, _ := strconv.ParseUint(fmt.Sprint(created_by_id), 10, 32)
		_u64 := uint(u64)
		model.CreatedByID = &_u64
	}
	if start_date, ok := params["start_date"]; ok && len(fmt.Sprint(start_date)) >= len("2006-01-02") {
		dt, err := time.Parse("2006-01-02", fmt.Sprint(start_date))
		if err != nil {
			return err
		}
		model.StartDate = dt
	}
	if end_date, ok := params["end_date"]; ok && len(fmt.Sprint(end_date)) >= len("2006-01-02") {
		dt, err := time.Parse("2006-01-02", fmt.Sprint(end_date))
		if err != nil {
			return err
		}
		model.EndDate = &dt
	}
	if start_time, ok := params["start_time"]; ok {
		model.StartTime = fmt.Sprint(start_time)
	}
	if end_time, ok := params["end_time"]; ok {
		model.EndTime = fmt.Sprint(end_time)
	}
	if duration, ok := params["duration"]; ok {
		u64, err := strconv.ParseUint(fmt.Sprint(duration), 10, 32)
		if err != nil {
			return err
		}
		model.Duration = uint(u64)
	}
	if all_day, ok := params["all_day"]; ok {
		model.AllDay, _ = strconv.ParseBool(fmt.Sprint(all_day))
	}
	if repeat, ok := params["repeat"]; ok {
		v, _ := strconv.Atoi(fmt.Sprint(repeat))
		model.Repeat = base_domain.EventRepeat(v)
	}
	if repeat_end_date, ok := params["repeat_end_date"]; ok {
		if fmt.Sprint(repeat_end_date) == "" || repeat_end_date == nil {
			model.RepeatEndDate = nil
		} else {
			dt, err := time.Parse("2006-01-02", fmt.Sprint(repeat_end_date))
			if err != nil {
				return err
			}
			model.RepeatEndDate = &dt
		}
	}
	//days
	if model.Repeat == base_domain.EventRepeatDaily {
		if day, ok := params["sunday"]; ok {
			model.Sunday, _ = strconv.ParseBool(fmt.Sprint(day))
		}
		if day, ok := params["monday"]; ok {
			model.Monday, _ = strconv.ParseBool(fmt.Sprint(day))
		}
		if day, ok := params["tuesday"]; ok {
			model.Tuesday, _ = strconv.ParseBool(fmt.Sprint(day))
		}
		if day, ok := params["wednesday"]; ok {
			model.Wednesday, _ = strconv.ParseBool(fmt.Sprint(day))
		}
		if day, ok := params["thursday"]; ok {
			model.Thursday, _ = strconv.ParseBool(fmt.Sprint(day))
		}
		if day, ok := params["friday"]; ok {
			model.Friday, _ = strconv.ParseBool(fmt.Sprint(day))
		}
		if day, ok := params["saturday"]; ok {
			model.Saturday, _ = strconv.ParseBool(fmt.Sprint(day))
		}
	}
	StartTime, _ := strconv.ParseInt(model.StartTime, 10, 32)
	start := model.StartDate.Add(time.Minute * time.Duration(StartTime))
	EndTime, _ := strconv.ParseInt(model.EndTime, 10, 32)
	end := model.EndDate.Add(time.Minute * time.Duration(EndTime))
	duration := end.Sub(start)
	model.Duration = uint(duration.Minutes())
	return nil
}
