package base_blocs

import (
	"errors"
	"fmt"
	"strconv"

	"gitlab.com/cesmanager/base-server/core/base_domain"
	"gitlab.com/cesmanager/base-server/core/base_port"
)

type status struct {
	base_domain.Feature
	port base_port.Status
}

func NewStatus(port base_port.Status) *status {
	mod := "status"
	return &status{
		port: port,
		Feature: base_domain.Feature{
			Name:    "Status",
			Mod:     &mod,
			Actions: "list,create,update,delete",
		},
	}
}

func (srv *status) Get(id string) (base_domain.Status, error) {
	status, err := srv.port.Get(id)
	if err != nil {
		return base_domain.Status{}, errors.New("unable to get status")
	}
	return status, nil
}

func (srv *status) List(param map[string]interface{}, order map[string]interface{}, page string, pageSize string) (base_domain.List, error) {
	pg, _ := strconv.Atoi(page)
	pgs, _ := strconv.Atoi(pageSize)
	list, err := srv.port.List(param, order, pg, pgs)
	if err != nil {
		return list, errors.New("unable to get list")
	}
	return list, nil
}

func (srv *status) Find(param map[string]interface{}) ([]base_domain.Status, error) {
	status, err := srv.port.Find(param)
	if err != nil {
		return status, errors.New("unable to get status")
	}
	return status, nil
}

func (srv *status) Create(params map[string]interface{}) (base_domain.Status, error) {
	var status = base_domain.Status{}
	srv.updateFields(&status, params)
	if err := srv.port.Store(&status); err != nil {
		return base_domain.Status{}, errors.New("unable to create status")
	}
	return status, nil
}

func (srv *status) CreateByInstance(status base_domain.Status) (base_domain.Status, error) {
	return status, srv.port.Store(&status)
}
func (srv *status) Update(id string, params map[string]interface{}) (base_domain.Status, error) {
	status, err := srv.Get(id)
	if err != nil {
		return status, err
	}
	srv.updateFields(&status, params)
	if err := srv.port.Update(&status); err != nil {
		return base_domain.Status{}, errors.New("unable to create status")
	}
	return status, nil
}

func (srv *status) Delete(id string) error {
	err := srv.port.Delete(id)
	if err != nil {
		return errors.New("unable to delete")
	}
	return nil
}

func (srv *status) updateFields(status *base_domain.Status, params map[string]interface{}) {
	if title, ok := params["title"]; ok {
		status.Title = fmt.Sprint(title)
	}
	if _type, ok := params["type"]; ok {
		status.Type = fmt.Sprint(_type)
	}
}
