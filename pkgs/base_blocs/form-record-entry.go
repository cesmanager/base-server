package base_blocs

import (
	"fmt"
	"strconv"

	"gitlab.com/cesmanager/base-server/core/base_domain"
	"gitlab.com/cesmanager/base-server/core/base_port"
)

type formRecordEntry struct {
	port base_port.FormRecordEntryPort
	base_domain.Feature
}

func NewFormRecordEntry(port base_port.FormRecordEntryPort) *formRecordEntry {
	return &formRecordEntry{
		port: port,
		Feature: base_domain.Feature{
			Name:    "e-Form Record Entry",
			Mod:     NewFormRecord(nil).Feature.Mod,
			Actions: NewFormRecord(nil).Feature.Actions,
		},
	}
}

func (b *formRecordEntry) Find(params map[string]interface{}) ([]base_domain.FormRecordEntry, error) {
	return b.port.Find(params)
}

func (b *formRecordEntry) Save(params map[string]interface{}) (base_domain.FormRecordEntry, error) {
	rec := base_domain.FormRecordEntry{}
	b.parse(&rec, params)
	err := b.port.Store(&rec)
	return rec, err
}

func (b *formRecordEntry) Delete(params map[string]interface{}) error {
	return b.port.Delete(params)
}

func (b *formRecordEntry) parse(rec *base_domain.FormRecordEntry, params map[string]interface{}) {
	if _, ok := params["name"]; ok {
		rec.Name = fmt.Sprint(params["name"])
	}
	if _, ok := params["value"]; ok {
		rec.Value = fmt.Sprint(params["value"])
	}
	if _, ok := params["form_record_id"]; ok {
		u64, _ := strconv.ParseUint(fmt.Sprint(params["form_record_id"]), 10, 64)
		rec.FormRecordID = uint(u64)
	}
}
