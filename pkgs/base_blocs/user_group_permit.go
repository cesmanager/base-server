package base_blocs

import (
	"errors"
	"fmt"
	"strconv"

	"gitlab.com/cesmanager/base-server/core/base_domain"
	"gitlab.com/cesmanager/base-server/core/base_port"
)

type userGroupPermit struct {
	base_domain.Feature
	port base_port.UserGroupPermit
}

func NewUserGroupPermit(port base_port.UserGroupPermit) *userGroupPermit {
	return &userGroupPermit{
		port: port,
		Feature: base_domain.Feature{
			Name:    NewUserGroup(nil).Feature.Name,
			Mod:     NewUserGroup(nil).Feature.Mod,
			Actions: NewUserGroup(nil).Feature.Actions,
		},
	}
}

func (srv *userGroupPermit) Save(id string, params map[string]interface{}) (base_domain.UserGroupPermit, error) {
	var data = base_domain.UserGroupPermit{}
	srv.updateFields(&data, params)
	u, _ := strconv.Atoi(fmt.Sprint(id))
	data.UserGroupID = uint(u)
	err := srv.port.Store(&data)
	if err != nil {
		return data, errors.New("unable to save permission")
	}
	return data, nil
}
func (srv *userGroupPermit) updateFields(data *base_domain.UserGroupPermit, params map[string]interface{}) {
	if mod, ok := params["mod"]; ok {
		data.Mod = fmt.Sprint(mod)
	}
	if self, ok := params["self"]; ok {
		data.Self, _ = strconv.ParseBool(fmt.Sprint(self))
	}
	if actions, ok := params["actions"]; ok {
		data.Actions = fmt.Sprint(actions)
	}
}
