FROM golang:1.18 AS builder

#
ENV PKG_NAME=cesmanager/
ENV GO_SRC=$GOPATH/src
ENV PKG_PATH=$GO_SRC/$PKG_NAME
RUN mkdir -p $PKG_PATH

#get dep
RUN go get -u github.com/golang/dep/cmd/dep

COPY . $PKG_PATH

ADD . $PKG_PATH

WORKDIR $PKG_PATH 

#run dep
RUN dep init
RUN dep ensure -v

#build project for server mode
RUN go build -o ./cmb/server/main ./cmb/server/


ENTRYPOINT ./cmb/server/main

EXPOSE 8082

