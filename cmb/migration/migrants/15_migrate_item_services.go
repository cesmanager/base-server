package migrants

import (
	"runtime"
	"strings"

	"gitlab.com/cesmanager/base-server/cmb/migration/gormigrator"
	"gitlab.com/cesmanager/base-server/core/base_domain"
	"gorm.io/gorm"
)

func init() {
	_, file, _, _ := runtime.Caller(0)
	Migrants = append(Migrants, gormigrator.MigratorItems{
		Key: strings.Split(strings.Split(file, "/")[len(strings.Split(file, "/"))-1], ".go")[0],
		Up: func(db *gorm.DB) error {
			err := db.AutoMigrate(&base_domain.ItemService{})
			return err
		},
		Down: func(db *gorm.DB) error {
			err := db.Migrator().DropTable(&base_domain.ItemService{})
			return err
		},
	})
}
