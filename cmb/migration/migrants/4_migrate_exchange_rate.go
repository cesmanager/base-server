package migrants

import (
	"log"
	"runtime"
	"strings"

	"gitlab.com/cesmanager/base-server/cmb/migration/gormigrator"
	"gitlab.com/cesmanager/base-server/core/base_domain"
	"gitlab.com/cesmanager/base-server/pkgs/base_blocs"
	"gitlab.com/cesmanager/base-server/pkgs/base_repository"
	"gorm.io/gorm"
)

func init() {
	_, file, _, _ := runtime.Caller(0)
	Migrants = append(Migrants, gormigrator.MigratorItems{
		Key: strings.Split(strings.Split(file, "/")[len(strings.Split(file, "/"))-1], ".go")[0],
		Up: func(db *gorm.DB) error {
			exchange := base_domain.CurrencyExchange{}
			err := db.AutoMigrate(&exchange)
			if err != nil {
				return err
			}
			exchangeRateServ := base_blocs.NewCurrencyExchange(base_repository.NewCurrencyExchange(db))
			for _, ex := range exchange.Initialize() {
				_, err = exchangeRateServ.CreateByInstance(ex)
				if err != nil {
					log.Println(err)
				}
			}
			return nil
		},
		Down: func(db *gorm.DB) error {
			err := db.Migrator().DropTable(&base_domain.CurrencyExchange{})
			return err
		},
	})
}
