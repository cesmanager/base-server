package migrants

import (
	"runtime"
	"strings"

	"gitlab.com/cesmanager/base-server/cmb/migration/gormigrator"
	"gitlab.com/cesmanager/base-server/core/base_domain"
	"gorm.io/gorm"
)

func init() {
	_, file, _, _ := runtime.Caller(0)
	Migrants = append(Migrants, gormigrator.MigratorItems{
		Key: strings.Split(strings.Split(file, "/")[len(strings.Split(file, "/"))-1], ".go")[0],
		Up: func(db *gorm.DB) error {
			if db.Migrator().HasTable(&base_domain.Settings{}) && !db.Migrator().HasColumn(&base_domain.Settings{}, "ProfileActive") {
				err := db.Migrator().AddColumn(&base_domain.Settings{}, "ProfileActive")
				if err != nil {
					return err
				}
			}
			if db.Migrator().HasTable(&base_domain.Settings{}) && !db.Migrator().HasColumn(&base_domain.Settings{}, "ProfileSuspended") {
				err := db.Migrator().AddColumn(&base_domain.Settings{}, "ProfileSuspended")
				if err != nil {
					return err
				}
			}
			if db.Migrator().HasTable(&base_domain.Settings{}) && !db.Migrator().HasColumn(&base_domain.Settings{}, "ProfileDisabled") {
				err := db.Migrator().AddColumn(&base_domain.Settings{}, "ProfileDisabled")
				if err != nil {
					return err
				}
			}
			if db.Migrator().HasTable(&base_domain.Settings{}) && !db.Migrator().HasColumn(&base_domain.Settings{}, "UserActive") {
				err := db.Migrator().AddColumn(&base_domain.Settings{}, "UserActive")
				if err != nil {
					return err
				}
			}
			if db.Migrator().HasTable(&base_domain.Settings{}) && !db.Migrator().HasColumn(&base_domain.Settings{}, "UserDisabled") {
				err := db.Migrator().AddColumn(&base_domain.Settings{}, "UserDisabled")
				if err != nil {
					return err
				}
			}
			return nil
		},
		Down: func(db *gorm.DB) error {
			if db.Migrator().HasTable(&base_domain.Settings{}) && db.Migrator().HasColumn(&base_domain.Settings{}, "ProfileActive") {
				err := db.Migrator().DropColumn(&base_domain.Settings{}, "ProfileActive")
				if err != nil {
					return err
				}
			}
			if db.Migrator().HasTable(&base_domain.Settings{}) && db.Migrator().HasColumn(&base_domain.Settings{}, "ProfileSuspended") {
				err := db.Migrator().DropColumn(&base_domain.Settings{}, "ProfileSuspended")
				if err != nil {
					return err
				}
			}
			if db.Migrator().HasTable(&base_domain.Settings{}) && db.Migrator().HasColumn(&base_domain.Settings{}, "ProfileDisabled") {
				err := db.Migrator().DropColumn(&base_domain.Settings{}, "ProfileDisabled")
				if err != nil {
					return err
				}
			}
			if db.Migrator().HasTable(&base_domain.Settings{}) && db.Migrator().HasColumn(&base_domain.Settings{}, "UserActive") {
				err := db.Migrator().DropColumn(&base_domain.Settings{}, "UserActive")
				if err != nil {
					return err
				}
			}
			if db.Migrator().HasTable(&base_domain.Settings{}) && db.Migrator().HasColumn(&base_domain.Settings{}, "UserDisabled") {
				err := db.Migrator().DropColumn(&base_domain.Settings{}, "UserDisabled")
				if err != nil {
					return err
				}
			}
			return nil
		},
	})
}
