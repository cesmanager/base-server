package migrants

import (
	"encoding/json"
	"io"
	"net/http"
	"os"
	"runtime"
	"sort"
	"strings"

	"gitlab.com/cesmanager/base-server/cmb/migration/gormigrator"
	"gitlab.com/cesmanager/base-server/core/base_domain"
	"gitlab.com/cesmanager/base-server/pkgs/base_blocs"
	"gitlab.com/cesmanager/base-server/pkgs/base_repository"
	"gorm.io/gorm"
)

func init() {
	_, file, _, _ := runtime.Caller(0)
	Migrants = append(Migrants, gormigrator.MigratorItems{
		Key: strings.Split(strings.Split(file, "/")[len(strings.Split(file, "/"))-1], ".go")[0],
		Up: func(db *gorm.DB) error {
			skip := db.Migrator().HasTable(&base_domain.GeoLocation{})
			//cretate the table
			err := db.AutoMigrate(&base_domain.GeoLocation{})
			//retrun if ther was an error or the the table already exist
			if err != nil || skip {
				return err
			}
			//try to load the country joson file
			rawCountries, err := os.ReadFile("countries.json")
			if err != nil { //if could not read file, get it online
				//make api call and get all world countries
				proxyReq, err := http.NewRequest("GET", "https://restcountries.com/v3.1/all", nil)
				if err != nil {
					return err
				}
				client := &http.Client{}
				proxyRes, err := client.Do(proxyReq)
				if err != nil {
					return err
				}
				rawCountries, err = io.ReadAll(proxyRes.Body)
				proxyRes.Body.Close()
				if err != nil {
					return err
				}
				//write the fetched country data to a countries.json file
				os.WriteFile("countries.json", rawCountries, os.ModePerm)
			}
			type CountryName struct {
				Common   string `json:"common"`
				Official string `json:"official"`
			}
			type Flags struct {
				Png string `json:"png"`
			}
			type CallCodes struct {
				Root   string   `json:"root"`
				Suffix []string `json:"suffixes"`
			}
			type countryModel struct {
				Name         CountryName `json:"name"`
				Alpha2Code   string      `json:"cca2"`
				Alpha3Code   string      `json:"cca3"`
				CallingCodes CallCodes   `json:"idd"`
				Capital      []string    `json:"capital"`
				Region       string      `json:"region"`
				Subregion    string      `json:"subregion"`
				Flags        Flags       `json:"flags"`
			}
			type subRegionModel struct {
				name      string
				countries []countryModel
			}
			type regionModel struct {
				name      string
				subRegion []subRegionModel
			}

			var countries []countryModel
			err = json.Unmarshal(rawCountries, &countries)
			if err != nil {
				return err
			}
			//sort the country list
			sort.SliceStable(countries, func(i, j int) bool {
				return strings.ToLower(countries[i].Name.Common) < strings.ToLower(countries[j].Name.Common)
			})
			//get all the regions and sub region
			regions := map[string]regionModel{}
			for _, country := range countries {
				mKey := strings.ReplaceAll(strings.TrimSpace(country.Region), " ", "_")
				// if len(mKey) == 0 {
				// 	mKey = "blank-region"
				// }
				//get the states
				if _, ok := regions[mKey]; ok { //if the region has ben stores, capture the sub region
					found := false
					for index, subRegion := range regions[mKey].subRegion {
						if country.Subregion == subRegion.name {
							//if we have stored the sub region already, capture the country
							found = true
							regions[mKey].subRegion[index].countries = append(regions[mKey].subRegion[index].countries, country)
						}
					}
					if !found {
						//capture new subreqion
						subregion := subRegionModel{
							name:      country.Subregion,
							countries: []countryModel{country},
						}
						region := regions[mKey]
						region.subRegion = append(region.subRegion, subregion)
						regions[mKey] = region
					}
				} else {
					//capture new region
					subRegion := subRegionModel{
						name:      country.Subregion,
						countries: []countryModel{country},
					}
					regions[mKey] = regionModel{
						name:      country.Region,
						subRegion: []subRegionModel{subRegion},
					}
				}
			}
			//sort the regions
			regionkeys := make([]string, 0, len(regions))
			for k := range regions {
				regionkeys = append(regionkeys, k)
			}
			sort.Strings(regionkeys)
			//get geolocation service
			geoLocationServ := base_blocs.NewGeoLocation(base_repository.NewGeoLocation(db))
			//get utilities for current company tag
			utils := base_blocs.NewUtils(Config, &Company.Tag)
			//store the countries
			for _, regionkey := range regionkeys {
				region := regions[regionkey]
				//store region
				geoRegion, err := geoLocationServ.Create(map[string]interface{}{"name": region.name, "title": region.name})
				if err != nil {
					return err
				}
				//store its sub regions
				sort.SliceStable(region.subRegion, func(i, j int) bool {
					return strings.ToLower(region.subRegion[i].name) < strings.ToLower(region.subRegion[j].name)
				})
				//create the sub regions
				for _, subRegion := range region.subRegion {
					//store subregion
					geoSubRegion, err := geoLocationServ.Create(map[string]interface{}{"name": subRegion.name, "title": subRegion.name, "parent_id": geoRegion.ID})
					if err != nil {
						return err
					}
					//store the countries
					for _, country := range subRegion.countries {
						//store country
						capital := ""
						if len(country.Capital) > 0 {
							capital = country.Capital[0]
						}
						callcode := country.CallingCodes.Root
						if len(country.CallingCodes.Suffix) == 1 && len(country.CallingCodes.Suffix[0]) < 3 {
							callcode += country.CallingCodes.Suffix[0]
						}
						ctry, err := geoLocationServ.Create(map[string]interface{}{
							"name":         country.Name.Common,
							"title":        country.Name.Official,
							"alpha_code_2": country.Alpha2Code,
							"alpha_code_3": country.Alpha3Code,
							"call_code":    callcode,
							"capital":      capital,
							"parent_id":    geoSubRegion.ID,
						})
						if err != nil {
							return err
						}
						//download and store the image
						if country.Flags.Png != "" {
							resp, err := http.Get(country.Flags.Png)
							if err != nil {
								return err
							}
							defer resp.Body.Close()
							path, err := utils.UploadFile(resp.Body, utils.PixPath(*geoLocationServ.Mod, ctry.ID), "photo.png")
							if err != nil {
								return err
							}
							ctry.Photo = strings.Replace(path, utils.GetPath(), "/static", 1)
							//set the photo to the currency record
							_, err = geoLocationServ.UpdateInstance(ctry)
							if err != nil {
								return err
							}
						}
					}
				}
			}
			return nil
		},
		Down: func(db *gorm.DB) error {
			if !db.Migrator().HasTable(&base_domain.GeoLocation{}) {
				return nil
			}
			err := db.Migrator().DropTable(&base_domain.GeoLocation{})
			return err
		},
	})
}
