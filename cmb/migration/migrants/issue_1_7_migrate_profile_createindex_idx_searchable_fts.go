package migrants

import (
	"runtime"
	"strings"

	"gitlab.com/cesmanager/base-server/cmb/migration/gormigrator"
	"gitlab.com/cesmanager/base-server/core/base_domain"
	"gorm.io/gorm"
)

func init() {
	_, file, _, _ := runtime.Caller(0)
	Migrants = append(Migrants, gormigrator.MigratorItems{
		Key: strings.Split(strings.Split(file, "/")[len(strings.Split(file, "/"))-1], ".go")[0],
		Up: func(db *gorm.DB) error {
			if db.Migrator().HasTable(&base_domain.Profile{}) && !db.Migrator().HasIndex(&base_domain.Profile{}, "idx_searchable_fts") {
				err := db.Migrator().CreateIndex(&base_domain.Profile{}, "idx_searchable_fts")
				if err != nil {
					return err
				}
			}
			return nil
		},
		Down: func(db *gorm.DB) error {
			if db.Migrator().HasTable(&base_domain.Profile{}) && db.Migrator().HasIndex(&base_domain.Profile{}, "idx_searchable_fts") {
				err := db.Migrator().DropIndex(&base_domain.Profile{}, "idx_searchable_fts")
				if err != nil {
					return err
				}
			}
			return nil
		},
	})
}
