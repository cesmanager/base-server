package migrants

import (
	"runtime"
	"strings"

	"gitlab.com/cesmanager/base-server/cmb/migration/gormigrator"
	"gitlab.com/cesmanager/base-server/core/base_domain"
	"gorm.io/gorm"
)

func init() {
	_, file, _, _ := runtime.Caller(0)
	Migrants = append(Migrants, gormigrator.MigratorItems{
		Key: strings.Split(strings.Split(file, "/")[len(strings.Split(file, "/"))-1], ".go")[0],
		Up: func(db *gorm.DB) error {
			//migrate the user table
			if db.Migrator().HasColumn(&base_domain.User{}, "active") {
				err := db.Migrator().RenameColumn(&base_domain.User{}, "active", "status")
				if err != nil {
					return err
				}
			}
			if db.Migrator().HasColumn(&base_domain.Profile{}, "active") {
				return db.Migrator().RenameColumn(&base_domain.Profile{}, "active", "status")
			}
			return nil
		},
		Down: func(db *gorm.DB) error {
			//migrate the user table
			if db.Migrator().HasColumn(&base_domain.User{}, "status") {
				err := db.Migrator().RenameColumn(&base_domain.User{}, "status", "active")
				if err != nil {
					return err
				}
			}
			if db.Migrator().HasColumn(&base_domain.Profile{}, "status") {
				return db.Migrator().RenameColumn(&base_domain.Profile{}, "status", "active")
			}
			return nil
		},
	})
}
