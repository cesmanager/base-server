package migrants

import (
	"runtime"
	"strings"

	"gitlab.com/cesmanager/base-server/cmb/migration/gormigrator"
	"gitlab.com/cesmanager/base-server/core/base_domain"
	"gorm.io/gorm"
)

func init() {
	_, file, _, _ := runtime.Caller(0)
	Migrants = append(Migrants, gormigrator.MigratorItems{
		Key: strings.Split(strings.Split(file, "/")[len(strings.Split(file, "/"))-1], ".go")[0],
		Up: func(db *gorm.DB) error {
			if db.Migrator().HasTable(&base_domain.Profile{}) && !db.Migrator().HasColumn(&base_domain.Profile{}, "GenderID") {
				err := db.Migrator().AddColumn(&base_domain.Profile{}, "GenderID")
				if err != nil {
					return err
				}
			}
			if db.Migrator().HasTable(&base_domain.Profile{}) && !db.Migrator().HasColumn(&base_domain.Profile{}, "Mobile") {
				err := db.Migrator().AddColumn(&base_domain.Profile{}, "Mobile")
				if err != nil {
					return err
				}
			}
			if db.Migrator().HasTable(&base_domain.Profile{}) && !db.Migrator().HasColumn(&base_domain.Profile{}, "Birthday") {
				err := db.Migrator().AddColumn(&base_domain.Profile{}, "Birthday")
				if err != nil {
					return err
				}
			}
			if db.Migrator().HasTable(&base_domain.Profile{}) && !db.Migrator().HasColumn(&base_domain.Profile{}, "DeletedAt") {
				err := db.Migrator().AddColumn(&base_domain.Profile{}, "DeletedAt")
				if err != nil {
					return err
				}
			}
			return nil
		},
		Down: func(db *gorm.DB) error {
			if db.Migrator().HasTable(&base_domain.Profile{}) && db.Migrator().HasColumn(&base_domain.Profile{}, "GenderID") {
				err := db.Migrator().DropColumn(&base_domain.Profile{}, "GenderID")
				if err != nil {
					return err
				}
			}
			if db.Migrator().HasTable(&base_domain.Profile{}) && db.Migrator().HasColumn(&base_domain.Profile{}, "Mobile") {
				err := db.Migrator().DropColumn(&base_domain.Profile{}, "Mobile")
				if err != nil {
					return err
				}
			}
			if db.Migrator().HasTable(&base_domain.Profile{}) && db.Migrator().HasColumn(&base_domain.Profile{}, "Birthday") {
				err := db.Migrator().DropColumn(&base_domain.Profile{}, "Birthday")
				if err != nil {
					return err
				}
			}
			if db.Migrator().HasTable(&base_domain.Profile{}) && db.Migrator().HasColumn(&base_domain.Profile{}, "DeletedAt") {
				err := db.Migrator().DropColumn(&base_domain.Profile{}, "DeletedAt")
				if err != nil {
					return err
				}
			}
			return nil
		},
	})
}
