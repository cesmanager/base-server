package migrants

import (
	"log"
	"runtime"
	"strings"

	"gitlab.com/cesmanager/base-server/cmb/migration/gormigrator"
	"gitlab.com/cesmanager/base-server/core/base_domain"
	"gitlab.com/cesmanager/base-server/pkgs/base_blocs"
	"gitlab.com/cesmanager/base-server/pkgs/base_repository"
	"gorm.io/gorm"
)

func init() {
	_, file, _, _ := runtime.Caller(0)
	Migrants = append(Migrants, gormigrator.MigratorItems{
		Key: strings.Split(strings.Split(file, "/")[len(strings.Split(file, "/"))-1], ".go")[0],
		Up: func(db *gorm.DB) error {
			userGroup := base_domain.UserGroup{}
			err := db.AutoMigrate(&userGroup)
			if err != nil {
				return err
			}
			userServ := base_blocs.NewUserGroup(base_repository.NewUserGroup(db))
			for _, u := range userGroup.Initialize() {
				_, err = userServ.CreateByInstance(u)
				if err != nil {
					log.Println(err)
					return err
				}
			}
			return nil
		},
		Down: func(db *gorm.DB) error {
			return db.Migrator().DropTable(&base_domain.UserGroup{})
		},
	})
}
