package migrants

import (
	"runtime"
	"strings"

	"gitlab.com/cesmanager/base-server/cmb/migration/gormigrator"
	"gitlab.com/cesmanager/base-server/core/base_domain"
	"gitlab.com/cesmanager/base-server/pkgs/base_blocs"
	"gitlab.com/cesmanager/base-server/pkgs/base_repository"
	"gorm.io/gorm"
)

func init() {
	_, file, _, _ := runtime.Caller(0)
	Migrants = append(Migrants, gormigrator.MigratorItems{
		Key: strings.Split(strings.Split(file, "/")[len(strings.Split(file, "/"))-1], ".go")[0],
		Up: func(db *gorm.DB) error {
			settings := base_domain.Settings{}
			skip := db.Migrator().HasTable(&settings)
			err := db.AutoMigrate(&settings)
			//retrun if ther was an error or the the table already exist
			if err != nil || skip {
				return err
			}

			settingsServ := base_blocs.NewSettings(base_repository.NewSettings(db))
			_, err = settingsServ.CreateByInstance(settings.Initialize())
			return err
		},
		Down: func(db *gorm.DB) error {
			err := db.Migrator().DropTable(&base_domain.Settings{})
			return err
		},
	})
}
