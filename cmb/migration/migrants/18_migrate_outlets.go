package migrants

import (
	"runtime"
	"strings"

	"gitlab.com/cesmanager/base-server/cmb/migration/gormigrator"
	"gitlab.com/cesmanager/base-server/core/base_domain"
	"gitlab.com/cesmanager/base-server/pkgs/base_blocs"
	"gitlab.com/cesmanager/base-server/pkgs/base_repository"
	"gorm.io/gorm"
)

func init() {
	_, file, _, _ := runtime.Caller(0)
	Migrants = append(Migrants, gormigrator.MigratorItems{
		Key: strings.Split(strings.Split(file, "/")[len(strings.Split(file, "/"))-1], ".go")[0],
		Up: func(db *gorm.DB) error {
			outlet := base_domain.Outlet{}
			err := db.AutoMigrate(&outlet)
			//retrun if ther was an error or the the table already exist
			if err != nil {
				return err
			}
			//get outlet service
			outetServ := base_blocs.NewOutlet(base_repository.NewOutlet(db))
			for _, o := range outlet.Initialize() {
				_, err = outetServ.CreateByInstance(o)
				if err != nil {
					return err
				}
			}
			return nil
		},
		Down: func(db *gorm.DB) error {
			err := db.Migrator().DropTable(&base_domain.Outlet{})
			return err
		},
	})
}
