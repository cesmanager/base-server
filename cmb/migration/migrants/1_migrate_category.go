package migrants

import (
	"log"
	"runtime"
	"strings"

	"gitlab.com/cesmanager/base-server/cmb/migration/gormigrator"
	"gitlab.com/cesmanager/base-server/core/base_domain"
	"gitlab.com/cesmanager/base-server/pkgs/base_blocs"
	"gitlab.com/cesmanager/base-server/pkgs/base_repository"
	"gorm.io/gorm"
)

func init() {
	_, file, _, _ := runtime.Caller(0)
	Migrants = append(Migrants, gormigrator.MigratorItems{
		Key: strings.Split(strings.Split(file, "/")[len(strings.Split(file, "/"))-1], ".go")[0],
		Up: func(db *gorm.DB) error {
			category := base_domain.Category{}
			err := db.AutoMigrate(&category)
			if err != nil {
				return err
			}
			categoryServ := base_blocs.NewCategory(base_repository.NewCategory(db))
			for _, cat := range category.Initialize() {
				_, err = categoryServ.CreateByInstance(cat)
				if err != nil {
					log.Println(err)
				}
			}
			return nil
		},
		Down: func(db *gorm.DB) error {
			err := db.Migrator().DropTable(&base_domain.Category{})
			return err
		},
	})
}
