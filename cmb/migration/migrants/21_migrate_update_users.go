package migrants

import (
	"runtime"
	"strings"

	"gitlab.com/cesmanager/base-server/cmb/migration/gormigrator"
	"gitlab.com/cesmanager/base-server/core/base_domain"
	"gorm.io/gorm"
)

func init() {
	_, file, _, _ := runtime.Caller(0)
	Migrants = append(Migrants, gormigrator.MigratorItems{
		Key: strings.Split(strings.Split(file, "/")[len(strings.Split(file, "/"))-1], ".go")[0],
		Up: func(db *gorm.DB) error {
			//migrate the user table
			err := db.AutoMigrate(&base_domain.User{})
			if err != nil {
				return err
			}
			//assign admin user to the Administration group
			err = db.Model(base_domain.User{}).Where("id = ?", 1).Update("user_group_id", 1).Error
			if err != nil {
				return err
			}
			err = db.Model(base_domain.User{}).Where("id != ?", 1).Update("user_group_id", 2).Error
			//assign every other user to the noaccess group
			return err
		},
		Down: func(db *gorm.DB) error {
			return db.Migrator().DropColumn(&base_domain.User{}, "user_group_id")
		},
	})
}
