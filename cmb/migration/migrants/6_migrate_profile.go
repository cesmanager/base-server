package migrants

import (
	"log"
	"runtime"
	"strings"

	"gitlab.com/cesmanager/base-server/cmb/migration/gormigrator"
	"gitlab.com/cesmanager/base-server/core/base_domain"
	"gitlab.com/cesmanager/base-server/pkgs/base_blocs"
	"gitlab.com/cesmanager/base-server/pkgs/base_repository"
	"gorm.io/gorm"
)

func init() {
	_, file, _, _ := runtime.Caller(0)
	Migrants = append(Migrants, gormigrator.MigratorItems{
		Key: strings.Split(strings.Split(file, "/")[len(strings.Split(file, "/"))-1], ".go")[0],
		Up: func(db *gorm.DB) error {
			profile := base_domain.Profile{}
			skip := db.Migrator().HasTable(&profile)
			db.DisableForeignKeyConstraintWhenMigrating = !db.DisableForeignKeyConstraintWhenMigrating
			err := db.AutoMigrate(&profile)
			db.DisableForeignKeyConstraintWhenMigrating = !db.DisableForeignKeyConstraintWhenMigrating
			//retrun if ther was an error or the the table already exist
			if err != nil || skip {
				return err
			}
			profServ := base_blocs.NewProfile(base_repository.NewProfile(db))
			//initialize table
			for _, p := range profile.Initialize() {
				_, err = profServ.CreateByInstance(p)
				if err != nil {
					log.Println(err)
				}
			}
			return nil
		},
		Down: func(db *gorm.DB) error {
			err := db.Migrator().DropTable(&base_domain.Profile{})
			return err
		},
	})
}
