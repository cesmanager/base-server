package migrants

import (
	"runtime"
	"strings"

	"gitlab.com/cesmanager/base-server/cmb/migration/gormigrator"
	"gitlab.com/cesmanager/base-server/core/base_domain"
	"gitlab.com/cesmanager/base-server/pkgs/base_blocs"
	"gitlab.com/cesmanager/base-server/pkgs/base_repository"
	"gorm.io/gorm"
)

func init() {
	_, file, _, _ := runtime.Caller(0)
	Migrants = append(Migrants, gormigrator.MigratorItems{
		Key: strings.Split(strings.Split(file, "/")[len(strings.Split(file, "/"))-1], ".go")[0],
		Up: func(db *gorm.DB) error {
			user := base_domain.User{}
			db.DisableForeignKeyConstraintWhenMigrating = !db.DisableForeignKeyConstraintWhenMigrating
			err := db.AutoMigrate(&user)
			db.DisableForeignKeyConstraintWhenMigrating = !db.DisableForeignKeyConstraintWhenMigrating
			//retrun if ther was an error or the the table already exist
			if err != nil {
				return err
			}
			userServ := base_blocs.NewUser(base_repository.NewUser(db))
			for _, u := range user.Initialize() {
				_, err = userServ.CreateByInstance(u)
				if err != nil {
					return err
				}
			}
			return nil
		},
		Down: func(db *gorm.DB) error {
			return db.Migrator().DropTable(&base_domain.User{})
		},
	})
}
