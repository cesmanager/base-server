package migrants

import (
	"runtime"
	"strings"

	"gitlab.com/cesmanager/base-server/cmb/migration/gormigrator"
	"gitlab.com/cesmanager/base-server/core/base_domain"
	"gorm.io/gorm"
)

func init() {
	_, file, _, _ := runtime.Caller(0)
	Migrants = append(Migrants, gormigrator.MigratorItems{
		Key: strings.Split(strings.Split(file, "/")[len(strings.Split(file, "/"))-1], ".go")[0],
		Up: func(db *gorm.DB) error {
			return db.AutoMigrate(&base_domain.Notification{}, &base_domain.Notified{})
		},
		Down: func(db *gorm.DB) error {
			if db.Migrator().HasTable(&base_domain.Notification{}) && db.Migrator().HasTable(&base_domain.Notified{}) {
				return db.Migrator().DropTable(&base_domain.Notification{}, &base_domain.Notified{})
			}
			return nil
		},
	})
}
