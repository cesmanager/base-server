package migrants

import (
	"gitlab.com/cesmanager/base-server/cmb/migration/gormigrator"
	"gitlab.com/cesmanager/base-server/core/base_domain"
)

//Migrants ...
var Migrants []gormigrator.MigratorItems
var Config base_domain.AppConfig //the global config data
var Company base_domain.CoyInfo  //currnt company migrating
