package main

import (
	"flag"
	"fmt"
	"log"
	"os"

	"gitlab.com/cesmanager/base-server/cmb/migration/gormigrator"
	migrants "gitlab.com/cesmanager/base-server/cmb/migration/migrants"
	"gitlab.com/cesmanager/base-server/pkgs/base_blocs"
	"gitlab.com/cesmanager/base-server/pkgs/base_repository"
)

const usageText = `This program runs command on the db. Supported commands are:
  - m=[option] or migrate=[option] - Indicate your migration direction(either up or down).
Usage:
	//for upward migration
	go -migrate=up
	//for rollback migration
	go -migrate=down
`

func main() {
	flag.Usage = usage
	action := flag.String("m", "up", "-m=[up / down] : Indicate your migration direction(either up or down).")
	key := flag.String("k", "", "-k=[option] : Indicate the specific migration to run.")
	force := flag.Bool("f", false, "-f=[true / false] : Indicate if the migration should be forced.")
	coy := flag.String("coy", "all", "-k=[option] : Indicate the specific company tag you want to migrade for.")
	configPath := flag.String("config", "config.yml", "-config=[path to config.json file]")
	flag.Parse()
	//load config
	config, err := base_blocs.LoadConfig(configPath)
	if err != nil {
		log.Panicln(err.Error())
	}
	err = base_blocs.ValidateConfig(config)
	if err != nil {
		log.Println(os.Args)
		log.Println(config)
		log.Panicln(err.Error())
	}
	utils := base_blocs.NewUtils(config, nil)

	//open ssh connections for db if required
	sshcon := utils.ConnectSSH(config.DataBase[config.ActiveDB].SShTag)
	if sshcon != nil {
		defer sshcon.Close()
		//register an ssh dailer for database
		base_repository.RegisterDBSSH(sshcon, config.DataBase[config.ActiveDB].SShTag)
	}
	//send config to all migrants globally
	migrants.Config = config

	db, close, err := base_repository.MySQL(config.DataBase[config.ActiveDB], "")
	if err != nil {
		log.Panicln(err.Error())
	}
	//get a list of all the registered companies
	coyService := base_blocs.NewCoyInfo(base_repository.NewCoyInfo(db))
	coycon := map[string]interface{}{}
	if (*coy) != "all" {
		coycon["tag"] = (*coy)
	}
	companies, err := coyService.Find(coycon)
	//close this connection since we have no need for it
	close()
	if err != nil {
		log.Fatalln(err.Error())
	}
	//migrate for each company
	for _, company := range companies {
		//set current compiny in migrants
		migrants.Company = company
		//open a connection for this company
		dbname := config.DataBase[config.ActiveDB].DBPrefix + company.Tag
		//backup the database
		err := base_blocs.BackUp(config, company.Tag)
		if err != nil {
			//lof connection error for this company and proceed to the next conpany
			fmt.Println(err.Error())
			fmt.Println(dbname, "backup failed and migration was terminated")
			continue
		}

		db, close, err := base_repository.MySQL(config.DataBase[config.ActiveDB], dbname)
		if err != nil {
			//lof connection error for this company and proceed to the next conpany
			fmt.Println(err.Error())
			fmt.Println("Count not connect to " + dbname)
			continue
		}
		tx := db.Begin()
		fmt.Println("Begin Migration : Database = " + dbname)
		//create the migrator object
		migrator := gormigrator.Gormigrator{
			Action: *action,
			Key:    *key,
			Force:  *force,
			System: "base_server",
			Items:  migrants.Migrants,
		}
		//run the migration
		err = migrator.Run(tx)
		if err != nil {
			fmt.Println(err.Error())
			fmt.Println("Migration Failed : Database = " + dbname)
			fmt.Println("Rolling Back : Database = " + dbname)
			tx.Rollback()
			continue
		}
		//commit the change
		tx.Commit()
		//close the connection
		close()
		fmt.Println("Migration Complete : Database = " + dbname)
	}
}
func usage() {
	fmt.Print(usageText)
	flag.PrintDefaults()
	os.Exit(2)
}
