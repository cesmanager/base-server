package gormigrator

import (
	"fmt"
	"runtime/debug"
	"sort"
	"strconv"
	"strings"

	"gorm.io/gorm"
)

//Gormigrator ...
type Gormigrator struct {
	Action string
	Key    string
	Force  bool
	System string
	Items  []MigratorItems
}
type sortByName []MigratorItems

func (a sortByName) Len() int { return len(a) }
func (a sortByName) Less(i, j int) bool {
	tsi := strings.Split(a[i].Key, "_")
	si, _ := strconv.Atoi(tsi[0])
	tsj := strings.Split(a[j].Key, "_")
	sj, _ := strconv.Atoi(tsj[0])
	return sj > si
}
func (a sortByName) Swap(i, j int) { a[i], a[j] = a[j], a[i] }

//Run ...
func (e *Gormigrator) Run(db *gorm.DB) (err error) {
	defer func() {
		if rec := recover(); rec != nil {
			err = fmt.Errorf(fmt.Sprintf("%v\n\t%v", rec, string(debug.Stack())))
		}
		if err != nil {
			println(err.Error())
		}
	}()
	if e.Force && len(e.Key) == 0 {
		err = fmt.Errorf("migrant 'key' must be provide if you intended to 'force' migration")
		return
	}
	//backup db

	db.AutoMigrate(&MigratorItems{})
	if len(e.Action) == 0 {
		e.Action = "up"
	}
	cnt := 0
	sort.Sort(sortByName(e.Items))
	switch e.Action {
	case "up":
		//get the higest batch id in migration
		batchID := e.getBatchID(db, "")
		batchID++
		for _, item := range e.Items {
			if len(e.Key) != 0 && e.Key != item.Key {
				continue
			}
			//check if it has been migrated before
			migrated := item.migtrated(db)
			if migrated && !e.Force {
				continue
			}
			println("Migrating : " + item.Key)
			err = item.Up(db)
			if err != nil {
				return
			}
			item.BatchID = batchID
			if !e.Force {
				item.Add(db, e.System)
			}
			//record the migration
			println("Migration " + item.Key + " completed")
			cnt++
		}
	case "down":
		//get the higest batch id in migration
		batchID := e.getBatchID(db, e.System)

		//get all migration in the last batch
		var migrates []MigratorItems
		db.Where("batch_id", batchID).Order("id DESC").Find(&migrates)
		if len(e.Key) != 0 {
			db.Where("key", e.Key).Order("id DESC").Find(&migrates)
		} else {
			db.Where("batch_id", batchID).Order("id DESC").Find(&migrates)
		}

		for _, item := range migrates {
			if len(e.Key) != 0 && e.Key != item.Key {
				continue
			}
			var migrate MigratorItems
			for _, toRoll := range e.Items {
				if toRoll.Key == item.Key {
					//roll back
					migrate = toRoll
					migrate.ID = item.ID
					break
				}
			}
			if migrate.ID == 0 {
				continue
			}
			println("RollBack : " + item.Key + " started")
			err = migrate.Down(db)
			if err != nil {
				return
			}
			err = item.Delete(db)
			if err != nil {
				return
			}
			println("RollBack : " + item.Key + " completed")
			cnt++
		}
	}
	if cnt == 0 {
		println("Nothing to migrate")
	}
	return
}

func (e *Gormigrator) getBatchID(db *gorm.DB, system string) int {
	//get the higest batch id in migration
	var migrates []MigratorItems
	_db := db
	if len(system) > 0 {
		_db = db.Where("system= ?", e.System)
	}
	_db.Order("batch_id DESC").Limit(1).Find(&migrates)
	if len(migrates) == 0 {
		return 0
	}
	return migrates[0].BatchID
}

//DoMigrate ...
type DoMigrate func(*gorm.DB) error

//MigratorItems ...
type MigratorItems struct {
	ID      int    //the id of the migration to be executed
	Key     string `grom:"unique"`
	System  string
	BatchID int
	Up      DoMigrate `gorm:"-"`
	Down    DoMigrate `gorm:"-"`
}

func (e *MigratorItems) migtrated(db *gorm.DB) bool {
	var migrates []MigratorItems
	db.Where("key", e.Key).Limit(1).Find(&migrates)
	return len(migrates) > 0
}

//Add ...
func (e *MigratorItems) Add(db *gorm.DB, system string) error {
	e.System = system
	// insert into the migration table
	result := db.Create(&e)
	return result.Error
}

//Delete Currency account
func (e *MigratorItems) Delete(db *gorm.DB) error {
	result := db.Delete(&e)
	return result.Error
}
