package main

import (
	"context"
	"errors"
	"flag"
	"fmt"
	"log"
	"net"
	"net/http"
	"os"
	"os/exec"
	"os/signal"
	"strconv"
	"strings"
	"syscall"
	"time"

	"gitlab.com/cesmanager/base-server/core/base_domain"
	"gitlab.com/cesmanager/base-server/pkgs/base_blocs"
	"gitlab.com/cesmanager/base-server/pkgs/base_repository"
	routers "gitlab.com/cesmanager/base-server/pkgs/handlers"
	"golang.org/x/crypto/ssh"
)

type SSHDialer struct {
	client *ssh.Client
}

func (s *SSHDialer) DialContext(_ context.Context, addr string) (net.Conn, error) {
	return s.client.Dial("tcp", addr)
}

type service struct {
	*http.Server
	utils *base_blocs.Utilities
}

func NewService(server *http.Server) *service {
	return &service{
		Server: server,
	}
}

func (s *service) WaitShutdown() {
	sig := make(chan os.Signal, 1)
	signal.Notify(sig, syscall.SIGINT, syscall.SIGTERM)

	//Wait interrupt for interupt OS Signal
	<-sig
	s.utils.Log("stopping http server ...", base_blocs.StdLog)

	//Create shutdown context with 10 second timeout
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	//shutdown the server
	err := s.Shutdown(ctx)
	if err != nil {
		s.utils.Log(fmt.Sprintf("http shutdown error: %v", err), base_blocs.RunTimeError)
	}
}
func (s *service) start() error {
	s.utils.Log("closing previous deamon", base_blocs.StdLog)
	//stop previous deamons
	err := s.stop("")
	if err != nil {
		return err
	}
	args := []string{}
	for _, arg := range os.Args[1:] {
		if arg == "-service=start" {
			continue
		}
		args = append(args, arg)
	}
	cmd := exec.Command(os.Args[0], args...)
	err = cmd.Start()
	if err != nil {
		return err
	}
	err = savePID(strconv.Itoa(cmd.Process.Pid))
	if err != nil {
		s.stop(fmt.Sprint(cmd.Process.Pid)) //then stop the service immediately
		return err
	}
	s.utils.Log(fmt.Sprint("PID = ", cmd.Process.Pid), base_blocs.StdLog)
	return nil
}
func (s *service) stop(_pid string) (err error) {
	if _pid == "" {
		_pid, err = getLastPID()
		if err != nil {
			return err
		}
	}
	if _pid == "" {
		s.utils.Log("no active process", base_blocs.StdLog)
		return nil
	}
	pid, _ := strconv.Atoi(_pid)
	process, err := os.FindProcess(pid)
	if err != nil {
		return err
	}
	//interupt it
	err = process.Signal(os.Interrupt)
	if err != nil && !errors.Is(err, os.ErrProcessDone) {
		err = process.Signal(os.Kill)
		if err != nil {
			return err
		}
	}
	removePID() //delete the tracling file
	return nil
}

func processConfig() base_domain.AppConfig {
	configPath := flag.String("config", "config.yml", "-config=[path to config.yml file] default value is config.yml")
	domain := flag.String("domain", "", "-config=[domain name for the project] eg. example.com")
	port := flag.String("port", "", "-port=[local port for the system] eg. 2121")
	root := flag.String("root", "", `-root=[path to location where files will be managed] eg. C:\Users\{{user}}\custom or /home/{{user}}/custom`)
	dbh := flag.String("dbh", "", "-dbh=[database host]")
	dbu := flag.String("dbu", "", "-dbu=[database user]")
	dbp := flag.String("dbp", "", "-dbp=[database password]")
	dbport := flag.String("dbport", "", "-dbp=[database password]")
	dblog := flag.Bool("dblog", false, "-dbp=[database password]")
	//default will be stop so as to stop previously running service
	flag.Parse()

	//load config
	config, err := base_blocs.LoadConfig(configPath)
	if err != nil {
		log.Println("Invalid config file")
		log.Fatal(err)
	}
	/**
	* update the config with passed parameters
	and likewise rebuiled the parameters
	**/
	if (*domain) != "" && (*domain) != config.Domain {
		config.Domain = (*domain)
	}
	if (*port) != "" && (*port) != config.Port {
		config.Port = (*port)
	}
	if (*root) != "" && (*root) != config.CustomPath {
		config.CustomPath = (*root)
	}
	//update the acive db with passed values
	if db, ok := config.DataBase[config.ActiveDB]; ok {
		if (*dbh) != "" && (*dbh) != db.Host {
			db.Host = (*dbh)
		}
		if (*dbu) != "" && (*dbu) != db.User {
			db.User = (*dbu)
		}
		if (*dbp) != "" && (*dbp) != db.Password {
			db.Password = (*dbp)
		}
		if (*dbport) != "" && (*dbport) != db.Port {
			db.Port = (*dbport)
		}
		db.Debug = ((*dblog) || db.Debug)
		//clean the ssh tag field
		db.SShTag = strings.ReplaceAll(strings.TrimSpace(db.SShTag), "*", "")
		config.DataBase[config.ActiveDB] = db
	}
	err = base_blocs.ValidateConfig(config)
	if err != nil {
		log.Fatalln(err.Error())
	}
	return config
}

func getLastPID() (string, error) {
	if _, err := os.Stat("pid.txt"); errors.Is(err, os.ErrNotExist) {
		return "", nil
	}
	byt, err := os.ReadFile("pid.txt")
	return string(byt), err
}

func savePID(pid string) error {
	return os.WriteFile("pid.txt", []byte(pid), os.ModePerm)
}

func removePID() error {
	return os.Remove("pid.txt")
}

func main() {
	svcFlag := flag.String("service", "none", "-service=[start/stop/none] start or stop the active deamon. none will run the without a deamon")
	config := processConfig()
	handlers := routers.Web(config)
	utils := base_blocs.NewUtils(config, nil)
	// Connect to the SSH Server for database
	sshcon := utils.ConnectSSH(config.DataBase[config.ActiveDB].SShTag)
	if sshcon != nil {
		defer sshcon.Close()
		//register an ssh dailer for database
		base_repository.RegisterDBSSH(sshcon, config.DataBase[config.ActiveDB].SShTag)
	}

	//create Service
	svc := NewService(&http.Server{
		Addr:    ":" + config.Port,
		Handler: handlers,
	})
	svc.utils = utils
	//get utilities
	switch *svcFlag {
	case "start":
		utils.Log("starting deamon", base_blocs.StdLog)
		err := svc.start()
		if err != nil {
			utils.Log(err, base_blocs.PanicError)
			os.Exit(1)
		}
		utils.Log("start complete", base_blocs.StdLog)
		return
	case "stop":
		utils.Log("stopping deamon", base_blocs.StdLog)
		err := svc.stop("")
		if err != nil {
			utils.Log(err, base_blocs.PanicError)
			os.Exit(1)
		}
		utils.Log("stop complete", base_blocs.StdLog)
		return
	default:
		//stop any instance that is not the current instance
		pid, _ := getLastPID()
		if len(pid) != 0 && fmt.Sprint(os.Getpid()) != pid {
			utils.Log("stopping active deamon", base_blocs.StdLog)
			err := svc.stop(pid)
			if err != nil {
				utils.Log(err, base_blocs.PanicError)
				os.Exit(1)
			}
			utils.Log("stop complete", base_blocs.StdLog)
		}
	}

	done := make(chan bool, 1)
	go func() {
		err := svc.ListenAndServe()
		if err != nil {
			utils.Log(err, base_blocs.StdLog)
		}
		done <- true
	}()

	go func() {
		err := utils.KeepSSHAlive(sshcon, 10, done)
		if err != nil {
			utils.Log(err, base_blocs.StdLog)
			err := svc.stop("")
			if err != nil {
				utils.Log(err, base_blocs.PanicError)
				os.Exit(1)
			}
		}
	}()

	svc.WaitShutdown()
	<-done
	utils.Log("shutdown complete", base_blocs.StdLog)
}
