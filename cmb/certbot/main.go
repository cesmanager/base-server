package main

import (
	"bytes"
	"encoding/json"
	"encoding/xml"
	"flag"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"regexp"
	"strings"
	"time"

	"gopkg.in/yaml.v3"
)

// namecheap xml struct
type ApiResponse struct {
	XMLName           xml.Name        `xml:"ApiResponse"`
	Status            string          `xml:"Status,attr"`
	Xmlns             string          `xml:"xmlns,attr"`
	RequestedCommand  string          `xml:"RequestedCommand"`
	Server            string          `xml:"Server"`
	GMTTimeDifference string          `xml:"GMTTimeDifference"`
	ExecutionTime     string          `xml:"ExecutionTime"`
	Errors            Errors          `xml:"Errors"`
	CommandResponse   CommandResponse `xml:"CommandResponse"`
}

type Errors struct {
	XMLName xml.Name `xml:"Errors"`
	Errors  []Error  `xml:"Error"`
}
type Error struct {
	Number string `xml:"Number,attr"`
	Value  string `xml:",chardata"`
}
type CommandResponse struct {
	XMLName           xml.Name           `xml:"CommandResponse"`
	Type              string             `xml:"Type,attr"`
	DNSGetHostsResult *DNSGetHostsResult `xml:"DomainDNSGetHostsResult"`
	DNSSetHostsResult *DNSSetHostsResult `xml:"DomainDNSSetHostsResult"`
}
type DNSSetHostsResult struct {
	XMLName   xml.Name `xml:"DomainDNSSetHostsResult"`
	Domain    string   `xml:"Domain,attr"`
	IsSuccess string   `xml:"IsSuccess,attr"`
}
type DNSGetHostsResult struct {
	XMLName       xml.Name `xml:"DomainDNSGetHostsResult"`
	Domain        string   `xml:"Domain,attr"`
	IsUsingOurDNS string   `xml:"IsUsingOurDNS,attr"`
	Hosts         []Host   `xml:"host"`
}
type Host struct {
	XMLName xml.Name `xml:"host"`
	HostID  string   `xml:"HostId,attr"`
	Name    string   `xml:"Name,attr"`
	Type    string   `xml:"Type,attr"`
	Address string   `xml:"Address,attr"`
	MXPref  string   `xml:"MXPref,attr"`
	TTL     string   `xml:"TTL,attr"`
}

func main() {
	file, _ := os.Executable()
	configPath := filepath.Join(append(filepath.SplitList(filepath.Dir(file)), "certbot.yml")...)
	//default will be stop so as to stop previously running service
	flag.Parse()
	data, err := os.ReadFile(configPath)
	if err != nil {
		log.Fatal(err)
	}
	config := map[string]string{}
	err = yaml.Unmarshal(data, &config)
	if err != nil {
		log.Fatal(err)
	}
	if _, ok := config["host"]; !ok {
		log.Fatal("Host is required in config")
	}
	domain := getDomain(config)
	log.Println("DOMAINS:", domain)
	val := "dummy_text"
	if _d, ok := os.LookupEnv("CERTBOT_VALIDATION"); ok {
		val = _d
	}
	log.Println("CERTBOT_VALIDATION:", val)
	switch config["host"] {
	case "namecheap":
		CheckNCConfig(config)
		domain_parts := strings.Split(domain, ".")
		base_url := fmt.Sprintf("https://%s/xml.response?apiuser=%s&apikey=%s&username=%s&ClientIp=%s&SLD=%s&TLD=%s",
			config["url"],
			config["api_user"],
			config["api_key"],
			config["api_user"],
			config["ip"],
			domain_parts[0],
			domain_parts[1],
		)

		req_url := fmt.Sprintf("%s&Command=namecheap.domains.dns.getHosts", base_url)

		resp, err := http.Get(req_url)
		if err != nil {
			log.Fatal(err)
		}
		_d, err := io.ReadAll(resp.Body)
		if err != nil {
			log.Fatal(err)
		}
		var data ApiResponse
		err = xml.Unmarshal(_d, &data)
		if err != nil {
			log.Fatal(err)
		}
		req_url = fmt.Sprintf("%s&Command=namecheap.domains.dns.setHosts", base_url)

		if len(data.Errors.Errors) > 0 {
			for _, err := range data.Errors.Errors {
				log.Printf("%s:%s", err.Number, err.Value)
			}
			log.Fatal("request failed with above error(s)")
		}
		cleanup := false
		for i, rec := range (*data.CommandResponse.DNSGetHostsResult).Hosts {
			if rec.Name == "_acme-challenge" && rec.Address == val {
				cleanup = true
				continue
			}
			req_url = fmt.Sprintf("%s&%s=%s", req_url, fmt.Sprintf("HostName%d", i+1), rec.Name)
			req_url = fmt.Sprintf("%s&%s=%s", req_url, fmt.Sprintf("RecordType%d", i+1), rec.Type)
			req_url = fmt.Sprintf("%s&%s=%s", req_url, fmt.Sprintf("Address%d", i+1), rec.Address)
			req_url = fmt.Sprintf("%s&%s=%s", req_url, fmt.Sprintf("TTL%d", i+1), rec.TTL)
		}
		//add the extra
		if !cleanup {
			i := len((*data.CommandResponse.DNSGetHostsResult).Hosts) + 1
			req_url = fmt.Sprintf("%s&%s=%s", req_url, fmt.Sprintf("HostName%d", i), "_acme-challenge")
			req_url = fmt.Sprintf("%s&%s=%s", req_url, fmt.Sprintf("RecordType%d", i), "TXT")
			req_url = fmt.Sprintf("%s&%s=%s", req_url, fmt.Sprintf("Address%d", i), val)
			req_url = fmt.Sprintf("%s&%s=%s", req_url, fmt.Sprintf("TTL%d", i), "1200")
		}
		//clean data
		data = ApiResponse{}
		resp, err = http.Get(req_url)
		if err != nil {
			log.Fatal(err)
		}
		_d, err = io.ReadAll(resp.Body)
		if err != nil {
			log.Fatal(err)
		}
		err = xml.Unmarshal(_d, &data)
		if err != nil {
			log.Fatal(err)
		}
		if len(data.Errors.Errors) > 0 {
			for _, err := range data.Errors.Errors {
				log.Printf("%s:%s", err.Number, err.Value)
			}
			log.Fatal("request failed with above error(s)")
		}
	case "godaddy":
		CheckGDConfig(config)
		base_url := fmt.Sprintf("https://%s/v1/domains/%s/records/",
			config["url"],
			domain,
		)
		headers := map[string][]string{
			"accept":        {"application/json"},
			"Content-Type":  {"application/json"},
			"Authorization": {fmt.Sprintf("sso-key %s:%s", config["api_key"], config["api_secret"])},
		}
		req, err := http.NewRequest(http.MethodGet,
			fmt.Sprintf("%s/TXT/_acme-challenge", base_url),
			bytes.NewBuffer([]byte("")),
		)
		if err != nil {
			log.Fatal(err)
		}
		req.Header = headers
		client := &http.Client{}
		resp, err := client.Do(req)
		if err != nil {
			log.Fatal(err)
		}
		//read the body
		body, err := io.ReadAll(resp.Body)
		resp.Body.Close()
		if err != nil {
			log.Fatal(err)
		}
		if resp.StatusCode != 200 {
			var data map[string]interface{}
			err = json.Unmarshal(body, &data)
			if err != nil {
				log.Fatal(err)
			}
			log.Fatal(data)
		}
		var records []map[string]interface{}
		err = json.Unmarshal(body, &records)
		if err != nil {
			log.Fatal(err)
		}
		if len(records) > 0 {
			//delete all
			req, err := http.NewRequest(http.MethodDelete,
				fmt.Sprintf("%s/TXT/_acme-challenge", base_url),
				bytes.NewBuffer([]byte("")),
			)
			if err != nil {
				log.Fatal(err)
			}
			req.Header = headers
			client := &http.Client{}
			resp, err = client.Do(req)
			if err != nil {
				log.Fatal(err)
			}
			//read the body
			body, err := io.ReadAll(resp.Body)
			resp.Body.Close()
			if err != nil {
				log.Fatal(err)
			}
			if (resp.StatusCode - 200) >= 100 {
				var data map[string]interface{}
				err = json.Unmarshal(body, &data)
				if err != nil {
					log.Fatal(err)
				}
				log.Fatal(data)
			}
		}
		add_rec := func(r []map[string]interface{}) {
			_r, err := json.Marshal(r)
			if err != nil {
				log.Fatal(err)
			}
			//delete it
			req, err := http.NewRequest(http.MethodPatch, base_url,
				bytes.NewBuffer(_r),
			)
			if err != nil {
				log.Fatal(err)
			}
			req.Header = headers
			client := &http.Client{}
			resp, err = client.Do(req)
			if err != nil {
				log.Fatal(err)
			}
			//read the body
			body, err := io.ReadAll(resp.Body)
			resp.Body.Close()
			if err != nil {
				log.Fatal(err)
			}
			if resp.StatusCode != 200 {
				var data map[string]interface{}
				err = json.Unmarshal(body, &data)
				if err != nil {
					log.Fatal(err)
				}
				log.Fatal(data)
			}
		}
		exist := false
		for _, r := range records {
			if fmt.Sprint(r["name"]) == "_acme-challenge" && fmt.Sprint(r["data"]) != val {
				_rec := []map[string]interface{}{}
				_rec = append(_rec, r)
				add_rec(_rec)
			} else {
				exist = true
			}
		}
		if !exist {
			_rec := []map[string]interface{}{}
			_rec = append(_rec, map[string]interface{}{
				"data": val,
				"name": "_acme-challenge",
				"ttl":  1200,
				"type": "TXT",
			})
			add_rec(_rec)
		}
	case "":
		log.Fatal("host is required")
	default:
		log.Fatalf("host:%s is not understood", config["host"])
	}
	time.Sleep(time.Minute / 2) //30 seconds
}

func CheckNCConfig(config map[string]string) {
	if _, ok := config["url"]; !ok {
		log.Fatal("config.url required")
	}
	if _, ok := config["api_user"]; !ok {
		log.Fatal("config.api_user required")
	}
	if _, ok := config["api_key"]; !ok {
		log.Fatal("config.api_key required")
	}
	if _, ok := config["username"]; !ok {
		log.Fatal("config.username required")
	}
}
func CheckGDConfig(config map[string]string) {
	if _, ok := config["url"]; !ok {
		log.Fatal("config.url required")
	}
	if _, ok := config["api_key"]; !ok {
		log.Fatal("config.api_key required")
	}
	if _, ok := config["api_secret"]; !ok {
		log.Fatal("config.api_secret required")
	}
}
func getDomain(config map[string]string) string {
	domain := ""
	if _d, ok := os.LookupEnv("CERTBOT_DOMAIN"); ok {
		domain = _d
	} else {
		domain = config["domain"]
	}
	if domain == "" {
		log.Fatal("domain cant be empty")
	}
	re := regexp.MustCompile(`(?m)(.*\.)?(.*\..*)`)
	domain = re.FindStringSubmatch(domain)[len(re.FindStringSubmatch(domain))-1]
	return domain
}
