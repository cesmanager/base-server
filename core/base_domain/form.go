package base_domain

import (
	"time"
)

type Form struct {
	ID               uint        `json:"id"`
	Title            string      `json:"title"`
	Description      string      `json:"description"`
	Ref              string      `json:"ref"`
	DocumentRequired bool        `json:"document_required"`
	Data             string      `json:"data" gorm:"type:text"`
	MapedData        interface{} `json:"maped_data" gorm:"-"`
	CategoryID       *uint       `json:"category_id"`
	Category         *Category   `json:"category"`
	CreatedAt        time.Time   `json:"created_at"`
	UpdatedAt        time.Time   `json:"updated_at"`
}
type FormRecord struct {
	ID          uint     `json:"id"`
	FormID      uint     `json:"form_id"`
	Form        Form     `json:"form"`
	OutletID    *uint    `json:"outlet_id"`
	Outlet      *Outlet  `json:"outlet"`
	ProfileID   *uint    `json:"profile_id"`
	Profile     *Profile `json:"profile"`
	ReviewdByID *uint    `json:"reviewd_by_id"`
	ReviewdBy   *Profile `json:"reviewd_by"`
	Status      string   `json:"status"`
	// Reference   string            `json:"reference" gorm:"unique"`
	Comment   string            `json:"comment" gorm:"type:text"`
	Entries   []FormRecordEntry `json:"entries"`
	CreatedAt time.Time         `json:"created_at"`
	UpdatedAt time.Time         `json:"updated_at"`
}
type FormRecordEntry struct {
	FormRecordID uint       `json:"form_record_id" gorm:"primaryKey;autoIncrement:false"`
	FormRecord   FormRecord `json:"form_record"`
	Name         string     `json:"name" gorm:"primaryKey"`
	Value        string     `json:"value"`
	CreatedAt    time.Time  `json:"created_at"`
	UpdatedAt    time.Time  `json:"updated_at"`
}
