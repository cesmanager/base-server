package base_domain

//CurrencyExchange ...
type CurrencyExchange struct {
	ID         uint      `json:"id"`
	Title      string    `json:"title"`
	CurrencyID uint      `json:"currency_id"`
	Currency   *Currency `json:"currency"`
	ToID       uint      `json:"to_id"`
	To         *Currency `json:"to"`
	ExValue    float32   `json:"ex_value"`
}

func (d *CurrencyExchange) Initialize() []CurrencyExchange {
	//exchange rates
	return []CurrencyExchange{
		{
			ID:         1,
			Title:      "Naira to Dollar",
			CurrencyID: 1,
			ToID:       2,
			ExValue:    360,
		}, {
			ID:         2,
			Title:      "Naira to Pounds",
			CurrencyID: 1,
			ToID:       3,
			ExValue:    400,
		}, {
			ID:         3,
			Title:      "Dollar to Naira",
			CurrencyID: 2,
			ToID:       1,
			ExValue:    320,
		}, {
			ID:         4,
			Title:      "Dollar to Pounds",
			CurrencyID: 2,
			ToID:       3,
			ExValue:    1,
		}, {
			ID:         5,
			Title:      "Pounds to Naira",
			CurrencyID: 3,
			ToID:       1,
			ExValue:    400,
		}, {
			ID:         6,
			Title:      "Pounds to Dollar",
			CurrencyID: 3,
			ToID:       2,
			ExValue:    20,
		},
	}
}
