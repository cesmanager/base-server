package base_domain

import (
	"time"
)

//ItemService defination
type ItemService struct {
	ItemID    uint
	Item      Item
	Capacity  float32
	Active    int8
	CreatedAt time.Time
	UpdatedAt time.Time
}
