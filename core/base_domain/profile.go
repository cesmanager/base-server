package base_domain

import (
	"time"

	"gorm.io/gorm"
)

// Profile model
type Profile struct {
	ID         uint           `json:"id"`
	Type       int8           `json:"type"` //0- Individual;1- Company,
	Photo      string         `json:"photo"`
	Email      *string        `json:"email" gorm:"unique;index:idx_searchable_fts,class:FULLTEXT;"`
	AltEmail   *string        `json:"alt_email" gorm:"index:idx_searchable_fts,class:FULLTEXT;"`
	Mobile     string         `json:"mobile"`
	Phone      string         `json:"phone"`
	Fname      string         `json:"fname" gorm:"index:idx_searchable_fts,class:FULLTEXT;"`
	Lname      string         `json:"lname" gorm:"index:idx_searchable_fts,class:FULLTEXT;"`
	Mname      string         `json:"mname" gorm:"index:idx_searchable_fts,class:FULLTEXT;"`
	Bname      string         `json:"bname" gorm:"index:idx_searchable_fts,class:FULLTEXT;"`
	GenderID   *uint          `json:"gender_id"`
	Gender     *Status        `json:"gender"`
	Birthday   *time.Time     `json:"birthday" gorm:"type:date"`
	OutletID   *uint          `json:"outlet_id"`
	Outlet     *Outlet        `json:"outlet" gorm:"foreignKey:OutletID"`
	Address1   string         `json:"address1"`
	Address2   string         `json:"address2"`
	City       string         `json:"city"`
	ZipCode    string         `json:"zip_code"`
	State      string         `json:"state"`
	CountryID  *uint          `json:"country_id"`
	Country    *GeoLocation   `json:"country"`
	CategoryID *uint          `json:"category_id"`
	Category   *Category      `json:"category"`
	Status     uint           `json:"status"`
	CreatedAt  time.Time      `json:"create_date"`
	UpdatedAt  time.Time      `json:"updated_date"`
	DeletedAt  gorm.DeletedAt `gorm:"index"`
}

func (profile *Profile) Initialize() []Profile {
	adm := "admin@example.com"
	cus := "customer@example.com"
	return []Profile{
		{
			ID:     1,
			Type:   1,
			Email:  &adm,
			Fname:  "Admin",
			Lname:  "Admin",
			Mname:  "Admin",
			Status: 1,
		}, {
			ID:     2,
			Type:   1,
			Email:  &cus,
			Fname:  "Customer",
			Lname:  "Customer",
			Mname:  "Customer",
			Status: 1,
		},
	}
}

/**
*User types
* 1- Staff/Employee
 */
