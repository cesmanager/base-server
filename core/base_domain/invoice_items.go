package base_domain

//InvoicedItem ...
type InvoicedItem struct {
	ID        uint
	Title     string
	InvoiceID uint
	Invoice   Invoice
	ItemID    uint
	Item      Item
	/**
	* sometime a product/item will not be in the same currency as the buyer
	* so a rate has to be used to convert the current price of the product to the
	* corresponding price for the currency matching the invoice
	**/
	ExFrom    float32 //the from exchange value
	ExTo      float32 //the to exchange value
	UnitPrice float32 //default price for an item
	UnitPack  float32 //units per pack
	Pack      int     //Number of packs
	Dsc       float32 //%discount
	Discount  float32 //value discount
}
