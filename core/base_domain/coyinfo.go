package base_domain

import (
	"time"
)

// CoyInfo ...
type CoyInfo struct {
	ID        uint      `json:"id"`
	Name      string    `json:"name"`
	Tag       string    `json:"tag"`
	Domain    string    `json:"domain"`
	TypeID    *int      `json:"type_id"`
	Type      *Status   `json:"type"`
	Features  string    `json:"features" gorm:"type:TEXT"` //comma seperated mod codes
	Addons    string    `json:"addons"`                    //comma seperated addon codes
	Contact   *uint     `json:"contact"`                   //profile id of the user associated with this company
	Root      bool      `json:"root"`
	Status    int8      `json:"status"` //0-Active, 1-Suspended,2-Disabled,3-Ongoing Maintenance
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
}

type Company struct {
	ID        uint      `json:"id"`
	Name      string    `json:"name"`
	Photo     string    `json:"photo"`
	Status    int8      `json:"status"` //0-Active, 1-Suspended,2-Disabled,3-Ongoing Maintenance
	TypeID    *int      `json:"type_id"`
	Type      *Status   `json:"type"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
	CoyInfo
	*Profile
	*Settings
}

//table change
// ALTER TABLE `coy_infos`
//  ADD COLUMN `tag` VARCHAR(256) NULL DEFAULT NULL COLLATE 'utf16_unicode_ci' AFTER `name`;
// 	CHANGE COLUMN `sub_domain` `domain` VARCHAR(256) NULL DEFAULT NULL COLLATE 'utf16_unicode_ci' AFTER `tag`,
//  ADD COLUMN `contact` BIGINT(20) NULL DEFAULT NULL AFTER `type_id`;
// 	ADD COLUMN `root` TINYINT(4) NOT NULL DEFAULT '0' AFTER `contact`,
// 	CHANGE COLUMN `active` `status` TINYINT(4) NULL DEFAULT NULL AFTER `root`;

// Features of the software
type Feature struct {
	ID        uint      `json:"id"`
	Name      string    `json:"name"`
	Title     string    `json:"title"`
	Cnt       int64     `json:"cnt" gorm:"-"` //count of children
	GroupTag  string    `json:"group_tag"`    //for parent-index-index
	ParentID  *uint     `json:"parent_id"`    //for parent
	Features  []Feature `json:"features" gorm:"foreignkey:ParentID"`
	Parent    *Feature  `json:"parent"`
	Mod       *string   `json:"mod"`
	Url       string    `json:"url" gorm:"-"`
	Actions   string    `json:"actions"` //comma seperated list of posible action this feature can perform
	Icon      string    `json:"icon"`
	Menu      bool      `json:"menu"`
	Root      bool      `json:"root"`
	Active    bool      `json:"active"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
}

func (Feature) TableName() string {
	return "features"
}
func (m Feature) SetID(id uint) Feature {
	m.ID = id
	return m
}
func (m Feature) SetParent(id uint) Feature {
	m.ParentID = &id
	return m
}
func (m Feature) SetIcon(icon string) Feature {
	m.Icon = icon
	return m
}
func (m Feature) IsActive(active bool) Feature {
	m.Active = active
	return m
}
func (m Feature) IsRoot() Feature {
	m.Root = true
	return m
}
