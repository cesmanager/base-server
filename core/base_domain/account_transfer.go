package base_domain

//Transfer struct for transaction tracking
type Transfer struct {
	ID                   uint        `json:"id"`
	Transaction          Transaction `json:"transaction" gorm:"polymorphic:Kind;"`
	SenderID             uint        `json:"sender_id"` //user account where the amount is debited
	Sender               Account     `json:"sender"`
	BeneficiaryName      string      `json:"beneficiary_name"`
	BeneficiaryAccountNo string      `json:"beneficiary_accountNo"`
	BeneficiaryBank      string      `json:"beneficiary_bank"`
}
