package base_domain

import (
	"time"
)

type FileInfo struct {
	Name         string    `json:"name"`
	Path         string    `json:"path" gorm:"unique;not null"`
	IsDir        bool      `json:"is_dir"`
	System       bool      `json:"system"` //used to identify system generated files
	Secured      bool      `json:"secured"`
	SecuredGroup string    `json:"secured_group"` //comma seperated user-group IDs
	Mode         string    `json:"mode" gorm:"-"`
	OwnerID      *uint     `json:"owner_id"`
	Owner        *User     `json:"owner"`
	CreatedBy    *uint     `json:"created_by"`
	CreatedAt    time.Time `json:"created_at"`
	UpdatedAt    time.Time `json:"updated_at"`
}
