package base_domain

import "time"

type EventRepeat int8

const (
	EventRepeatNone EventRepeat = iota
	EventRepeatDaily
	EventRepeatWeekly
	EventRepeatMonthly
	EventRepeatYearly
)

func (d EventRepeat) String() string {
	return [...]string{"none", "daily", "weekly", "monthly", "yearly"}[d]
}
func (d EventRepeat) All() []string {
	return []string{"none", "daily", "weekly", "monthly", "yearly"}
}

type Event struct {
	ID            uint        `json:"id"`
	Title         string      `json:"title"`
	Description   string      `json:"description"`
	Series        uint        `json:"series"`
	Tag           string      `json:"tag"`
	CreatedByID   *uint       `json:"created_by_id"`
	CreatedBy     *Profile    `json:"created_by"`
	StartDate     time.Time   `json:"start_date"`
	EndDate       *time.Time  `json:"end_date"`
	StartTime     string      `json:"start_time"`
	EndTime       string      `json:"end_time"`
	Duration      uint        `json:"duration"` //minutes
	TimeZone      string      `json:"time_zone"`
	AllDay        bool        `json:"all_day"`
	Repeat        EventRepeat `json:"repeat" gorm:"type:tinyint"`
	RepeatEndDate *time.Time  `json:"repeat_end_date"`
	Sunday        bool        `json:"sunday"`
	Monday        bool        `json:"monday"`
	Tuesday       bool        `json:"tuesday"`
	Wednesday     bool        `json:"wednesday"`
	Thursday      bool        `json:"thursday"`
	Friday        bool        `json:"friday"`
	Saturday      bool        `json:"saturday"`
	CreatedAt     time.Time   `json:"create_date"`
	UpdatedAt     time.Time   `json:"updated_date"`
}

type EventMember struct {
	EventID      uint       `json:"event_id"`
	Event        Event      `json:"event" gorm:"constraint:OnDelete:CASCADE;"`
	ProfileID    uint       `json:"profile_id"`
	Host         bool       `json:"host"`
	Cohost       bool       `json:"cohost"`
	Acknowledged *time.Time `json:"acknowledged"`
	CreatedAt    time.Time  `json:"create_date"`
	UpdatedAt    time.Time  `json:"updated_date"`
}
