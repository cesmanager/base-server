package base_domain

import "time"

//Access ...
type Access struct {
	UserID    uint      `json:"user_id" gorm:"not null"`
	User      *User     //
	Token     string    `json:"token" gorm:"unique;not null"`
	IP        string    `json:"ip"`
	Agent     string    `json:"agent"`
	Expiry    time.Time `json:"expiry"`
	CreatedAt time.Time `json:"create_date"`
	UpdatedAt time.Time `json:"modified_date"`
}
