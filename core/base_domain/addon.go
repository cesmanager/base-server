package base_domain

//Addon struct
type Addon struct {
	Scheme string `json:"scheme"`
	Domain string `json:"base_domain"`
	UILink string `json:"uilink"`
	Port   string `json:"port"`
	Auth   bool   `json:"auth"` //use to determin if that end point requires user authentication
	Active bool   `json:"active"`
}
