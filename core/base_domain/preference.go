package base_domain

import (
	"time"
)

type preferenceKey string

const (
	EmailNotification preferenceKey = "by_email"
	PhoneNotification preferenceKey = "by_phone"
	PushNotification  preferenceKey = "by_push"
)

// Profile model
type Preference struct {
	ProfileID uint      `json:"profile_id" gorm:"primaryKey;autoIncrement:false"`
	Key       string    `json:"key" gorm:"primaryKey"`
	Value     string    `json:"value"`
	CreatedAt time.Time `json:"create_at"`
	UpdatedAt time.Time `json:"updated_at"`
}
