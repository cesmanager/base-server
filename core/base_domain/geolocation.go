package base_domain

import (
	"time"
)

// GeoCountryState ...
type GeoLocation struct {
	ID         uint         `json:"id"`
	Name       string       `json:"name"`
	Title      string       `json:"title"`
	Photo      string       `json:"photo"`
	AlphaCode2 string       `json:"alpha_code_2"`
	AlphaCode3 string       `json:"alpha_code_3"`
	CallCode   string       `json:"call_code"`
	Capital    string       `json:"capital"`
	GroupTag   string       `json:"group_tag"` //for GroupTag
	ParentID   *uint        `json:"parent_id"` //for ParentID
	Parent     *GeoLocation `json:"parent"`
	Type       int8         `json:"type"`
	CreatedAt  time.Time    `json:"created_at"`
	UpdatedAt  time.Time    `json:"updated_at"`
}

//	{
//		"value": "UTC-11",
//		"abbr": "U",
//		"offset": -11,
//		"isdst": false,
//		"text": "(UTC-11:00) Coordinated Universal Time-11",
//		"utc": [
//		  "Etc/GMT+11",
//		  "Pacific/Midway",
//		  "Pacific/Niue",
//		  "Pacific/Pago_Pago"
//		]
//	  }
type TimeZone struct {
	Value  string   `json:"value"`
	Abbr   string   `json:"abbr"`
	Offset float32  `json:"offset"`
	Isdst  bool     `json:"isdst"`
	Text   string   `json:"text"`
	Utc    []string `json:"utc"`
}
