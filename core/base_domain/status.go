package base_domain

import "time"

// Status for modules
type Status struct {
	ID        uint      `json:"id"`
	Title     string    `json:"title"`
	Type      string    `json:"type"`
	CreatedAt time.Time `json:"create_date"`
	UpdatedAt time.Time `json:"updated_date"`
}

func (m *Status) BaseInitialize() []Status {
	return []Status{
		{
			Title: "Service Business",
			Type:  "coy_type",
		},
		{
			Title: "Manufacturing Business",
			Type:  "coy_type",
		},
		{
			Title: "Merchandising Business",
			Type:  "coy_type",
		},
		{
			Title: "Sole Proprietorship",
			Type:  "coy_type",
		},
		{
			Title: "Partnership",
			Type:  "coy_type",
		},
		{
			Title: "Corporation",
			Type:  "coy_type",
		},
		{
			Title: "Multi-National Corporations (MNCs)",
			Type:  "coy_type",
		},
		{
			Title: "Franchises",
			Type:  "coy_type",
		},
		{
			Title: "Limited Liability Company",
			Type:  "coy_type",
		},
		{
			Title: "Cooperative",
			Type:  "coy_type",
		},
	}
}

func (m *Status) Initialize() []Status {
	return []Status{
		{
			ID:    1,
			Title: "Male",
			Type:  "profile_gender",
		},
		{
			ID:    2,
			Title: "Female",
			Type:  "profile_gender",
		},
		{
			ID:    3,
			Title: "Active",
			Type:  "profile_status",
		},
		{
			ID:    4,
			Title: "Suspended",
			Type:  "profile_status",
		},
		{
			ID:    5,
			Title: "Disabled",
			Type:  "profile_status",
		},
		{
			ID:    6,
			Title: "Active",
			Type:  "user_status",
		},
		{
			ID:    7,
			Title: "Disabled",
			Type:  "user_status",
		},
	}
}
