package base_domain

import (
	"strings"
)

type UserGroupPermit struct {
	UserGroupID  uint             `json:"user_group_id" gorm:"primaryKey;autoIncrement:false"`
	Mod          string           `json:"mod" gorm:"primaryKey"`
	Self         bool             `json:"self"`    //once true, the users can only perform this action on their record
	Actions      string           `json:"actions"` //can be an combination of list|create|update|delete or any other custom possible access in the module
	MappedAction *map[string]bool `json:"mapped_action" gorm:"-"`
}

func (m *UserGroupPermit) MapActions() *UserGroupPermit {
	m.MappedAction = &map[string]bool{}
	mp := strings.Split(m.Actions, ",")
	for _, v := range mp {
		(*m.MappedAction)[v] = true
	}
	return m
}
