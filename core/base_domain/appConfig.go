package base_domain

//AppConfig ...
type AppConfig struct {
	Name          string         `yaml:"name"`
	Setup         string         `yaml:"setup_key"`
	Version       string         `yaml:"version"`
	Domain        string         `yaml:"domain"`
	Port          string         `yaml:"port"`
	CustomPath    string         `yaml:"root_path"`
	TempPath      string         `yaml:"temp_path"`
	Path          string         `yaml:"path"`
	Exec          string         `yaml:"exec"`
	TokenKey      string         `yaml:"token_key"`
	SessionKey    string         `yaml:"session_key"`
	SessionExpire string         `yaml:"session_expire"`
	Sftp          string         `yaml:"sftp"`
	ActiveDB      string         `yaml:"active_db"`
	DataBase      map[string]DB  `yaml:"db"`
	SSH           map[string]SSH `yaml:"ssh"`
	PicPath       string         `yaml:"-"`
	VideoPath     string         `yaml:"-"`
	DocPath       string         `yaml:"-"`
	Config        string         `yaml:"-"`
}

//DB struct
type DB struct {
	BaseDB   string `yaml:"base_db"`
	DBPrefix string `yaml:"db_prefix"`
	Host     string `yaml:"host"`
	Port     string `yaml:"port"`
	User     string `yaml:"user"`
	Password string `yaml:"password"`
	Ssl      string `yaml:"ssl"`
	Debug    bool   `yaml:"debug"`
	SShTag   string `yaml:"ssh_tag"` //tag that contains the prefared ssh connection for
}

//SSH struct
type SSH struct {
	Host     string `yaml:"host"`
	Port     string `yaml:"port"`
	User     string `yaml:"user"`
	Password string `yaml:"password"`
	Key      string `yaml:"key"`
	Path     string `yaml:"path"`
}
