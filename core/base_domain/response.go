package base_domain

//Response this is a standard Response type to be used throughout the software
type Response struct {
	Status bool        `json:"status"`
	Code   int         `json:"code"`
	Token  string      `json:"token"`
	Body   interface{} `json:"body"`
}
