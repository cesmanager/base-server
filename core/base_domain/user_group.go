package base_domain

import (
	"time"
)

type UserGroup struct {
	ID            uint                        `json:"id"`
	Title         string                      `json:"title"`
	Description   string                      `json:"description"`
	Tag           string                      `json:"tag"`
	SuperGroup    bool                        `json:"super_group"` //once true, the users in this group can perform any action
	Permits       *[]UserGroupPermit          `json:"permits"`
	MappedPermits *map[string]UserGroupPermit `json:"mapped_permits" gorm:"-"`
	Active        bool                        `json:"active"`
	CreatedAt     time.Time                   `json:"create_date"`
	UpdatedAt     time.Time                   `json:"updated_date"`
}

func (m *UserGroup) MapPermits() *UserGroup {
	m.MappedPermits = &map[string]UserGroupPermit{}
	for _, v := range *m.Permits {
		(*m.MappedPermits)[v.Mod] = *v.MapActions()
	}
	return m
}

func (user *UserGroup) Initialize() []UserGroup {
	ugs := make([]UserGroup, 0)
	ugs = append(ugs, UserGroup{
		ID:         1,
		Title:      "Administrator",
		SuperGroup: true,
	})
	ugs = append(ugs, UserGroup{
		ID:         2,
		Title:      "No Access",
		SuperGroup: false,
	})
	return ugs
}
