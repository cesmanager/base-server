package base_domain

import (
	"time"

	"golang.org/x/crypto/bcrypt"
)

// User model
type User struct {
	ID          uint       `json:"id"`
	Uname       string     `json:"uname" gorm:"unique"`
	Pword       string     `json:"pword"`
	Vcode       string     `json:"vcode"`
	Status      uint       `json:"status"`
	ProfileID   uint       `json:"profile_id"`
	Profile     *Profile   `json:"profile"`
	UserGroupID uint       `json:"user_group_id"`
	UserGroup   *UserGroup `json:"user_group"`
	CreatedAt   time.Time  `json:"created_at"`
	UpdatedAt   time.Time  `json:"updated_at"`
}

func (m *User) Initialize() []User {
	//Initial Users
	bytes, _ := bcrypt.GenerateFromPassword([]byte("123456"), bcrypt.DefaultCost)
	return []User{
		{
			Uname:       "admin",
			Pword:       string(bytes),
			ProfileID:   1,
			UserGroupID: 1,
			CreatedAt:   time.Now(),
			UpdatedAt:   time.Now(),
			Vcode:       "",
		},
	}
}
