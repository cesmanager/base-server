package base_domain

import "time"

//Currency struct for transaction
type Currency struct {
	ID                uint                `json:"id"`
	Photo             string              `json:"photo" gorm:"photo"`
	Name              string              `json:"name"`
	Symbol            string              `json:"symbol"`
	UnitName          string              `json:"unit_name"`
	UnitSymbol        string              `json:"unit_symbol"`
	CurrencyExchanges []*CurrencyExchange `json:"currency_exchange"`
	Active            bool                `json:"active"`
	CreatedAt         time.Time           `json:"created_at"`
	UpdatedAt         time.Time           `json:"updated_at"`
}

func (d *Currency) Initialize() []Currency {
	//currency
	return []Currency{
		{
			ID:         1,
			Name:       "Naira",
			Symbol:     "₦",
			UnitName:   "Kobo",
			UnitSymbol: "K",
			Active:     true,
		},
		{
			ID:         2,
			Name:       "Dollar",
			Symbol:     "$",
			UnitName:   "Cents",
			UnitSymbol: "¢",
			Active:     true,
		},
		{
			ID:         3,
			Name:       "Pounds",
			Symbol:     "£",
			UnitName:   "Rence",
			UnitSymbol: "p",
			Active:     true,
		},
	}
}
