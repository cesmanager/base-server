package base_domain

import (
	"time"
)

//Invoice struct for transaction tracking
type Invoice struct {
	ID         uint
	Title      string
	Dsc        float32 //%discount on the tatal item value excluding tax
	Discount   float32 //value discount on the total item value excluding tax
	Tax        float32 //%tax
	RawTotal   float32 //totat value of the invoice just by summing all item value
	Total      float32 //total value of the invoice after all calculation
	AccountID  uint    //id of account from which the user is paying from
	Account    Account
	CashierID  uint //id of the staff that created the invoice
	Cashier    Profile
	PostByID   uint //id of the staff that posted the invoice
	PostBy     Profile
	CategoryID uint
	Category   Category
	Posted     int8
	CreatedAt  time.Time
	UpdatedAt  time.Time
	Items      []InvoicedItem
}
