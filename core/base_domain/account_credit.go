package base_domain

//Deposit struct form incoming transaction
type Credit struct {
	ID          uint        `json:"id"`
	Transaction Transaction `json:"transaction" gorm:"polymorphic:Kind;"`
	CreditorID  uint        `json:"creditor_id"` //user account where the amount is to be credited
	Creditor    Account     `json:"creditor"`
}
