package base_domain

import (
	"time"
)

// Category for modules
type Category struct {
	ID        uint      `json:"id"`
	Name      string    `json:"name"`
	Title     string    `json:"title"`
	Cnt       int64     `json:"cnt" gorm:"-"` //count of children
	ParentID  *uint     `json:"parent_id"`    //for parent
	Parent    *Category `json:"parent"`
	GroupTag  string    `json:"group_tag"` //for parent-index-index
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
}

func (d *Category) Initialize() []Category {
	//CategoryTypes ...
	cats := []Category{
		{ID: 1, Name: "Profile"},
		{ID: 2, Name: "Account"},
		{ID: 3, Name: "Debit"},
		{ID: 4, Name: "Credit"},
		{ID: 5, Name: "Items"},
		{ID: 6, Name: "Invoice"},
	}
	cats = append(cats, Category{ID: 100, Name: "Employee", ParentID: &cats[0].ID})
	cats = append(cats, Category{ID: 101, Name: "Customer", ParentID: &cats[0].ID})
	cats = append(cats, Category{ID: 102, Name: "Supplier", ParentID: &cats[0].ID})
	return cats
}
