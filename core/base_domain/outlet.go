package base_domain

type Outlet struct {
	ID                   uint      `json:"id"`
	Title                string    `json:"title"`
	ProfileID            uint      `json:"profile_id"`
	Profile              Profile   `json:"profile"`
	CustomerID           *uint     `json:"customer_id"` //the default customer for that outlet
	Customer             Profile   `json:"customer"`
	CurrencyID           uint      `json:"currency_id"` //default company currency
	Currency             Currency  `json:"currency"`
	TaxAccountID         *uint     `json:"tax_account_id"` //account where tax goes. this account must be associated with the
	TaxAccount           *Account  `json:"tax_account"`
	InvoiceCategoryID    *uint     `json:"invoice_category_id"` //default invoice category category
	InvoiceCategory      *Category `json:"invoice_category"`
	VerificationTemplate string    `json:"verification_template"` //recovery templates for mails
	RecoveryTemplate     string    `json:"recovery_template"`     //recovery templates for mails
}

func (m *Outlet) Initialize() []Outlet {
	p := uint(6)
	return []Outlet{
		{
			Title:             "default",
			ProfileID:         1,
			CurrencyID:        2,
			InvoiceCategoryID: &p,
			VerificationTemplate: `
			Hello {{$fname}},
			Your account has been created.
			LOGIN DETAILS
			Username: {{$uname}}
			Password: ******
	
			Your verification code is {{$vcode}}`,
			RecoveryTemplate: `
			Hello {{$fname}},
			Your recovery password is {{$re_password}},
			Use the above password in the recovery page.`,
		},
	}
}
