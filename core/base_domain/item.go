package base_domain

import "time"

//Item defination
type Item struct {
	ID         uint
	Title      string
	Code       string
	ItemType   int8    //1 = products, 2 = services
	UnitPrice  float32 //default price for an item
	CurrencyID uint
	Currency   Currency
	CategoryID *uint
	Category   *Category
	Product    *ItemProduct `sql:"-"`
	Service    *ItemService `sql:"-"`
	CreatedAt  time.Time    `json:"create_date"`
	UpdatedAt  time.Time    `json:"updated_date"`
}
