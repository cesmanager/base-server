package base_domain

//Withdrawal struct for transaction tracking
type Debit struct {
	ID          uint        `json:"id"`
	Transaction Transaction `json:"transaction" gorm:"polymorphic:Kind;"`
	DebtorID    uint        `json:"debtor_id"` //user account where the amount is to be debited
	Debtor      Account     `json:"debtor"`
}
