package base_domain

import (
	"time"

	"gorm.io/gorm"
)

type Notification struct {
	ID          uint
	Title       string         `json:"title"`
	Message     string         `json:"message" gorm:"type:text"`
	CreatedByID *uint          `json:"created_by_id"`
	CreatedBy   *Profile       `json:"created_by"`
	Tag         *string        `json:"tag"`
	CreatedAt   time.Time      `json:"create_date"`
	UpdatedAt   time.Time      `json:"updated_date"`
	DeletedAt   gorm.DeletedAt `gorm:"index"`
}

type Notified struct {
	NotificationID uint           `json:"notification_id"`
	Notification   Notification   `json:"notification" gorm:"constraint:OnDelete:CASCADE;"`
	ProfileID      uint           `json:"profile_id"`
	Seen           bool           `json:"seen"`
	SentAt         *time.Time     `json:"sent_date"`
	CreatedAt      time.Time      `json:"create_date"`
	UpdatedAt      time.Time      `json:"updated_date"`
	DeletedAt      gorm.DeletedAt `gorm:"index"`
}
