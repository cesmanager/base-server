package base_domain

import (
	"time"
)

// Settings ...
type Settings struct {
	ID              uint     `json:"id"`
	Name            string   `json:"name"` //name of the company
	Slog            string   `json:"slog"` //slogan of the company
	Photo           string   `json:"photo"`
	SystemProfileID int      `json:"system_profile_id"` //the profile that is used to make records in the background
	SystemProfile   *Profile `gorm:"-"`

	//profile status
	ProfileActive    uint `json:"profile_active"`
	ProfileSuspended uint `json:"profile_suspended"`
	ProfileDisabled  uint `json:"profile_disabled"`

	//user status
	UserActive   uint `json:"user_active"`
	UserDisabled uint `json:"user_disabled"`

	//mail fields
	SMTPServer string `json:"smtp_server"` //
	SMTPUser   string `json:"smtp_user"`   //
	SMTPPass   string `json:"smtp_pass"`   //
	SMTPPort   int    `json:"smtp_port"`   //

	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
}

func (user *Settings) Initialize() Settings {
	return Settings{
		ID:              1,
		SystemProfileID: 1,
		SMTPServer:      "localhost",
		SMTPUser:        "noreply@example.com",
		SMTPPort:        25,
	}
}
