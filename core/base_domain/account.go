package base_domain

import (
	"time"
)

// Account struct for transaction tracking
type Account struct {
	ID         uint      `json:"id"`
	Title      string    `json:"title"`
	Balance    float64   `json:"balance"`
	Overdraft  float64   `json:"overdraft"`
	CategoryID *uint     `json:"category_id"`
	Category   *Category `json:"category"`
	CurrencyID uint      `json:"currency_id"`
	Currency   *Currency `json:"currency"`
	ProfileID  uint      `json:"profile_id"`
	Profile    *Profile  `json:"profile"`
	Active     int8      `json:"active"`
	CreatedAt  time.Time `json:"created_at"`
	UpdatedAt  time.Time `json:"updated_at"`
}
