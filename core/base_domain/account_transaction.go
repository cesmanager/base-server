package base_domain

import (
	"time"
)

// Transaction struct for transaction tracking
type Transaction struct {
	ID         uint      `json:"id"`
	Title      string    `json:"title"`
	AccountID  uint      `json:"account_id"` //Company Account where the amount is credited/debited
	Account    Account   `json:"account"`
	Amount     float64   `json:"amount"`
	KindID     uint      `json:"kind_id"`
	KindType   string    `json:"kind_type"` // debits / credits / transfers
	Comment    string    `json:"comment"`
	CategoryID *uint     `json:"category_id"`
	Category   *Category `json:"category"`
	CreatorID  uint      `json:"creator_id"` // id of the user that created the transaction
	Creator    Profile   `json:"creator"`
	PosterID   *uint     `json:"poster_id"` // id of the user that posted the invoice
	Poster     *Profile  `json:"poster"`
	IP         string    `json:"ip"` //ip address of the device that initiated the transaction
	Posted     bool      `json:"posted"`
	CreatedAt  time.Time `json:"created_at"`
	UpdatedAt  time.Time `json:"updated_at"`

	//this section only applies to situations where the transaction is initialied by another user. Like third-party deposits
	TransactorName     string `json:"transactor_name"`
	TransactorAddress1 string `json:"transactor_address1"`
	TransactorAddress2 string `json:"transactor_address2"`
	TransactorCity     string `json:"transactor_city"`
	TransactorState    string `json:"transactor_state"`
	TransactorCountry  string `json:"transactor_country"`
}
