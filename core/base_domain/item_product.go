package base_domain

import (
	"time"
)

//ItemProduct defination
type ItemProduct struct {
	ItemID    uint
	Item      Item
	Unit      string  //definition of the unit measurement
	UnitPack  float32 //units per pack
	Active    int8
	CreatedAt time.Time
	UpdatedAt time.Time
}
