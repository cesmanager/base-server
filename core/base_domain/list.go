package base_domain

type List struct {
	Items    []interface{} `json:"items"`
	Count    int64         `json:"count"`     //count of the total unpaged list
	Page     int           `json:"page"`      //current page of the list
	PageSize int           `json:"page_size"` //size of each page
	Pages    int           `json:"pages"`     //current page of the list
}
