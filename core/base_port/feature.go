package base_port

import "gitlab.com/cesmanager/base-server/core/base_domain"

type Feature interface {
	Get(id string) (base_domain.Feature, error)
	Create(Profile *base_domain.Feature) error
	Update(Profile *base_domain.Feature) error
	List(param map[string]interface{}, order map[string]interface{}, page int, pageSize int) (base_domain.List, error)
	Find(param map[string]interface{}) ([]base_domain.Feature, error)
	FindWithPreload(param map[string]interface{}, preload interface{}) ([]base_domain.Feature, error)
	Count(param map[string]interface{}, operators *map[string]string) (int64, error)
	Delete(param map[string]interface{}) error
}
