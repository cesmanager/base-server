package base_port

import "gitlab.com/cesmanager/base-server/core/base_domain"

type UserGroupPermit interface {
	// Get(id string) (base_domain.User, error)
	Store(User *base_domain.UserGroupPermit) error
	GetPermit(user_id string, mod string) (base_domain.UserGroupPermit, error)
	// Update(User *base_domain.User) error
	// FindAll(param map[string]interface{}) ([]base_domain.User, error)
	// Delete(id string) error
}
