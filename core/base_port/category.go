package base_port

import "gitlab.com/cesmanager/base-server/core/base_domain"

type Category interface {
	Get(id string) (base_domain.Category, error)
	Store(Profile *base_domain.Category) error
	Update(Profile *base_domain.Category) error
	List(param map[string]interface{}, order map[string]interface{}, page int, pageSize int) (base_domain.List, error)
	Find(param map[string]interface{}) ([]base_domain.Category, error)
	Count(param map[string]interface{}, operators *map[string]string) (int64, error)
	Delete(param map[string]interface{}) error
}
