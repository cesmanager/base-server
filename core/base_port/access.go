package base_port

import "gitlab.com/cesmanager/base-server/core/base_domain"

type Access interface {
	Get(token string) (base_domain.Access, error)
	Store(access *base_domain.Access) error
	Update(access *base_domain.Access) error
	Find(param map[string]interface{}) ([]base_domain.Access, error)
	Delete(token string) error
}
