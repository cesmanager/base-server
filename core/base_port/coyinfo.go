package base_port

import "gitlab.com/cesmanager/base-server/core/base_domain"

type CoyInfo interface {
	List(param map[string]interface{}, order map[string]interface{}, page int, pageSize int) (base_domain.List, error)
	Get(id string) (base_domain.CoyInfo, error)
	Store(Profile *base_domain.CoyInfo) error
	Update(Profile *base_domain.CoyInfo) error
	Find(param map[string]interface{}) ([]base_domain.CoyInfo, error)
	// Delete(id string) error
}
