package base_port

import "gitlab.com/cesmanager/base-server/core/base_domain"

type User interface {
	Get(id string) (base_domain.User, error)
	Store(User *base_domain.User) error
	Update(User *base_domain.User) error
	List(param map[string]interface{}, order map[string]interface{}, page int, pageSize int) (base_domain.List, error)
	Find(param map[string]interface{}) ([]base_domain.User, error)
	// Delete(id string) error
}
