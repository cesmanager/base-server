package base_port

import "gitlab.com/cesmanager/base-server/core/base_domain"

type UserGroup interface {
	Get(id string) (base_domain.UserGroup, error)
	Store(User *base_domain.UserGroup) error
	Update(User *base_domain.UserGroup) error
	List(param map[string]interface{}, order map[string]interface{}, page int, pageSize int) (base_domain.List, error)
	GetByUser(user_id string) (base_domain.UserGroup, error)
	Find(param map[string]interface{}) ([]base_domain.UserGroup, error)
	// Delete(id string) error
}
