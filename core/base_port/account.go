package base_port

import "gitlab.com/cesmanager/base-server/core/base_domain"

type Account interface {
	Get(id string) (base_domain.Account, error)
	Store(Profile *base_domain.Account) error
	Update(Profile *base_domain.Account) error
	List(param map[string]interface{}, order map[string]interface{}, page int, pageSize int) (base_domain.List, error)
	Find(param map[string]interface{}) ([]base_domain.Account, error)
	Lock(id string) error
}

type Credit interface {
	Get(id string) (base_domain.Credit, error)
	Store(Profile *base_domain.Credit) error
	Update(Profile *base_domain.Credit) error
	Find(param map[string]interface{}) ([]base_domain.Credit, error)
	// Delete(id string) error
}

type Debit interface {
	Get(id string) (base_domain.Debit, error)
	Store(Profile *base_domain.Debit) error
	Update(Profile *base_domain.Debit) error
	Find(param map[string]interface{}) ([]base_domain.Debit, error)
	// Delete(id string) error
}
