package base_port

import "gitlab.com/cesmanager/base-server/core/base_domain"

type CurrencyExchange interface {
	Get(id string) (base_domain.CurrencyExchange, error)
	Store(CurrencyExchange *base_domain.CurrencyExchange) error
	Update(CurrencyExchange *base_domain.CurrencyExchange) error
	Find(param map[string]interface{}) ([]base_domain.CurrencyExchange, error)
	// Delete(id string) error
}
