package base_port

import "gitlab.com/cesmanager/base-server/core/base_domain"

type Status interface {
	Get(id string) (base_domain.Status, error)
	Store(Status *base_domain.Status) error
	Update(Status *base_domain.Status) error
	List(param map[string]interface{}, order map[string]interface{}, page int, pageSize int) (base_domain.List, error)
	Find(param map[string]interface{}) ([]base_domain.Status, error)
	Delete(id string) error
}
