package base_port

import "gitlab.com/cesmanager/base-server/core/base_domain"

type Event interface {
	Get(id string) (base_domain.Event, error)
	Save(model *base_domain.Event) error
	List(params map[string]interface{}, order map[string]interface{}, page int, limit int) (base_domain.List, error)
	Find(params map[string]interface{}) ([]base_domain.Event, error)
	Delete(id []string) error
}

type EventMemeber interface {
	InviteAll(eventID string) error
	InviteProfiles(eventID string, profileID []string, host bool, cohost bool) error
	InviteUserGroup(eventID string, userGroupID []string) error
	InviteCustom(query string) error
	Update(model *base_domain.EventMember) error
	List(params map[string]interface{}, order map[string]interface{}, page int, pageSize int) (base_domain.List, error)
	Find(params map[string]interface{}) ([]base_domain.EventMember, error)
	Delete(eventID string, profileID string) error
}
