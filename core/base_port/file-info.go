package base_port

import "gitlab.com/cesmanager/base-server/core/base_domain"

type FileInfoPort interface {
	Get(path string) (base_domain.FileInfo, error)
	Store(model *base_domain.FileInfo) error
	Update(path string, model *base_domain.FileInfo) error
	Delete(path string) error
	Find(params map[string]interface{}) ([]base_domain.FileInfo, error)
}
