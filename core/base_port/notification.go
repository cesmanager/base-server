package base_port

import "gitlab.com/cesmanager/base-server/core/base_domain"

type Notification interface {
	Get(id string) (base_domain.Notification, error)
	Store(model *base_domain.Notification) error
	Update(model *base_domain.Notification) error
	List(param map[string]interface{}, order map[string]interface{}, page int, pageSize int) (base_domain.List, error)
	Delete(id []string) error
}
type Notified interface {
	Read(notificationID string, profileID string) (base_domain.Notified, error)
	NotifyAll(notificationID string) error
	NotifyProfiles(notificationID string, profileID []string) error
	NotifyUserGroup(notificationID string, userGroupID []string) error
	NotifyCustom(qry string) error
	Update(model *base_domain.Notified) error
	List(param map[string]interface{}, order map[string]interface{}, page int, pageSize int) (base_domain.List, error)
	Delete(notificationID string, profileID string) error
}
