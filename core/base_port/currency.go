package base_port

import "gitlab.com/cesmanager/base-server/core/base_domain"

type Currency interface {
	Get(id string) (base_domain.Currency, error)
	Store(Currency *base_domain.Currency) error
	Update(Currency *base_domain.Currency) error
	List(search string, order map[string]interface{}, page int, pageSize int) (base_domain.List, error)
	Find(param map[string]interface{}) ([]base_domain.Currency, error)
	// Delete(id string) error
}
