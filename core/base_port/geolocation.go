package base_port

import "gitlab.com/cesmanager/base-server/core/base_domain"

type GeoLocation interface {
	Get(id string) (base_domain.GeoLocation, error)
	Store(Profile *base_domain.GeoLocation) error
	Update(Profile *base_domain.GeoLocation) error
	List(param map[string]interface{}, order map[string]interface{}, page int, pageSize int) (base_domain.List, error)
	Find(param map[string]interface{}, order *map[string]interface{}) ([]base_domain.GeoLocation, error)
	Count(param map[string]interface{}) (int64, error)
}
