package base_port

import "gitlab.com/cesmanager/base-server/core/base_domain"

type Preference interface {
	Get(profile_id string) ([]base_domain.Preference, error)
	Find(profile_id string, key string) (base_domain.Preference, error)
	Store(preferences *[]base_domain.Preference) error
}
