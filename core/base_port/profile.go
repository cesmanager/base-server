package base_port

import "gitlab.com/cesmanager/base-server/core/base_domain"

type Profile interface {
	Get(id string) (base_domain.Profile, error)
	Store(Profile *base_domain.Profile) error
	Update(Profile *base_domain.Profile) error
	List(params map[string]interface{}, order map[string]interface{}, page int, pageSize int) (base_domain.List, error)
	Find(params map[string]interface{}) ([]base_domain.Profile, error)
	Delete(id string) error
}
