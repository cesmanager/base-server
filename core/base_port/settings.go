package base_port

import "gitlab.com/cesmanager/base-server/core/base_domain"

type Settings interface {
	Get() (base_domain.Settings, error)
	Store(settings *base_domain.Settings) error
	Update(settings *base_domain.Settings) error
}
