package base_port

import "gitlab.com/cesmanager/base-server/core/base_domain"

type FormPort interface {
	Get(id string) (base_domain.Form, error)
	Store(model *base_domain.Form) error
	Update(model *base_domain.Form) error
	Find(params map[string]interface{}) ([]base_domain.Form, error)
	List(param map[string]interface{}, order map[string]interface{}, page int, pageSize int) (base_domain.List, error)
}
type FormRecordPort interface {
	Get(id string) (base_domain.FormRecord, error)
	Store(model *base_domain.FormRecord) error
	Update(model *base_domain.FormRecord) error
	Delete(param map[string]interface{}) error
	Find(params map[string]interface{}) ([]base_domain.FormRecord, error)
	List(param map[string]interface{}, order map[string]interface{}, page int, pageSize int) (base_domain.List, error)
}
type FormRecordEntryPort interface {
	Find(params map[string]interface{}) ([]base_domain.FormRecordEntry, error)
	Store(model *base_domain.FormRecordEntry) error
	Delete(param map[string]interface{}) error
}
