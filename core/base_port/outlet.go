package base_port

import "gitlab.com/cesmanager/base-server/core/base_domain"

type Outlet interface {
	Get(id string) (base_domain.Outlet, error)
	Store(Outlet *base_domain.Outlet) error
	Update(Outlet *base_domain.Outlet) error
	List(param map[string]interface{}, order map[string]interface{}, page int, pageSize int) (base_domain.List, error)
	Find(param map[string]interface{}) ([]base_domain.Outlet, error)
	// Delete(id string) error
}
